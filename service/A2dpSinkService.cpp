/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "A2dpSinkService.hpp"
#include "Adapter.hpp"
#include "Common.hpp"
#include "MediaManager.hpp"

#define LOGTAG "A2DPSINK_SERVICE "

A2dpSinkService::A2dpSinkService(Adapter *pAdapter)
    : A2dpProfileService(pAdapter, LOCAL_UUID, REMOTE_UUID),
      m_btA2dpSinkIF(nullptr),
      m_btA2dpSinkVendorIF(nullptr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

A2dpSinkService::~A2dpSinkService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  m_codecList.clear();
}

bool A2dpSinkService::addSupportedCodec(const btav_codec_configuration_t &codec) {
  ADK_LOG_DEBUG(LOGTAG "%s: %u (%s)\n", __func__, codec.codec_type,
                MediaManager::getCodecName(codec.codec_type));

  // The A2DP Sink Vendor interface only accepts codec configs with a single
  // sample rate set in the bitfield, so if multiple bits are set, the config
  // must be split into multiple duplicates - one for each sample rate.
  btav_codec_configuration_t codec_item = codec;

  switch (codec.codec_type) {
    case A2DP_SINK_AUDIO_CODEC_PCM:
      // This is likely SBC with the A2DP_SINK_ENABLE_SBC_DECODING flag set in
      // Fluoride to use its own internal decoder (which then outputs PCM)
    case A2DP_SINK_AUDIO_CODEC_SBC: {
      std::vector<uint8_t> rates = {SBC_SAMP_FREQ_48, SBC_SAMP_FREQ_44,
                                    SBC_SAMP_FREQ_32, SBC_SAMP_FREQ_16};
      // Note that this creates a very long list of codec items. Preferred codec
      // list size is limited by Fluoride MAX_NUM_CODEC_CONFIGS definition.
      // In the future it could be advisable to reduce this to only the mandatory
      // parameters. For SBC, mandatory frequencies: 44.1KHz / 48KHz
      for (auto &rate : rates) {
        if (codec.codec_config.sbc_config.samp_freq & rate) {
          codec_item.codec_config.sbc_config.samp_freq = rate;
          m_codecList.push_back(codec_item);
        }
      }
    } break;

    case A2DP_SINK_AUDIO_CODEC_MP3: {
      std::vector<uint8_t> layers = {MP3_LAYER_1, MP3_LAYER_2, MP3_LAYER_3};
      std::vector<uint8_t> rates = {MP3_SAMP_FREQ_48000, MP3_SAMP_FREQ_44100,
                                    MP3_SAMP_FREQ_32000, MP3_SAMP_FREQ_24000,
                                    MP3_SAMP_FREQ_22050, MP3_SAMP_FREQ_16000};
      for (auto &layer : layers) {
        for (auto &rate : rates) {
          if (codec.codec_config.mp3_config.layer & layer) {
            if (codec.codec_config.mp3_config.sampling_freq & rate) {
              codec_item.codec_config.mp3_config.layer = layer;
              codec_item.codec_config.mp3_config.sampling_freq = rate;
              m_codecList.push_back(codec_item);
            }
          }
        }
      }
    } break;

    case A2DP_SINK_AUDIO_CODEC_AAC: {
      // std::vector<uint8_t> obj_types = {
      //     AAC_OBJ_TYPE_MPEG_2_AAC_LC, AAC_OBJ_TYPE_MPEG_4_AAC_LC,
      //     AAC_OBJ_TYPE_MPEG_4_AAC_LTP, AAC_OBJ_TYPE_MPEG_4_AAC_SCA};
      // std::vector<uint16_t> rates = {
      //     AAC_SAMP_FREQ_96000, AAC_SAMP_FREQ_88200, AAC_SAMP_FREQ_64000,
      //     AAC_SAMP_FREQ_48000, AAC_SAMP_FREQ_44100, AAC_SAMP_FREQ_32000,
      //     AAC_SAMP_FREQ_24000, AAC_SAMP_FREQ_22050, AAC_SAMP_FREQ_16000,
      //     AAC_SAMP_FREQ_12000, AAC_SAMP_FREQ_11025, AAC_SAMP_FREQ_8000};
      // Note that this creates a very long list of codec items. Preferred codec
      // list size is limited by Fluoride MAX_NUM_CODEC_CONFIGS definition.

      // We are reducing the list above to only the mandatory AAC parameters:
      // frequencies: 44.1KHz / 48KHz
      // object type: MPEG-2-LC
      std::vector<uint8_t> obj_types = {
          AAC_OBJ_TYPE_MPEG_2_AAC_LC};
      std::vector<uint16_t> rates = {
          AAC_SAMP_FREQ_48000, AAC_SAMP_FREQ_44100};
      for (auto &type : obj_types) {
        for (auto &rate : rates) {
          if (codec.codec_config.aac_config.obj_type & type) {
            if (codec.codec_config.aac_config.sampling_freq & rate) {
              codec_item.codec_config.aac_config.obj_type = type;
              codec_item.codec_config.aac_config.sampling_freq = rate;
              m_codecList.push_back(codec_item);
            }
          }
        }
      }
    } break;

    case A2DP_SINK_AUDIO_CODEC_APTX: {
      std::vector<uint8_t> rates = {APTX_SAMPLERATE_48000,
                                    APTX_SAMPLERATE_44100};
      for (auto &rate : rates) {
        if (codec.codec_config.aptx_config.sampling_freq & rate) {
          codec_item.codec_config.aptx_config.sampling_freq = rate;
          m_codecList.push_back(codec_item);
        }
      }
    } break;

    case A2DP_SINK_AUDIO_CODEC_APTX_AD: {
      std::vector<uint8_t> rates = {APTX_AD_SAMPLERATE_48000,
                                    APTX_AD_SAMPLERATE_44100};
      for (auto &rate : rates) {
        if (codec.codec_config.aptx_ad_config.sampling_freq & rate) {
          codec_item.codec_config.aptx_ad_config.sampling_freq = rate;
          m_codecList.push_back(codec_item);
        }
      }
    } break;

    default:
      ADK_LOG_WARNING(LOGTAG "%s: Codec (%u) not added to preferred codec list\n", __func__,
                    codec.codec_type);
      return false;
  }

  // Currently BtDaemon does not have concept of codecs priority. Currently
  // the priority list is created based on the order of endpoint registration.
  // Commenting out next line would allow Fluoride priorities to be
  // applied instead of application priorities.
  updateSupportedCodecs();
  return true;
}

bool A2dpSinkService::removeSupportedCodec(const btav_codec_configuration_t &codec) {
  ADK_LOG_DEBUG(LOGTAG "%s: %u (%s)\n", __func__, codec.codec_type,
                MediaManager::getCodecName(codec.codec_type));
  bool found = false;
  for ( auto iter = m_codecList.begin(); iter != m_codecList.end(); ) {
    if (iter->codec_type == codec.codec_type) {
      iter = m_codecList.erase(iter);
      found = true;
    } else {
      ++iter;
    }
  }

  // The supported codec list has changed so update Fluoride
  if (m_codecList.size() != 0) {
    updateSupportedCodecs();
  }
  return found;
}

void A2dpSinkService::disableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Clear any object associated with the base service like
  // endpoint connections and transport (if any).
  A2dpProfileService::disableService();

  // Cleanup stack interfaces
  if (m_btA2dpSinkIF) {
    m_btA2dpSinkIF->cleanup();
    m_btA2dpSinkIF = nullptr;
  }
  if (m_btA2dpSinkVendorIF) {
    m_btA2dpSinkVendorIF->cleanup_vendor();
    m_btA2dpSinkVendorIF = nullptr;
  }
}

bool A2dpSinkService::connect(bt_bdaddr_t *devAddr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (getDeviceState() == DeviceState::kConnected ||
      getDeviceState() == DeviceState::kPending) {
    ADK_LOG_NOTICE(LOGTAG " A2DP Sink Active connection with %s\n",
                   getConnectedDevice().ToString().c_str());
    return false;
  }

  // Send the connection request for the device and update the device connection
  // state to pending
  if ((m_btA2dpSinkIF) &&
      (m_btA2dpSinkIF->connect(*devAddr) == BT_STATUS_SUCCESS)) {
    ADK_LOG_NOTICE(LOGTAG
                   " A2DP Sink connection initiated with device with %s\n",
                   devAddr->ToString().c_str());
    setDeviceForService(DeviceState::kPending, *devAddr);
    return true;
  }
  return false;
}

bool A2dpSinkService::disconnect(bt_bdaddr_t *devAddr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (getDeviceState() == DeviceState::kDisconnected) {
    ADK_LOG_NOTICE(LOGTAG " No A2DP Sink Active connection available \n");
    return false;
  }
  if (getConnectedDevice() == *devAddr) {
    ADK_LOG_NOTICE(LOGTAG " A2DP Sink DisConnecting from %s\n",
                   getConnectedDevice().ToString().c_str());
    if (m_btA2dpSinkIF) {
      m_btA2dpSinkIF->disconnect(*devAddr);
      setDeviceForService(DeviceState::kPending, *devAddr);
      return true;
    }
  }
  return false;
}


void A2dpSinkService::scmstCpCb(const RawAddress& bd_addr, uint8_t cpHeader) {

    Device *device = getAdapter()->getDevice(bd_addr);
    if (!device) {
      ADK_LOG_ERROR(LOGTAG "- Unexpectedly remote device is not available! \n");
      return;
    }
    else {
      std::shared_ptr<MediaTransport> transport = device->getTransport();
      if (!transport) {
       ADK_LOG_ERROR(LOGTAG "- Unexpectedly remote device is not available! \n");
       return;
      }
      transport->setCpScms(cpHeader);
    }
}

void A2dpSinkService::updateSupportedCodecs() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (!m_btA2dpSinkVendorIF) {
    ADK_LOG_INFO(LOGTAG "  -- no service (yet)");
    return;
  }

  ADK_LOG_NOTICE(LOGTAG "  -- list size %d\n", m_codecList.size());
  // This Fluoride API is called to provide a codec priority list.
  // Max size of btav_codec_configuration_t list is MAX_NUM_CODEC_CONFIGS,
  // (which is currently defined as 20).
  // Therefore, if MAX_NUM_CODEC_CONFIGS is reached there might be a need
  // to parse the m_codecList to send only codec_configuration items for
  // the mandatory parameters for a codec, rather than for all possible
  // combinations supported by the registered endpoints, i.e.:
  // for SBC: Only frequencies: 44.1KHz / 48KHz
  // for AAC: Only frequencies: 44.1KHz / 48KHz and Object type ( MPEG-2-LC )
  //
  bt_status_t status =
      m_btA2dpSinkVendorIF->update_supported_codecs_param_vendor(
          const_cast<btav_codec_configuration_t *>(m_codecList.data()),
          m_codecList.size());
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG "  -- failed, status = %d", status);
    return;
  }
}

bool A2dpSinkService::forcePreferredCodecList(btav_codec_configuration_t *codec, uint8_t num_codec_configs) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (!m_btA2dpSinkVendorIF) {
    ADK_LOG_INFO(LOGTAG "  -- no service (yet)");
    return false;
  }
  bt_status_t status =
      m_btA2dpSinkVendorIF->update_supported_codecs_param_vendor(codec,num_codec_configs);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG "  -- supported_codecs_param_vendor failed");
    return false;
  }
  return true;
}
