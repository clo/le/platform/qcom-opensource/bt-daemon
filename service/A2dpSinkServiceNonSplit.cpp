/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "A2dpSinkServiceNonSplit.hpp"
#include "MediaTransportNonSplitSink.hpp"
#include "Adapter.hpp"
#include "Common.hpp"
#include "MediaManager.hpp"

#define LOGTAG "A2DPSINK_SERVICE_NONSPLIT "

static btav_sink_callbacks_t sBtA2dpSinkCbs = {
    sizeof(sBtA2dpSinkCbs),
    A2dpSinkServiceNonSplit::connectionStateCb,
    A2dpSinkServiceNonSplit::audioStateCb,
    A2dpSinkServiceNonSplit::audioConfigCb,
};

static btav_sink_vendor_callbacks_t sBtA2dpSinkVendorCbs = {
    sizeof(sBtA2dpSinkVendorCbs),
    A2dpSinkServiceNonSplit::audioFocusRequestVendorCb,
    A2dpSinkServiceNonSplit::audioCodecConfigVendorCb,
    A2dpSinkServiceNonSplit::audioDataReadVendorCb,
    A2dpSinkServiceNonSplit::registrationVendorCb,
    nullptr, nullptr, nullptr,
    A2dpSinkServiceNonSplit::scmstCapabilitiesCb,
    A2dpSinkServiceNonSplit::scmstCpCb
};

A2dpSinkServiceNonSplit *A2dpSinkServiceNonSplit::gA2dpSinkService = nullptr;

A2dpSinkServiceNonSplit::A2dpSinkServiceNonSplit(Adapter *pAdapter)
    : A2dpSinkService(pAdapter){
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

A2dpSinkServiceNonSplit::~A2dpSinkServiceNonSplit() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Disable service in case it has not explicitly been done already
  disableService();
}

void A2dpSinkServiceNonSplit::enableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  const bt_interface_t *pBtIf = getAdapter()->getBtInterface();
  if (!pBtIf) {
    ADK_LOG_NOTICE(LOGTAG "enableService failed: no Bt interface\n");
    return;
  }

  m_btA2dpSinkIF = (btav_sink_interface_t *)pBtIf->get_profile_interface(
      BT_PROFILE_ADVANCED_AUDIO_SINK_ID);
  m_btA2dpSinkVendorIF =
      (btav_sink_vendor_interface_t *)pBtIf->get_profile_interface(
          BT_PROFILE_ADVANCED_AUDIO_SINK_VENDOR_ID);

  if (!m_btA2dpSinkIF || !m_btA2dpSinkVendorIF) {
    ADK_LOG_NOTICE(LOGTAG "enableService failed: no sink\n");
    return;
  }
  // Init of Vendor Interface takes RTP header as flag
  // parameter"A2DP_SINK_RETREIVE_RTP_HEADER", as of now it is not included
  // during init
  bt_status_t status = m_btA2dpSinkIF->init(&sBtA2dpSinkCbs);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "failed to init A2DP sink base: %d\n", status);
  }

  uint8_t streamParam = A2DP_SINK_RETREIVE_RTP_HEADER |
                        A2DP_SINK_ENABLE_DELAY_REPORTING |
                        A2DP_SINK_ENABLE_NOTIFICATION_CB; // Notify data available
  status = m_btA2dpSinkVendorIF->init_vendor(
      &sBtA2dpSinkVendorCbs, /*max_a2dp_conn*/ 1, 0, streamParam);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "failed to init A2DP sink vendor: %d\n", status);
  }

  serviceState() = State::kRunning;

  // Reference for callback handling
  gA2dpSinkService = this;

  ADK_LOG_NOTICE(LOGTAG "enableService: running\n");
}

void A2dpSinkServiceNonSplit::disableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (m_btA2dpSinkVendorIF) {
    auto connected_dev = getConnectedDevice();
    m_btA2dpSinkVendorIF->audio_focus_state_vendor(0, &connected_dev);
  }

  // Clear any object associated with the generic service
  A2dpSinkService::disableService();

  gA2dpSinkService = nullptr;
}

int A2dpSinkServiceNonSplit::serviceProcessData(void *data) {
  MediaTransportNonSplit::TransportBuffer *tBuffer =
    static_cast<MediaTransportNonSplit::TransportBuffer *>(data);

  if (!tBuffer->buffer) {
    ADK_LOG_NOTICE(LOGTAG
                   " serviceProcessData : Sink Buffer is not allocated\n");
    tBuffer->size = 0;
    return 0;
  }
  if (m_btA2dpSinkVendorIF) {
    tBuffer->size = m_btA2dpSinkVendorIF->get_a2dp_sink_streaming_data_vendor(
        tBuffer->codec, tBuffer->buffer, tBuffer->size);
    return 1;
  }
  ADK_LOG_NOTICE(LOGTAG "serviceProcessData failed\n");
  return -1;
}

// Accept delay in 1/10ms units
void A2dpSinkServiceNonSplit::updateDelay(uint16_t delay) {
  ADK_LOG_NOTICE(LOGTAG "%s (%d, units of 1/10ms)\n", __func__, delay);
  // Convert from 1/10 ms units to ms
  m_btA2dpSinkVendorIF->update_qahw_delay_vendor(delay / 10);
}

void A2dpSinkServiceNonSplit::connectionStateCb(const RawAddress &bd_addr,
                                        btav_connection_state_t state) {
  ADK_LOG_NOTICE(LOGTAG "%s: state %d", __func__, state);
  struct CallbackData {
    btav_connection_state_t state;
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{state, bd_addr});
  queueBtCallback([data](void) -> void {
    Device *device = gA2dpSinkService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      if (gA2dpSinkService->getConnectedDevice() == data->bd_addr) {
        gA2dpSinkService->setDeviceForService(DeviceState::kDisconnected, {});}
      return;
    }

    auto transport = device->getTransport();

    switch (data->state) {
      case BTAV_CONNECTION_STATE_DISCONNECTED:
        // Only one profile is supported per remote device. Only clear the transport if this is the connected profile
        if (transport && (transport->getUUID() == LOCAL_UUID)) {
          gA2dpSinkService->clearTransportConfig();
        }
        gA2dpSinkService->setDeviceForService(DeviceState::kDisconnected, {});
        // Update vendor interface in Fluoride with connected device
        gA2dpSinkService->m_btA2dpSinkVendorIF->audio_focus_state_vendor(
          0, &data->bd_addr);
        break;
      case BTAV_CONNECTION_STATE_CONNECTING:
        // Only proceed if this device is not yet associated to any transport
        if (!transport) {
          gA2dpSinkService->setDeviceForService(DeviceState::kPending, data->bd_addr);
        } else {
          ADK_LOG_DEBUG(LOGTAG "Not handling CONNECTING event");
        }
        // Can notify the agent waiting for connection state after connection
        // request as the connection is in progress
        break;
      case BTAV_CONNECTION_STATE_CONNECTED:
        // Only one profile is supported per remote device. Only proceed if this is the connected profile
        if (transport && (transport->getUUID() == LOCAL_UUID)) {
          gA2dpSinkService->setDeviceForService(DeviceState::kConnected, data->bd_addr);
          device->devAddConnection(gA2dpSinkService->srvUUID());
        } else {
          // In the very unlikely case where we get here, initiate disconnection as this device
          // is already connected to a different profile
          ADK_LOG_WARNING(LOGTAG "Another profile was already connected!");
          gA2dpSinkService->disconnect(&data->bd_addr);
        }
        break;
      case BTAV_CONNECTION_STATE_DISCONNECTING:
        // Only one profile is supported per remote device. Only process disconnecting
        // if this is the connected profile
        if (transport && (transport->getUUID() == LOCAL_UUID)) {
          gA2dpSinkService->setDeviceForService(DeviceState::kPending, data->bd_addr);
        }
        break;
    }
  });
}

void A2dpSinkServiceNonSplit::audioStateCb(const RawAddress &bd_addr,
                                   btav_audio_state_t state) {
  ADK_LOG_NOTICE(LOGTAG "%s: state %d", __func__, state);
  struct CallbackData {
    btav_audio_state_t state;
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{state, bd_addr});
  queueBtCallback([data](void) -> void {
    Device *device = gA2dpSinkService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      return;
    }
    std::shared_ptr<MediaTransport> transport = device->getTransport();
    if (!transport) {
      return;
    }

    switch (data->state) {
      case BTAV_AUDIO_STATE_STARTED:
        if ((transport->getTransportState() == MediaTransport::State::kIdle) &&
            gA2dpSinkService->m_btA2dpSinkVendorIF) {
          // Update vendor interface state in Fluoride
          gA2dpSinkService->m_btA2dpSinkVendorIF
              ->update_streaming_device_vendor(&data->bd_addr);
          gA2dpSinkService->m_btA2dpSinkVendorIF->audio_focus_state_vendor(
              3, &data->bd_addr);

          // Signal transport, start timer to fetch and feed data
          transport->startMediaTransport();
          ADK_LOG_NOTICE(LOGTAG " Audio Started:%d\n", data->state);
        }
        break;
      case BTAV_AUDIO_STATE_REMOTE_SUSPEND:
        ADK_LOG_NOTICE(LOGTAG " Audio Suspended\n");
        // Fall through
      case BTAV_AUDIO_STATE_STOPPED:
        // Signal transport to stop, cancel data timer
        if ((transport->getTransportState() != MediaTransport::State::kIdle) &&
            gA2dpSinkService->m_btA2dpSinkVendorIF) {
          transport->stopMediaTransport();
          gA2dpSinkService->m_btA2dpSinkVendorIF->audio_focus_state_vendor(
              0, &data->bd_addr);
          ADK_LOG_NOTICE(LOGTAG " Audio Stopped:%d\n", data->state);
        }
        break;
      default:
        break;
    }
  });
}

void A2dpSinkServiceNonSplit::audioConfigCb(const RawAddress &UNUSED(bd_addr),
                                    uint32_t UNUSED(sample_rate),
                                    uint8_t UNUSED(channel_count)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void A2dpSinkServiceNonSplit::audioFocusRequestVendorCb(bt_bdaddr_t *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void A2dpSinkServiceNonSplit::audioCodecConfigVendorCb(
    bt_bdaddr_t *bd_addr, uint16_t codec_type,
    btav_codec_config_t codec_config) {
  ADK_LOG_NOTICE(LOGTAG "%s: type %d", __func__, codec_type);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    uint16_t codec_type;
    btav_codec_config_t codec_config;
  };
  std::shared_ptr<CallbackData> data = std::make_shared<CallbackData>(
      CallbackData{*bd_addr, codec_type, codec_config});
  queueBtCallback([data](void) -> void {
    // Create Specialized Media Transport interface for this service and
    // initialize with the codec parameters.
    Device *device = gA2dpSinkService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      return;
    }
    std::shared_ptr<MediaTransportNonSplitSink> transport =
        std::make_shared<MediaTransportNonSplitSink>(device, gA2dpSinkService,
                           data->codec_type,
                           dynamic_cast<IA2dpDataPathProcessing *>(gA2dpSinkService));
    if (!transport->mediaTransportInit(data->codec_config)) {
      ADK_LOG_ERROR(LOGTAG "- mediaTransportInit failed \n");
      gA2dpSinkService->disconnect(&data->bd_addr);
    }
    // Attempt to complete the Transport Configuration between
    // remote device and the end point
    gA2dpSinkService->setTransportConfig(transport);
  });
}

void A2dpSinkServiceNonSplit::audioDataReadVendorCb(bt_bdaddr_t *bd_addr) {
  struct CallbackData {
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{*bd_addr});
  queueBtCallback([data](void) -> void {
    Device *device = gA2dpSinkService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      return;
    }

    std::shared_ptr<MediaTransportNonSplitSink> transport =
        std::dynamic_pointer_cast<MediaTransportNonSplitSink>(device->getTransport());
    if (!transport) {
      return;
    }

   transport->audioDataReady();
  });
}

void A2dpSinkServiceNonSplit::registrationVendorCb(bool UNUSED(state)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void A2dpSinkServiceNonSplit::scmstCapabilitiesCb(bt_bdaddr_t *bd_addr,
                                                  bool scmstEnabled) {
  ADK_LOG_NOTICE(LOGTAG "%s DeviceAddress: %s SCMST %s: \n", __func__,
                 bd_addr->ToString().c_str(), scmstEnabled? "true":"false");
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    bool scmstEnabled;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{*bd_addr, scmstEnabled});
  queueBtCallback([data](void) -> void {
    gA2dpSinkService->setScmstCapabilities(data->bd_addr, data->scmstEnabled);
  });
}

void A2dpSinkServiceNonSplit::scmstCpCb(const RawAddress& bd_addr,
                                        uint8_t cpHeader) {
  ADK_LOG_NOTICE(LOGTAG "%s DeviceAddress: %s cpHeader %d: \n", __func__,
                 bd_addr.ToString().c_str(), cpHeader);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    uint8_t cpHeader;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{bd_addr, cpHeader});
  queueBtCallback([data](void) -> void {
    gA2dpSinkService->scmstCpCb(data->bd_addr, data->cpHeader);
  });
}
