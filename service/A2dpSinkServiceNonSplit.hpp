/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef A2DPSINKSERVICENONSPLIT_HPP
#define A2DPSINKSERVICENONSPLIT_HPP

#include "A2dpSinkService.hpp"

class A2dpSinkServiceNonSplit final: public A2dpSinkService,
                                            IA2dpDataPathProcessing {
 public:
  explicit A2dpSinkServiceNonSplit(Adapter *pAdapter);
  ~A2dpSinkServiceNonSplit() override;

  // From A2dpSinkService
  void enableService() final;
  void disableService() final;

  // From IA2dpDataPathProcessing
  int serviceProcessData(void *data) final;

  // Propagate updateDelay changes to remote device
  void updateDelay(uint16_t delay); // in 1/10ms units

  // Fluoride callbacks
  static void connectionStateCb(const RawAddress &bd_addr,
                                btav_connection_state_t state);
  static void audioStateCb(const RawAddress &bd_addr,
                           btav_audio_state_t state);
  static void audioConfigCb(const RawAddress &bd_addr,
                            uint32_t sample_rate,
                            uint8_t channel_count);

  // Fluoride Vendor callbacks
  static void audioFocusRequestVendorCb(bt_bdaddr_t *bd_addr);
  static void audioCodecConfigVendorCb(bt_bdaddr_t *bd_addr,
                                       uint16_t codec_type,
                                       btav_codec_config_t codec_config);
  static void audioDataReadVendorCb(bt_bdaddr_t *bd_addr);
  static void registrationVendorCb(bool state);

  static void scmstCapabilitiesCb(bt_bdaddr_t *bd_addr,
                                  bool scmstEnabled);
  static void scmstCpCb(const RawAddress& bd_addr,
                        uint8_t cpHeader);
 private:
  // Service reference for Fluoride callbacks
  static A2dpSinkServiceNonSplit *gA2dpSinkService;
};

#endif  // A2DPSINKSERVICENONSPLIT_HPP
