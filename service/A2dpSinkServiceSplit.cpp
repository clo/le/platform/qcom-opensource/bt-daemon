/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "A2dpSinkServiceSplit.hpp"
#include "MediaTransportSplitSink.hpp"
#include "Adapter.hpp"
#include "Common.hpp"
#include "MediaManager.hpp"

#define LOGTAG "A2DPSINK_SERVICE_SPLIT "

static btav_sink_callbacks_t sBtA2dpSinkCbs = {
    sizeof(sBtA2dpSinkCbs),
    A2dpSinkServiceSplit::connectionStateCb,
    A2dpSinkServiceSplit::audioStateCb,
    nullptr
};

static btav_sink_vendor_callbacks_t sBtA2dpSinkVendorCbs = {
    sizeof(sBtA2dpSinkVendorCbs),
    nullptr,
    A2dpSinkServiceSplit::audioCodecConfigVendorCb,
    nullptr,
    nullptr,
    A2dpSinkServiceSplit::startIndCb,
    A2dpSinkServiceSplit::suspendIndCb,
    nullptr,
    A2dpSinkServiceSplit::scmstCapabilitiesCb,
    A2dpSinkServiceSplit::scmstCpCb
};

A2dpSinkServiceSplit *A2dpSinkServiceSplit::gA2dpSinkService = nullptr;

A2dpSinkServiceSplit::A2dpSinkServiceSplit(Adapter *pAdapter)
    : A2dpSinkService(pAdapter) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

A2dpSinkServiceSplit::~A2dpSinkServiceSplit() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // DisableService in case it has not explicitly been done already
  disableService();
}

void A2dpSinkServiceSplit::enableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  const bt_interface_t *pBtIf = getAdapter()->getBtInterface();
  if (!pBtIf) {
    ADK_LOG_NOTICE(LOGTAG "enableService failed: no Bt interface\n");
    return;
  }

  m_btA2dpSinkIF = (btav_sink_interface_t *)pBtIf->get_profile_interface(
      BT_PROFILE_ADVANCED_AUDIO_SINK_SPLIT_ID);
  m_btA2dpSinkVendorIF =
      (btav_sink_vendor_interface_t *)pBtIf->get_profile_interface(
          BT_PROFILE_ADVANCED_AUDIO_SINK_SPLIT_VENDOR_ID);

  if (!m_btA2dpSinkIF || !m_btA2dpSinkVendorIF) {
    ADK_LOG_NOTICE(LOGTAG "enableService failed: no Fluoride sink I/F!\n");
    return;
  }

  // Init of Vendor Interface takes RTP header as flag
  // parameter"A2DP_SINK_RETREIVE_RTP_HEADER", as of now it is not included
  // during init
  bt_status_t status = m_btA2dpSinkIF->init(&sBtA2dpSinkCbs);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "failed to init A2DP sink base: %d\n", status);
  }
  status = m_btA2dpSinkVendorIF->init_vendor(
      &sBtA2dpSinkVendorCbs, /*max_a2dp_conn*/ 1, 0, /*streaming_param*/ 0);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG "failed to init A2DP sink vendor: %d\n", status);
  }

  serviceState() = State::kRunning;

  // Reference for callback handling
  gA2dpSinkService = this;
}

void A2dpSinkServiceSplit::disableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Clear any object associated with the generic service
  A2dpSinkService::disableService();

  gA2dpSinkService = nullptr;
}

void A2dpSinkServiceSplit::connectionStateCb(const RawAddress &bd_addr,
                                             btav_connection_state_t state) {
  ADK_LOG_NOTICE(LOGTAG "%s: state %d", __func__, state);
  struct CallbackData {
    btav_connection_state_t state;
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{state, bd_addr});
  queueBtCallback([data](void) -> void {
    Device *device = gA2dpSinkService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      if (gA2dpSinkService->getConnectedDevice() == data->bd_addr) {
        gA2dpSinkService->setDeviceForService(DeviceState::kDisconnected, {});}
      return;
    }

    auto transport = device->getTransport();

    switch (data->state) {
      case BTAV_CONNECTION_STATE_DISCONNECTED:
        // Only one profile is supported per remote device. Only disconnect if this is the connected profile
        if (transport && (transport->getUUID() == LOCAL_UUID)) {
          gA2dpSinkService->clearTransportConfig();
        }
        gA2dpSinkService->setDeviceForService(DeviceState::kDisconnected, {});
        break;
      case BTAV_CONNECTION_STATE_CONNECTING:
        // Only proceed if this device is not yet associated to any transport
        if (!transport) {
          gA2dpSinkService->setDeviceForService(DeviceState::kPending, data->bd_addr);
        }
        break;
      case BTAV_CONNECTION_STATE_CONNECTED:
        // Only one profile is supported per remote device. Only proceed if this is the connected profile
        if (transport && (transport->getUUID() == LOCAL_UUID)) {
          gA2dpSinkService->setDeviceForService(DeviceState::kConnected, data->bd_addr);
          device->devAddConnection(gA2dpSinkService->srvUUID());
        } else {
          // In the very unlikely case where we get here, initiate disconnection as this device
          // is already connected to a different profile
          ADK_LOG_WARNING(LOGTAG "Another profile was already connected!");
          gA2dpSinkService->disconnect(&data->bd_addr);
        }
        break;
      case BTAV_CONNECTION_STATE_DISCONNECTING:
        // Only one profile is supported per remote device. Only process disconnecting
        // if this is the connected profile
        if (transport && (transport->getUUID() == LOCAL_UUID)) {
          gA2dpSinkService->setDeviceForService(DeviceState::kPending, data->bd_addr);
        }
        break;
    }
  });
}

void A2dpSinkServiceSplit::audioStateCb(const RawAddress &bd_addr,
                                        btav_audio_state_t state) {
  ADK_LOG_NOTICE(LOGTAG "%s: state %d", __func__, state);
  struct CallbackData {
    btav_audio_state_t state;
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{state, bd_addr});
  queueBtCallback([data](void) -> void {
    Device *device = gA2dpSinkService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      return;
    }
    std::shared_ptr<MediaTransport> transport = device->getTransport();
    if (!transport) {
      return;
    }

    switch (data->state) {
      case BTAV_AUDIO_STATE_STARTED:
        if ((transport->getTransportState() == MediaTransport::State::kActive) &&
            gA2dpSinkService->m_btA2dpSinkVendorIF) {
          // Update vendor interface state in Fluoride
          gA2dpSinkService->m_btA2dpSinkVendorIF
              ->update_streaming_device_vendor(&data->bd_addr);
          ADK_LOG_NOTICE(LOGTAG "Audio Started:%d\n", data->state);
        }
        break;
      case BTAV_AUDIO_STATE_REMOTE_SUSPEND:
        ADK_LOG_NOTICE(LOGTAG "Audio Suspended\n", data->state);
        // Fall through
      case BTAV_AUDIO_STATE_STOPPED:
        // In split mode Transport is signalled to Stop on suspendIndCb()
        ADK_LOG_NOTICE(LOGTAG " Audio Stopped:%d\n", data->state);
        break;
      default:
        break;
    }
  });
}

void A2dpSinkServiceSplit::audioCodecConfigVendorCb(bt_bdaddr_t *bd_addr,
                                                    uint16_t codec_type,
                                                    btav_codec_config_t codec_config) {
  ADK_LOG_NOTICE(LOGTAG "%s: type %d", __func__, codec_type);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    uint16_t codec_type;
    btav_codec_config_t codec_config;
  };
  std::shared_ptr<CallbackData> data = std::make_shared<CallbackData>(
      CallbackData{*bd_addr, codec_type, codec_config});
  queueBtCallback([data](void) -> void {
    // Create Specialized Media Transport interface for this service and
    // initialize with the codec parameters.
    Device *device = gA2dpSinkService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      return;
    }
    std::shared_ptr<MediaTransportSplitSink> transport =
        std::make_shared<MediaTransportSplitSink>(device, gA2dpSinkService,
                                         data->codec_type);
    if (!transport->mediaTransportInit(data->codec_config)) {
      ADK_LOG_ERROR(LOGTAG "- mediaTransportInit failed \n");
      gA2dpSinkService->disconnect(&data->bd_addr);
    }
    // Attempt to complete the Transport Configuration between
    // remote device and the end point
    gA2dpSinkService->setTransportConfig(transport);
  });
}

void A2dpSinkServiceSplit::startIndCb(const RawAddress& bd_addr) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{bd_addr});
  queueBtCallback([data](void) -> void {
    Device *device = gA2dpSinkService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      return;
    }
    std::shared_ptr<MediaTransport> transport = device->getTransport();
    if (!transport) {
      ADK_LOG_NOTICE(LOGTAG "%s - Transport not available!", __func__);
      return;
    }

    if (transport->getTransportState() == MediaTransport::State::kIdle) {
      // Trigger transport signal so that in can be Aquired by client
      transport->startMediaTransport();
    }
  });
}

void A2dpSinkServiceSplit::suspendIndCb(const RawAddress& bd_addr) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{bd_addr});
  queueBtCallback([data](void) -> void {
    Device *device = gA2dpSinkService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      return;
    }
    std::shared_ptr<MediaTransport> transport = device->getTransport();
    if (!transport) {
      ADK_LOG_NOTICE(LOGTAG "%s - Transport not available!", __func__);
      return;
    }

    // Signal transport to stop
    if (transport->getTransportState() != MediaTransport::State::kIdle)  {
      transport->stopMediaTransport();
    }
  });
}

void A2dpSinkServiceSplit::scmstCapabilitiesCb(bt_bdaddr_t *bd_addr,
                                               bool scmstEnabled) {
  ADK_LOG_NOTICE(LOGTAG "%s DeviceAddress: %s SCMST %s: \n", __func__,
                 bd_addr->ToString().c_str(), scmstEnabled? "true":"false");
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    bool scmstEnabled;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{*bd_addr, scmstEnabled});
  queueBtCallback([data](void) -> void {
    gA2dpSinkService->setScmstCapabilities(data->bd_addr, data->scmstEnabled);
  });
}

void A2dpSinkServiceSplit::scmstCpCb(const RawAddress& bd_addr,
                                     uint8_t cpHeader) {
  ADK_LOG_NOTICE(LOGTAG "%s DeviceAddress: %s cpHeader %d: \n", __func__,
                 bd_addr.ToString().c_str(), cpHeader);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    uint8_t cpHeader;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{bd_addr, cpHeader});
  queueBtCallback([data](void) -> void {
    gA2dpSinkService->scmstCpCb(data->bd_addr, data->cpHeader);
  });
}

void A2dpSinkServiceSplit::startIndResponse(const RawAddress &bd_addr,
                                            int accepted) {
  ADK_LOG_NOTICE(LOGTAG "%s, accepted %d ", __func__, accepted);

  // Respond to accept/reject start indication
  gA2dpSinkService->m_btA2dpSinkVendorIF->start_ind_rsp(bd_addr, accepted);
}

void A2dpSinkServiceSplit::suspendIndResponse(const RawAddress &bd_addr,
                                              int accepted) {
  ADK_LOG_NOTICE(LOGTAG "%s, accepted %d ", __func__, accepted);

  // Respond to accept/reject suspend indication
  gA2dpSinkService->m_btA2dpSinkVendorIF->suspend_ind_rsp(bd_addr, accepted);
}
