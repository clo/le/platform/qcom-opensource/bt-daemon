/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef A2DPSINKSERVICESPLIT_HPP
#define A2DPSINKSERVICESPLIT_HPP

#include "A2dpSinkService.hpp"

class A2dpSinkServiceSplit final : public A2dpSinkService {
 public:
  explicit A2dpSinkServiceSplit(Adapter *pAdapter);
  ~A2dpSinkServiceSplit() final;

  // From A2dpSinkService
  void enableService() final;
  void disableService() final;

  void startIndResponse(const RawAddress &bd_addr,
                        int accepted);
  void suspendIndResponse(const RawAddress &bd_addr,
                          int accepted);

  // Fluoride callbacks
  static void connectionStateCb(const RawAddress &bd_addr,
                                btav_connection_state_t state);
  static void audioStateCb(const RawAddress &bd_addr,
                           btav_audio_state_t state);

  // Fluoride Vendor callbacks
  static void audioCodecConfigVendorCb(bt_bdaddr_t *bd_addr,
                                       uint16_t codec_type,
                                       btav_codec_config_t codec_config);
  static void startIndCb(const RawAddress& bd_addr);
  static void suspendIndCb(const RawAddress& bd_addr);
  static void scmstCapabilitiesCb(bt_bdaddr_t *bd_addr,
                                  bool scmstEnabled);
  static void scmstCpCb(const RawAddress& bd_addr,
                        uint8_t cpHeader);
 private:
  // Service reference for Fluoride callbacks
  static A2dpSinkServiceSplit *gA2dpSinkService;
};
#endif  // A2DPSINKSERVICESPLIT_HPP
