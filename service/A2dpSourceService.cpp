/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "A2dpSourceService.hpp"
#include "MediaManager.hpp"
#include "Adapter.hpp"
#include "Common.hpp"


#define LOGTAG "A2DPSOURCE_SERVICE "

A2dpSourceService *A2dpSourceService::gA2dpSourceService = nullptr;

static btav_source_callbacks_t sBtA2dpSourceCbs = {
    sizeof(sBtA2dpSourceCbs),
    A2dpSourceService::connectionStateCb,
    A2dpSourceService::audioStateCb,
    A2dpSourceService::audioConfigCb};

static btav_a2dp_codec_sample_rate_t getA2dpCodecSampleRate(
    btav_sbc_codec_config_t sbc_config) {
  btav_a2dp_codec_sample_rate_t rate = BTAV_A2DP_CODEC_SAMPLE_RATE_NONE;
  if (sbc_config.samp_freq & SBC_SAMP_FREQ_16) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_16000);
  }
  if (sbc_config.samp_freq & SBC_SAMP_FREQ_44) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_44100);
  }
  if (sbc_config.samp_freq & SBC_SAMP_FREQ_48) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_48000);
  }
  if (rate == BTAV_A2DP_CODEC_SAMPLE_RATE_NONE) {
    ADK_LOG_WARNING(LOGTAG "%s: Unsupported SBC sample rate 0x%02x", __func__,
                    sbc_config.samp_freq);
  }
  return rate;
}

static btav_a2dp_codec_sample_rate_t getA2dpCodecSampleRate(
    btav_aac_codec_config_t aac_config) {
  btav_a2dp_codec_sample_rate_t rate = BTAV_A2DP_CODEC_SAMPLE_RATE_NONE;
  if (aac_config.sampling_freq & AAC_SAMP_FREQ_16000) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_16000);
  }
  if (aac_config.sampling_freq & AAC_SAMP_FREQ_24000) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_24000);
  }
  if (aac_config.sampling_freq & AAC_SAMP_FREQ_44100) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_44100);
  }
  if (aac_config.sampling_freq & AAC_SAMP_FREQ_48000) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_48000);
  }
  if (aac_config.sampling_freq & AAC_SAMP_FREQ_88200) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_88200);
  }
  if (aac_config.sampling_freq & AAC_SAMP_FREQ_96000) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_96000);
  }
  if (rate == BTAV_A2DP_CODEC_SAMPLE_RATE_NONE) {
    ADK_LOG_WARNING(LOGTAG "%s: Unsupported AAC sample rate 0x%04x", __func__,
                    aac_config.sampling_freq);
  }
  return rate;
}

static btav_a2dp_codec_sample_rate_t getA2dpCodecSampleRate(
    btav_aptx_codec_config_t aptx_config) {
  btav_a2dp_codec_sample_rate_t rate = BTAV_A2DP_CODEC_SAMPLE_RATE_NONE;
  if (aptx_config.sampling_freq & APTX_SAMPLERATE_44100) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_44100);
  }
  if (aptx_config.sampling_freq & APTX_SAMPLERATE_48000) {
    rate = static_cast<btav_a2dp_codec_sample_rate_t>(
        rate | BTAV_A2DP_CODEC_SAMPLE_RATE_48000);
  }
  if (rate == BTAV_A2DP_CODEC_SAMPLE_RATE_NONE) {
    ADK_LOG_WARNING(LOGTAG "%s: Unsupported APTX sample rate 0x%02x", __func__,
                    aptx_config.sampling_freq);
  }
  return rate;
}

static btav_a2dp_codec_channel_mode_t getA2dpCodecChannelMode(
    btav_sbc_codec_config_t sbc_config) {
  btav_a2dp_codec_channel_mode_t mode = BTAV_A2DP_CODEC_CHANNEL_MODE_NONE;
  if (sbc_config.ch_mode & SBC_CH_MONO) {
    mode = static_cast<btav_a2dp_codec_channel_mode_t>(
        mode | BTAV_A2DP_CODEC_CHANNEL_MODE_MONO);
  }
  if (sbc_config.ch_mode & SBC_CH_DUAL || sbc_config.ch_mode & SBC_CH_STEREO ||
      sbc_config.ch_mode & SBC_CH_JOINT) {
    mode = static_cast<btav_a2dp_codec_channel_mode_t>(
        mode | BTAV_A2DP_CODEC_CHANNEL_MODE_STEREO);
  }
  if (mode == BTAV_A2DP_CODEC_CHANNEL_MODE_NONE) {
    ADK_LOG_WARNING(LOGTAG "%s: Unsupported SBC channel mode 0x%02x", __func__,
                    sbc_config.ch_mode);
  }
  return mode;
}

static btav_a2dp_codec_channel_mode_t getA2dpCodecChannelMode(
    btav_aac_codec_config_t aac_config) {
  btav_a2dp_codec_channel_mode_t mode = BTAV_A2DP_CODEC_CHANNEL_MODE_NONE;
  if (aac_config.channel_count & AAC_CHANNELS_1) {
    mode = static_cast<btav_a2dp_codec_channel_mode_t>(
        mode | BTAV_A2DP_CODEC_CHANNEL_MODE_MONO);
  }
  if (aac_config.channel_count & AAC_CHANNELS_2) {
    mode = static_cast<btav_a2dp_codec_channel_mode_t>(
        mode | BTAV_A2DP_CODEC_CHANNEL_MODE_STEREO);
  }
  if (mode == BTAV_A2DP_CODEC_CHANNEL_MODE_NONE) {
    ADK_LOG_WARNING(LOGTAG "%s: Unsupported AAC channel count 0x%02x", __func__,
                    aac_config.channel_count);
  }
  return mode;
}

static btav_a2dp_codec_channel_mode_t getA2dpCodecChannelMode(
    btav_aptx_codec_config_t aptx_config) {
  btav_a2dp_codec_channel_mode_t mode = BTAV_A2DP_CODEC_CHANNEL_MODE_NONE;
  if (aptx_config.channel_count & APTX_CHANNELS_MONO) {
    mode = static_cast<btav_a2dp_codec_channel_mode_t>(
        mode | BTAV_A2DP_CODEC_CHANNEL_MODE_MONO);
  }
  if (aptx_config.channel_count & APTX_CHANNELS_STEREO) {
    mode = static_cast<btav_a2dp_codec_channel_mode_t>(
        mode | BTAV_A2DP_CODEC_CHANNEL_MODE_STEREO);
  }
  if (mode == BTAV_A2DP_CODEC_CHANNEL_MODE_NONE) {
    ADK_LOG_WARNING(LOGTAG "%s: Unsupported APTX channel count 0x%02x",
                    __func__, aptx_config.channel_count);
  }
  return mode;
}

static bool getA2dpCodecConfig(const btav_codec_configuration_t &codec,
                               btav_a2dp_codec_config_t &a2dp_codec_config) {
  switch (codec.codec_type) {
    case A2DP_SINK_AUDIO_CODEC_SBC:
      a2dp_codec_config.codec_type = BTAV_A2DP_CODEC_INDEX_SOURCE_SBC;
      a2dp_codec_config.sample_rate =
          getA2dpCodecSampleRate(codec.codec_config.sbc_config);
      a2dp_codec_config.channel_mode =
          getA2dpCodecChannelMode(codec.codec_config.sbc_config);
      a2dp_codec_config.codec_specific_1 =
          codec.codec_config.sbc_config.block_len;
      a2dp_codec_config.codec_specific_2 =
          codec.codec_config.sbc_config.num_subbands;
      a2dp_codec_config.codec_specific_3 =
          codec.codec_config.sbc_config.alloc_mthd;
      a2dp_codec_config.codec_specific_4 =
          codec.codec_config.sbc_config.max_bitpool;
      a2dp_codec_config.codec_specific_5 =
          codec.codec_config.sbc_config.min_bitpool;
      break;

    case A2DP_SINK_AUDIO_CODEC_AAC:
      a2dp_codec_config.codec_type = BTAV_A2DP_CODEC_INDEX_SOURCE_AAC;
      a2dp_codec_config.sample_rate =
          getA2dpCodecSampleRate(codec.codec_config.aac_config);
      a2dp_codec_config.channel_mode =
          getA2dpCodecChannelMode(codec.codec_config.aac_config);
      break;

    case A2DP_SINK_AUDIO_CODEC_APTX:
      a2dp_codec_config.codec_type = BTAV_A2DP_CODEC_INDEX_SOURCE_APTX;
      a2dp_codec_config.sample_rate =
          getA2dpCodecSampleRate(codec.codec_config.aptx_config);
      a2dp_codec_config.channel_mode =
          getA2dpCodecChannelMode(codec.codec_config.aptx_config);
      break;

    default:
      ADK_LOG_ERROR(LOGTAG "%s: Unsupported codec (%d)", __func__,
                    codec.codec_type);
      return false;
  }

  if (a2dp_codec_config.sample_rate == BTAV_A2DP_CODEC_SAMPLE_RATE_NONE ||
      a2dp_codec_config.channel_mode == BTAV_A2DP_CODEC_CHANNEL_MODE_NONE) {
    return false;
  }

  return true;
}

A2dpSourceService::A2dpSourceService(Adapter *pAdapter)
    : A2dpProfileService(pAdapter, LOCAL_UUID, REMOTE_UUID),
      m_btA2dpSourceIF(nullptr),
      m_btA2dpSourceVendorIF(nullptr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

A2dpSourceService::~A2dpSourceService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  disableService();
}

void A2dpSourceService::enableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // A2dpSrc service is enabled when the Adapter is powered up

  const bt_interface_t *pBtIf = getAdapter()->getBtInterface();
  if (!pBtIf) {
    // Leaves state of this service as STOPPED to signal it is not ready ???
    ADK_LOG_ERROR(LOGTAG "enableService failed: no Bt interface\n");
    return;
  }

  // Get the Fluoride interfaces for a2dpSrc profile and vendor extensions
  m_btA2dpSourceIF = (btav_source_interface_t *)pBtIf->get_profile_interface(
      BT_PROFILE_ADVANCED_AUDIO_ID);
  m_btA2dpSourceVendorIF =
      (btav_vendor_interface_t *)pBtIf->get_profile_interface(
          BT_PROFILE_ADVANCED_AUDIO_VENDOR_ID);

  if (!m_btA2dpSourceIF || !m_btA2dpSourceVendorIF) {
    // Leaves state of this service as STOPPED to signal it is not ready ???
    ADK_LOG_ERROR(LOGTAG
                  "enableService failed: no bt a2dpsrc profile interface\n");
    return;
  }

  bt_status_t status =
      m_btA2dpSourceIF->init(&sBtA2dpSourceCbs, 1, m_codecList);
  if (status != BT_STATUS_SUCCESS) {
    // Leaves state of this service as STOPPED to signal it is not ready!
    ADK_LOG_ERROR(LOGTAG "failed to init a2dpsrc profile: %d\n", status);
    return;
  }
}

void A2dpSourceService::disableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Clear any object associated with the base service like
  // endpoint connections and transport (if any).
  A2dpProfileService::disableService();

  // Cleanup stack interfaces
  if (m_btA2dpSourceIF) {
    m_btA2dpSourceIF->cleanup();
    m_btA2dpSourceIF = nullptr;
  }
  if (m_btA2dpSourceVendorIF) {
    m_btA2dpSourceVendorIF->cleanup_vendor();
    m_btA2dpSourceVendorIF = nullptr;
  }

  gA2dpSourceService = nullptr;
}

bool A2dpSourceService::connect(bt_bdaddr_t *devAddr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (getDeviceState() == DeviceState::kConnected ||
      getDeviceState() == DeviceState::kPending) {
    ADK_LOG_NOTICE(LOGTAG " A2DP Source Active connection with %s\n",
                   getConnectedDevice().ToString().c_str());
    // Do not send a new connection request, profile is already connected
    // or a connected request is already in progress.
    return false;
  }

  // Send the connection request for the device and
  // update the device connection state to pending
  if ((m_btA2dpSourceIF) &&
      (m_btA2dpSourceIF->connect(*devAddr) == BT_STATUS_SUCCESS)) {
    ADK_LOG_NOTICE(LOGTAG
                   " A2DP Source connection initiated with device with %s\n",
                   devAddr->ToString().c_str());
    // Change the state to reflect request in progress.
    setDeviceForService(DeviceState::kPending, *devAddr);

    return true;
  }
  return false;
}

bool A2dpSourceService::disconnect(bt_bdaddr_t *devAddr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (getDeviceState() == DeviceState::kDisconnected) {
    ADK_LOG_NOTICE(LOGTAG " No A2DP Source Active connection available \n");
    // Do not send a new disconnection request,
    // profile is already disconnected.
    return false;
  }

  // Send the connection request for the device and
  // update the device connection state to pending
  if ((getConnectedDevice() == *devAddr) && (m_btA2dpSourceIF)) {
    ADK_LOG_NOTICE(LOGTAG " A2DP Source DisConnecting from %s\n",
                   getConnectedDevice().ToString().c_str());
    m_btA2dpSourceIF->disconnect(*devAddr);

    // Change the state to reflect request in progress.
    setDeviceForService(DeviceState::kPending, *devAddr);
    return true;
  }
  return false;
}

bool A2dpSourceService::addSupportedCodec(const btav_codec_configuration_t &codec) {
  ADK_LOG_DEBUG(LOGTAG "%s: %u  (%s)\n", __func__, codec.codec_type,
                MediaManager::getCodecName(codec.codec_type));

  btav_a2dp_codec_config_t a2dp_codec_config;

  if (!getA2dpCodecConfig(codec, a2dp_codec_config)) {
    ADK_LOG_WARNING(LOGTAG "%s: Could not convert codec config to new format\n",
                  __func__);
    return false;
  }

  a2dp_codec_config.codec_priority = BTAV_A2DP_CODEC_PRIORITY_DEFAULT;
  a2dp_codec_config.bits_per_sample = BTAV_A2DP_CODEC_BITS_PER_SAMPLE_16;

  m_codecList.push_back(a2dp_codec_config);
  // Commenting out this line as it is causing system instability when it is called more than once.
  // The below call passes a list of codecs into Fluoride that defines the priority of codec
  // however we only support SBC at the moment so can temporarily remove the call.
  // Need to understand the reason behind the instability before adding this back in.
  // Note: Also add back the updateSupportedCodecs() on removeSupportedCodec()
  //updateSupportedCodecs();

  return true;
}

bool A2dpSourceService::removeSupportedCodec(const btav_codec_configuration_t &codec) {
  ADK_LOG_DEBUG(LOGTAG "%s: %u (%s)\n", __func__, codec.codec_type,
                MediaManager::getCodecName(codec.codec_type));
  bool found = false;
  for ( auto iter = m_codecList.begin(); iter != m_codecList.end(); ) {
    if (iter->codec_type == codec.codec_type) {
      iter = m_codecList.erase(iter);
      found = true;
    } else {
      ++iter;
    }
  }

  // The supported codec list has changed so update Fluoride
  // Add back at the same time as the update in addSupportedCodec()
  // if (m_codecList.size() != 0) {
  //   updateSupportedCodecs();
  // }
  return found;
}

bt_status_t A2dpSourceService::setCpScms(bt_bdaddr_t *bd_addr, uint8_t cpScms) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  if (m_btA2dpSourceVendorIF) {
     return m_btA2dpSourceVendorIF->update_cp_header(bd_addr, cpScms);;
  }
  else {
      ADK_LOG_ERROR(LOGTAG "  --> Fluoride interface not available");
      return BT_STATUS_NOT_READY;
  }
}

void A2dpSourceService::connectionStateCb(const RawAddress &bd_addr,
                                          btav_connection_state_t state) {
  ADK_LOG_NOTICE(LOGTAG "%s: state %d", __func__, state);
  struct CallbackData {
    btav_connection_state_t state;
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{state, bd_addr});
  queueBtCallback([data](void) -> void {
    Device *device = gA2dpSourceService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      if (gA2dpSourceService->getConnectedDevice() == data->bd_addr) {
        gA2dpSourceService->setDeviceForService(DeviceState::kDisconnected, {});}
      return;
    }


    auto transport = device->getTransport();

    switch (data->state) {
      case BTAV_CONNECTION_STATE_DISCONNECTED:
        // Only one profile is supported per remote device. Only disconnect if this is the connected profile
        if (transport && (transport->getUUID() == LOCAL_UUID)) {
          gA2dpSourceService->clearTransportConfig();
        }
        gA2dpSourceService->setDeviceForService(DeviceState::kDisconnected, {});
        break;
      case BTAV_CONNECTION_STATE_CONNECTING:
        // Only proceed if this device is not yet associated to any transport
        if (!transport) {
          gA2dpSourceService->setDeviceForService(DeviceState::kPending, data->bd_addr);
        }
        // Can notify the agent waiting for connection state after connection
        // request as the connection is in progress
        break;
      case BTAV_CONNECTION_STATE_CONNECTED:
        // Only one profile is supported per remote device. Only proceed if this is the connected profile
        if (transport && (transport->getUUID() == LOCAL_UUID)) {
          gA2dpSourceService->setDeviceForService(DeviceState::kConnected, data->bd_addr);
          device->devAddConnection(gA2dpSourceService->srvUUID());
        // Propagate down "Connected" state for any additional set up needed.
        // Note: this is neeed at the moment to comply with the expected sequence of events for
        // a2dpsrc split mode where audio-manager requires the transport state to be changed to Pending
        // and it has to be synchronized with Fluoride requirement to call start_stream also.
        gA2dpSourceService->connectedCb(data->bd_addr);
        } else {
          // In the very unlikely case where we get here, initiate disconnection as this device
          // is already connected to a different profile
          ADK_LOG_WARNING(LOGTAG "%s: Do not accept as another profile was already connected!");
          gA2dpSourceService->disconnect(&data->bd_addr);
        }
        break;
      case BTAV_CONNECTION_STATE_DISCONNECTING:
        // Only one profile is supported per remote device. Only process disconnecting
        // if this is the connected profile
        if (transport && (transport->getUUID() == LOCAL_UUID)) {
          gA2dpSourceService->setDeviceForService(DeviceState::kPending, data->bd_addr);
        }
        break;
    }
  });
}

void A2dpSourceService::audioStateCb(const RawAddress &bd_addr,
                                     btav_audio_state_t state) {
  ADK_LOG_NOTICE(LOGTAG "%s: state %d", __func__, state);
  struct CallbackData {
    btav_audio_state_t state;
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{state, bd_addr});
  queueBtCallback([data](void) -> void {
    Device *device = gA2dpSourceService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      return;
    }
    std::shared_ptr<MediaTransport> transport = device->getTransport();
    if (!transport) {
      return;
    }

    switch (data->state) {
      case BTAV_AUDIO_STATE_STARTED:
          // Notify transport that sink side is now ready
          transport->startMediaTransport();
          ADK_LOG_NOTICE(LOGTAG " Audio Started:%d\n", data->state);
        break;
      case BTAV_AUDIO_STATE_REMOTE_SUSPEND:
        ADK_LOG_NOTICE(LOGTAG " Audio Suspended\n");
        // Fall through
      case BTAV_AUDIO_STATE_STOPPED:
        // Signal transport to stop, cancel data timer
        if (transport->getTransportState() != MediaTransport::State::kIdle) {
          transport->stopMediaTransport();
          ADK_LOG_NOTICE(LOGTAG " Audio Stopped:%d\n", data->state);
        }
        break;
      default:
        break;
    }
  });
}

void A2dpSourceService::audioConfigCb(
    const RawAddress &bd_addr, btav_a2dp_codec_config_t a2dp_codec_config,
    std::vector<btav_a2dp_codec_config_t> codecs_local_capabilities) {
  ADK_LOG_NOTICE(LOGTAG "%s: type %s", __func__,
                 a2dp_codec_config.ToString().c_str());
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    btav_a2dp_codec_config_t a2dp_codec_config;
    std::vector<btav_a2dp_codec_config_t> codecs_local_capabilities;
  };
  std::shared_ptr<CallbackData> data = std::make_shared<CallbackData>(
      CallbackData{bd_addr, a2dp_codec_config, codecs_local_capabilities});
  queueBtCallback([data](void) -> void {
    btav_codec_config_t codec_config;
    MediaTransport::convertCodecConfigTypes(data->a2dp_codec_config, &codec_config);
    gA2dpSourceService->createTransport(data->bd_addr,
                         data->a2dp_codec_config.codec_type, codec_config);
  });
}

int A2dpSourceService::startStream(bt_bdaddr_t *devAddr) const {
  ADK_LOG_NOTICE(LOGTAG "%s %s\n", __func__, devAddr->ToString().c_str());
  return m_btA2dpSourceVendorIF->start_stream(devAddr);
}

int A2dpSourceService::suspendStream(bt_bdaddr_t *devAddr) const {
  ADK_LOG_NOTICE(LOGTAG "%s %s\n", __func__, devAddr->ToString().c_str());
  return m_btA2dpSourceVendorIF->suspend_stream(devAddr);
}

void A2dpSourceService::scmstCapabilitiesCb(bt_bdaddr_t *bd_addr,
                         bool scmstEnabled) {
  ADK_LOG_NOTICE(LOGTAG "%s DeviceAddress: %s SCMST %s: \n", __func__,
                 bd_addr->ToString().c_str(), scmstEnabled? "true":"false");
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    bool scmstEnabled;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{*bd_addr, scmstEnabled});
  queueBtCallback([data](void) -> void {
    gA2dpSourceService->setScmstCapabilities(data->bd_addr, data->scmstEnabled);
  });
}

void A2dpSourceService::updateSupportedCodecs() {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);

  if (!m_btA2dpSourceIF) {
    ADK_LOG_INFO(LOGTAG " -- no bt service (adapter is not enabled yet)");
    return;
  }

  bt_status_t status = m_btA2dpSourceIF->config_codec(getConnectedDevice(), m_codecList);

  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG " -- failed, status = %d", status);
    return;
  }

  ADK_LOG_NOTICE(LOGTAG " -- codecs updated");
}
