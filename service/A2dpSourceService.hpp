/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef A2DPSOURCESERVICE_HPP
#define A2DPSOURCESERVICE_HPP

#include <hardware/bt_av.h>
#include <hardware/bt_av_vendor.h>
#include <vector>

#include "ProfileService.hpp"

class A2dpSourceService : public A2dpProfileService {
 public:
  static constexpr const char *REMOTE_UUID =
      "0000110b-0000-1000-8000-00805f9b34fb";  // aka A2DP Sink
  static constexpr const char *LOCAL_UUID =
      "0000110a-0000-1000-8000-00805f9b34fb";  // aka A2DP Source

  ~A2dpSourceService() override;

  // From A2dpProfileService
  void enableService() override = 0;
  void disableService() final;
  bool connect(bt_bdaddr_t *devAddr) final;
  bool disconnect(bt_bdaddr_t *devAddr) final;
  bool addSupportedCodec(const btav_codec_configuration_t &codec) final;
  bool removeSupportedCodec(const btav_codec_configuration_t &codec) final;
  int startStream(bt_bdaddr_t *devAddr) const;
  int suspendStream(bt_bdaddr_t *devAddr) const;
  bt_status_t setCpScms(bt_bdaddr_t *bd_addr, uint8_t cpScms) final;

  // Bt A2dpSource callbacks
  static void connectionStateCb(const RawAddress &bd_addr,
                                btav_connection_state_t state);
  static void audioStateCb(const RawAddress &bd_addr,
                           btav_audio_state_t state);
  static void audioConfigCb(const RawAddress &bd_addr,
                            btav_a2dp_codec_config_t codec_config,
                std::vector<btav_a2dp_codec_config_t> codecs_local_capabilities);
  static void scmstCapabilitiesCb(bt_bdaddr_t *bd_addr,
                                  bool scmstEnabled);
 
protected:
  explicit A2dpSourceService(Adapter *pAdapter);

  static A2dpSourceService *gA2dpSourceService;

  // Interfaces to Fluoride stack
  btav_source_interface_t *m_btA2dpSourceIF;
  btav_vendor_interface_t *m_btA2dpSourceVendorIF;

 private:
  virtual void createTransport(const RawAddress &bd_addr,
                               uint16_t codec,
                               btav_codec_config_t codec_config) = 0;
  virtual void connectedCb(const RawAddress &UNUSED(bd_addr)) {}
  void updateSupportedCodecs();

  // List of codec configs supported by the endpoints associated to this service
  std::vector<btav_a2dp_codec_config_t> m_codecList;
};
#endif  // A2DPSOURCESERVICE_HPP
