/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "A2dpSourceServiceNonSplit.hpp"
#include "MediaTransportNonSplitSource.hpp"
#include "Adapter.hpp"
#include "Common.hpp"

#define LOGTAG "A2DPSOURCE_SERVICE_NONSPLIT "

static btav_vendor_callbacks_t sBtA2dpSourceVendorCbs = {
    sizeof(sBtA2dpSourceVendorCbs),
    nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
    A2dpSourceServiceNonSplit::mtuPacketTypeVendorCb,
    nullptr,
    A2dpSourceService::scmstCapabilitiesCb
  };

A2dpSourceServiceNonSplit::A2dpSourceServiceNonSplit(Adapter *pAdapter)
    : A2dpSourceService(pAdapter) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

A2dpSourceServiceNonSplit::~A2dpSourceServiceNonSplit() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  disableService();
}

void A2dpSourceServiceNonSplit::enableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  A2dpSourceService::enableService();

  // Set A2DP_SRC_PUMP_ENCODED_DATA while testing.
  uint8_t streamParam = A2DP_SRC_PUMP_ENCODED_DATA;
  bt_status_t status = m_btA2dpSourceVendorIF->init_vendor(
      &sBtA2dpSourceVendorCbs, /*max_a2dp_conn*/ 1, /*a2dp_multicast_state*/ 0,
      streamParam);

  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG "failed to init A2DP source vendor: %d\n", status);
    // Leaves state of this service as STOPPED to signal it is not ready ???
    return;
  }

  serviceState() = State::kRunning;

  // Reference for callback handling
  gA2dpSourceService = this;
  ADK_LOG_NOTICE(LOGTAG "%s: running\n", __func__);
}

int A2dpSourceServiceNonSplit::serviceProcessData(void *data) {
  MediaTransportNonSplit::TransportBuffer *tBuffer =
    static_cast<MediaTransportNonSplit::TransportBuffer *>(data);

  if (!tBuffer->buffer) {
    ADK_LOG_NOTICE(LOGTAG
                   " serviceProcessData : Source Buffer is not allocated\n");
    tBuffer->size = 0;
    return 0;
  }

  auto devAddr = getConnectedDevice();
  if (m_btA2dpSourceVendorIF) {
    return (m_btA2dpSourceVendorIF->btav_send_encoded_data_vendor(
        &devAddr, tBuffer->buffer, tBuffer->size, tBuffer->codec));
  }

  ADK_LOG_NOTICE(LOGTAG "serviceProcessData failed\n");
  return -1;
}

void A2dpSourceServiceNonSplit::mtuPacketTypeVendorCb(uint16_t mtu, uint8_t UNUSED(packettype),
                                              bt_bdaddr_t *bd_addr) {
  ADK_LOG_NOTICE(LOGTAG "%s src_mtu: %d", __func__, mtu);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    uint16_t mtu;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{*bd_addr, mtu});
  queueBtCallback([data](void) -> void {

    Device *device = gA2dpSourceService->getAdapter()->getDevice(data->bd_addr);
    if (!device) {
      return;
    }

    std::shared_ptr<MediaTransport> transport = device->getTransport();
    if (transport) {
      ADK_LOG_DEBUG(LOGTAG " --> Setting Mtus ");
      transport->setMTUs(data->mtu, data->mtu);
      // Attempt to complete the Transport Configuration between
      // remote device and the end point
      gA2dpSourceService->setTransportConfig(transport);
    }
  });
}

void A2dpSourceServiceNonSplit::createTransport(const RawAddress &bd_addr,
                                                uint16_t codec_type,
                                                btav_codec_config_t codec_config) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  // Create Specialized Media Transport interface for this service and
  // initialize with the codec parameters.
  Device *device = getAdapter()->getDevice(bd_addr);
  if (!device) {
    return;
  }
  std::shared_ptr<MediaTransportNonSplitSource> transport =
       std::make_shared<MediaTransportNonSplitSource>(device, this, codec_type,
                        dynamic_cast<IA2dpDataPathProcessing *> (this));
  if (!transport->mediaTransportInit(codec_config)) {
    ADK_LOG_ERROR(LOGTAG "- mediaTransportInit failed \n");
    transport->triggerTransportRelease();
  }
}
