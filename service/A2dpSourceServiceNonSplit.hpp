/*
Copyright (c) 2016-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef A2DPSOURCESERVICENONSPLIT_HPP
#define A2DPSOURCESERVICENONSPLIT_HPP

#include "A2dpSourceService.hpp"

class A2dpSourceServiceNonSplit final : public A2dpSourceService,
                                               IA2dpDataPathProcessing {
 public:
  explicit A2dpSourceServiceNonSplit(Adapter *pAdapter);
  ~A2dpSourceServiceNonSplit() final;

  // From A2dpProfileService
  void enableService() final;

  // From IA2dpDataPathProcessing
  int serviceProcessData(void *data) final;

  // Bt vendor callbacks
  static void mtuPacketTypeVendorCb(uint16_t mtu,
                                    uint8_t packettype,
                                    bt_bdaddr_t *bd_addr);

 private:
  // From A2dpProfileService
  void createTransport(const RawAddress &bd_addr,
                       uint16_t codec,
                       btav_codec_config_t codec_config) final;
};
#endif  // A2DPSOURCESERVICENONSPLIT_HPP