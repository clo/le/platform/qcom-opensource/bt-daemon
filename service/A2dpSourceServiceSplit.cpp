/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "A2dpSourceServiceSplit.hpp"
#include "MediaTransportSplitSource.hpp"
#include "Adapter.hpp"
#include "Common.hpp"

#define LOGTAG "A2DPSOURCE_SERVICE_SPLIT "

static btav_vendor_callbacks_t sBtA2dpSourceVendorCbs = {
    sizeof(sBtA2dpSourceVendorCbs),
    nullptr, nullptr, nullptr, nullptr,
    nullptr, nullptr, nullptr, nullptr,
    A2dpSourceService::scmstCapabilitiesCb
  };

A2dpSourceServiceSplit::A2dpSourceServiceSplit(Adapter *pAdapter)
    : A2dpSourceService(pAdapter) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

A2dpSourceServiceSplit::~A2dpSourceServiceSplit() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  disableService();
}

void A2dpSourceServiceSplit::enableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  A2dpSourceService::enableService();

  bt_status_t status = m_btA2dpSourceVendorIF->init_vendor(&sBtA2dpSourceVendorCbs, 1, 0, 0);

  if (status != BT_STATUS_SUCCESS) {
    // Leaves state of this service as STOPPED to signal it is not ready!
    ADK_LOG_ERROR(LOGTAG "failed to init A2DP source vendor: %d\n", status);
    return;
  }

  serviceState() = State::kRunning;

  // Reference for callback handling
  gA2dpSourceService = this;

  ADK_LOG_NOTICE(LOGTAG "%s: running\n", __func__);
}

void A2dpSourceServiceSplit::createTransport(const RawAddress &bd_addr,
                                             uint16_t codec_type,
                                             btav_codec_config_t codec_config) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  // Create Specialized Media Transport interface for this service and
  // initialize with the codec parameters.
  Device *device = getAdapter()->getDevice(bd_addr);
  if (!device) {
    return;
  }
  std::shared_ptr<MediaTransportSplitSource> transport =
       std::make_shared<MediaTransportSplitSource>(device, this, codec_type);
  if (!transport->mediaTransportInit(codec_config)) {
    ADK_LOG_ERROR(LOGTAG "- mediaTransportInit failed \n");
    transport->triggerTransportRelease();
  }
}

void A2dpSourceServiceSplit::connectedCb(const RawAddress &bd_addr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  Device *device = getAdapter()->getDevice(bd_addr);
  if (!device) {
    return;
  }

  // Attempt to complete the Transport Configuration between
  // remote device and the end point
  setTransportConfig(device->getTransport());
  bt_bdaddr_t devAddr = bd_addr;
  if (startStream(&devAddr) != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG "  --> Error returned on startStream!\n");
  }
}