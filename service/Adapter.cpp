/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Adapter.hpp"

#include <signal.h>
#include <string.h>

#include <hardware/bluetooth.h>
#include <hardware/hardware.h>

#include <cstring>
#include <sstream>
#include <iostream>
#include <list>
#include <map>

#include "A2dpSinkServiceSplit.hpp"
#include "A2dpSinkServiceNonSplit.hpp"
#include "A2dpSourceServiceSplit.hpp"
#include "A2dpSourceServiceNonSplit.hpp"
#include "AvrcpCTService.hpp"
#include "AvrcpTGService.hpp"
#include "HidService.hpp"
#include "MediaManager.hpp"
#include "MediaTransport.hpp"
#include "Error.hpp"
#include "Sdbus.hpp"
#include "GattNativeDefines.hpp"

#define ADAPTER_INTERFACE "org.bluez.Adapter1"

#define LOGTAG "ADAPTER "
#define BT_DISCOVERABLE_TIMEOUT 30  // 30 seconds

// Object path of D-Bus interface
static char const *sObjPath = "/org/bluez/hci0";

Adapter *g_Adapter = nullptr;

static bool SetWakeAlarm(uint64_t UNUSED(delay_millis), bool UNUSED(should_wake), alarm_cb UNUSED(cb),
                         void *UNUSED(data)) {
  return BT_STATUS_SUCCESS;
}

static int AcquireWakeLock(const char *UNUSED(lock_name)) { return BT_STATUS_SUCCESS; }

static int ReleaseWakeLock(const char *UNUSED(lock_name)) { return BT_STATUS_SUCCESS; }

static bt_os_callouts_t callouts = {
    sizeof(bt_os_callouts_t),
    SetWakeAlarm,
    AcquireWakeLock,
    ReleaseWakeLock,
};

// Load vendor interface and initialize GAP specific functionalities
static bt_callbacks_t sBluetoothCallbacks = {
    sizeof(sBluetoothCallbacks),  Adapter::adapterStateChangeCb,
    Adapter::adapterPropertiesCb, Adapter::remoteDevicePropertiesCb,
    Adapter::deviceFoundCb,       Adapter::discoveryStateChangedCb,
    Adapter::pinRequestCb,        Adapter::sspRequestCb,
    Adapter::bondStateChangedCb,  nullptr,
    Adapter::threadEventCb,       Adapter::dutModeRecvCb,
    Adapter::leTestModeRecvCb,    Adapter::energyInfoCb};

static btvendor_callbacks_t sVendorCallbacks = {
    sizeof(sVendorCallbacks), Adapter::bredrCleanupCb,
    nullptr,                  nullptr,
    nullptr,                  nullptr,
    Adapter::aclStateChangedWithReasonCb};

Adapter::Adapter(const bt_interface_t *bt_interface, MediaManager *mediaManager)
    : m_stopping(false),
      m_btIF(bt_interface),
      m_btVendorInterface(nullptr),
      m_bdAddr(RawAddress::kEmpty),
      m_bdCOD(0),
      m_discovering(false),
      m_splitmode(false),
      m_powered(false),
      m_discoverable(false),
      m_state(BT_ADAPTER_OFF),
      m_discoverableTimeout(BT_DISCOVERABLE_TIMEOUT),
      m_enableTimer(nullptr),
      m_disableTimer(nullptr),
      m_discoverableTimer(nullptr),
      m_isPairing(false),
      m_sdbusSlot(nullptr),
      m_mediaManager(mediaManager) {}

Adapter::~Adapter() {
  m_stopping = true;

  flushDeviceList(true);

  sd_event_source_unref(m_discoverableTimer);
  m_discoverableTimer = nullptr;

  sd_event_source_unref(m_enableTimer);
  m_enableTimer = nullptr;

  sd_event_source_unref(m_disableTimer);
  m_disableTimer = nullptr;

  m_bdUUIDs.clear();

  sd_bus_slot_unref(m_sdbusSlot);
  m_sdbusSlot = nullptr;

  ADK_LOG_NOTICE(LOGTAG "Adapter unregistered\n");

  if (m_btVendorInterface) {
    m_btVendorInterface->cleanup();
    m_btVendorInterface = nullptr;
  }

  if (m_gattLib) {
    delete m_gattLib;
  }

  if (m_btIF) {
    m_btIF->cleanup();
    m_btIF = nullptr;
  }

  g_Adapter = nullptr;
}

bool Adapter::init() {
  // Note: Init of bluetooth interface will be done again on power on as it is
  // de-initialised after each power off, but we need it now too to use the API
  int res = m_btIF->init(&sBluetoothCallbacks);
  if (res != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG "Failed to init BT interface: %d - %s\n", -res,
                  strerror(-res));
    return false;
  }
  ADK_LOG_NOTICE(LOGTAG "Initialised BT interface\n");

  m_btIF->set_os_callouts(&callouts);  // Not sure about the usage

  m_btVendorInterface = (btvendor_interface_t *)m_btIF->get_profile_interface(
      BT_PROFILE_VENDOR_ID);

  if (m_btVendorInterface) {
    m_btVendorInterface->init(&sVendorCallbacks);
  }

  // Reference for callback handling
  g_Adapter = this;

  // Create DBus interface for Adapter functionalities
  static const sd_bus_vtable sSdAdapterVTable[] = {
      SD_BUS_VTABLE_START(0),

      SD_BUS_METHOD("StartDiscovery", nullptr, nullptr,
                    Adapter::sd_startDiscovery, SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("StopDiscovery", nullptr, nullptr,
                    Adapter::sd_stopDiscovery, SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("RemoveDevice", "o", nullptr, Adapter::sd_removeDevice,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("SetAFHMap", "s", nullptr,
                    Adapter::sd_setAFHMap, SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("SetDiscoveryFilter", "a{sv}", nullptr,
                    Adapter::sd_setDiscoveryFilter, SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_PROPERTY("Address", "s", Adapter::sd_getBtAddress, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Name", "s", Adapter::sd_getBtName, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("Alias", "s", Adapter::sd_getBtName,
                               Adapter::sd_setBtName, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Class", "u", Adapter::sd_getBtDevClass, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("Powered", "b", Adapter::sd_getBtPowered,
                               Adapter::sd_setBtPowered, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("SplitMode", "b", Adapter::sd_getBtSplitMode,
                               Adapter::sd_setBtSplitMode, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("UUIDs", "as", Adapter::sd_getBtUUIDs, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("Discoverable", "b",
                               Adapter::sd_getBtDiscoverable,
                               Adapter::sd_setBtDiscoverable, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("DiscoverableTimeout", "u",
                               Adapter::sd_getBtDiscoverableTimeout,
                               Adapter::sd_setBtDiscoverableTimeout, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      // No separate support for pairable and discoverable with BT HAL so use
      // the same handlers
      SD_BUS_WRITABLE_PROPERTY("Pairable", "b", Adapter::sd_getBtDiscoverable,
                               Adapter::sd_setBtDiscoverable, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("PairableTimeout", "u",
                               Adapter::sd_getBtDiscoverableTimeout,
                               Adapter::sd_setBtDiscoverableTimeout, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Discovering", "b", Adapter::sd_getBtDiscovering, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Modalias", "s", Adapter::sd_getModalias, 0,
                      SD_BUS_VTABLE_PROPERTY_EXPLICIT),
      SD_BUS_VTABLE_END};

  res = sd_bus_add_object_vtable(g_sdbus, &m_sdbusSlot, sObjPath,
                                 ADAPTER_INTERFACE, sSdAdapterVTable, this);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "interface init failed on path %s: %d - %s\n",
                   sObjPath, -res, strerror(-res));
    return false;
  }

  return true;
}

void Adapter::adapterStateChangeCb(bt_state_t state) {
  ADK_LOG_NOTICE(LOGTAG "%s: state %d\n", __func__, state);
  queueBtCallback([state](void) -> void {
    Adapter *pAdapter = g_Adapter;

    if ((!pAdapter) || pAdapter->m_stopping) return;

    switch (state) {
      case BT_STATE_ON: {
        if (pAdapter->m_enableTimer) {
          sd_event_source_unref(pAdapter->m_enableTimer);
          pAdapter->m_enableTimer = nullptr;
        }

        pAdapter->m_mediaManager->updateMediaInterfaceLists();

        // Set the initial properties of the Adapter ( Should be read from
        // config file) These config parameters are to be retained upon power
        // cycle
        bt_property_t prop;
        bt_scan_mode_t scan_mode = BT_SCAN_MODE_CONNECTABLE;

        prop.type = BT_PROPERTY_ADAPTER_SCAN_MODE;
        prop.val = &scan_mode;
        prop.len = sizeof(bt_scan_mode_t);
        pAdapter->m_btIF->set_adapter_property(&prop);

        prop.type = BT_PROPERTY_BDNAME;
        prop.val = const_cast<char *>(pAdapter->m_bdName.c_str());
        prop.len = pAdapter->m_bdName.size();
        pAdapter->m_btIF->set_adapter_property(&prop);

        pAdapter->m_state = BT_ADAPTER_ON;
        pAdapter->m_powered = true;

      } break;
      case BT_STATE_OFF: {
        if (pAdapter->m_disableTimer) {
          sd_event_source_unref(pAdapter->m_disableTimer);
          pAdapter->m_disableTimer = nullptr;
        }

        if (pAdapter->m_discoverableTimer) {
          sd_event_source_unref(pAdapter->m_discoverableTimer);
          pAdapter->m_discoverableTimer = nullptr;
        }

        pAdapter->m_state = BT_ADAPTER_OFF;
        pAdapter->m_powered = false;
        pAdapter->m_discoverable = false;
        pAdapter->m_discovering = false;

        // De-init stack interface
        bt_property_t prop;
        bt_scan_mode_t scan_mode = BT_SCAN_MODE_NONE;
        prop.type = BT_PROPERTY_ADAPTER_SCAN_MODE;
        prop.val = &scan_mode;
        prop.len = sizeof(bt_scan_mode_t);
        pAdapter->m_btIF->set_adapter_property(&prop);
        pAdapter->m_btIF->cleanup();

        // Clean up the complete device list even paired as it will be
        // initialized on adapter poweron/enable
        pAdapter->flushDeviceList(true);

        // Remove services associated with the adapter
        for (const auto &serviceIter : pAdapter->m_services) {
          delete serviceIter.second;
          pAdapter->m_services.erase(serviceIter.first);
        }
        pAdapter->m_mediaManager->updateAdapterServices(
                    nullptr, nullptr, nullptr, nullptr);
        ADK_LOG_NOTICE(LOGTAG "%s: Updated m_state, now %d, will emit Props changed\n", __func__, state);
        // Remove BLE service
        delete pAdapter->m_gattLib;
        delete pAdapter->m_bleService;
      } break;
    }
    sd_bus_emit_properties_changed(g_sdbus, sObjPath, ADAPTER_INTERFACE,
                                   "Powered", nullptr);
  });
}

void Adapter::adapterPropertiesCb(bt_status_t status, int numProperties,
                                  bt_property_t *properties) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  struct CallbackData {
    bt_status_t status;
    std::vector<BtProperty> properties;
  };
  std::shared_ptr<CallbackData> data = std::make_shared<CallbackData>();
  data->status = status;
  for (int i = 0; i < numProperties; ++i) {
    data->properties.emplace_back(
        properties[i].type,
        std::vector<uint8_t>(
            static_cast<uint8_t *>(properties[i].val),
            static_cast<uint8_t *>(properties[i].val) + properties[i].len));
  }
  queueBtCallback([data](void) -> void {
    Adapter *pAdapter = g_Adapter;
    for (const BtProperty &property : data->properties) {
      switch (property.type) {
        case BT_PROPERTY_BDADDR: {
          ADK_LOG_DEBUG(LOGTAG "AdapterProperties::BT_PROPERTY_BDADDR");
          memcpy(&pAdapter->m_bdAddr, property.value.data(),
                 property.value.size());
          sd_bus_emit_properties_changed(g_sdbus, sObjPath, ADAPTER_INTERFACE,
                                         "Address", nullptr);
            BtEvent *event = new BtEvent;
            event->event_id = GATT_EVENT_ADAPTER_PROPERTIES;
            event->gatt_adapter_property_event.len = property.value.size();
            event->gatt_adapter_property_event.val = &pAdapter->m_bdAddr;
            event->gatt_adapter_property_event.type = BT_PROPERTY_BDADDR;
            auto *sGattLibService = gatt::GattLibService::getGatt();
            if (sGattLibService) {
              sGattLibService->NativeEvent(event);
            }
          }
          break;
        case BT_PROPERTY_BDNAME:
          ADK_LOG_DEBUG(LOGTAG "AdapterProperties::BT_PROPERTY_BDNAME");
          if (property.value.size()) {
            pAdapter->m_bdName = std::string(
                reinterpret_cast<const char *>(property.value.data()),
                property.value.size());
            sd_bus_emit_properties_changed(g_sdbus, sObjPath, ADAPTER_INTERFACE,
                                           "Name", "Alias", nullptr);
            BtEvent *event = new BtEvent;
            event->event_id = GATT_EVENT_ADAPTER_PROPERTIES;
            event->gatt_adapter_property_event.len = property.value.size();
            event->gatt_adapter_property_event.val = pAdapter->m_bdName.c_str();
            event->gatt_adapter_property_event.type = BT_PROPERTY_BDNAME;
            auto *sGattLibService = gatt::GattLibService::getGatt();
            if (sGattLibService) {
              sGattLibService->NativeEvent(event);
            }
          }
          break;
        case BT_PROPERTY_CLASS_OF_DEVICE:
          memcpy(&pAdapter->m_bdCOD, property.value.data(),
                 property.value.size());
          sd_bus_emit_properties_changed(g_sdbus, sObjPath, ADAPTER_INTERFACE,
                                         "Class", nullptr);
          break;
        case BT_PROPERTY_UUIDS: {
          bluetooth::Uuid::UUID128Bit m_uuid;
          pAdapter->m_bdUUIDs.clear();
          for (size_t iter = 0; iter < property.value.size() / 16; iter++) {
            memcpy((uint8_t *)&m_uuid[0], property.value.data() + (iter * 16),
                   16);
            pAdapter->m_bdUUIDs.insert(
                bluetooth::Uuid::From128BitBE(m_uuid).ToString());
          }
          sd_bus_emit_properties_changed(g_sdbus, sObjPath, ADAPTER_INTERFACE,
                                         "UUIDs", nullptr);
        } break;
        case BT_PROPERTY_ADAPTER_SCAN_MODE: {
          bt_scan_mode_t scanMode = BT_SCAN_MODE_NONE;
          memcpy(&scanMode, property.value.data(), property.value.size());
          bool discoverable =
              (scanMode == BT_SCAN_MODE_CONNECTABLE_DISCOVERABLE);
          if (pAdapter->m_discoverable != discoverable) {
            pAdapter->m_discoverable = discoverable;
            sd_bus_emit_properties_changed(g_sdbus, sObjPath, ADAPTER_INTERFACE,
                                           "Discoverable", "Pairable", nullptr);
            if (discoverable && (pAdapter->m_discoverableTimeout > 0)) {
              int res = sd_event_add_time(
                  g_eventLoop, &pAdapter->m_discoverableTimer, CLOCK_MONOTONIC,
                  get_timer_delay(pAdapter->m_discoverableTimeout * 1000ull *
                                  1000ull),
                  0, Adapter::discoverableTimerExpired, pAdapter);
              if (res < 0) {
                ADK_LOG_ERROR(LOGTAG "Failed to create timer: %d - %s\n", -res,
                              strerror(-res));
              }
            }
          }
        } break;
        case BT_PROPERTY_ADAPTER_DISCOVERY_TIMEOUT:
          ADK_LOG_NOTICE(
              LOGTAG
              " adapterPropertiesCb:BT_PROPERTY_ADAPTER_DISCOVERY_TIMEOUT\n");
          // Only seems to get called just after power on, not sure has much to
          // do with device discovery (which comes via discoveryStateChangedCb)
          pAdapter->m_discovering = false;
          sd_bus_emit_properties_changed(g_sdbus, sObjPath, ADAPTER_INTERFACE,
                                         "Discovering", nullptr);
          break;
        case BT_PROPERTY_ADAPTER_BONDED_DEVICES:
          // Gives the list of Bonded device address upon power on for us to
          // create the Device object for paired devices
          ADK_LOG_NOTICE(
              LOGTAG
              " adapterPropertiesCb:BT_PROPERTY_ADAPTER_BONDED_DEVICES %d\n",
              property.value.size() / sizeof(bt_bdaddr_t));
          // Create the device object for the bonded devices with bond state as
          // true whose properties will be updated on later callbacks
          for (size_t i = 0; i < (property.value.size() / sizeof(bt_bdaddr_t));
               i++) {
            bt_bdaddr_t bdAddr;
            memcpy((bt_bdaddr_t *)&bdAddr,
                   property.value.data() + (i * sizeof(bt_bdaddr_t)),
                   sizeof(bt_bdaddr_t));

            const auto &iter = pAdapter->m_devices.find(bdAddr);
            if (iter == pAdapter->m_devices.end()) {
              Device *pRemoteDev =
                  new Device(pAdapter, bdAddr, std::string(), std::string(), 0,
                             0, BT_DEVICE_DEVTYPE_BREDR, BT_BOND_STATE_BONDED);
              pAdapter->m_devices.insert({bdAddr, pRemoteDev});
            } else {
              ADK_LOG_ERROR(
                  LOGTAG
                  " FLUORIDE is sending a duplicated bonded device - %s!\n",
                  bdAddr.ToString().c_str());
            }
          }
          break;
        case BT_PROPERTY_LOCAL_LE_FEATURES: {
            ADK_LOG_DEBUG(LOGTAG "AdapterProperties::BT_PROPERTY_LOCAL_LE_FEATURES");
            pAdapter->m_leFeat.assign(reinterpret_cast<const uint8_t*>(property.value.data()),
                property.value.data()+property.value.size());
            BtEvent *event = new BtEvent;
            event->event_id = GATT_EVENT_ADAPTER_PROPERTIES;
            event->gatt_adapter_property_event.len = property.value.size();
            event->gatt_adapter_property_event.val = pAdapter->m_leFeat.data();
            event->gatt_adapter_property_event.type = BT_PROPERTY_LOCAL_LE_FEATURES;
            auto *sGattLibService = gatt::GattLibService::getGatt();
            if (sGattLibService) {
              sGattLibService->NativeEvent(event);
            }
          }
          break;
        default:
          ADK_LOG_NOTICE(LOGTAG "Unhandled property %d\n", property.type);
          break;
      }
    }
  });
}

void Adapter::remoteDevicePropertiesCb(bt_status_t status, bt_bdaddr_t *pBdAddr,
                                       int numProperties,
                                       bt_property_t *properties) {
  ADK_LOG_NOTICE(LOGTAG "%s %s\n", __func__, pBdAddr->ToString().c_str());
  struct CallbackData {
    bt_status_t status;
    bt_bdaddr_t bdAddr;
    std::vector<BtProperty> properties;
  };
  std::shared_ptr<CallbackData> data = std::make_shared<CallbackData>();
  data->status = status;
  data->bdAddr = *pBdAddr;
  for (int i = 0; i < numProperties; ++i) {
    data->properties.emplace_back(
        properties[i].type,
        std::vector<uint8_t>(
            static_cast<uint8_t *>(properties[i].val),
            static_cast<uint8_t *>(properties[i].val) + properties[i].len));
  }
  queueBtCallback([data](void) -> void {
    Adapter *pAdapter = g_Adapter;
    Device *pRemoteDev = nullptr;
    const auto &iter = pAdapter->m_devices.find(data->bdAddr);
    if (iter != pAdapter->m_devices.end()) {
      pRemoteDev = iter->second;
    } else {
      pRemoteDev = new Device(pAdapter, data->bdAddr, std::string(),
                              std::string(), 0, 0, BT_DEVICE_DEVTYPE_BREDR, BT_BOND_STATE_NONE);
      pAdapter->m_devices.insert({data->bdAddr, pRemoteDev});
    }

    pRemoteDev->updateDeviceProperties(data->properties);
    // After updating the device's properties, if the device supports the HID
    // UUID then connect the HID Profile
    auto supported_uuids = pRemoteDev->getSupportedUUIDs();
    if (supported_uuids.find(HidService::REMOTE_UUID) != supported_uuids.end()) {
      g_Adapter->connectHidProfile(pRemoteDev);
    }
  });
}

Device *Adapter::getDevice(const bt_bdaddr_t &bdAddr) {
  const auto &iter = m_devices.find(bdAddr);
  return (iter != m_devices.end()) ? iter->second : nullptr;
}

Device *Adapter::getDeviceFromPath(std::string device_object_path)
{
  for (const auto &entry: m_devices) {
    if (entry.second->getDeviceObjectPath() == device_object_path){
      return entry.second;
    }
  }
  return nullptr;
}

void Adapter::deviceFoundCb(int numProperties, bt_property_t *properties) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  struct CallbackData {
    std::vector<BtProperty> properties;
  };
  std::shared_ptr<CallbackData> data = std::make_shared<CallbackData>();
  for (int i = 0; i < numProperties; ++i) {
    data->properties.emplace_back(
        properties[i].type,
        std::vector<uint8_t>(
            static_cast<uint8_t *>(properties[i].val),
            static_cast<uint8_t *>(properties[i].val) + properties[i].len));
  }
  queueBtCallback([data](void) -> void {
    Adapter *pAdapter = g_Adapter;
    bt_bdaddr_t bdAddr;
    std::string bdName;
    std::string bdFriendlyName;
    uint32_t nCOD;
    int8_t rssi;
    bt_device_type_t bdType;

    for (const BtProperty &property : data->properties) {
      switch (property.type) {
        case BT_PROPERTY_BDADDR:
          memcpy((bt_bdaddr_t *)&bdAddr, property.value.data(),
                 property.value.size());
          break;
        case BT_PROPERTY_BDNAME:
          bdName =
              std::string(reinterpret_cast<const char *>(property.value.data()),
                          property.value.size());
          break;
        case BT_PROPERTY_CLASS_OF_DEVICE:
          memcpy(&nCOD, property.value.data(), property.value.size());
          break;
        case BT_PROPERTY_REMOTE_RSSI:
          memcpy(&rssi, property.value.data(), property.value.size());
          ADK_LOG_INFO(LOGTAG " -- BT_PROPERTY_REMOTE_RSSI: %d\n", rssi);
          break;
        case BT_PROPERTY_REMOTE_FRIENDLY_NAME:
          bdFriendlyName =
              std::string(reinterpret_cast<const char *>(property.value.data()),
                          property.value.size());
          break;
        case BT_PROPERTY_TYPE_OF_DEVICE:
          memcpy(&bdType, property.value.data(), property.value.size());
          break;
        default:
          break;
      }
    }
    // Find if the device is already there in the device list using the MAC and
    // device Type as unique key combination
    const auto &iter = pAdapter->m_devices.find(bdAddr);
    if (iter != pAdapter->m_devices.end()) {
      // Device already there in list
      return;
    }
    Device *pRemoteDevice = new Device(pAdapter, bdAddr, bdName, bdFriendlyName,
                                       rssi, nCOD, bdType, BT_BOND_STATE_NONE);
    pAdapter->m_devices.insert({bdAddr, pRemoteDevice});
  });
}

void Adapter::bondStateChangedCb(bt_status_t status, bt_bdaddr_t *bdAddr,
                                 bt_bond_state_t state) {
  ADK_LOG_NOTICE(LOGTAG "%s: bt_status %d bond state %d\n", __func__, status, state);
  struct CallbackData {
    bt_status_t status;
    bt_bdaddr_t bdAddr;
    bt_bond_state_t state;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{status, *bdAddr, state});
  queueBtCallback([data](void) -> void {
    Adapter *pAdapter = g_Adapter;
    Device *pRemoteDev = nullptr;
    bool bRemove = false;

    const auto &iter = pAdapter->m_devices.find(data->bdAddr);
    if (iter != pAdapter->m_devices.end()) {
      // Device already there in list
      pRemoteDev = iter->second;
    }

    // As Device is not there in list create a new one
    if (!pRemoteDev) {
      pRemoteDev = new Device(pAdapter, data->bdAddr, std::string(),
                              std::string(), 0, 0, BT_DEVICE_DEVTYPE_BREDR, BT_BOND_STATE_NONE);
      pAdapter->m_devices.insert({data->bdAddr, pRemoteDev});
    }

    pRemoteDev->bondStateChanged(data->status, data->state, &bRemove);

    if (bRemove) {
      // the device is unpaired and needs to be removed from device's list
      pAdapter->m_devices.erase(pRemoteDev->getDeviceAddr());
      delete pRemoteDev;
    }
    // reset the pairing state variable upon status change
    if (data->state == BT_BOND_STATE_BONDING) {
      pAdapter->m_isPairing = true;
    } else {
      pAdapter->m_isPairing = false;
      if ((data->state == BT_BOND_STATE_NONE) &&
          (pRemoteDev->m_sd_removeDeviceReq != nullptr) &&
           !(pRemoteDev->isACLConnected())) {
        // If a transport is up for this device clear it up
        if (pRemoteDev->getTransport()) {
          for (const auto &serviceIter : pAdapter->m_services) {
            if (serviceIter.second->srvUUID() ==
                pRemoteDev->getTransport()->getUUID()){
              dynamic_cast<A2dpProfileService*>(serviceIter.second)->
                                                  clearTransportConfig();
              break;
            }
          }
        }
        pRemoteDev->removeDeviceReqComplete();
        pAdapter->m_devices.erase(pRemoteDev->getDeviceAddr());
        delete pRemoteDev;
      }
    }
  });
}

void Adapter::aclStateChangedWithReasonCb(bt_status_t status,
                                          bt_bdaddr_t *remote_bd_addr,
                                          bt_acl_state_t state,
                                          uint8_t reason,
                                          uint8_t transport_type) {
  ADK_LOG_NOTICE(LOGTAG "%s: state %d reason %d transport_type %d\n", __func__, state,
                 reason, transport_type);
  struct CallbackData {
    bt_status_t status;
    bt_bdaddr_t bdAddr;
    bt_acl_state_t state;
    uint8_t reason;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{status, *remote_bd_addr, state, reason});
  queueBtCallback([data](void) -> void {

    Adapter *pAdapter = g_Adapter;
    Device *pRemoteDev = nullptr;

    const auto &iter = pAdapter->m_devices.find(data->bdAddr);
    if (iter != pAdapter->m_devices.end()) {
      // Device already there in list
      pRemoteDev = iter->second;
    }
    if (!pRemoteDev) {
      pRemoteDev = new Device(pAdapter, data->bdAddr, std::string(),
                              std::string(), 0, 0, BT_DEVICE_DEVTYPE_BREDR, BT_BOND_STATE_NONE);
      pAdapter->m_devices.insert({data->bdAddr, pRemoteDev});
    }

    pRemoteDev->updateConnection(data->state, data->reason);
  });
}

void Adapter::discoveryStateChangedCb(bt_discovery_state_t state) {
  ADK_LOG_NOTICE(LOGTAG "%s: state %d\n", __func__, state);
  queueBtCallback([state](void) -> void {
    Adapter *pAdapter = g_Adapter;
    switch (state) {
      case BT_DISCOVERY_STOPPED:
        pAdapter->m_discovering = false;
        break;
      case BT_DISCOVERY_STARTED:
        pAdapter->m_discovering = true;
        break;
    }
    int res = sd_bus_emit_properties_changed(
        g_sdbus, sObjPath, ADAPTER_INTERFACE, "Discovering", nullptr);
    if (res < 0) {
      ADK_LOG_ERROR(LOGTAG "Failed to emit PropertiesChanged signal: %d - %s\n",
                    -res, strerror(-res));
    }
  });
}

void Adapter::pinRequestCb(bt_bdaddr_t *bdAddr, bt_bdname_t *bdName,
                           uint32_t cod, bool min16Digit) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  struct CallbackData {
    bt_bdaddr_t bdAddr;
    std::string bdName;
    uint32_t cod;
    bool min16Digit;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{
          *bdAddr, reinterpret_cast<char *>(bdName->name), cod, min16Digit});
  queueBtCallback([data](void) -> void {
    Adapter *pAdapter = g_Adapter;
    Device *pRemoteDev = nullptr;

    const auto &iter = pAdapter->m_devices.find(data->bdAddr);
    if (iter != pAdapter->m_devices.end()) {
      // Device already there in list
      pRemoteDev = iter->second;
    }

    // As Device is not there in list create a new one
    if (!pRemoteDev) {
      pRemoteDev = new Device(pAdapter, data->bdAddr, data->bdName,
                              std::string(), 0, 0, BT_DEVICE_DEVTYPE_BREDR, BT_BOND_STATE_NONE);
      pAdapter->m_devices.insert({data->bdAddr, pRemoteDev});
    }

    pRemoteDev->deviceRequestPin(
        data->min16Digit);  // has to handle everything on device object itself.
  });
}

void Adapter::sspRequestCb(bt_bdaddr_t *bdAddr, bt_bdname_t *bdName,
                           uint32_t cod, bt_ssp_variant_t pairingVariant,
                           uint32_t passKey) {
  ADK_LOG_NOTICE(LOGTAG "%s: name %s\n", __func__, bdName);
  struct CallbackData {
    bt_bdaddr_t bdAddr;
    std::string bdName;
    uint32_t cod;
    bt_ssp_variant_t pairingVariant;
    uint32_t passKey;
  };
  std::shared_ptr<CallbackData> data = std::make_shared<CallbackData>(
      CallbackData{*bdAddr, reinterpret_cast<char *>(bdName->name), cod,
                   pairingVariant, passKey});
  queueBtCallback([data](void) -> void {
    Adapter *pAdapter = g_Adapter;
    Device *pRemoteDev = nullptr;

    const auto &iter = pAdapter->m_devices.find(data->bdAddr);
    if (iter != pAdapter->m_devices.end()) {
      pRemoteDev = iter->second;
    }
    if (!pRemoteDev) {
      pRemoteDev = new Device(pAdapter, data->bdAddr, data->bdName,
                              std::string(), 0, 0, BT_DEVICE_DEVTYPE_BREDR, BT_BOND_STATE_NONE);
      pAdapter->m_devices.insert({data->bdAddr, pRemoteDev});
    }
    pRemoteDev->deviceSspRequest(data->pairingVariant, data->passKey);
  });
}

void Adapter::threadEventCb(bt_cb_thread_evt UNUSED(event)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

void Adapter::dutModeRecvCb(uint16_t UNUSED(opcode), uint8_t *UNUSED(buf), uint8_t UNUSED(len)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

void Adapter::leTestModeRecvCb(bt_status_t UNUSED(status), uint16_t UNUSED(pktCount)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

void Adapter::energyInfoCb(bt_activity_energy_info *UNUSED(energy_info),
                           bt_uid_traffic_t *UNUSED(uid_data)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

void Adapter::bredrCleanupCb(bool UNUSED(status)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

void Adapter::vendor_hci_event_recv_cb(uint8_t event_code, uint8_t *buf, uint8_t len) {
   ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
   ADK_LOG_NOTICE(LOGTAG "vendor_hci:: Event: %02x Len: %02x", event_code, len);
   int index = 2;
   while (index < (len+2)) {
       if ((index % 16) == 0) {
         ADK_LOG_NOTICE(LOGTAG "\n");
       }
       ADK_LOG_NOTICE(LOGTAG "%02x", buf[index-2]);
       index++;
   }
   // Return status 0 on success
   ADK_LOG_NOTICE(LOGTAG "Status: %02x", buf[len-1]);
   if (buf[len-1] == 0) {
     ADK_LOG_NOTICE(LOGTAG "\nVendor specific command completed successfully");
   } else {
     ADK_LOG_NOTICE(LOGTAG "\nVendor specific command failed");
   }
}

void Adapter::updateSnooplogStatusCb(bool UNUSED(status)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

int Adapter::enableTimerExpired(sd_event_source *UNUSED(s), uint64_t UNUSED(usec),
                                void *userdata) {
  ADK_LOG_NOTICE(LOGTAG "Adapter::enableTimerExpired\n");
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  pAdapter->m_state = BT_ADAPTER_OFF;
  pAdapter->m_powered = false;

  return 0;
}

int Adapter::disableTimerExpired(sd_event_source *UNUSED(s), uint64_t UNUSED(usec),
                                 void *userdata) {
  ADK_LOG_NOTICE(LOGTAG "Adapter::disableTimerExpired\n");
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  pAdapter->m_state = BT_ADAPTER_ON;
  pAdapter->m_powered = true;

  return 0;
}

int Adapter::discoverableTimerExpired(sd_event_source *UNUSED(s), uint64_t UNUSED(usec),
                                      void *userdata) {
  ADK_LOG_NOTICE(LOGTAG "Adapter::discoverableTimerExpired\n");
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  bt_property_t prop;
  bt_scan_mode_t scan_mode = BT_SCAN_MODE_CONNECTABLE;
  prop.type = BT_PROPERTY_ADAPTER_SCAN_MODE;
  prop.val = &scan_mode;
  prop.len = sizeof(bt_scan_mode_t);
  pAdapter->m_btIF->set_adapter_property(&prop);

  return 0;
}

char const *Adapter::getAdapterObjectPath() { return sObjPath; }

void Adapter::flushDeviceList(bool bAll) {
  ADK_LOG_NOTICE(LOGTAG "Adapter::flushDeviceList\n");
  auto iter = m_devices.begin();
  while (iter != m_devices.end()) {
    Device *dev = iter->second;
    if (bAll || !(dev->isACLConnected() || dev->getIsBonded())) {
      iter = m_devices.erase(iter);
      delete dev;
    } else {
      iter++;
    }
  }
}

int32_t Adapter::createBonding(bt_bdaddr_t *bdAddr, bt_device_type_t transport) {
  // Check if paring is in progress
  if (m_isPairing) {
    ADK_LOG_NOTICE(LOGTAG
                   "Pairing not allowed as another pairing is in progress\n");
    return BT_STATUS_BUSY;
  }
  int ret = m_btIF->create_bond(bdAddr, transport);
  ADK_LOG_NOTICE(LOGTAG "btIF->create_bond() = %d", ret);
  return ret;
}

int32_t Adapter::cancelBonding(bt_bdaddr_t *bdAddr) {
  int ret = 0;
  if (m_isPairing) {
    ret = m_btIF->cancel_bond(bdAddr);
  }
  return ret;
}

int32_t Adapter::setPinReply(bt_bdaddr_t *bdAddr, uint8_t bAccept,
                             uint8_t pinLen, bt_pin_code_t *pin) {
  return m_btIF->pin_reply(bdAddr, bAccept, pinLen, pin);
}

int32_t Adapter::sspReply(bt_bdaddr_t *bdAddr, bt_ssp_variant_t pairingVariant,
                          uint8_t bAccept, uint32_t passKey) {
  return m_btIF->ssp_reply(bdAddr, pairingVariant, bAccept, passKey);
}

int32_t Adapter::removeBond(bt_bdaddr_t *bdAddr) {
  return m_btIF->remove_bond(bdAddr);
}

ProfileService *Adapter::findConnectableService(const std::string &uuid) {
  // Convert UUID string to all lower-case hex before comparison
  std::string lower_case_uuid = uuid;
  for (auto &c : lower_case_uuid) {
    c = tolower(c);
  }
  // Look for UUID in profile service map
  const auto &serviceIter = m_services.find(lower_case_uuid);
  if (serviceIter != m_services.end()) {
    return serviceIter->second;
  }
  return nullptr;
}

void Adapter::probeDeviceServices(Device *dev) {
  std::set<std::string> uuids = dev->getSupportedUUIDs();

  // When target is acting as Sink it plays the AVRCP CT role, register
  // the bluez control interfaces required to control the remote device.
  if (uuids.find(A2dpSinkService::REMOTE_UUID) != uuids.end()) {
    ADK_LOG_NOTICE(LOGTAG " Found a UUID which supports the Audio control\n");
    // Register the Control object interface for the remote device
    AvrcpCTService *pAvrcp = findConnectableService<AvrcpCTService>();
    if (pAvrcp) {
      pAvrcp->registerControlObjectForDevice(dev);
    }
    // Register the MediaPlayer object interface for the remote device
    dev->createDbusMediaPlayerObject();
  }
  // Device is now fully bonded. If transport has been established
  // finalize configuration between remote device and the end point.
  auto transport = dev->getTransport();
  if (transport){
    if (!m_mediaManager->setConfiguration(transport)) {
      ADK_LOG_ERROR(LOGTAG "- setConfiguration failed\n");
      // Disconnect the BT ACL link as the MediaTransport interface isn't ready
      transport->triggerTransportRelease();
    }
  }
}

void Adapter::onDeviceRemoval(Device *dev) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  std::set<std::string> uuids = dev->getSupportedUUIDs();
  if ((uuids.find(AvrcpCTService::REMOTE_UUID) != uuids.end()) ||
      (uuids.find(A2dpSinkService::REMOTE_UUID) != uuids.end())) {
    // unregister the Control object interface for the device when its getting
    // removed
    AvrcpCTService *pAvrcp = findConnectableService<AvrcpCTService>();
    if (pAvrcp) {
      pAvrcp->unregisterControlObjectForDevice(dev);
    }
  }
}

int Adapter::disconnectProfiles(Device *dev, sd_bus_message *m,
                                const std::string &uuid,
                                sd_bus_error *ret_error) {
  if (!m_powered) {
    ADK_LOG_NOTICE(LOGTAG "disconnectProfiles not ready \n");
    return bt_error_not_ready(ret_error);
  }
  ADK_LOG_DEBUG(LOGTAG "%s\n", __func__);
  int bSent = false;
  if (!uuid.empty()) {
    ProfileService *pServ = findConnectableService(uuid);
    if ((pServ) &&
        (pServ->serviceState() == ProfileService::State::kRunning)) {
      bt_bdaddr_t devAddr = dev->getDeviceAddr();
      pServ->disconnect(&devAddr);
      // Add a reference to the msg, so it can be returned upon connection
      // completion along with timeout handler setup
      dev->setDisconnectReqTimer(m);
      return 1;  // indicate success to sd-bus, reply will come later
    }
    ADK_LOG_NOTICE(LOGTAG
                   " No Matching Profile with connected state available\n");
    return bt_error_not_available(
        ret_error);  // there is no connectable service for the remote uuid
  } else {
    std::set<std::string> connUuids = dev->getSupportedUUIDs();
    for (const auto &uuid : connUuids) {
      ProfileService *pServ = findConnectableService(uuid);
      if (pServ &&
          (pServ->serviceState() == ProfileService::State::kRunning)) {
        bt_bdaddr_t devAddr = dev->getDeviceAddr();
        pServ->disconnect(&devAddr);
        bSent = true;
      }
    }
  }
  if (!bSent) {
    ADK_LOG_ERROR(LOGTAG " No profile services to disconnect\n");
    return bt_error_not_available(ret_error);
  }
  // Add a reference to the msg, so it can be returned upon disconnection
  // completion along with timeout handler setup
  dev->setDisconnectReqTimer(m);
  // sd_bus seems to expect 1 to indicate success,
  // (note that 0 causes org.freedesktop.DBus.Error.UnknownMethod)
  return 1;
}

int Adapter::connectProfiles(Device *dev, sd_bus_message *m,
                             const std::string &uuid, sd_bus_error *ret_error) {
  if (!m_powered) {
    ADK_LOG_NOTICE(LOGTAG "connectProfiles not ready \n");
    return bt_error_not_ready(ret_error);
  }
  ADK_LOG_DEBUG(LOGTAG "%s\n", __func__);
  if (!uuid.empty()) {
    // check if the connect request is for specific uuid and the service
    // availabilty before connection initiation
    ProfileService *pServ = findConnectableService(uuid);
    if (pServ) {
      ADK_LOG_NOTICE(LOGTAG " Got a Connectable service available \n");
      if ((pServ->serviceState() == ProfileService::State::kRunning) &&
          (pServ->isConnectable())) {
        bt_bdaddr_t devAddr = dev->getDeviceAddr();
        if (pServ->connect(&devAddr)) {
          // Add a reference to the msg, so it can be returned upon connection
          // completion along with timeout handler setup
          dev->setConnectReqTimer(m);
          return 1;  // indicate success to sd-bus, reply will come later
        } else {
          // Connection of some services is deferred and only connected as part
          // of another profile/service connection (ex. avrcp connects as part
          // of a2dp connection).
          ADK_LOG_NOTICE(LOGTAG " Profile not connectable in isolation %s.\n",
                         uuid.c_str());
          return bt_error_failed(ret_error);
        }
      } else {
        ADK_LOG_NOTICE(LOGTAG " Already connected\n");
        return bt_error_already_connected(ret_error);
      }
    }
    ADK_LOG_NOTICE(LOGTAG
                   " No Matching Profile with connectable state available\n");
    // there is no connectable service for the remote uuid
    return bt_error_not_available(ret_error);
  } else {
    bool bConnectRequestSent = false;
    bool bServiceFound = false;

    // Have to connect all the profiles supported by the device and adapter
    std::set<std::string> remUuids = dev->getSupportedUUIDs();
    for (const auto &uuid : remUuids) {
      ProfileService *pServ = findConnectableService(uuid);
      if (pServ) {
        bServiceFound = true;  // at least one service match was found
        ADK_LOG_DEBUG(LOGTAG " Found service for uuid %s", uuid.c_str());
        if ((pServ->serviceState() == ProfileService::State::kRunning) &&
            (pServ->isConnectable())) {
          bt_bdaddr_t devAddr = dev->getDeviceAddr();
          if (pServ->connect(&devAddr)) {
            // Add a reference to the msg, so it can be returned upon connection
            // completion along with timeout handler setup
            dev->setConnectReqTimer(m);
            bConnectRequestSent = true;
          }
        }
      }
    }

    if (!bConnectRequestSent) {
      if (bServiceFound) {
        ADK_LOG_NOTICE(LOGTAG " Already connected\n");
        return bt_error_already_connected(ret_error);
      } else {
        ADK_LOG_NOTICE(LOGTAG " No connectable profile services available\n");
        return bt_error_failed(ret_error);
      }
    }
    // sd_bus seems to expect 1 to indicate success,
    // (note that 0 causes org.freedesktop.DBus.Error.UnknownMethod)
    return 1;
  }
}

bool Adapter::connectHidProfile(Device *pDev) {
  ADK_LOG_DEBUG(LOGTAG "%s\n", __func__);
  if (!m_powered) {
    ADK_LOG_NOTICE(LOGTAG "connectHidProfile not ready \n");
    return false;
  }

  bool bConnectRequestSent = false;
  bool bServiceFound = false;

  std::string hid_uuid(HidService::REMOTE_UUID);
  ProfileService *pServ = findConnectableService(hid_uuid);
  // If the HID service is connectectable then try to connect
  if (pServ) {
    bServiceFound = true;  // at least one service match was found
    ADK_LOG_DEBUG(LOGTAG " Found service for uuid %s", hid_uuid.c_str());
    if ((pServ->serviceState() == ProfileService::State::kRunning) && (pServ->isConnectable())) {
      bt_bdaddr_t devAddr = pDev->getDeviceAddr();
      // Connect to the HID profile service
      if (pServ->connect(&devAddr)) {
        bConnectRequestSent = true;
      }
    }
  }
  if (!bConnectRequestSent) {
    if (bServiceFound) {
      ADK_LOG_NOTICE(LOGTAG " Already connected\n");
      return false;
    } else {
      ADK_LOG_NOTICE(LOGTAG " No connectable profile services available\n");
      return false;
    }
  }
  return true;
}

int Adapter::sd_startDiscovery(sd_bus_message *m, void *userdata,
                               sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  if (pAdapter->m_discovering) {
    return bt_error_in_progress(ret_error);
  }

  if (!pAdapter->m_powered) {
    ADK_LOG_NOTICE(LOGTAG " sd_startDiscovery not ready \n");
    return bt_error_not_ready(ret_error);
  }

  // Clear just the discovered device list
  pAdapter->flushDeviceList(false);
  if (pAdapter->m_btIF->start_discovery() != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG "Start Discovery failed: \n");
    return bt_error_failed(ret_error);
  }

  pAdapter->m_discovering = true;
  ADK_LOG_NOTICE(LOGTAG " Start Discovery successful \n");

  return sd_bus_reply_method_return(m, nullptr);
}
int Adapter::sd_stopDiscovery(sd_bus_message *m, void *userdata,
                              sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  Adapter *pAdapter = static_cast<Adapter *>(userdata);

  if ((!pAdapter->m_powered) || (!pAdapter->m_discovering)) {
    ADK_LOG_NOTICE(LOGTAG " sd_stopDiscovery not ready \n");
    return bt_error_not_ready(ret_error);
  }

  if (pAdapter->m_btIF->cancel_discovery() != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG "Stop Discovery failed: \n");
    return bt_error_failed(ret_error);
  }

  pAdapter->m_discovering = false;
  ADK_LOG_NOTICE(LOGTAG " Stop Discovery successful \n");

  return sd_bus_reply_method_return(m, nullptr);
}

int Adapter::sd_setAFHMap(sd_bus_message *m, void *userdata,
                             sd_bus_error *ret_error) {

  ADK_LOG_DEBUG("BTAdapter::sd_setAFHMap");
#define MAP_SIZE 10
#define VSC_OCF (0x003F | (0x03 << 10))
  const char *afhMap;
  std::string token;
  uint8_t final_afhMap[MAP_SIZE];
  int index = 0;
  bool status = true;
  Adapter *pAdapter = static_cast<Adapter *>(userdata);

  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &afhMap);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }

  // Convert the bad channel byte stream to integer
  std::stringstream ss(afhMap);

  while (getline(ss, token, ',')) {
      if (index >= MAP_SIZE) {
        ADK_LOG_DEBUG("BTAdapter::Ignore AFH mapping - invalid map received, greater than expected size of %d", MAP_SIZE);
        status = false;
        break;
      }
      final_afhMap[index] = std::stoi (token,nullptr,0);
      index++;
  }

  if (status) {
    pAdapter->m_btVendorInterface->hci_cmd_send(VSC_OCF, final_afhMap, MAP_SIZE);
  }
  return sd_bus_reply_method_return(m, nullptr);
}

int Adapter::sd_removeDevice(sd_bus_message *m, void *userdata,
                             sd_bus_error *ret_error) {
  const char *path;
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  Device *pDev = nullptr;

  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_OBJECT_PATH, &path);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }

  for (const auto &iter : pAdapter->m_devices) {
    if (iter.second->getDeviceObjectPath() == path) {
      // Device there in list
      pDev = iter.second;
      break;
    }
  }

  if (!pDev) {
    return bt_error_does_not_exist(ret_error);
  }

  // If bonded, initiate the clear bond request and complete DBus command
  // on callback
  if (pDev->removeBonding(m) != BT_STATUS_SUCCESS) {
    // If it is not a bonded device remove from list and let the Device
    // destructor to deal with the transient states (bonding & connecting)
    // cleanup
    pAdapter->m_devices.erase(pDev->getDeviceAddr());
    delete pDev;
  } else {
    pDev->setRemoveDeviceReqTimer(m);
  }
  return sd_bus_reply_method_return(m, nullptr);
}

int Adapter::sd_setDiscoveryFilter(sd_bus_message *UNUSED(m), void *UNUSED(userdata),
                                   sd_bus_error *ret_error) {
  return bt_error_not_implemented(ret_error);
}

int Adapter::sd_getBtAddress(sd_bus *UNUSED(bus), const char *UNUSED(path),
                             const char *UNUSED(interface), const char *UNUSED(property),
                             sd_bus_message *reply, void *userdata,
                             sd_bus_error *UNUSED(ret_error)) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING,
                                     pAdapter->m_bdAddr.ToString().c_str());
}

int Adapter::sd_getBtName(sd_bus *UNUSED(bus), const char *UNUSED(path), const char *UNUSED(interface),
                          const char *UNUSED(property), sd_bus_message *reply,
                          void *userdata, sd_bus_error *UNUSED(ret_error)) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING,
                                     pAdapter->m_bdName.c_str());
}

int Adapter::sd_setBtName(sd_bus *UNUSED(bus), const char *UNUSED(path), const char *UNUSED(interface),
                          const char *UNUSED(property), sd_bus_message *value,
                          void *userdata, sd_bus_error *UNUSED(ret_error)) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  const char *name;

  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_STRING, &name);
  if (res < 0) {
    return res;
  }

  if (!pAdapter->m_powered) {
    // Set this once bt is powered on
    pAdapter->m_bdName = name;
    return 0;
  }

  bt_property_t prop{BT_PROPERTY_BDNAME, static_cast<int>(strlen(name)),
                     const_cast<char *>(name)};
  pAdapter->m_btIF->set_adapter_property(&prop);

  return 0;
}

int Adapter::sd_getBtDevClass(sd_bus *UNUSED(bus), const char *UNUSED(path),
                              const char *UNUSED(interface), const char *UNUSED(property),
                              sd_bus_message *reply, void *userdata,
                              sd_bus_error *UNUSED(ret_error)) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_UINT32>::value,
                               pAdapter->m_powered ? pAdapter->m_bdCOD : 0);
}

int Adapter::sd_getBtPowered(sd_bus *UNUSED(bus), const char *UNUSED(path),
                             const char *UNUSED(interface), const char *UNUSED(property),
                             sd_bus_message *reply, void *userdata,
                             sd_bus_error *UNUSED(ret_error)) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value,
                               pAdapter->m_powered);
}

int Adapter::sd_getBtSplitMode(sd_bus *UNUSED(bus), const char *UNUSED(path),
                               const char *UNUSED(interface), const char *UNUSED(property),
                               sd_bus_message *reply, void *userdata,
                               sd_bus_error *UNUSED(ret_error)) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value,
                               pAdapter->m_splitmode);
}

int Adapter::sd_getBtDiscoverable(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                  const char *UNUSED(interface), const char *UNUSED(property),
                                  sd_bus_message *reply, void *userdata,
                                  sd_bus_error *UNUSED(ret_error)) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value,
                               pAdapter->m_discoverable);
}

int Adapter::sd_getBtDiscoverableTimeout(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                         const char *UNUSED(interface),
                                         const char *UNUSED(property),
                                         sd_bus_message *reply, void *userdata,
                                         sd_bus_error *UNUSED(ret_error)) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_UINT32>::value,
                               pAdapter->m_discoverableTimeout);
}

int Adapter::sd_getBtDiscovering(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value,
                               pAdapter->m_discovering);
}

int Adapter::sd_getModalias(sd_bus *UNUSED(bus), const char *UNUSED(path),
                            const char *UNUSED(interface), const char *UNUSED(property),
                            sd_bus_message *UNUSED(reply), void *UNUSED(userdata),
                            sd_bus_error *ret_error) {
  return bt_error_not_implemented(ret_error);
}

int Adapter::sd_getBtUUIDs(sd_bus *UNUSED(bus), const char *UNUSED(path), const char *UNUSED(interface),
                           const char *UNUSED(property), sd_bus_message *reply,
                           void *userdata, sd_bus_error *UNUSED(ret_error)) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  int res = sd_bus_message_open_container(reply, SD_BUS_TYPE_ARRAY,
                                          TYPE_TO_STR<SD_BUS_TYPE_STRING>::value);
  if (res < 0) {
    return res;
  }

  if (pAdapter->m_powered) {
    for (const auto &uuid : pAdapter->m_bdUUIDs) {
      sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING, uuid.c_str());
    }
  }

  res = sd_bus_message_close_container(reply);
  if (res < 0) {
    return res;
  }

  return 0;
}

int Adapter::sd_setBtSplitMode(sd_bus *UNUSED(bus), const char *UNUSED(path),
                               const char *UNUSED(interface), const char *UNUSED(property),
                               sd_bus_message *value, void *userdata,
                               sd_bus_error *ret_error) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);

  if (pAdapter->m_powered) {
    // Splitmode can only be set if Adapter is not Powered
    ADK_LOG_NOTICE(LOGTAG "Request failed due to Adapter already being on\n");
    return bt_error_failed(ret_error);
  }

  int splitmode;

  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_BOOLEAN, &splitmode);
  if (res < 0) {
    return res;
  } else {
    pAdapter->m_splitmode = splitmode;
    sd_bus_emit_properties_changed(g_sdbus, sObjPath, ADAPTER_INTERFACE,
                                   "SplitMode", nullptr);
    return 0;
  }
}

int Adapter::sd_setBtPowered(sd_bus *UNUSED(bus), const char *UNUSED(path),
                             const char *UNUSED(interface), const char *UNUSED(property),
                             sd_bus_message *value, void *userdata,
                             sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  int enable;

  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_BOOLEAN, &enable);
  if (res < 0) {
    return res;
  }
  ADK_LOG_NOTICE(LOGTAG "state(enable) arg = %d", enable);

  if (enable == pAdapter->m_powered) {
    return 0;
  }

  if (pAdapter->m_state > BT_ADAPTER_ON) {
    return bt_error_failed(ret_error, "Transitioning");
  }
  if (enable) {
    // Initialise stack interface (will already have been done if this is the
    // first power on, but required again after a power off)
    res = pAdapter->m_btIF->init(&sBluetoothCallbacks);
    if ((res != BT_STATUS_SUCCESS) && (res != BT_STATUS_DONE)) {
      ADK_LOG_ERROR(LOGTAG "Failed to init BT interface: %d\n", res);
      return bt_error_failed(ret_error);
    }

    // Create and instance for each of the supported services
    ProfileService *pService;
    if (pAdapter->m_splitmode){
      pService = new A2dpSinkServiceSplit(pAdapter);
    } else {
      pService = new A2dpSinkServiceNonSplit(pAdapter);
    }
    pAdapter->m_services.insert({pService->remoteUUID(), pService});

    pService = new AvrcpCTService(pAdapter);
    pAdapter->m_services.insert({pService->remoteUUID(), pService});

    if (pAdapter->m_splitmode){
      pService = new A2dpSourceServiceSplit(pAdapter);
    } else {
      pService = new A2dpSourceServiceNonSplit(pAdapter);
    }
    pAdapter->m_services.insert({pService->remoteUUID(), pService});
    pService = new AvrcpTGService(pAdapter);
    pAdapter->m_services.insert({pService->remoteUUID(), pService});

    // Add the HID service to the list of known profiles
    pService = new HidService(pAdapter);
    pAdapter->m_services.insert({pService->remoteUUID(), pService});

    pAdapter->m_mediaManager->updateAdapterServices(
                   pAdapter->findConnectableService<A2dpSinkService>(),
                   pAdapter->findConnectableService<A2dpSourceService>(),
                   pAdapter->findConnectableService<AvrcpTGService>(),
                   pAdapter->findConnectableService<HidService>());

    // Enable the Profile Services supported by the adapter
    for (const auto &serviceIter : pAdapter->m_services) {
      serviceIter.second->enableService();
    }

    // Add and Enable the Ble Service
    pAdapter->m_bleService = new BleService(pAdapter);
    pAdapter->m_bleService->enableService();
    // Instantiate GattService Library
    pAdapter->m_gattLib = gatt::GattLibService::getInstance(pAdapter->m_bleService);

    if (pAdapter->m_btIF->enable(false) == BT_STATUS_SUCCESS) {
      int res = sd_event_add_time(g_eventLoop, &pAdapter->m_enableTimer,
                                  CLOCK_MONOTONIC,
                                  get_timer_delay(ENABLE_TIMEOUT_DELAY), 0,
                                  Adapter::enableTimerExpired, pAdapter);
      if (res < 0) {
        ADK_LOG_WARNING(LOGTAG "Failed to create enable timer: %d - %s\n", -res,
                        strerror(-res));
        return bt_error_failed(ret_error);
      }
      pAdapter->m_state = BT_ADAPTER_TURNING_ON;
    } else {
      ADK_LOG_ERROR(LOGTAG "Adapter::SetPowered ON Failed\n");
      return bt_error_failed(ret_error);
    }
  } else {
    // Disable services associated with the adapter
    for (const auto &serviceIter : pAdapter->m_services) {
      serviceIter.second->disableService();
    }

    pAdapter->m_bleService->disableService();

    // Disable bluetooth and wait for callback before cleaning up further
    if (pAdapter->m_btIF->disable() == BT_STATUS_SUCCESS) {
      int res = sd_event_add_time(g_eventLoop, &pAdapter->m_disableTimer,
                                  CLOCK_MONOTONIC,
                                  get_timer_delay(DISABLE_TIMEOUT_DELAY), 0,
                                  Adapter::disableTimerExpired, pAdapter);
      if (res < 0) {
        ADK_LOG_WARNING(LOGTAG "Failed to create disable timer: %d - %s\n",
                        -res, strerror(-res));
        return bt_error_failed(ret_error);
      }
      pAdapter->m_state = BT_ADAPTER_TURNING_OFF;
    } else {
      ADK_LOG_ERROR(LOGTAG "Adapter::SetPowered OFF Failed\n");
      return bt_error_failed(ret_error);
    }
  }

  return 0;
}

int Adapter::sd_setBtDiscoverable(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                  const char *UNUSED(interface), const char *UNUSED(property),
                                  sd_bus_message *value, void *userdata,
                                  sd_bus_error *ret_error) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  int enable;
  bt_property_t prop;
  bt_scan_mode_t scan_mode = BT_SCAN_MODE_NONE;

  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_BOOLEAN, &enable);
  if (res < 0) {
    return res;
  }

  if (!pAdapter->m_powered) {
    ADK_LOG_NOTICE(LOGTAG "Request failed due to Adapter being off\n");
    return bt_error_failed(ret_error);
  }

  if (pAdapter->m_discoverable == enable) {
    return 0;
  }
  scan_mode =
      enable ? BT_SCAN_MODE_CONNECTABLE_DISCOVERABLE : BT_SCAN_MODE_CONNECTABLE;
  prop.type = BT_PROPERTY_ADAPTER_SCAN_MODE;
  prop.val = &scan_mode;
  prop.len = sizeof(bt_scan_mode_t);
  pAdapter->m_btIF->set_adapter_property(&prop);

  return 0;
}

int Adapter::sd_setBtDiscoverableTimeout(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                         const char *UNUSED(interface),
                                         const char *UNUSED(property),
                                         sd_bus_message *value, void *userdata,
                                         sd_bus_error *ret_error) {
  Adapter *pAdapter = static_cast<Adapter *>(userdata);
  uint32_t ivalue = 0;

  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_UINT32, &ivalue);
  if (res < 0) {
    return res;
  }

  if (!pAdapter->m_powered) {
    ADK_LOG_NOTICE(LOGTAG "Request failed due to Adapter being off\n");
    return bt_error_failed(ret_error);
  }

  pAdapter->m_discoverableTimeout = ivalue;

  sd_bus_emit_properties_changed(g_sdbus, sObjPath, ADAPTER_INTERFACE,
                                 "DiscoverableTimeout", "PairableTimeout",
                                 nullptr);
  return 0;
}
