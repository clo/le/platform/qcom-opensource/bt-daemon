/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BT_ADAPTER_HPP
#define BT_ADAPTER_HPP

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <hardware/bluetooth.h>
#include <hardware/bt_av_vendor.h>
#include <hardware/bt_sock.h>
#include <hardware/vendor.h>
#include <systemdq/sd-bus.h>

#include <memory>
#include <set>
#include <string>

#include "Device.hpp"
#include "MediaManager.hpp"

#include "BleService.hpp"
#include "GattLibService.hpp"

/**
 * @file Adapter.hpp
 * @brief Adapter header file
 */

#define ENABLE_TIMEOUT_DELAY (12000 * 1000ull)       // 12s (in us)
#define DISABLE_TIMEOUT_DELAY (8000 * 1000ull)       // 8s (in us)
#define REMOVEDEVICE_TIMEOUT_DELAY (5000 * 1000ull)  // 5s (in us)

// Maintain the order for the states as 0 off and 1 is powered
// any states greater than 1 are processing states
typedef enum {
  BT_ADAPTER_OFF,
  BT_ADAPTER_ON,
  BT_ADAPTER_TURNING_ON,
  BT_ADAPTER_TURNING_OFF
} BtAdapterState;

/**
 * @class Adapter
 *
 * @brief Adapter Class
 */
class Adapter {
 private:
  bool m_stopping;
  const bt_interface_t
      *m_btIF;  // structure object for standard Bluetooth DM interface
  const btvendor_interface_t
      *m_btVendorInterface;  // structure object for Vendor interface

  // Adapter Propeties
  bt_bdaddr_t m_bdAddr;             // Address of Bluetooth Adapter device
  std::string m_bdName;             // Name currently in use for the Adapter
  std::vector<uint8_t> m_leFeat;    // LE Features data

  uint32_t m_bdCOD;                 // Adapter's Class of Device
  std::set<std::string> m_bdUUIDs;  // UUID's supported by Adapter

  bool m_discovering;
  bool m_splitmode;
  bool m_powered;
  bool m_discoverable;

  BtAdapterState m_state;

  uint32_t m_discoverableTimeout;

  sd_event_source *m_enableTimer;
  sd_event_source *m_disableTimer;
  sd_event_source *m_discoverableTimer;

  std::map<bt_bdaddr_t, Device *> m_devices;
  std::map<std::string, ProfileService *>
      m_services;  // key: remote UUID for the service

  // BLE Service and GattService library handles
  BleService *m_bleService;
  gatt::GattLibService *m_gattLib;

  bool m_isPairing;

  sd_bus_slot *m_sdbusSlot;

 public:
  MediaManager *m_mediaManager;

 public:
  Adapter(const bt_interface_t *bt_interface, MediaManager *mediaManager);
  ~Adapter();

  // Initialises the Adapter object with GAP interfaces
  bool init();

  // Checks the current BT state, If state is BT_STATE_ON it will return true
  // else it will return false
  bool isEnabled() { return m_powered; }

  static int enableTimerExpired(sd_event_source *s, uint64_t usec,
                                void *userdata);
  static int disableTimerExpired(sd_event_source *s, uint64_t usec,
                                 void *userdata);
  static int discoverableTimerExpired(sd_event_source *s, uint64_t usec,
                                      void *userdata);

  // BT Interface Callback Functions
  static void adapterStateChangeCb(bt_state_t state);
  static void adapterPropertiesCb(bt_status_t status, int numProperties,
                                  bt_property_t *properties);
  static void remoteDevicePropertiesCb(bt_status_t status, bt_bdaddr_t *bdAddr,
                                       int numProperties,
                                       bt_property_t *properties);
  static void deviceFoundCb(int numProperties, bt_property_t *properties);
  static void bondStateChangedCb(bt_status_t status, bt_bdaddr_t *bdAddr,
                                 bt_bond_state_t state);
  static void aclStateChangedWithReasonCb(bt_status_t status,
                                          bt_bdaddr_t *remote_bd_addr,
                                          bt_acl_state_t state,
                                          uint8_t reason,
                                          uint8_t transport_type);
  static void discoveryStateChangedCb(bt_discovery_state_t state);
  static void pinRequestCb(bt_bdaddr_t *bdAddr, bt_bdname_t *bdName,
                           uint32_t cod, bool min16Digit);
  static void sspRequestCb(bt_bdaddr_t *bdAddr, bt_bdname_t *bdName,
                           uint32_t cod, bt_ssp_variant_t pairingVariant,
                           uint32_t passKey);
  static void threadEventCb(bt_cb_thread_evt event);
  static void dutModeRecvCb(uint16_t opcode, uint8_t *buf, uint8_t len);
  static void leTestModeRecvCb(bt_status_t status, uint16_t pktCount);
  static void energyInfoCb(bt_activity_energy_info *energy_info,
                           bt_uid_traffic_t *uid_data);

  // Vendor specific BT Interface callbacks
  static void bredrCleanupCb(bool status);

  static void vendor_hci_event_recv_cb(uint8_t event_code, uint8_t *buf, uint8_t len);

  static void updateSnooplogStatusCb(bool status);

  static int sd_startDiscovery(sd_bus_message *m, void *userdata,
                               sd_bus_error *ret_error);
  static int sd_stopDiscovery(sd_bus_message *m, void *userdata,
                              sd_bus_error *ret_error);
  static int sd_removeDevice(sd_bus_message *m, void *userdata,
                             sd_bus_error *ret_error);
  static int sd_setAFHMap(sd_bus_message *m, void *userdata,
                             sd_bus_error *ret_error);
  static int sd_setDiscoveryFilter(sd_bus_message *m, void *userdata,
                                   sd_bus_error *ret_error);
  static int sd_getBtAddress(sd_bus *bus, const char *path,
                             const char *interface, const char *property,
                             sd_bus_message *reply, void *userdata,
                             sd_bus_error *ret_error);
  static int sd_getBtName(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *reply,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_getBtDevClass(sd_bus *bus, const char *path,
                              const char *interface, const char *property,
                              sd_bus_message *reply, void *userdata,
                              sd_bus_error *ret_error);
  static int sd_getBtSplitMode(sd_bus *bus, const char *path,
                             const char *interface, const char *property,
                             sd_bus_message *reply, void *userdata,
                             sd_bus_error *ret_error);
  static int sd_getBtPowered(sd_bus *bus, const char *path,
                             const char *interface, const char *property,
                             sd_bus_message *reply, void *userdata,
                             sd_bus_error *ret_error);
  static int sd_getBtDiscoverable(sd_bus *bus, const char *path,
                                  const char *interface, const char *property,
                                  sd_bus_message *reply, void *userdata,
                                  sd_bus_error *ret_error);
  static int sd_getBtDiscoverableTimeout(sd_bus *bus, const char *path,
                                         const char *interface,
                                         const char *property,
                                         sd_bus_message *reply, void *userdata,
                                         sd_bus_error *ret_error);
  static int sd_getBtDiscovering(sd_bus *bus, const char *path,
                                 const char *interface, const char *property,
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *ret_error);
  static int sd_getBtUUIDs(sd_bus *bus, const char *path, const char *interface,
                           const char *property, sd_bus_message *reply,
                           void *userdata, sd_bus_error *ret_error);
  static int sd_setBtName(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *value,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_setBtSplitMode(sd_bus *bus, const char *path,
                             const char *interface, const char *property,
                             sd_bus_message *value, void *userdata,
                             sd_bus_error *ret_error);
  static int sd_setBtPowered(sd_bus *bus, const char *path,
                             const char *interface, const char *property,
                             sd_bus_message *value, void *userdata,
                             sd_bus_error *ret_error);
  static int sd_setBtDiscoverable(sd_bus *bus, const char *path,
                                  const char *interface, const char *property,
                                  sd_bus_message *value, void *userdata,
                                  sd_bus_error *ret_error);
  static int sd_setBtDiscoverableTimeout(sd_bus *bus, const char *path,
                                         const char *interface,
                                         const char *property,
                                         sd_bus_message *value, void *userdata,
                                         sd_bus_error *ret_error);
  static int sd_getModalias(sd_bus *bus, const char *path,
                            const char *interface, const char *property,
                            sd_bus_message *reply, void *userdata,
                            sd_bus_error *ret_error);

  char const *getAdapterObjectPath();
  void flushDeviceList(bool bAll);

  // Transport is by default BR/EDR.
  int32_t createBonding(bt_bdaddr_t *bdAddr, bt_device_type_t transport = BT_DEVICE_DEVTYPE_BREDR);
  int32_t cancelBonding(bt_bdaddr_t *bdAddr);
  int32_t setPinReply(bt_bdaddr_t *bdAddr, uint8_t bAccept, uint8_t pinLen,
                      bt_pin_code_t *pin);
  int32_t sspReply(bt_bdaddr_t *bdAddr, bt_ssp_variant_t pairingVariant,
                   uint8_t bAccept, uint32_t passKey);
  int32_t removeBond(bt_bdaddr_t *bdAddr);

  Device *getDevice(const bt_bdaddr_t &bdAddr);
  Device *getDeviceFromPath(std::string device_object_path);

  const bt_interface_t *getBtInterface() { return m_btIF; };
  void probeDeviceServices(Device *);
  void onDeviceRemoval(Device *);
  ProfileService *findConnectableService(const std::string &uuid);
  template <class T>
  T *findConnectableService() {
    return static_cast<T *>(findConnectableService(T::REMOTE_UUID));
  }
  int connectProfiles(Device *dev, sd_bus_message *m, const std::string &uuid,
                      sd_bus_error *ret_error);
  bool connectHidProfile(Device * pDev);
  int disconnectProfiles(Device *dev, sd_bus_message *m,
                         const std::string &uuid, sd_bus_error *ret_error);
};
#endif  // BT_ADAPTER_HPP
