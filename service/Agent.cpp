/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Agent.hpp"

#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <algorithm>
#include <iostream>

#include "Common.hpp"
#include "Error.hpp"
#include "Sdbus.hpp"

#define AGENT_MANAGER_INTERFACE "org.bluez.AgentManager1"
#define AGENT_INTERFACE "org.bluez.Agent1"
#define REQUEST_TIMEOUT (60 * 1000) /* 60 seconds */

static AgentManager *gAgentManager = nullptr;

const char *const dev_path = "/org/bluez/hci0";
#define LOGTAG "AGENT "

struct AgentRequest {
  agent_request_type_t type;
  std::weak_ptr<Agent> agent;
  sd_bus_slot *slot;
  void *cb;
  void *user_data;

  ~AgentRequest();

  template <typename T, typename... Args>
  void callback(Args... args) {
    reinterpret_cast<T>(cb)(args...);
    cb = nullptr;
  }
};

const char *to_string(AgentCapability value) {
  switch (value) {
    case AgentCapability::INVALID:
      return nullptr;
    case AgentCapability::DISPLAYONLY:
      return "DisplayOnly";
    case AgentCapability::DISPLAYYESNO:
      return "DisplayYesNo";
    case AgentCapability::KEYBOARDONLY:
      return "KeyboardOnly";
    case AgentCapability::NOINPUTNOOUTPUT:
      return "NoInputNoOutput";
    case AgentCapability::KEYBOARDDISPLAY:
      return "KeyboardDisplay";
  }
  return nullptr;
}
template <>
AgentCapability from_string<AgentCapability>(const std::string &str) {
  if (str.empty()) {
    return AgentCapability::DISPLAYYESNO;
  }  // per BlueZ's AgentManager1 specs
  else if (str == to_string(AgentCapability::DISPLAYONLY)) {
    return AgentCapability::DISPLAYONLY;
  } else if (str == to_string(AgentCapability::DISPLAYYESNO)) {
    return AgentCapability::DISPLAYYESNO;
  } else if (str == to_string(AgentCapability::KEYBOARDONLY)) {
    return AgentCapability::KEYBOARDONLY;
  } else if (str == to_string(AgentCapability::NOINPUTNOOUTPUT)) {
    return AgentCapability::NOINPUTNOOUTPUT;
  } else if (str == to_string(AgentCapability::KEYBOARDDISPLAY)) {
    return AgentCapability::KEYBOARDDISPLAY;
  } else {
    return AgentCapability::INVALID;
  }
}

static int sd_agentDisconnect(sd_bus_track *UNUSED(track), void *userdata) {
  std::shared_ptr<Agent> agent =
      AgentManager::getAgent(static_cast<Agent *>(userdata));
  if (!agent) {
    return 0;
  }

  ADK_LOG_NOTICE(LOGTAG "Agent %s disconnected", agent->owner.c_str());
  if (agent->sd_watch) {
    sd_bus_track_unref(agent->sd_watch);
    agent->sd_watch = nullptr;
  }

  // Clear the agent's reference from Agent Manager
  gAgentManager->removeDefaultAgent(agent);
  gAgentManager->m_agentList.erase(agent->owner);

  return 1;
}

AgentRequest::~AgentRequest() {
  if (slot) {
    sd_bus_slot_unref(slot);
    slot = nullptr;
  }

  if (cb) {
    switch (type) {
      case AGENT_REQUEST_PINCODE:
        callback<agentPincodeCb>(bt_error_canceled(),
                                 static_cast<const char *>(nullptr), user_data);
        break;
      case AGENT_REQUEST_PASSKEY:
        callback<agentPasskeyCb>(bt_error_canceled(), 0, user_data);
        break;
      default:
        callback<agentCb>(bt_error_canceled(), user_data);
        break;
    }
  }
}

Agent::~Agent() {
  ADK_LOG_DEBUG(LOGTAG "agent %s", owner.c_str());

  if (sd_watch) {
    sd_bus_track_unref(sd_watch);
    sd_watch = nullptr;
  }

  if (request) {
    sendCancelRequest();
    request.reset();
  }

  agentRelease();

  request = nullptr;
}

static std::shared_ptr<Agent> agentCreate(const char *name, const char *path,
                                          AgentCapability capability) {
  std::shared_ptr<Agent> agent = std::make_shared<Agent>();

  agent->owner = name;
  agent->path = path;
  agent->capability = capability;

  int res = sd_bus_track_new(g_sdbus, &(agent->sd_watch), sd_agentDisconnect,
                             agent.get());
  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Failed to create tracker: %d - %s\n", -res,
                    strerror(-res));
  }
  res = sd_bus_track_add_name(agent->sd_watch, name);
  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Failed to track name: %d - %s\n", -res,
                    strerror(-res));
  }
  return agent;
}

int Agent::sendCancelRequest() {
  ADK_LOG_DEBUG(LOGTAG "Sending Cancel request to %s, %s", owner.c_str(),
                path.c_str());

  int res = sd_bus_call_method_async(g_sdbus, nullptr, owner.c_str(),
                                     path.c_str(), AGENT_INTERFACE, "Cancel",
                                     nullptr, nullptr, nullptr);
  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Failed cancel: %d - %s\n", -res, strerror(-res));
    return res;
  }

  return res;
}

void Agent::agentRelease() {
  ADK_LOG_DEBUG(LOGTAG "Releasing agent %s, %s", owner.c_str(), path.c_str());

  ADK_LOG_WARNING(LOGTAG "bus call " AGENT_INTERFACE "::Release\n");
  int res = sd_bus_call_method_async(g_sdbus, nullptr, owner.c_str(),
                                     path.c_str(), AGENT_INTERFACE, "Release",
                                     nullptr, nullptr, nullptr);
  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Failed release: %d - %s\n", -res, strerror(-res));
    return;
  }
}

int sd_pincodeReply(sd_bus_message *m, void *userdata,
                    sd_bus_error *UNUSED(ret_error)) {
  struct AgentRequest *req = (struct AgentRequest *)userdata;
  std::shared_ptr<Agent> agent = req->agent.lock();
  if (!agent) {
    return 0;
  }

  size_t len;
  char *pin;
  int res;

  const sd_bus_error *error = sd_bus_message_get_error(m);
  if (sd_bus_error_is_set(error)) {
    ADK_LOG_ERROR(LOGTAG "Agent %s replied with an error: %s, %s",
                  agent->path.c_str(), error->name, error->message);

    req->callback<agentPincodeCb>(error, static_cast<const char *>(nullptr),
                                  req->user_data);
    goto done;
  }

  res = sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &pin);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Wrong passkey reply signature: %s",
                  sd_bus_message_get_signature(m, true));

    req->callback<agentPincodeCb>(bt_error_invalid_args(),
                                  static_cast<const char *>(nullptr),
                                  req->user_data);
    goto done;
  }

  len = strlen(pin);

  if (len > 16 || len < 1) {
    ADK_LOG_ERROR(LOGTAG "Invalid PIN length (%zu) from agent", len);

    req->callback<agentPincodeCb>(bt_error_invalid_args(),
                                  static_cast<const char *>(nullptr),
                                  req->user_data);
    goto done;
  }

  req->callback<agentPincodeCb>(static_cast<const sd_bus_error *>(nullptr), pin,
                                req->user_data);

done:
  agent->request.reset();

  return 0;
}

int sd_passphraseSimpleAgentReply(sd_bus_message *m, void *userdata,
                                  sd_bus_error *UNUSED(ret_error)) {
  struct AgentRequest *req = (struct AgentRequest *)userdata;
  std::shared_ptr<Agent> agent = req->agent.lock();
  if (!agent) {
    return -1;
  }

  const sd_bus_error *error = sd_bus_message_get_error(m);
  if (sd_bus_error_is_set(error)) {
    ADK_LOG_INFO(LOGTAG "Agent error reply: %s, %s", error->name,
                 error->message);

    req->callback<agentCb>(error, req->user_data);

    if (strcmp(error->name, SD_BUS_ERROR_NO_REPLY) == 0) {
      ADK_LOG_ERROR(LOGTAG "Timed out waiting for reply from agent");
      return -1;
    }

    goto done;
  }

  req->callback<agentCb>(static_cast<const sd_bus_error *>(nullptr),
                         req->user_data);

done:
  agent->request.reset();
  return 0;
}

static int pincodeRequestNew(const std::shared_ptr<AgentRequest> &req,
                             const char *device_path) {
  std::shared_ptr<Agent> agent = req->agent.lock();
  if (!agent) {
    return -1;
  }

  ADK_LOG_WARNING(LOGTAG "bus call " AGENT_INTERFACE "::RequestPinCode\n");
  int res = sd_bus_call_method_async(g_sdbus, nullptr, agent->owner.c_str(),
                                     agent->path.c_str(), AGENT_INTERFACE,
                                     "RequestPinCode", sd_pincodeReply,
                                     req.get(), "o", device_path);
  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Failed to request pin: %d - %s\n", -res,
                    strerror(-res));
    return res;
  }

  return res;
}

static int confirmationRequestNew(const std::shared_ptr<AgentRequest> &req,
                                  const char *device_path, uint32_t passkey) {
  std::shared_ptr<Agent> agent = req->agent.lock();
  if (!agent) {
    return -1;
  }

  ADK_LOG_WARNING(LOGTAG "bus call " AGENT_INTERFACE "::RequestConfirmation\n");
  int res = sd_bus_call_method_async(
      g_sdbus, nullptr, agent->owner.c_str(), agent->path.c_str(),
      AGENT_INTERFACE, "RequestConfirmation", sd_passphraseSimpleAgentReply,
      req.get(), "ou", device_path, passkey);
  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Failed to request confirmation: %d - %s\n", -res,
                    strerror(-res));
    return res;
  }

  return res;
}

static int authorizationRequestNew(const std::shared_ptr<AgentRequest> &req,
                                   const char *UNUSED(device_path)) {
  std::shared_ptr<Agent> agent = req->agent.lock();
  if (!agent) {
    return -1;
  }

  ADK_LOG_WARNING(LOGTAG "bus call " AGENT_INTERFACE
                         "::RequestAuthorization\n");
  int res = sd_bus_call_method_async(
      g_sdbus, nullptr, agent->owner.c_str(), agent->path.c_str(),
      AGENT_INTERFACE, "RequestAuthorization", sd_passphraseSimpleAgentReply,
      req.get(), nullptr);
  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Failed request authorization: %d - %s\n", -res,
                    strerror(-res));
    return res;
  }

  return res;
}

static std::shared_ptr<AgentRequest> agentRequestNew(
    const std::shared_ptr<Agent> &agent, agent_request_type_t type, void *cb,
    void *user_data) {
  std::shared_ptr<AgentRequest> req;

  req = std::make_shared<AgentRequest>();

  req->agent = agent;
  req->type = type;
  req->cb = cb;
  req->user_data = user_data;
  return req;
}

int agentRequestPincode(const std::shared_ptr<Agent> &agent, agentPincodeCb cb,
                        void *user_data) {
  if (agent->request) {
    return -EBUSY;
  }

  std::shared_ptr<AgentRequest> req =
      agentRequestNew(agent, AGENT_REQUEST_PINCODE, (void *)cb, user_data);

  int err = pincodeRequestNew(req, dev_path);
  if (err < 0) {
    return err;
  }

  agent->request = req;

  return 0;
}

int agentRequestConfirmation(const std::shared_ptr<Agent> &agent,
                             uint32_t passkey, agentCb cb, void *user_data) {
  if (agent->request) {
    return -EBUSY;
  }

  ADK_LOG_NOTICE(
      LOGTAG
      " Calling Agent.RequestConfirmation: name=%s, path=%s, passkey=%06u\n",
      agent->owner.c_str(), agent->path.c_str(), passkey);

  std::shared_ptr<AgentRequest> req =
      agentRequestNew(agent, AGENT_REQUEST_CONFIRMATION, (void *)cb, user_data);

  int err = confirmationRequestNew(req, dev_path, passkey);
  if (err < 0) {
    return err;
  }

  agent->request = req;

  return 0;
}

int agentRequestAuthorization(const std::shared_ptr<Agent> &agent, agentCb cb,
                              void *user_data) {
  if (agent->request) return -EBUSY;

  ADK_LOG_NOTICE(LOGTAG
                 " Calling Agent.RequestAuthorization: name=%s, path=%s\n",
                 agent->owner.c_str(), agent->path.c_str());

  std::shared_ptr<AgentRequest> req = agentRequestNew(
      agent, AGENT_REQUEST_AUTHORIZATION, (void *)cb, user_data);

  int err = authorizationRequestNew(req, dev_path);
  if (err < 0) {
    return err;
  }

  agent->request = req;

  return 0;
}

int agentDisplayPasskey(const std::shared_ptr<Agent> &agent, uint32_t passkey,
                        uint16_t entered) {
  ADK_LOG_WARNING(LOGTAG "bus call " AGENT_INTERFACE "::DisplayPasskey\n");
  int res = sd_bus_call_method_async(g_sdbus, nullptr, agent->owner.c_str(),
                                     agent->path.c_str(), AGENT_INTERFACE,
                                     "DisplayPasskey", nullptr, nullptr, "ouq",
                                     dev_path, passkey, entered);
  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Failed display passkey: %d - %s\n", -res,
                    strerror(-res));
    return res;
  }

  return res;
}

AgentManager::AgentManager() : m_sdbusSlot(nullptr) {
  init();
  gAgentManager = this;
}

AgentManager::~AgentManager() {
  cleanup();
  gAgentManager = nullptr;
}

void AgentManager::init(void) {
  static const sd_bus_vtable sd_agent_manager_methods[] = {
      SD_BUS_VTABLE_START(0),
      SD_BUS_METHOD("RegisterAgent", "os", "", AgentManager::sd_registerAgent,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("UnregisterAgent", "o", "",
                    AgentManager::sd_unregisterAgent,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("RequestDefaultAgent", "o", "",
                    AgentManager::sd_requestDefault,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_VTABLE_END};

  int res = sd_bus_add_object_vtable(g_sdbus, &m_sdbusSlot, "/org/bluez",
                                     AGENT_MANAGER_INTERFACE,
                                     sd_agent_manager_methods, this);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Agent interface init failed: %d - %s\n", -res,
                   strerror(-res));
  }
}

void AgentManager::cleanup(void) {
  sd_bus_slot_unref(m_sdbusSlot);

  m_agentList.clear();
}

int AgentManager::sd_registerAgent(sd_bus_message *m, void *userdata,
                                   sd_bus_error *ret_error) {
  AgentManager *pAgentManager = static_cast<AgentManager *>(userdata);
  const char *sender, *path, *capability;
  AgentCapability cap;

  sender = sd_bus_message_get_sender(m);

  const auto &iter = pAgentManager->m_agentList.find(sender);
  if (iter != pAgentManager->m_agentList.end()) {
    return bt_error_already_exists(ret_error);
  }

  int res = sd_bus_message_read(
      m, TYPE_TO_STR<SD_BUS_TYPE_OBJECT_PATH, SD_BUS_TYPE_STRING>::value, &path,
      &capability);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }

  cap = from_string<AgentCapability>(capability);
  if (cap == AgentCapability::INVALID) {
    return bt_error_invalid_args(ret_error);
  }

  std::shared_ptr<Agent> agent = agentCreate(sender, path, cap);
  if (!agent) {
    return bt_error_invalid_args(ret_error);
  }

  ADK_LOG_NOTICE(LOGTAG " Agent %s", agent->owner.c_str());

  pAgentManager->m_agentList.insert({agent->owner, agent});

  return sd_bus_reply_method_return(m, nullptr);
}

int AgentManager::sd_unregisterAgent(sd_bus_message *m, void *userdata,
                                     sd_bus_error *ret_error) {
  AgentManager *pAgentManager = static_cast<AgentManager *>(userdata);
  const char *sender, *path;

  sender = sd_bus_message_get_sender(m);

  const auto &iter = pAgentManager->m_agentList.find(sender);
  if (iter == pAgentManager->m_agentList.end()) {
    return bt_error_does_not_exist(ret_error);
  }

  ADK_LOG_DEBUG(LOGTAG "Agent %s", iter->second->owner.c_str());

  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_OBJECT_PATH, &path);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }

  if (path != iter->second->path) {
    return bt_error_does_not_exist(ret_error);
  }

  sd_agentDisconnect(iter->second->sd_watch, iter->second.get());

  return sd_bus_reply_method_return(m, nullptr);
}

int AgentManager::sd_requestDefault(sd_bus_message *m, void *userdata,
                                    sd_bus_error *ret_error) {
  AgentManager *pAgentManager = static_cast<AgentManager *>(userdata);
  const char *sender, *path;

  sender = sd_bus_message_get_sender(m);

  const auto &iter = pAgentManager->m_agentList.find(sender);
  if (iter == pAgentManager->m_agentList.end()) {
    return bt_error_does_not_exist(ret_error);
  }

  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_OBJECT_PATH, &path);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }

  if (path != iter->second->path) {
    return bt_error_does_not_exist(ret_error);
  }

  if (!pAgentManager->addDefaultAgent(iter->second)) {
    return bt_error_failed(ret_error, "Failed to set as default");
  }

  return sd_bus_reply_method_return(m, nullptr);
}

bool AgentManager::addDefaultAgent(const std::shared_ptr<Agent> &agent) {
  removeDefaultAgent(agent);
  m_defaultAgents.push_back(agent);

  ADK_LOG_DEBUG(LOGTAG "Default Agent set to %s %s", agent->owner.c_str(),
                agent->path.c_str());

  return true;
}

void AgentManager::removeDefaultAgent(const std::shared_ptr<Agent> &agent) {
  std::remove_if(
      m_defaultAgents.begin(), m_defaultAgents.end(),
      [&](const std::weak_ptr<Agent> &x) -> bool { return x.lock() == agent; });
}

std::shared_ptr<Agent> AgentManager::getAgent(const std::string &owner) {
  const auto &iter = gAgentManager->m_agentList.find(owner);
  if (iter != gAgentManager->m_agentList.end()) {
    return iter->second;
  }

  if (!gAgentManager->m_defaultAgents.empty()) {
    std::shared_ptr<Agent> agent = gAgentManager->m_defaultAgents.back().lock();
    return agent;
  }
  return std::shared_ptr<Agent>();
}

std::shared_ptr<Agent> AgentManager::getAgent(Agent *agent) {
  return getAgent(agent->owner);
}
