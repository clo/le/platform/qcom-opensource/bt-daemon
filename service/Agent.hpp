/*
Copyright (c) 2017-2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef AGENT_MANAGER_HPP
#define AGENT_MANAGER_HPP

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <hardware/bluetooth.h>
#include <systemdq/sd-bus.h>

#include <deque>
#include <map>
#include <memory>
#include <string>

#include "Common.hpp"

enum struct AgentCapability {
  INVALID = -1,
  DISPLAYONLY = 0x00,
  DISPLAYYESNO = 0x01,
  KEYBOARDONLY = 0x02,
  NOINPUTNOOUTPUT = 0x03,
  KEYBOARDDISPLAY = 0x04,
};
const char *to_string(AgentCapability value);
template <>
AgentCapability from_string<AgentCapability>(const std::string &str);

typedef enum {
  AGENT_REQUEST_PASSKEY,
  AGENT_REQUEST_CONFIRMATION,
  AGENT_REQUEST_AUTHORIZATION,
  AGENT_REQUEST_PINCODE,
  AGENT_REQUEST_AUTHORIZE_SERVICE,
  AGENT_REQUEST_DISPLAY_PINCODE,
} agent_request_type_t;

struct AgentRequest;

struct Agent {
  std::string owner;
  std::string path;
  AgentCapability capability;
  std::shared_ptr<AgentRequest> request;

  sd_bus_track *sd_watch;

  ~Agent();

 private:
  void agentRelease();
  int sendCancelRequest();
};

typedef void (*agentCb)(const sd_bus_error *err, void *user_data);
typedef void (*agentPincodeCb)(const sd_bus_error *err, const char *pincode,
                               void *user_data);
typedef void (*agentPasskeyCb)(const sd_bus_error *err, uint32_t passkey,
                               void *user_data);

int agentRequestPincode(const std::shared_ptr<Agent> &agent, agentPincodeCb cb,
                        void *user_data);
int agentRequestConfirmation(const std::shared_ptr<Agent> &agent,
                             uint32_t passkey, agentCb cb, void *user_data);
int agentRequestAuthorization(const std::shared_ptr<Agent> &agent, agentCb cb,
                              void *user_data);
int agentDisplayPasskey(const std::shared_ptr<Agent> &agent, uint32_t passkey,
                        uint16_t entered);

class AgentManager {
 public:
  std::map<std::string, std::shared_ptr<Agent>> m_agentList;
  std::deque<std::weak_ptr<Agent>> m_defaultAgents;

 private:
  sd_bus_slot *m_sdbusSlot;

 public:
  AgentManager();
  ~AgentManager();

  void init();
  void cleanup();

  static std::shared_ptr<Agent> getAgent(const std::string &owner);
  static std::shared_ptr<Agent> getAgent(Agent *agent);

  bool addDefaultAgent(const std::shared_ptr<Agent> &agent);
  void removeDefaultAgent(const std::shared_ptr<Agent> &agent);

  static int sd_registerAgent(sd_bus_message *m, void *userdata,
                              sd_bus_error *ret_error);
  static int sd_unregisterAgent(sd_bus_message *m, void *userdata,
                                sd_bus_error *ret_error);
  static int sd_requestDefault(sd_bus_message *m, void *userdata,
                               sd_bus_error *ret_error);
};

#endif  // AGENT_MANAGER_HPP
