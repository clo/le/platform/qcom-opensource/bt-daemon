/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "AvrcpCTService.hpp"

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include <list>
#include <map>

#include "Adapter.hpp"
#include "Common.hpp"
#include "Sdbus.hpp"

#define LOGTAG "AvrcpCT "
#define AUDIO_CONTROL_INTERFACE "org.bluez.MediaControl1"

static btrc_ctrl_callbacks_t sAvrcpCtrlCbs = {
    sizeof(sAvrcpCtrlCbs),
    AvrcpCTService::avrcpPassthroughRspCb,
    AvrcpCTService::avrcpGroupnavigationRspCb,
    AvrcpCTService::avrcpConnectionStateCb,
    AvrcpCTService::avrcpGetRcFeaturesCb,
    AvrcpCTService::avrcpSetPlayerAppSettingRspCb,
    AvrcpCTService::avrcpPlayerAppSettingCb,
    AvrcpCTService::avrcpPlayerAppSettingChangedCb,
    AvrcpCTService::avrcpSetAbsVolCmdCb,
    AvrcpCTService::avrcpRegisterNotificationAbsVolCb,
    AvrcpCTService::avrcpTrackChangedCb,
    AvrcpCTService::avrcpPlayPositionChangedCb,
    AvrcpCTService::avrcpPlayStatusChangedCb,
    AvrcpCTService::avrcpGetFolderItemsCb,
    AvrcpCTService::avrcpChangePathCb,
    AvrcpCTService::avrcpSetBrowsedPlayerCb,
    AvrcpCTService::avrcpSetAddressedPlayerCb};

static btrc_ctrl_vendor_callbacks_t sAvrcpCtrlVendorCbs = {
    sizeof(sAvrcpCtrlVendorCbs),
    AvrcpCTService::avrcpVendorGetCapRspCb,
    AvrcpCTService::avrcpVendorListPlayerAppSettingAttribRspCb,
    AvrcpCTService::avrcpVendorListPlayerAppSettingValueRspCb,
    AvrcpCTService::avrcpVendorCurrentPlayerAppSettingRspCb,
    AvrcpCTService::avrcpVendorNotificationRspCb,
    AvrcpCTService::avrcpVendorGetElementAttribRspCb,
    AvrcpCTService::avrcpVendorGetPlayStatusRspCb,
    AvrcpCTService::avrcpVendorPassthroughRspCb,
    AvrcpCTService::avrcpVendorBrowseConnectionStateCb,
    AvrcpCTService::avrcpVendorSetAddressedPlayerCb,
    AvrcpCTService::avrcpVendorSetBrowsedPlayerCb,
    AvrcpCTService::avrcpVendorChangePathCb,
    AvrcpCTService::avrcpVendorGetFolderItemsCb,
    AvrcpCTService::avrcpVendorGetItemAttributesCb,
    AvrcpCTService::avrcpVendorPlayItemCb,
    AvrcpCTService::avrcpVendorAddToNowPlayingCb,
    AvrcpCTService::avrcpVendorSearchCb};

AvrcpCTService *AvrcpCTService::gAvrcpCTService = nullptr;

AvrcpCTService::AvrcpCTService(Adapter *pAdapter)
    : AvrcpProfileService(pAdapter, LOCAL_UUID, REMOTE_UUID),
      m_btAvrcpIF(nullptr),
      m_btAvrcpVendorIF(nullptr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  gAvrcpCTService = this;
}

AvrcpCTService::~AvrcpCTService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  disableService();
  gAvrcpCTService = nullptr;
}

void AvrcpCTService::enableService(void) {
  ADK_LOG_NOTICE(LOGTAG " enableService Avrcp \n");
  const bt_interface_t *pBtIf = getAdapter()->getBtInterface();
  if (!pBtIf) {
    return;
  }
  // AVRCP CT Initialization
  m_btAvrcpIF = (btrc_ctrl_interface_t *)pBtIf->get_profile_interface(
      BT_PROFILE_AV_RC_CTRL_ID);

  // AVRCP CT Vendor Initialization
  m_btAvrcpVendorIF =
      (btrc_ctrl_vendor_interface_t *)pBtIf->get_profile_interface(
          BT_PROFILE_AV_RC_CTRL_VENDOR_ID);

  if (!m_btAvrcpIF || !m_btAvrcpVendorIF) {
    return;
  }
  serviceState() = State::kInitialized;

  if (m_btAvrcpIF->init(&sAvrcpCtrlCbs) == BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG " enableService int ctrl Success \n");
  }

  // Currently provide the max possible value for max connection, later can be
  // made as config param
  if (m_btAvrcpVendorIF->init_vendor(&sAvrcpCtrlVendorCbs, 1) ==
      BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG " enableService init vendor Success \n");
  }

  serviceState() = State::kRunning;
  ADK_LOG_NOTICE(LOGTAG " enableService Avrcp Success \n");
}

void AvrcpCTService::disableService(void) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  if (m_btAvrcpIF) {
    m_btAvrcpIF->cleanup();
    m_btAvrcpIF = nullptr;
  }
  if (m_btAvrcpVendorIF) {
    m_btAvrcpVendorIF->cleanup_vendor();
    m_btAvrcpVendorIF = nullptr;
  }

  while (!m_AvrcpDevices.empty()) {
    Device *dev = m_AvrcpDevices.begin()->second;
    m_AvrcpDevices.erase(
        m_AvrcpDevices
            .begin());  // TODO: check that we are not leaking a Device
    sd_bus_emit_properties_changed(g_sdbus, dev->getDeviceObjectPath().c_str(),
                                   AUDIO_CONTROL_INTERFACE, "Connected",
                                   nullptr);
  }

  serviceState() = State::kStopped;
}

bool AvrcpCTService::registerControlObjectForDevice(Device *avrcpDev) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  std::string devPath = avrcpDev->getDeviceObjectPath();
  if (devPath.empty()) {
    return false;
  }

  static const sd_bus_vtable sSdAvrcpVTable[] = {
      SD_BUS_VTABLE_START(0),

      SD_BUS_METHOD("Play", nullptr, nullptr, AvrcpCTService::sd_controlPlay,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Pause", nullptr, nullptr, AvrcpCTService::sd_controlPause,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Stop", nullptr, nullptr, AvrcpCTService::sd_controlStop,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Next", nullptr, nullptr, AvrcpCTService::sd_controlNext,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Previous", nullptr, nullptr,
                    AvrcpCTService::sd_controlPrevious,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("VolumeUp", nullptr, nullptr,
                    AvrcpCTService::sd_controlVolumeUp,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("VolumeDown", nullptr, nullptr,
                    AvrcpCTService::sd_controlVolumeDown,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("FastForward", nullptr, nullptr,
                    AvrcpCTService::sd_controlFastForward,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Rewind", nullptr, nullptr,
                    AvrcpCTService::sd_controlRewind,
                    SD_BUS_VTABLE_UNPRIVILEGED),

      SD_BUS_PROPERTY("Connected", "b",
                      AvrcpCTService::sd_controlPropertyGetConnected, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),

      SD_BUS_VTABLE_END};

  int res = sd_bus_add_object_vtable(g_sdbus, avrcpDev->ControlIFSlot(),
                                     devPath.c_str(), AUDIO_CONTROL_INTERFACE,
                                     sSdAvrcpVTable, avrcpDev);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "interface init failed on path %s: %d - %s\n",
                   devPath.c_str(), -res, strerror(-res));
    return false;
  }


  res = sd_bus_emit_interfaces_added(g_sdbus, devPath.c_str(),
                                     AUDIO_CONTROL_INTERFACE, nullptr);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG
                   "object manager failed to signal new interface %s in %s: %d - %s\n",
                   AUDIO_CONTROL_INTERFACE, devPath.c_str(), -res, strerror(-res));
    return false;
  }
  return true;
}

void AvrcpCTService::unregisterControlObjectForDevice(Device *avrcpDev) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  std::string devPath = avrcpDev->getDeviceObjectPath();
  if (!devPath.empty()) {

    int res = sd_bus_emit_interfaces_removed(g_sdbus, devPath.c_str(),
                                             AUDIO_CONTROL_INTERFACE, nullptr);
    if (res < 0) {
      ADK_LOG_NOTICE(LOGTAG
                     "object manager failed to signal deleted interface %s in %s: %d\n",
                     AUDIO_CONTROL_INTERFACE, devPath.c_str(), -res, strerror(-res));
    }

    ADK_LOG_NOTICE(LOGTAG " Removing Control Interface for device on path %s\n",
                   devPath.c_str());
    sd_bus_slot_unref(*avrcpDev->ControlIFSlot());
    *avrcpDev->ControlIFSlot() = nullptr;
  }
}

void AvrcpCTService::handlePassThroughCmdReq(PassthroughKeys keyId,
                                             Device *pAvrcpDev) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  bt_bdaddr_t addr = pAvrcpDev->getDeviceAddr();
  m_btAvrcpIF->send_pass_through_cmd(&addr, keyId, KEY_PRESSED);
  m_btAvrcpIF->send_pass_through_cmd(&addr, keyId, KEY_RELEASED);
}

bool AvrcpCTService::setVolume(Device *dev, uint16_t volume, uint8_t label) {
  ADK_LOG_INFO(LOGTAG "%s: Volume, %d label %d\n", __func__, volume, label);
  bt_bdaddr_t addr = dev->getDeviceAddr();
  bt_status_t status = m_btAvrcpIF->register_abs_vol_rsp(
      &addr, BTRC_NOTIFICATION_TYPE_CHANGED, volume, label);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "Failed to notify volume changed to device %s: %d\n",
                   addr.ToString().c_str(), status);
    return false;
  }
  return true;
}

bool AvrcpCTService::setEqualizer(Device *dev, Equalizer mode) {
  bt_bdaddr_t addr = dev->getDeviceAddr();
  uint8_t name = BTRC_PLAYER_ATTR_EQUALIZER;
  uint8_t value = static_cast<uint8_t>(mode);
  bt_status_t status =
      m_btAvrcpIF->set_player_app_setting_cmd(&addr, 1, &name, &value);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "Failed to set equalizer for %s: %d\n",
                   addr.ToString().c_str(), status);
    return false;
  }
  return true;
}

bool AvrcpCTService::setRepeat(Device *dev, Repeat mode) {
  bt_bdaddr_t addr = dev->getDeviceAddr();
  uint8_t name = BTRC_PLAYER_ATTR_REPEAT;
  uint8_t value = static_cast<uint8_t>(mode);
  bt_status_t status =
      m_btAvrcpIF->set_player_app_setting_cmd(&addr, 1, &name, &value);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "Failed to set repeat for %s: %d\n",
                   addr.ToString().c_str(), status);
    return false;
  }
  return true;
}

bool AvrcpCTService::setShuffle(Device *dev, Shuffle mode) {
  bt_bdaddr_t addr = dev->getDeviceAddr();
  uint8_t name = BTRC_PLAYER_ATTR_SHUFFLE;
  uint8_t value = static_cast<uint8_t>(mode);
  bt_status_t status =
      m_btAvrcpIF->set_player_app_setting_cmd(&addr, 1, &name, &value);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "Failed to set shuffle for %s: %d\n",
                   addr.ToString().c_str(), status);
    return false;
  }
  return true;
}

bool AvrcpCTService::setScan(Device *dev, Scan mode) {
  bt_bdaddr_t addr = dev->getDeviceAddr();
  uint8_t name = BTRC_PLAYER_ATTR_SCAN;
  uint8_t value = static_cast<uint8_t>(mode);
  bt_status_t status =
      m_btAvrcpIF->set_player_app_setting_cmd(&addr, 1, &name, &value);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "Failed to set scan for %s: %d\n",
                   addr.ToString().c_str(), status);
    return false;
  }
  return true;
}

void AvrcpCTService::avrcpPassthroughRspCb(RawAddress *UNUSED(bd_addr), int UNUSED(id),
                                           int UNUSED(key_state)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpGroupnavigationRspCb(int UNUSED(id), int UNUSED(key_state)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpConnectionStateCb(bool rc_connect, bool br_connect,
                                            RawAddress *bd_addr) {
  ADK_LOG_NOTICE(LOGTAG "%s: rc %d, br %d", __func__, rc_connect, br_connect);
  struct CallbackData {
    bool rc_connect;
    bool br_connect;
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data = std::make_shared<CallbackData>(
      CallbackData{rc_connect, br_connect, *bd_addr});
  queueBtCallback([data](void) -> void {
    if (data->rc_connect) {
      gAvrcpCTService->deviceConnectedHandler(data->bd_addr);
    } else {
      gAvrcpCTService->deviceDisconnectedHandler(data->bd_addr);
    }
  });
}

void AvrcpCTService::avrcpGetRcFeaturesCb(RawAddress *bd_addr, int features) {
  ADK_LOG_NOTICE(LOGTAG "%s: features %02x", __func__, features);
  struct CallbackData {
    btrc_remote_features_t features;
    bt_bdaddr_t bdAddr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{(btrc_remote_features_t)features, *bd_addr});
  queueBtCallback([data](void) -> void {
      gAvrcpCTService->avrcpGetRcFeaturesHandler(data->bdAddr,data->features);
  });
}

void AvrcpCTService::avrcpSetPlayerAppSettingRspCb(RawAddress *UNUSED(bd_addr),
                                                   uint8_t UNUSED(accepted)) {
  ADK_LOG_NOTICE(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpPlayerAppSettingCb(
    RawAddress *UNUSED(bd_addr), uint8_t UNUSED(num_attr), btrc_player_app_attr_t *UNUSED(app_attrs),
    uint8_t UNUSED(num_ext_attr), btrc_player_app_ext_attr_t *UNUSED(ext_attrs)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpPlayerAppSettingChangedCb(
    RawAddress *UNUSED(bd_addr), btrc_player_settings_t *UNUSED(p_vals)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpSetAbsVolCmdCb(RawAddress *bd_addr, uint8_t abs_vol,
                                         uint8_t label) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    uint8_t abs_vol;
    uint8_t label;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{*bd_addr, abs_vol, label});
  queueBtCallback([data](void) -> void {
    const auto iter = gAvrcpCTService->m_AvrcpDevices.find(data->bd_addr);
    if (iter != gAvrcpCTService->m_AvrcpDevices.end()) {
      iter->second->setVolumeFromBt(data->abs_vol);
    }

    gAvrcpCTService->m_btAvrcpIF->set_volume_rsp(&data->bd_addr, data->abs_vol,
                                                 data->label);
  });
}

void AvrcpCTService::avrcpRegisterNotificationAbsVolCb(RawAddress *bd_addr,
                                                       uint8_t label) {
  ADK_LOG_NOTICE(LOGTAG "%s: label %d", __func__, label);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    uint8_t label;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{*bd_addr, label});
  queueBtCallback([data](void) -> void {
    const auto iter = gAvrcpCTService->m_AvrcpDevices.find(data->bd_addr);
    if (iter == gAvrcpCTService->m_AvrcpDevices.end()) {
      return;
    }

    uint16_t volume = iter->second->registerVolumeNotif(data->label);
    gAvrcpCTService->m_btAvrcpIF->register_abs_vol_rsp(
        &data->bd_addr, BTRC_NOTIFICATION_TYPE_INTERIM, volume, data->label);
  });
}

void AvrcpCTService::avrcpTrackChangedCb(RawAddress *bd_addr, uint8_t UNUSED(num_attr),
                                         btrc_element_attr_val_t *UNUSED(p_attrs)) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);

  struct CallbackData {
    bt_bdaddr_t bd_addr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{*bd_addr});
  queueBtCallback([data](void) -> void {
    const auto iter = gAvrcpCTService->m_AvrcpDevices.find(data->bd_addr);
    if (iter == gAvrcpCTService->m_AvrcpDevices.end()) {
      return;
    }
    gAvrcpCTService->getElementAttrAsync(iter->second);
  });
}

void AvrcpCTService::avrcpPlayPositionChangedCb(RawAddress *UNUSED(bd_addr),
                                                uint32_t UNUSED(song_len),
                                                uint32_t UNUSED(song_pos)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpPlayStatusChangedCb(RawAddress *UNUSED(bd_addr),
                                              btrc_play_status_t UNUSED(play_status)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpGetFolderItemsCb(
    RawAddress *UNUSED(bd_addr), btrc_status_t UNUSED(status),
    const btrc_folder_items_t *UNUSED(folder_items), uint8_t UNUSED(count)) {
  ADK_LOG_NOTICE(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpChangePathCb(RawAddress *UNUSED(bd_addr), uint8_t UNUSED(count)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpSetBrowsedPlayerCb(RawAddress *UNUSED(bd_addr),
                                             uint8_t UNUSED(num_items), uint8_t UNUSED(depth)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpSetAddressedPlayerCb(RawAddress *UNUSED(bd_addr),
                                               uint8_t UNUSED(status)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpVendorGetCapRspCb(bt_bdaddr_t *UNUSED(bd_addr), int UNUSED(cap_id),
                                            uint32_t *UNUSED(supported_values),
                                            int UNUSED(num_supported),
                                            uint8_t UNUSED(rsp_type)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpVendorListPlayerAppSettingAttribRspCb(
    bt_bdaddr_t *UNUSED(bd_addr), uint8_t *UNUSED(supported_attribs), int UNUSED(num_attrib),
    uint8_t UNUSED(rsp_type)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpVendorListPlayerAppSettingValueRspCb(
    bt_bdaddr_t *UNUSED(bd_addr), uint8_t *UNUSED(supported_val), uint8_t UNUSED(num_supported),
    uint8_t UNUSED(rsp_type)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpCTService::avrcpVendorCurrentPlayerAppSettingRspCb(
    bt_bdaddr_t *UNUSED(bd_addr), uint8_t *UNUSED(supported_ids), uint8_t *UNUSED(supported_val),
    uint8_t UNUSED(num_attrib), uint8_t UNUSED(rsp_type)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

bt_status_t AvrcpCTService::avrcpVendorNotificationRspCb(
    bt_bdaddr_t *bd_addr, btrc_event_id_t event_id,
    btrc_notification_type_t type, btrc_register_notification_t *p_param) {
  ADK_LOG_NOTICE(LOGTAG "%s: event %d", __func__, event_id);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    btrc_event_id_t event_id;
    btrc_notification_type_t type;
    btrc_register_notification_t param;
  };
  std::shared_ptr<CallbackData> data = std::make_shared<CallbackData>(
      CallbackData{*bd_addr, event_id, type, *p_param});
  queueBtCallback([data](void) -> void {
    const auto iter = gAvrcpCTService->m_AvrcpDevices.find(data->bd_addr);
    if (iter == gAvrcpCTService->m_AvrcpDevices.end()) {
      return;
    }
    switch (data->event_id) {
      case BTRC_EVT_PLAY_STATUS_CHANGED:
        iter->second->setPlayState(
            static_cast<PlayState>(data->param.play_status));
        break;
      case BTRC_EVT_PLAY_POS_CHANGED:
        iter->second->setPlayPosition(data->param.song_pos);
        break;
      case BTRC_EVT_TRACK_CHANGE:
        gAvrcpCTService->getElementAttrAsync(iter->second);
        break;
      case BTRC_EVT_APP_SETTINGS_CHANGED: {
        for (uint8_t i = 0; i < data->param.player_setting.num_attr; ++i) {
          switch (data->param.player_setting.attr_ids[i]) {
            case BTRC_PLAYER_ATTR_EQUALIZER:
              iter->second->setEqualizerFromBt(static_cast<Equalizer>(
                  data->param.player_setting.attr_values[i]));
              break;
            case BTRC_PLAYER_ATTR_REPEAT:
              iter->second->setRepeatFromBt(static_cast<Repeat>(
                  data->param.player_setting.attr_values[i]));
              break;
            case BTRC_PLAYER_ATTR_SHUFFLE:
              iter->second->setShuffleFromBt(static_cast<Shuffle>(
                  data->param.player_setting.attr_values[i]));
              break;
            case BTRC_PLAYER_ATTR_SCAN:
              iter->second->setScanFromBt(
                  static_cast<Scan>(data->param.player_setting.attr_values[i]));
              break;
            default:
              // Ignore unknown settings
              break;
          }
        }
      } break;
      default:
        ADK_LOG_WARNING(LOGTAG "%s: unhandled event", __func__);
        break;
    }
  });
  return BT_STATUS_SUCCESS;
}

void AvrcpCTService::avrcpVendorGetElementAttribRspCb(
    bt_bdaddr_t *bd_addr, uint8_t num_attributes,
    btrc_element_attr_val_t *p_attrs, uint8_t rsp_type) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    std::map<btrc_media_attr_t, std::string> attrs;
    uint8_t rsp_type;
  };
  std::shared_ptr<CallbackData> data = std::make_shared<CallbackData>();
  data->bd_addr = *bd_addr;
  for (int i = 0; i < num_attributes; ++i) {
    data->attrs.emplace(static_cast<btrc_media_attr_t>(p_attrs[i].attr_id),
                        std::string(reinterpret_cast<char *>(p_attrs[i].text)));
  }
  data->rsp_type = rsp_type;
  queueBtCallback([data](void) -> void {

    Metadata attrs;
    attrs.title = data->attrs[BTRC_MEDIA_ATTR_TITLE];
    attrs.artist = data->attrs[BTRC_MEDIA_ATTR_ARTIST];
    attrs.album = data->attrs[BTRC_MEDIA_ATTR_ALBUM];
    try {
      attrs.trackNum = std::stoul(data->attrs[BTRC_MEDIA_ATTR_TRACK_NUM]);
    } catch (const std::logic_error &) {
    }
    try {
      attrs.trackCount = std::stoul(data->attrs[BTRC_MEDIA_ATTR_NUM_TRACKS]);
    } catch (const std::logic_error &) {
    }
    attrs.genre = data->attrs[BTRC_MEDIA_ATTR_GENRE];
    try {
      attrs.duration = std::stoul(data->attrs[BTRC_MEDIA_ATTR_PLAYING_TIME]);
    } catch (const std::logic_error &) {
    }

    const auto iter = gAvrcpCTService->m_AvrcpDevices.find(data->bd_addr);
    if (iter == gAvrcpCTService->m_AvrcpDevices.end()) {
      return;
    }
    iter->second->setMetadata(attrs);
  });
}

bt_status_t AvrcpCTService::avrcpVendorGetPlayStatusRspCb(
    bt_bdaddr_t *UNUSED(bd_addr), btrc_play_status_t UNUSED(play_status), uint32_t UNUSED(song_len),
    uint32_t UNUSED(song_pos)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
  return BT_STATUS_SUCCESS;
}

void AvrcpCTService::avrcpVendorPassthroughRspCb(int UNUSED(id), int UNUSED(key_state),
                                                 bt_bdaddr_t *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

bt_status_t AvrcpCTService::avrcpVendorBrowseConnectionStateCb(
    bool UNUSED(state), bt_bdaddr_t *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
  return BT_STATUS_SUCCESS;
}

bt_status_t AvrcpCTService::avrcpVendorSetAddressedPlayerCb(
    bt_bdaddr_t *UNUSED(bd_addr), btrc_status_t UNUSED(rsp_status)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
  return BT_STATUS_SUCCESS;
}

bt_status_t AvrcpCTService::avrcpVendorSetBrowsedPlayerCb(
    bt_bdaddr_t *UNUSED(bd_addr), btrc_status_t UNUSED(rsp_status), uint32_t UNUSED(num_items),
    uint16_t UNUSED(charset_id), uint8_t UNUSED(folder_depth),
    btrc_br_folder_name_t *UNUSED(p_folders)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
  return BT_STATUS_SUCCESS;
}

bt_status_t AvrcpCTService::avrcpVendorChangePathCb(bt_bdaddr_t *UNUSED(bd_addr),
                                                    btrc_status_t UNUSED(rsp_status),
                                                    uint32_t UNUSED(num_items)) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  return BT_STATUS_SUCCESS;
}

bt_status_t AvrcpCTService::avrcpVendorGetFolderItemsCb(
    bt_bdaddr_t *UNUSED(bd_addr), uint32_t UNUSED(start_item), uint32_t UNUSED(end_item),
    btrc_status_t UNUSED(rsp_status), uint16_t UNUSED(num_items),
    btrc_folder_items_t *UNUSED(p_items)) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  return BT_STATUS_SUCCESS;
}

bt_status_t AvrcpCTService::avrcpVendorGetItemAttributesCb(
    bt_bdaddr_t *UNUSED(bd_addr), btrc_status_t UNUSED(rsp_status), uint8_t UNUSED(num_attr),
    btrc_element_attr_val_t *UNUSED(p_attrs)) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  return BT_STATUS_SUCCESS;
}

bt_status_t AvrcpCTService::avrcpVendorPlayItemCb(bt_bdaddr_t *UNUSED(bd_addr),
                                                  btrc_status_t UNUSED(rsp_status)) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  return BT_STATUS_SUCCESS;
}

bt_status_t AvrcpCTService::avrcpVendorAddToNowPlayingCb(
    bt_bdaddr_t *UNUSED(bd_addr), btrc_status_t UNUSED(rsp_status)) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  return BT_STATUS_SUCCESS;
}

bt_status_t AvrcpCTService::avrcpVendorSearchCb(bt_bdaddr_t *UNUSED(bd_addr),
                                                btrc_status_t UNUSED(rsp_status),
                                                uint16_t UNUSED(uid_counter),
                                                uint32_t UNUSED(num_item)) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  return BT_STATUS_SUCCESS;
}

int AvrcpCTService::sd_controlPlay(sd_bus_message *m, void *userdata,
                                   sd_bus_error *UNUSED(ret_error)) {
  gAvrcpCTService->handlePassThroughCmdReq(CMD_ID_PLAY,
                                           static_cast<Device *>(userdata));
  return sd_bus_reply_method_return(m, nullptr);
}

int AvrcpCTService::sd_controlPause(sd_bus_message *m, void *userdata,
                                    sd_bus_error *UNUSED(ret_error)) {
  gAvrcpCTService->handlePassThroughCmdReq(CMD_ID_PAUSE,
                                           static_cast<Device *>(userdata));
  return sd_bus_reply_method_return(m, nullptr);
}

int AvrcpCTService::sd_controlStop(sd_bus_message *m, void *userdata,
                                   sd_bus_error *UNUSED(ret_error)) {
  gAvrcpCTService->handlePassThroughCmdReq(CMD_ID_STOP,
                                           static_cast<Device *>(userdata));
  return sd_bus_reply_method_return(m, nullptr);
}

int AvrcpCTService::sd_controlNext(sd_bus_message *m, void *userdata,
                                   sd_bus_error *UNUSED(ret_error)) {
  gAvrcpCTService->handlePassThroughCmdReq(CMD_ID_FORWARD,
                                           static_cast<Device *>(userdata));
  return sd_bus_reply_method_return(m, nullptr);
}

int AvrcpCTService::sd_controlPrevious(sd_bus_message *m, void *userdata,
                                       sd_bus_error *UNUSED(ret_error)) {
  gAvrcpCTService->handlePassThroughCmdReq(CMD_ID_BACKWARD,
                                           static_cast<Device *>(userdata));
  return sd_bus_reply_method_return(m, nullptr);
}

int AvrcpCTService::sd_controlVolumeUp(sd_bus_message *m, void *userdata,
                                       sd_bus_error *UNUSED(ret_error)) {
  gAvrcpCTService->handlePassThroughCmdReq(CMD_ID_VOL_UP,
                                           static_cast<Device *>(userdata));
  return sd_bus_reply_method_return(m, nullptr);
}

int AvrcpCTService::sd_controlVolumeDown(sd_bus_message *m, void *userdata,
                                         sd_bus_error *UNUSED(ret_error)) {
  gAvrcpCTService->handlePassThroughCmdReq(CMD_ID_VOL_DOWN,
                                           static_cast<Device *>(userdata));
  return sd_bus_reply_method_return(m, nullptr);
}

int AvrcpCTService::sd_controlFastForward(sd_bus_message *m, void *userdata,
                                          sd_bus_error *UNUSED(ret_error)) {
  gAvrcpCTService->handlePassThroughCmdReq(CMD_ID_FF,
                                           static_cast<Device *>(userdata));
  return sd_bus_reply_method_return(m, nullptr);
}

int AvrcpCTService::sd_controlRewind(sd_bus_message *m, void *userdata,
                                     sd_bus_error *UNUSED(ret_error)) {
  gAvrcpCTService->handlePassThroughCmdReq(CMD_ID_REWIND,
                                           static_cast<Device *>(userdata));
  return sd_bus_reply_method_return(m, nullptr);
}

int AvrcpCTService::sd_controlPropertyGetConnected(
    sd_bus *UNUSED(bus), const char *UNUSED(path), const char *UNUSED(interface), const char *UNUSED(property),
    sd_bus_message *reply, void *userdata, sd_bus_error *UNUSED(ret_error)) {
  Device *pAvrcpDev = static_cast<Device *>(userdata);

  bool connected =
      (gAvrcpCTService->m_AvrcpDevices.find(pAvrcpDev->getDeviceAddr()) !=
       gAvrcpCTService->m_AvrcpDevices.end());
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value,
                               connected);
}

bool AvrcpCTService::getElementAttrAsync(Device *dev) {
  bt_bdaddr_t addr = dev->getDeviceAddr();
  std::vector<uint32_t> attrs = {
      BTRC_MEDIA_ATTR_TITLE,       BTRC_MEDIA_ATTR_ARTIST,
      BTRC_MEDIA_ATTR_ALBUM,       BTRC_MEDIA_ATTR_TRACK_NUM,
      BTRC_MEDIA_ATTR_NUM_TRACKS,  BTRC_MEDIA_ATTR_GENRE,
      BTRC_MEDIA_ATTR_PLAYING_TIME};

  bt_status_t status = m_btAvrcpVendorIF->get_element_attribute_command_vendor(
      &addr, (uint8_t)attrs.size(), attrs.data());
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "Failed to query current element %s: %d\n",
                   addr.ToString().c_str(), status);
    return false;
  }
  return true;
}

void AvrcpCTService::deviceConnectedHandler(const bt_bdaddr_t &bdAddr) {
  Device *pAvrcpDev = nullptr;
  ADK_LOG_NOTICE(LOGTAG " deviceConnectedHandler \n");
  if (m_AvrcpDevices.find(bdAddr) != m_AvrcpDevices.end()) {
    ADK_LOG_NOTICE(LOGTAG " Device Already Connected\n");
  } else {
    // Add the device object reference to avrcp connected device list
    pAvrcpDev = getAdapter()->getDevice(bdAddr);
    if (pAvrcpDev) {
      m_AvrcpDevices.insert({bdAddr, pAvrcpDev});
      std::string path = pAvrcpDev->getDeviceObjectPath();
      ADK_LOG_NOTICE(
          LOGTAG
          " Notifying avrcp connected state change for device object %s\n",
          path.c_str());
      sd_bus_emit_properties_changed(
          g_sdbus, path.c_str(), AUDIO_CONTROL_INTERFACE, "Connected", nullptr);
    }
  }
}

void AvrcpCTService::deviceDisconnectedHandler(const bt_bdaddr_t &bdAddr) {
  ADK_LOG_NOTICE(LOGTAG " deviceDisconnectedHandler \n");
  const auto &deviceIter = m_AvrcpDevices.find(bdAddr);
  if (deviceIter != m_AvrcpDevices.end()) {
    ADK_LOG_NOTICE(LOGTAG " Device is part of the connected list\n");
    Device *pAvrcpDev = deviceIter->second;
    // Emit a MediaPlayer1 Status update on Disconnecting.
    pAvrcpDev->setPlayState(PlayState::STOPPED);
    pAvrcpDev->setAbsVolumeSupport(false);
    m_AvrcpDevices.erase(
        deviceIter);  // TODO: check that we are not leaking a Device
    sd_bus_emit_properties_changed(
        g_sdbus, pAvrcpDev->getDeviceObjectPath().c_str(),
        AUDIO_CONTROL_INTERFACE, "Connected", nullptr);
  }
  ADK_LOG_NOTICE(LOGTAG " deviceDisconnectedHandler done\n");
}

void AvrcpCTService::avrcpGetRcFeaturesHandler(const bt_bdaddr_t &bdAddr,
                                          btrc_remote_features_t features) {
  const auto &deviceIter = m_AvrcpDevices.find(bdAddr);
  if (deviceIter != m_AvrcpDevices.end()) {
    Device *pAvrcpDev = deviceIter->second;
    if ((features & BTRC_FEAT_ABSOLUTE_VOLUME) != 0) {
      ADK_LOG_NOTICE(LOGTAG "%s, Abs vol supported for dev ");
      pAvrcpDev->setAbsVolumeSupport(true);
    } else {
      ADK_LOG_NOTICE(LOGTAG "%s, Abs vol NOT supported for dev ");
      pAvrcpDev->setAbsVolumeSupport(false);
    }
  }
  else{
      ADK_LOG_NOTICE(LOGTAG "%s, Device is not part of connected list ");
  }
}