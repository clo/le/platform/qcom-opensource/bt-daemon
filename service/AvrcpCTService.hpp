/*
Copyright (c) 2016-2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef AVRCPCTSERVICE_HPP
#define AVRCPCTSERVICE_HPP

#include <hardware/bluetooth.h>
#include <hardware/bt_rc.h>
#include <hardware/bt_rc_vendor.h>
#include <systemdq/sd-bus.h>

#include "Device.hpp"
#include "ProfileService.hpp"

class AvrcpCTService final : public AvrcpProfileService {
 public:
  static constexpr const char *REMOTE_UUID =
      "0000110e-0000-1000-8000-00805f9b34fb";  // aka AVRCP Remote (RC/CT)
  static constexpr const char *LOCAL_UUID =
      "0000110c-0000-1000-8000-00805f9b34fb";  // aka AVRCP Target (TG)
  
  AvrcpCTService(Adapter *pAdapter);
  ~AvrcpCTService();
  
  // From AvrcpProfileService
  void enableService();
  void disableService();

  bool registerControlObjectForDevice(Device *avrcpDev);
  void unregisterControlObjectForDevice(Device *avrcpDev);
  void handlePassThroughCmdReq(PassthroughKeys keyId, Device *pAvrcpDev);

  bool setVolume(Device *dev, uint16_t volume, uint8_t label);
  bool setEqualizer(Device *dev, Equalizer mode);
  bool setRepeat(Device *dev, Repeat mode);
  bool setShuffle(Device *dev, Shuffle mode);
  bool setScan(Device *dev, Scan mode);
  
  // Fluoride avrcp CT callbacks
  static void avrcpPassthroughRspCb(RawAddress *bd_addr, int id, int key_state);
  static void avrcpGroupnavigationRspCb(int id, int key_state);
  static void avrcpConnectionStateCb(bool rc_connect, bool bt_connect,
                                     RawAddress *bd_addr);
  static void avrcpGetRcFeaturesCb(RawAddress *bd_addr, int features);
  static void avrcpSetPlayerAppSettingRspCb(RawAddress *bd_addr,
                                            uint8_t accepted);
  static void avrcpPlayerAppSettingCb(RawAddress *bd_addr, uint8_t num_attr,
                                      btrc_player_app_attr_t *app_attrs,
                                      uint8_t num_ext_attr,
                                      btrc_player_app_ext_attr_t *ext_attrs);
  static void avrcpPlayerAppSettingChangedCb(RawAddress *bd_addr,
                                             btrc_player_settings_t *p_vals);
  static void avrcpSetAbsVolCmdCb(RawAddress *bd_addr, uint8_t abs_vol,
                                  uint8_t label);
  static void avrcpRegisterNotificationAbsVolCb(RawAddress *bd_addr,
                                                uint8_t label);
  static void avrcpTrackChangedCb(RawAddress *bd_addr, uint8_t num_attr,
                                  btrc_element_attr_val_t *p_attrs);
  static void avrcpPlayPositionChangedCb(RawAddress *bd_addr, uint32_t song_len,
                                         uint32_t song_pos);
  static void avrcpPlayStatusChangedCb(RawAddress *bd_addr,
                                       btrc_play_status_t play_status);
  static void avrcpGetFolderItemsCb(RawAddress *bd_addr, btrc_status_t status,
                                    const btrc_folder_items_t *folder_items,
                                    uint8_t count);
  static void avrcpChangePathCb(RawAddress *bd_addr, uint8_t count);
  static void avrcpSetBrowsedPlayerCb(RawAddress *bd_addr, uint8_t num_items,
                                      uint8_t depth);
  static void avrcpSetAddressedPlayerCb(RawAddress *bd_addr, uint8_t status);

  // Fluoride avrcp CT vendor callbacks
  static void avrcpVendorGetCapRspCb(bt_bdaddr_t *bd_addr, int cap_id,
                                     uint32_t *supported_values,
                                     int num_supported, uint8_t rsp_type);
  static void avrcpVendorListPlayerAppSettingAttribRspCb(
      bt_bdaddr_t *bd_addr, uint8_t *supported_attribs, int num_attrib,
      uint8_t rsp_type);
  static void avrcpVendorListPlayerAppSettingValueRspCb(bt_bdaddr_t *bd_addr,
                                                        uint8_t *supported_val,
                                                        uint8_t num_supported,
                                                        uint8_t rsp_type);
  static void avrcpVendorCurrentPlayerAppSettingRspCb(bt_bdaddr_t *bd_addr,
                                                      uint8_t *supported_ids,
                                                      uint8_t *supported_val,
                                                      uint8_t num_attrib,
                                                      uint8_t rsp_type);
  static bt_status_t avrcpVendorNotificationRspCb(
      bt_bdaddr_t *bd_addr, btrc_event_id_t event_id,
      btrc_notification_type_t type, btrc_register_notification_t *p_param);
  static void avrcpVendorGetElementAttribRspCb(bt_bdaddr_t *bd_addr,
                                               uint8_t num_attributes,
                                               btrc_element_attr_val_t *p_attrs,
                                               uint8_t rsp_type);
  static bt_status_t avrcpVendorGetPlayStatusRspCb(
      bt_bdaddr_t *bd_addr, btrc_play_status_t play_status, uint32_t song_len,
      uint32_t song_pos);
  static void avrcpVendorPassthroughRspCb(int id, int key_state,
                                          bt_bdaddr_t *bd_addr);
  static bt_status_t avrcpVendorBrowseConnectionStateCb(bool state,
                                                        bt_bdaddr_t *bd_addr);
  static bt_status_t avrcpVendorSetAddressedPlayerCb(bt_bdaddr_t *bd_addr,
                                                     btrc_status_t rsp_status);
  static bt_status_t avrcpVendorSetBrowsedPlayerCb(
      bt_bdaddr_t *bd_addr, btrc_status_t rsp_status, uint32_t num_items,
      uint16_t charset_id, uint8_t folder_depth,
      btrc_br_folder_name_t *p_folders);
  static bt_status_t avrcpVendorChangePathCb(bt_bdaddr_t *bd_addr,
                                             btrc_status_t rsp_status,
                                             uint32_t num_items);
  static bt_status_t avrcpVendorGetFolderItemsCb(bt_bdaddr_t *bd_addr,
                                                 uint32_t start_item,
                                                 uint32_t end_item,
                                                 btrc_status_t rsp_status,
                                                 uint16_t num_items,
                                                 btrc_folder_items_t *p_items);
  static bt_status_t avrcpVendorGetItemAttributesCb(
      bt_bdaddr_t *bd_addr, btrc_status_t rsp_status, uint8_t num_attr,
      btrc_element_attr_val_t *p_attrs);
  static bt_status_t avrcpVendorPlayItemCb(bt_bdaddr_t *bd_addr,
                                           btrc_status_t rsp_status);
  static bt_status_t avrcpVendorAddToNowPlayingCb(bt_bdaddr_t *bd_addr,
                                                  btrc_status_t rsp_status);
  static bt_status_t avrcpVendorSearchCb(bt_bdaddr_t *bd_addr,
                                         btrc_status_t rsp_status,
                                         uint16_t uid_counter,
                                         uint32_t num_item);

  // TODO: MediaControl1 should probably be a class of its own,
  // independent of the AvrcpCTService which already represents
  // the AVRCP CT profile service on Fluoride

  // Bluez MediaControl1 interface
  static int sd_controlPlay(sd_bus_message *m, void *userdata,
                            sd_bus_error *ret_error);
  static int sd_controlPause(sd_bus_message *m, void *userdata,
                             sd_bus_error *ret_error);
  static int sd_controlStop(sd_bus_message *m, void *userdata,
                            sd_bus_error *ret_error);
  static int sd_controlNext(sd_bus_message *m, void *userdata,
                            sd_bus_error *ret_error);
  static int sd_controlPrevious(sd_bus_message *m, void *userdata,
                                sd_bus_error *ret_error);
  static int sd_controlVolumeUp(sd_bus_message *m, void *userdata,
                                sd_bus_error *ret_error);
  static int sd_controlVolumeDown(sd_bus_message *m, void *userdata,
                                  sd_bus_error *ret_error);
  static int sd_controlFastForward(sd_bus_message *m, void *userdata,
                                   sd_bus_error *ret_error);
  static int sd_controlRewind(sd_bus_message *m, void *userdata,
                              sd_bus_error *ret_error);
  static int sd_controlPropertyGetConnected(sd_bus *bus, const char *path,
                                            const char *interface,
                                            const char *property,
                                            sd_bus_message *reply,
                                            void *userdata,
                                            sd_bus_error *ret_error);

private:
  bool getElementAttrAsync(Device *dev);
  void deviceConnectedHandler(const bt_bdaddr_t &bdAddr);
  void deviceDisconnectedHandler(const bt_bdaddr_t &bdAddr);
  void avrcpGetRcFeaturesHandler(const bt_bdaddr_t &bdAddr,
                                 btrc_remote_features_t features);

private:
  static AvrcpCTService *gAvrcpCTService;

  const btrc_ctrl_interface_t *m_btAvrcpIF;
  const btrc_ctrl_vendor_interface_t *m_btAvrcpVendorIF;

  std::map<bt_bdaddr_t, Device *> m_AvrcpDevices;
};

#endif  // AVRCPCTSERVICE_HPP
