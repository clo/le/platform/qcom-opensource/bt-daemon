/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "AvrcpTGService.hpp"

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <algorithm>

#include <list>
#include <map>

#include "Adapter.hpp"
#include "Common.hpp"
#include "Sdbus.hpp"

#define LOGTAG "AvrcpTG "



static btrc_callbacks_t sAvrcpTargetCbs = {
    sizeof(sAvrcpTargetCbs),
    AvrcpTGService::avrcpTgRcfeaturesCb,
    AvrcpTGService::avrcpTgGetplaystatusCb,
    AvrcpTGService::avrcpTgListplayerappAttrCb,
    AvrcpTGService::avrcpTgListplayerappValuesCb,
    AvrcpTGService::avrcpTgGetplayerappValueCb,
    nullptr,
    nullptr,
    AvrcpTGService::avrcpTgSetplayerappValueCb,
    AvrcpTGService::avrcpTgGetelemattrCb,
    AvrcpTGService::avrcpTgRegnotiCb,
    AvrcpTGService::avrcpTgVolchangedCb,
    AvrcpTGService::avrcpTgPassthroughCmdCb,  // impl
    AvrcpTGService::avrcpTgSetaddrplayerCmdCb,
    AvrcpTGService::avrcpTgSetbrowsedplayerCmdCb,
    AvrcpTGService::avrcpTgGetfolderitemsCmdCb,
    AvrcpTGService::avrcpTgChangePathCb,
    AvrcpTGService::avrcpTgGetItemAttrCb,
    AvrcpTGService::avrcpTgPlayItemCb,
    nullptr,
    nullptr,
    AvrcpTGService::avrcpTgAddToNowPlayingCb,
    AvrcpTGService::avrcpTgConnectionStateCb,  // impl
};

AvrcpTGService *AvrcpTGService::gAvrcpTGService = nullptr;

AvrcpTGService::AvrcpTGService(Adapter *pAdapter)
    : AvrcpProfileService(pAdapter, LOCAL_UUID, REMOTE_UUID),
      m_playStatus(BTRC_PLAYSTATE_STOPPED),
      m_btAvrcpIF(nullptr),
      m_btAvrcpVendorIF(nullptr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  gAvrcpTGService = this;
}

AvrcpTGService::~AvrcpTGService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  disableService();
  gAvrcpTGService = nullptr;
}

void AvrcpTGService::enableService(void) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  const bt_interface_t *pBtIf = getAdapter()->getBtInterface();
  if (!pBtIf) {
    return;
  }
  // AVRCP TG Initialization
  m_btAvrcpIF =
      (btrc_interface_t *)pBtIf->get_profile_interface(BT_PROFILE_AV_RC_ID);
  if (!m_btAvrcpIF) {
    return;
  }

  serviceState() = State::kInitialized;

  if (m_btAvrcpIF->init(&sAvrcpTargetCbs) == BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG " enableService int ctrl Success \n");
  }

  serviceState() = State::kRunning;
  ADK_LOG_NOTICE(LOGTAG " enableService Avrcp Success \n");
}

void AvrcpTGService::disableService(void) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  m_RegisteredEvents.clear();
  if (m_btAvrcpIF) {
    m_btAvrcpIF->cleanup();
    m_btAvrcpIF = nullptr;
  }
  if (m_btAvrcpVendorIF) {
    m_btAvrcpVendorIF->cleanup_vendor();
    m_btAvrcpVendorIF = nullptr;
  }

  while (!m_AvrcpDevices.empty()) {
    m_AvrcpDevices.erase(
        m_AvrcpDevices
            .begin());  // TODO: check that we are not leaking a Device
  }

  serviceState() = State::kStopped;
}

bool AvrcpTGService::setVolume(uint8_t volume) {
  if (m_btAvrcpIF) {
    return (m_btAvrcpIF->set_volume(volume));
  }
  return false;
}

bool AvrcpTGService::setDeviceMetadata(const Metadata &metadata) {
  // Current support is only for one device.
  // The device array should only contain a single device.
  for (auto device_entry : m_AvrcpDevices) {
    if (device_entry.second == nullptr) {
      return false;
    }
    device_entry.second->setMetadata(metadata);
  }

  if (gAvrcpTGService == nullptr) {
    return false;
  }
  auto it = std::find(metadata.tracklist_map.begin(),metadata.tracklist_map.end(),metadata.currentTrackId);
  if (it == metadata.tracklist_map.end()) {
    gAvrcpTGService->setCurrentTrackId(NO_TRACK_SELECTED);
  }
  else {
    gAvrcpTGService->setCurrentTrackId((long) std::distance(metadata.tracklist_map.begin(), it));
  }
  return sendTrackChangedNotification(gAvrcpTGService->getCurrentTrackId());
}

bool AvrcpTGService::sendTrackChangedNotification(long track_id)
{
  ADK_LOG_NOTICE(LOGTAG "%s -- track_id: %ld", __func__, track_id);
  if (gAvrcpTGService == nullptr){
    return false;
  }
  if (gAvrcpTGService->getTrackChangeNotiType() == BTRC_NOTIFICATION_TYPE_INTERIM) {
    gAvrcpTGService->setTrackChangeNotiType(BTRC_NOTIFICATION_TYPE_CHANGED);
    btrc_register_notification_t response_param;
    for (int i = 0; i < 8; ++i) {
        response_param.track[i] = (uint8_t) (track_id >> (56 - 8 * i));
    }
    auto res = m_btAvrcpIF->register_notification_rsp(BTRC_EVT_TRACK_CHANGE,
                                                      gAvrcpTGService->getTrackChangeNotiType(),
                                                      &response_param);
    if (res != BT_STATUS_SUCCESS) {
      return false;
    }
  }
  return true;
}

void AvrcpTGService::avrcpTgRcfeaturesCb(bt_bdaddr_t *bd_addr,
                                         btrc_remote_features_t features) {
  ADK_LOG_NOTICE(LOGTAG "%s: features %02x", __func__, features);
  struct CallbackData {
    btrc_remote_features_t features;
    bt_bdaddr_t bdAddr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{features, *bd_addr});
  queueBtCallback([data](void) -> void {
      gAvrcpTGService->avrcpTgRcfeaturesHandler(data->bdAddr,data->features);
  });
}

void AvrcpTGService::avrcpTgGetplaystatusCb(bt_bdaddr_t *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpTGService::avrcpTgListplayerappAttrCb(bt_bdaddr_t *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpTGService::avrcpTgListplayerappValuesCb(btrc_player_attr_t UNUSED(attr_id),
                                                  bt_bdaddr_t *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpTGService::avrcpTgGetplayerappValueCb(uint8_t UNUSED(num_attr),
                                                btrc_player_attr_t *UNUSED(p_attrs),
                                                bt_bdaddr_t *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpTGService::avrcpTgSetplayerappValueCb(btrc_player_settings_t *UNUSED(p_vals),
                                                bt_bdaddr_t *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

bool AvrcpTGService::CopyAttribute(uint8_t* attribute_value_array, const std::string &attribute){
  if (attribute.length()+1 > BTRC_MAX_ATTR_STR_LEN){
    return false;
  }
  std::size_t length = attribute.copy((char *)attribute_value_array,attribute.length());
  attribute_value_array[length]='\0';
  return true;
}

void AvrcpTGService::avrcpTgGetelemattrCb(uint8_t num_attr,
                                          btrc_media_attr_t *p_attrs,
                                          bt_bdaddr_t *bd_addr) {
  ADK_LOG_INFO(LOGTAG "%s", __func__);
  // Find the corresponding device
  const auto iter = gAvrcpTGService->m_AvrcpDevices.find(*bd_addr);
  if (iter == gAvrcpTGService->m_AvrcpDevices.end()) {
    ADK_LOG_ERROR(LOGTAG "%s -> Device not found!", __func__);
    return;
  }

  Metadata metadata = iter->second->getMetadata();
  std::vector<btrc_element_attr_val_t> attribute_array;
  ADK_LOG_INFO(LOGTAG "%s -> num_attr: %d", __func__, num_attr);

  for (int i = 0; i < num_attr; i++) {
    btrc_element_attr_val_t attribute;
    ADK_LOG_INFO(LOGTAG "%s -> attr_id: %d", __func__, p_attrs[i]);
    attribute.attr_id =  p_attrs[i];
    switch (p_attrs[i]) {
      case BTRC_MEDIA_ATTR_TITLE:{
        if (!CopyAttribute(attribute.text, metadata.title)){
          return;
        }
      }
      break;
      case BTRC_MEDIA_ATTR_ARTIST:{
        if (!CopyAttribute(attribute.text, metadata.artist)){
          return;
        }
      }
      break;
      case BTRC_MEDIA_ATTR_ALBUM:{
        if (!CopyAttribute(attribute.text, metadata.album)){
          return;
        }
      }
      break;
      case BTRC_MEDIA_ATTR_GENRE:{
        if (!CopyAttribute(attribute.text, metadata.genre)){
          return;
        }
      }
      break;
      case BTRC_MEDIA_ATTR_TRACK_NUM:{
        if (!CopyAttribute(attribute.text, std::to_string(metadata.trackNum))){
          return;
        }
      }
      break;
      case BTRC_MEDIA_ATTR_PLAYING_TIME:{
        if (!CopyAttribute(attribute.text, std::to_string(metadata.duration))){
          return;
        }
      }
      break;
      default: {
        if (!CopyAttribute(attribute.text, "")) {
          return;
        }
      }
      break;
    }
    ADK_LOG_DEBUG(LOGTAG "%s -> Attribute:%s", __func__,attribute.text );
    attribute_array.push_back(attribute);
  }
  gAvrcpTGService->m_btAvrcpIF->get_element_attr_rsp(bd_addr,num_attr,attribute_array.data());
}

void AvrcpTGService::avrcpTgRegnotiCb(btrc_event_id_t event_id, uint32_t param,
                                      bt_bdaddr_t *bd_addr) {
  ADK_LOG_INFO(LOGTAG "%s --> event_id:%d, param: %d, bd_addr: %s", __func__, event_id, param, bd_addr->ToString().c_str());
  btrc_register_notification_t response_param;
  auto avrcp_interface = gAvrcpTGService->m_btAvrcpIF;
  if (avrcp_interface==nullptr){
    return;
  }
  switch (event_id)
  {
    case BTRC_EVT_PLAY_STATUS_CHANGED:{
    ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_PLAY_STATUS_CHANGED",__func__);
    if (gAvrcpTGService == nullptr){
        return;
    }
    gAvrcpTGService->registerEvent(BTRC_EVT_PLAY_STATUS_CHANGED, true);
    response_param.play_status = gAvrcpTGService->m_playStatus;
    avrcp_interface->register_notification_rsp(
            BTRC_EVT_PLAY_STATUS_CHANGED,
            BTRC_NOTIFICATION_TYPE_INTERIM, &response_param);
    }
    break;
    case BTRC_EVT_TRACK_CHANGE:{
      ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_TRACK_CHANGE",__func__);
      if (gAvrcpTGService == nullptr){
        return;
      }
      gAvrcpTGService->setTrackChangeNotiType( BTRC_NOTIFICATION_TYPE_INTERIM);
      gAvrcpTGService->setCurrentTrackId(TRACK_IS_SELECTED);
      for (int i = 0; i < 8; ++i) {
        response_param.track[i] = (uint8_t) (gAvrcpTGService->getCurrentTrackId() >> (56 - 8 * i));
      }
      avrcp_interface->register_notification_rsp(
              BTRC_EVT_TRACK_CHANGE,
              gAvrcpTGService->getTrackChangeNotiType(), &response_param);
    }
    break;
    case BTRC_EVT_TRACK_REACHED_END:{
    ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_TRACK_REACHED_END",__func__);
    }
    break;
    case BTRC_EVT_TRACK_REACHED_START:{
    ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_TRACK_REACHED_START",__func__);
    }
    break;
    case BTRC_EVT_PLAY_POS_CHANGED:{
    ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_PLAY_POS_CHANGED",__func__);
    }
    break;
    case BTRC_EVT_APP_SETTINGS_CHANGED:{
    ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_APP_SETTINGS_CHANGED",__func__);
    }
    break;
    case BTRC_EVT_NOW_PLAYING_CONTENT_CHANGED:{
    ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_NOW_PLAYING_CONTENT_CHANGED",__func__);
    }
    break;
    case BTRC_EVT_AVAL_PLAYER_CHANGE:{
    ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_AVAL_PLAYER_CHANGE",__func__);
    }
    break;
    case BTRC_EVT_ADDR_PLAYER_CHANGE:{
    ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_ADDR_PLAYER_CHANGE",__func__);
    }
    break;
    case BTRC_EVT_UIDS_CHANGED:{
    ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_UIDS_CHANGED",__func__);
    }
    break;
    case BTRC_EVT_VOL_CHANGED:{
    ADK_LOG_DEBUG(LOGTAG "%s --> BTRC_EVT_VOL_CHANGED",__func__);
    }
    break;
  }
}


void AvrcpTGService::avrcpTgVolchangedCb(uint8_t volume, uint8_t ctype,
                                         bt_bdaddr_t *bd_addr) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  struct CallbackData {
    bt_bdaddr_t bd_addr;
    uint8_t abs_vol;
    uint8_t ctype;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{*bd_addr, volume, ctype});
  queueBtCallback([data](void) -> void {
    const auto iter = gAvrcpTGService->m_AvrcpDevices.find(data->bd_addr);
    if (iter != gAvrcpTGService->m_AvrcpDevices.end()) {
      if((data->ctype == BTRC_RSP_ACCEPT)||(data->ctype == BTRC_RSP_REJ)) {
        iter->second->setAbsVolInprogress(false);
      }
      iter->second->setVolumeFromBt(data->abs_vol);
    }
  });
}

void AvrcpTGService::avrcpTgPassthroughCmdCb(int id, int keyState,
                                             bt_bdaddr_t *bdAddr) {
  ADK_LOG_NOTICE(LOGTAG "%s id = %d keyState = %d\n", __func__, id, keyState);
  struct CallbackData {
    int id;
    int keyState;
    bt_bdaddr_t bdAddr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{id, keyState, *bdAddr});
  queueBtCallback([data](void) -> void {

    // TODO: need check on bdAddr ??
    int id = data->id;

    if ((id == CMD_ID_MUTED) && (data->keyState == KEY_RELEASED)) {
      const auto iter = gAvrcpTGService->m_AvrcpDevices.find(data->bdAddr);
      if (iter != gAvrcpTGService->m_AvrcpDevices.end()) {
        iter->second->setMutedFromBt();
      }
      return;
    }

    if ((id == CMD_ID_POWER) && (data->keyState == KEY_RELEASED)) {
      ADK_LOG_DEBUG(LOGTAG "CMD_ID_POWER\n");
      return;
    }

    // Send Command on Key released only
    if ((gAvrcpTGService->m_mprisPlayer) && (data->keyState == KEY_RELEASED)) {
      if (id == CMD_ID_VOL_UP) {
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_VOL_UP\n");
      } else if (id == CMD_ID_VOL_DOWN) {
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_VOL_DOWN\n");
      } else if (id == CMD_ID_PLAY) {
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_PLAY\n");
        gAvrcpTGService->m_mprisPlayer->Resume();
        gAvrcpTGService->m_mprisPlayer->Play();
      } else if (id == CMD_ID_STOP) {
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_STOP\n");
        gAvrcpTGService->m_mprisPlayer->Resume();
        gAvrcpTGService->m_mprisPlayer->Stop();
      } else if (id == CMD_ID_PAUSE) {
        gAvrcpTGService->m_mprisPlayer->Resume();
        gAvrcpTGService->m_mprisPlayer->Pause();
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_PAUSE\n");
      } else if (id == CMD_ID_REWIND) {
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_REWIND\n");
      } else if (id == CMD_ID_FF) {
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_FF\n");
      } else if (id == CMD_ID_FORWARD) {
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_FORWARD\n");
        gAvrcpTGService->m_mprisPlayer->Resume();
        gAvrcpTGService->m_mprisPlayer->Next();
      } else if (id == CMD_ID_BACKWARD) {
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_BACKWARD\n");
        gAvrcpTGService->m_mprisPlayer->Resume();
        gAvrcpTGService->m_mprisPlayer->Previous();
      }
    }
    if ((gAvrcpTGService->m_mprisPlayer) && (data->keyState == KEY_PRESSED)) {
      if (id == CMD_ID_REWIND) {
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_REWIND\n");
        gAvrcpTGService->m_mprisPlayer->Rewind();
      } else if (id == CMD_ID_FF) {
        ADK_LOG_DEBUG(LOGTAG "CMD_ID_FF\n");
        gAvrcpTGService->m_mprisPlayer->Fastforward();
      }
    }
  });
}

void AvrcpTGService::avrcpTgSetaddrplayerCmdCb(uint16_t UNUSED(player_id),
                                               bt_bdaddr_t *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpTGService::avrcpTgSetbrowsedplayerCmdCb(uint16_t UNUSED(player_id),
                                                  bt_bdaddr_t *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpTGService::avrcpTgGetfolderitemsCmdCb(
    uint8_t UNUSED(scope), uint32_t UNUSED(start_item), uint32_t UNUSED(end_item), uint8_t UNUSED(num_attr),
    uint32_t *UNUSED(p_attr_ids), uint16_t UNUSED(size), RawAddress *UNUSED(bd_addr)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

void AvrcpTGService::avrcpTgChangePathCb(uint8_t UNUSED(direction), uint8_t *UNUSED(folder_uid),
                                         RawAddress *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
  ;
}

void AvrcpTGService::avrcpTgGetItemAttrCb(uint8_t UNUSED(scope), uint8_t *UNUSED(uid),
                                          uint16_t UNUSED(uid_counter),
                                          uint8_t UNUSED(num_attr),
                                          btrc_media_attr_t *UNUSED(p_attrs),
                                          RawAddress *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpTGService::avrcpTgPlayItemCb(uint8_t UNUSED(scope), uint16_t UNUSED(uid_counter),
                                       uint8_t *UNUSED(uid), RawAddress *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpTGService::avrcpTgAddToNowPlayingCb(uint8_t UNUSED(scope), uint8_t *UNUSED(uid),
                                              uint16_t UNUSED(uid_counter),
                                              RawAddress *UNUSED(bd_addr)) {
  ADK_LOG_INFO(LOGTAG "%s -- Unsupported", __func__);
}

void AvrcpTGService::avrcpTgConnectionStateCb(bool state, bt_bdaddr_t *bdAddr) {
  ADK_LOG_NOTICE(LOGTAG "%s state %d\n", __func__, state);
  struct CallbackData {
    bool state;
    bt_bdaddr_t bdAddr;
  };
  std::shared_ptr<CallbackData> data =
      std::make_shared<CallbackData>(CallbackData{state, *bdAddr});
  queueBtCallback([data](void) -> void {
    if (data->state) {
      gAvrcpTGService->deviceConnectedHandler(data->bdAddr);
    } else {
      gAvrcpTGService->deviceDisconnectedHandler(data->bdAddr);
    }
  });
}

void AvrcpTGService::deviceConnectedHandler(const bt_bdaddr_t &bdAddr) {
  Device *pAvrcpDev = nullptr;
  ADK_LOG_NOTICE(LOGTAG " deviceConnectedHandler \n");
  if (m_AvrcpDevices.find(bdAddr) != m_AvrcpDevices.end()) {
    ADK_LOG_NOTICE(LOGTAG " Device Already Connected\n");
  } else {
    // Add the device object reference to avrcp connected device list
    pAvrcpDev = getAdapter()->getDevice(bdAddr);
    if (pAvrcpDev) {
      m_AvrcpDevices.insert({bdAddr, pAvrcpDev});
    }
  }
}

void AvrcpTGService::deviceDisconnectedHandler(const bt_bdaddr_t &bdAddr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  const auto &deviceIter = m_AvrcpDevices.find(bdAddr);
  if (deviceIter != m_AvrcpDevices.end()) {
    ADK_LOG_NOTICE(LOGTAG " Device is part of the connected list\n");
    Device *pAvrcpDev = deviceIter->second;
    pAvrcpDev->setAbsVolumeSupport(false);
    pAvrcpDev->setAbsVolInprogress(false);
    m_AvrcpDevices.erase(
        deviceIter);  // TODO: check that we are not leaking a Device
  }
  m_RegisteredEvents.clear();
}

bool AvrcpTGService::setPlaybackStatus(const std::string &playback_status)
{
  if(playback_status.compare("Playing") == 0) {
    m_playStatus = BTRC_PLAYSTATE_PLAYING;
  } else if(playback_status.compare("Paused") == 0) {
    m_playStatus = BTRC_PLAYSTATE_PAUSED;
  } else if(playback_status.compare("Stopped") == 0) {
    m_playStatus = BTRC_PLAYSTATE_STOPPED;
  } else {
    return false;
  }
  sendPlayStatusNotification(m_playStatus);
  return true;
}

bool AvrcpTGService::sendPlayStatusNotification(btrc_play_status_t play_status)
{
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (!isEventRegistered(BTRC_EVT_PLAY_STATUS_CHANGED)) {
    ADK_LOG_NOTICE(LOGTAG "BTRC_EVT_PLAY_STATUS_CHANGED is not registered\n");
    return false;
  }
  int res = BT_STATUS_FAIL;
  if (m_btAvrcpIF) {
    btrc_register_notification_t response_param;
    response_param.play_status = play_status;
    res = m_btAvrcpIF->register_notification_rsp(BTRC_EVT_PLAY_STATUS_CHANGED,
                                                      BTRC_NOTIFICATION_TYPE_CHANGED,
                                                      &response_param);
  }
  if (res != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG "Failed to send play status notification. Response: %d\n", res);
    return false;
  }
  return true;
}

void AvrcpTGService::avrcpTgRcfeaturesHandler(const bt_bdaddr_t &bdAddr,
                                              btrc_remote_features_t features) {
  const auto &deviceIter = m_AvrcpDevices.find(bdAddr);
  if (deviceIter != m_AvrcpDevices.end()) {
    Device *pAvrcpDev = deviceIter->second;
    if ((features & BTRC_FEAT_ABSOLUTE_VOLUME) != 0) {
      ADK_LOG_NOTICE(LOGTAG "%s, Abs vol supported for dev ");
      pAvrcpDev->setAbsVolumeSupport(true);
    } else {
      ADK_LOG_NOTICE(LOGTAG "%s, Abs vol NOT supported for dev ");
      pAvrcpDev->setAbsVolumeSupport(false);
    }
  }
  else{
      ADK_LOG_NOTICE(LOGTAG "%s, Device is not part of connected list ");
  }
}

void AvrcpTGService::registerEvent(btrc_event_id_t event, bool registered) {
  ADK_LOG_DEBUG(LOGTAG "%s\n", __func__);
  m_RegisteredEvents[event] = registered;
}

bool AvrcpTGService::isEventRegistered(btrc_event_id_t event) {
  ADK_LOG_DEBUG(LOGTAG "%s\n", __func__);
  std::map<int,bool>::iterator it;

  it = m_RegisteredEvents.find(event);
  if (it == m_RegisteredEvents.end()) {
    return false;
  }
  return it->second;
}
