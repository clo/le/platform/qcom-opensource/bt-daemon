/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef AVRCPTGSERVICE_HPP
#define AVRCPTGSERVICE_HPP

#include <hardware/bluetooth.h>
#include <hardware/bt_rc.h>
#include <hardware/bt_rc_vendor.h>
#include <systemdq/sd-bus.h>

#include "Device.hpp"
#include "ProfileService.hpp"

class MediaMPRISPlayer;

class AvrcpTGService final : public AvrcpProfileService {
 public:
  static constexpr const char *REMOTE_UUID =
      "0000110c-0000-1000-8000-00805f9b34fb";  // aka AVRCP Target (TG)
  static constexpr const char *LOCAL_UUID =
      "0000110e-0000-1000-8000-00805f9b34fb";  // aka AVRCP Remote (RC/CT)

  AvrcpTGService(Adapter *pAdapter);
  ~AvrcpTGService();

  // From AvrcpProfileService
  void enableService();
  void disableService();
  bool setVolume(uint8_t volume);

  bool setDeviceMetadata(const Metadata &metadata);

  void setCurrentTrackId(long track_id){mCurrentTrackID = track_id; }
  void setTrackChangeNotiType(btrc_notification_type_t notification_type){mTrackChangeNotiType = notification_type; }
  long getCurrentTrackId() const { return mCurrentTrackID;}
  btrc_notification_type_t getTrackChangeNotiType() const { return mTrackChangeNotiType;}

  bool setPlaybackStatus(const std::string &playback_status);

  void registerMPRISPlayer(MediaMPRISPlayer *player) { m_mprisPlayer = player; }
  void unregisterMPRISPlayer(MediaMPRISPlayer *UNUSED(player)) { m_mprisPlayer = nullptr; }

  // Fluoride avrcp TG callbacks
  static void avrcpTgRcfeaturesCb(bt_bdaddr_t *bd_addr,
                                  btrc_remote_features_t features);
  static void avrcpTgGetplaystatusCb(bt_bdaddr_t *bd_addr);
  static void avrcpTgListplayerappAttrCb(bt_bdaddr_t *bd_addr);
  static void avrcpTgListplayerappValuesCb(btrc_player_attr_t attr_id,
                                           bt_bdaddr_t *bd_addr);
  static void avrcpTgGetplayerappValueCb(uint8_t num_attr,
                                         btrc_player_attr_t *p_attrs,
                                         bt_bdaddr_t *bd_addr);
  static void avrcpTgSetplayerappValueCb(btrc_player_settings_t *p_vals,
                                         bt_bdaddr_t *bd_addr);
  static void avrcpTgGetelemattrCb(uint8_t num_attr, btrc_media_attr_t *p_attrs,
                                   bt_bdaddr_t *bd_addr);
  static void avrcpTgRegnotiCb(btrc_event_id_t event_id, uint32_t param,
                               bt_bdaddr_t *bd_addr);
  static void avrcpTgVolchangedCb(uint8_t volume, uint8_t ctype,
                                  bt_bdaddr_t *bd_addr);
  static void avrcpTgPassthroughCmdCb(int id, int key_state,
                                      bt_bdaddr_t *bd_addr);
  static void avrcpTgSetaddrplayerCmdCb(uint16_t player_id,
                                        bt_bdaddr_t *bd_addr);
  static void avrcpTgSetbrowsedplayerCmdCb(uint16_t player_id,
                                           bt_bdaddr_t *bd_addr);
  static void avrcpTgGetfolderitemsCmdCb(uint8_t scope, uint32_t start_item,
                                         uint32_t end_item, uint8_t num_attr,
                                         uint32_t *p_attr_ids, uint16_t size,
                                         RawAddress *bd_addr);
  static void avrcpTgChangePathCb(uint8_t direction, uint8_t *folder_uid,
                                  RawAddress *bd_addr);
  static void avrcpTgGetItemAttrCb(uint8_t scope, uint8_t *uid,
                                   uint16_t uid_counter, uint8_t num_attr,
                                   btrc_media_attr_t *p_attrs,
                                   RawAddress *bd_addr);
  static void avrcpTgPlayItemCb(uint8_t scope, uint16_t uid_counter,
                                uint8_t *uid, RawAddress *bd_addr);
  static void avrcpTgAddToNowPlayingCb(uint8_t scope, uint8_t *uid,
                                       uint16_t uid_counter,
                                       RawAddress *bd_addr);
  static void avrcpTgConnectionStateCb(bool state, bt_bdaddr_t *bd_addr);

private:
  static const long NO_TRACK_SELECTED = -1L;
  static const long TRACK_IS_SELECTED = 0L;

  void deviceConnectedHandler(const bt_bdaddr_t &bdAddr);
  void deviceDisconnectedHandler(const bt_bdaddr_t &bdAddr);
  void avrcpTgRcfeaturesHandler(const bt_bdaddr_t &bdAddr,
                                btrc_remote_features_t features);
  static bool CopyAttribute(uint8_t* attribute_value_array, const std::string &attribute);
  bool sendTrackChangedNotification(long track_id);
  bool sendPlayStatusNotification(btrc_play_status_t play_status);
private:
  static AvrcpTGService *gAvrcpTGService;

  btrc_play_status_t m_playStatus;

  long mCurrentTrackID;
  btrc_notification_type_t mTrackChangeNotiType;

  const btrc_interface_t *m_btAvrcpIF;
  const btrc_vendor_interface_t *m_btAvrcpVendorIF;

  std::map<bt_bdaddr_t, Device *> m_AvrcpDevices;
  // TODO:  could there be a list of players?
  MediaMPRISPlayer *m_mprisPlayer;

  void registerEvent(btrc_event_id_t event, bool registered);
  bool isEventRegistered(btrc_event_id_t event);
  std::map<int, bool> m_RegisteredEvents;
};

#endif  // AVRCPTGSERVICE_HPP
