/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "BleService.hpp"
#include <base/bind.h>
#include "Adapter.hpp"
#include "Common.hpp"
#include "hardware/ble_advertiser.h"
#include "GattLibService.hpp"


#define LOGTAG "BleService "

using namespace gatt;

static RawAddress str2addr(string address) {
  RawAddress bd_addr;
  RawAddress::FromString(std::string(address), bd_addr);
  return bd_addr;
}

static string* addr2Str(RawAddress address) {
  return new string(address.ToString());
}

///////////////////////////////////////////////////////////////////////////////////////
// NOTE: BleService will use GattLibService singleton in the le-gatt-service library //
//       in order to propagate the callbacks received from Fluoride                  //
///////////////////////////////////////////////////////////////////////////////////////

void BleService::btgattc_register_app_cb(int status, int clientIf, const bluetooth::Uuid& app_uuid) {
    ADK_LOG_NOTICE(LOGTAG " (%s) status = %d, clientIf = %d, app_uuid = %s",
      __FUNCTION__, status, clientIf, app_uuid.ToString().c_str());

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_REGISTER_APP_EVENT;
  event->gattc_register_app_event.status = status;
  event->gattc_register_app_event.clientIf = clientIf;
  event->gattc_register_app_event.app_uuid = bt::Uuid::From128BitBE(app_uuid.To128BitBE());

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_open_cb(int conn_id, int status, int clientIf,
                     const RawAddress& bda) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id: %d status: %d client_if: %d",
    __FUNCTION__, conn_id, status, clientIf);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_OPEN_EVENT;
  event->gattc_open_event.status= status;
  event->gattc_open_event.clientIf= clientIf;
  event->gattc_open_event.conn_id= conn_id;
  event->gattc_open_event.bda = addr2Str(bda);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_close_cb(int conn_id, int status, int clientIf,
                      const RawAddress& bda) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id: %d status: %d client_if: %d",
      __FUNCTION__, conn_id, status, clientIf);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_CLOSE_EVENT;
  event->gattc_close_event.status= status;
  event->gattc_close_event.clientIf= clientIf;
  event->gattc_close_event.conn_id= conn_id;
  event->gattc_close_event.bda = addr2Str(bda);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_search_complete_cb(int conn_id, int status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status: %d conn_id: %d",__FUNCTION__,status, conn_id);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_SEARCH_COMPLETE_EVENT;
  event->gattc_search_complete_event.status= status;
  event->gattc_search_complete_event.conn_id= conn_id;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_register_for_notification_cb(int conn_id, int registered,
                                          int status, uint16_t handle) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id: %d status: %d registered: %d, handle: %d",
      __FUNCTION__, conn_id, status, registered, handle);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_REGISTER_FOR_NOTIFICATION_EVENT;
  event->gattc_register_for_notification_event.status= status;
  event->gattc_register_for_notification_event.conn_id= conn_id;
  event->gattc_register_for_notification_event.registered= registered;
  event->gattc_register_for_notification_event.handle= handle;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_notify_cb(int conn_id, const btgatt_notify_params_t& p_data) {

  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id : %d",__FUNCTION__,conn_id);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_NOTIFY_EVENT;
  event->gattc_notify_event.conn_id= conn_id;
  std::memcpy(&event->gattc_notify_event.p_data, &p_data,sizeof(btgatt_notify_params_t));

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_read_characteristic_cb(int conn_id, int status,
                                    btgatt_read_params_t* p_data) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status: %d conn_id: %d",__FUNCTION__,status, conn_id);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_READ_CHARACTERISTIC_EVENT;
  event->gattc_read_characteristic_event.status= status;
  event->gattc_read_characteristic_event.conn_id= conn_id;

  if (status == 0 && p_data != NULL) {
    std::memcpy(&event->gattc_read_characteristic_event.p_data,
        p_data,sizeof(btgatt_read_params_t));
  }

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_write_characteristic_cb(int conn_id, int status, uint16_t handle) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d conn_id : %d, handle: %d",__FUNCTION__,status, conn_id, handle);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_WRITE_CHARACTERISTIC_EVENT;
  event->gattc_write_characteristic_event.status= status;
  event->gattc_write_characteristic_event.conn_id= conn_id;
  event->gattc_write_characteristic_event.handle= handle;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_execute_write_cb(int conn_id, int status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d conn_id : %d",__FUNCTION__,status, conn_id);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_EXECUTE_WRITE_EVENT;
  event->gattc_execute_write_event.status= status;
  event->gattc_execute_write_event.conn_id= conn_id;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_read_descriptor_cb(int conn_id, int status,
                                const btgatt_read_params_t& p_data) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d conn_id : %d",__FUNCTION__, status, conn_id);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_READ_DESCRIPTOR_EVENT;
  event->gattc_read_descriptor_event.status= status;
  event->gattc_read_descriptor_event.conn_id= conn_id;

  if (status == 0 && p_data.value.len != 0) {
    std::memcpy(&event->gattc_read_descriptor_event.p_data, &p_data,sizeof(btgatt_read_params_t));
  }

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_write_descriptor_cb(int conn_id, int status, uint16_t handle) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d conn_id : %d, handle: %d",__FUNCTION__,status, conn_id, handle);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_WRITE_DESCRIPTOR_EVENT;
  event->gattc_write_descriptor_event.status= status;
  event->gattc_write_descriptor_event.conn_id= conn_id;
  event->gattc_write_descriptor_event.handle= handle;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_remote_rssi_cb(int client_if, const RawAddress& bda, int rssi,
                            int status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d client_if : %d, rssi: %d",
    __FUNCTION__,status, client_if, rssi);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_REMOTE_RSSI_EVENT;
  event->gattc_remote_rssi_event.status = status;
  event->gattc_remote_rssi_event.client_if= client_if;
  event->gattc_remote_rssi_event.rssi= rssi;
  event->gattc_remote_rssi_event.bda = addr2Str(bda);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_configure_mtu_cb(int conn_id, int status, int mtu) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d conn_id : %d, mtu: %d",
    __FUNCTION__, status, conn_id, mtu);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_CONFIGURE_MTU_EVENT;
  event->gattc_configure_mtu_event.status= status;
  event->gattc_configure_mtu_event.conn_id= conn_id;
  event->gattc_configure_mtu_event.mtu= mtu;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_congestion_cb(int conn_id, bool congested) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id : %d congested : %d",__FUNCTION__, conn_id, congested);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_CONGESTION_EVENT;
  event->gattc_congestion_event.conn_id= conn_id;
  event->gattc_congestion_event.congested= congested;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_get_gatt_db_cb(int conn_id, const btgatt_db_element_t* db,
                            int count) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id: %d, count: %d",__FUNCTION__,conn_id, count);

  BtEvent *event = new BtEvent;
  btgatt_db_element_t *tmp_db = new btgatt_db_element_t[count];
  std::memcpy(tmp_db, db, sizeof(btgatt_db_element_t) * count);

  event->event_id = BTGATTC_GET_GATT_DB_EVENT;
  event->gattc_get_gatt_db_event.conn_id = conn_id;
  event->gattc_get_gatt_db_event.count = count;
  event->gattc_get_gatt_db_event.db = (gatt::gatt_db_element_t *)tmp_db;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_phy_updated_cb(int conn_id, uint8_t tx_phy, uint8_t rx_phy,
                            uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id: %d tx_phy: %d, rx_phy: %d, status: %d",
      __FUNCTION__,conn_id, tx_phy, rx_phy, status);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_PHY_UPDATED_EVENT;
  event->gattc_phy_updated_event.conn_id = conn_id;
  event->gattc_phy_updated_event.tx_phy = tx_phy;
  event->gattc_phy_updated_event.rx_phy = rx_phy;
  event->gattc_phy_updated_event.status= status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgattc_conn_updated_cb(int conn_id, uint16_t interval, uint16_t latency,
                             uint16_t timeout, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id: %d interval: %d, latency: %d, timeout: %d status: %d",
      __FUNCTION__,conn_id, interval, latency, timeout, status);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_CONN_UPDATED_EVENT;
  event->gattc_conn_updated_event.conn_id = conn_id;
  event->gattc_conn_updated_event.interval = interval;
  event->gattc_conn_updated_event.latency = latency;
  event->gattc_conn_updated_event.timeout = timeout;
  event->gattc_conn_updated_event.status = status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::readClientPhyCb(uint8_t clientIf, RawAddress bda, uint8_t tx_phy,
                            uint8_t rx_phy, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) clientIf: %d, bda: %s, tx_phy: %d, rx_phy: %d, status: %d",
      __FUNCTION__,clientIf,  bda.ToString().c_str(), tx_phy, rx_phy, status);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTC_READ_PHY_EVENT;
  event->gattc_read_phy_event.clientIf= clientIf;
  event->gattc_read_phy_event.tx_phy = tx_phy;
  event->gattc_read_phy_event.rx_phy = rx_phy;
  event->gattc_read_phy_event.status= status;
  event->gattc_read_phy_event.bda = addr2Str(bda);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}


// /**
//  * BTA server callbacks
//  */

void BleService::btgatts_register_app_cb(int status, int server_if, const bluetooth::Uuid& uuid) {
  ADK_LOG_NOTICE(LOGTAG "(%s) server_if: %d status: %d", __FUNCTION__,server_if, status);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_REGISTER_APP_EVENT;
  event->gatts_register_app_event.status = status;
  event->gatts_register_app_event.server_if = server_if;
  // event->gatts_register_app_event.uuid = bluetoothUuid2btUuid(uuid);
  event->gatts_register_app_event.uuid = bt::Uuid::From128BitBE(uuid.To128BitBE());

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  };
}

void BleService::btgatts_connection_cb(int conn_id, int server_if, int connected,
                           const RawAddress& bda) {
  ADK_LOG_NOTICE(LOGTAG "(%s) connid : %d server_if : %d status : %d bda (%s)",__FUNCTION__, conn_id,
      server_if, connected, bda.ToString().c_str());

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_CONNECTION_EVENT;
  event->gatts_connection_event.conn_id = conn_id;
  event->gatts_connection_event.server_if = server_if;
  event->gatts_connection_event.connected = connected;
  event->gatts_connection_event.bda = addr2Str(bda);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_service_added_cb(int status, int server_if,
                              std::vector<btgatt_db_element_t> service) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d server_if : %d",__FUNCTION__,
      status, server_if);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_SERVICE_ADDED_EVENT;
  event->gatts_service_added_event.status = status;
  event->gatts_service_added_event.server_if = server_if;
  event->gatts_service_added_event.service = new std::vector<gatt_db_element_t>();
  for(size_t i = 0; i < service.size(); i++)
  {
    btgatt_db_element_t temp = service.at(i);
    gatt_db_element_t temp1;
    temp1.id = temp.id;
    temp1.type = (gatt_db_attribute_type_t)temp.type;
    temp1.permissions = temp.permissions;
    temp1.properties = temp.properties;
    temp1.attribute_handle = temp.attribute_handle;
    temp1.start_handle = temp.start_handle;
    temp1.end_handle = temp.end_handle;
    // temp1.uuid = bluetoothUuid2btUuid(temp.uuid);
    temp1.uuid = bt::Uuid::From128BitBE(temp.uuid.To128BitBE());
    event->gatts_service_added_event.service->push_back(temp1);
  }

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_service_stopped_cb(int status, int server_if, int srvc_handle) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status:: %d, server_if: %d, srvc_handle: %d",
      __FUNCTION__, status, server_if, srvc_handle);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_SERVICE_STOPPED_EVENT;
  event->gatts_service_stopped_event.status = status;
  event->gatts_service_stopped_event.server_if = server_if;
  event->gatts_service_stopped_event.srvc_handle = srvc_handle ;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_service_deleted_cb(int status, int server_if, int srvc_handle) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status:: %d, server_if: %d, srvc_handle: %d",
      __FUNCTION__, status, server_if, srvc_handle);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_SERVICE_DELETED_EVENT;
  event->gatts_service_deleted_event.status = status;
  event->gatts_service_deleted_event.server_if = server_if;
  event->gatts_service_deleted_event.srvc_handle = srvc_handle ;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_request_read_characteristic_cb(int conn_id, int trans_id,
                                            const RawAddress& bda,
                                            int attr_handle, int offset,
                                            bool is_long) {
  ADK_LOG_NOTICE(LOGTAG "(%s) connid: %d trans_id: %d, bda: %s, attr_handle: %d, offset:%d is_long: %d",
      __FUNCTION__, conn_id, trans_id, bda.ToString().c_str(), attr_handle, offset, is_long);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_REQUEST_READ_CHARACTERISTIC_EVENT;
  event->gatts_request_read_characteristic_event.conn_id = conn_id;
  event->gatts_request_read_characteristic_event.trans_id = trans_id;
  event->gatts_request_read_characteristic_event.bda = addr2Str(bda);
  event->gatts_request_read_characteristic_event.attr_handle = attr_handle;
  event->gatts_request_read_characteristic_event.offset = offset;
  event->gatts_request_read_characteristic_event.is_long = is_long ;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_request_read_descriptor_cb(int conn_id, int trans_id,
                                        const RawAddress& bda, int attr_handle,
                                        int offset, bool is_long) {
  ADK_LOG_NOTICE(LOGTAG "(%s) connid: %d trans_id: %d, bda: %s, attr_handle: %d, offset:%d is_long: %d",
      __FUNCTION__, conn_id, trans_id, bda.ToString().c_str(), attr_handle, offset, is_long);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_REQUEST_READ_DESCRIPTOR_EVENT;
  event->gatts_request_read_descriptor_event.conn_id = conn_id;
  event->gatts_request_read_descriptor_event.trans_id = trans_id;
  event->gatts_request_read_descriptor_event.bda = addr2Str(bda);
  event->gatts_request_read_descriptor_event.attr_handle = attr_handle;
  event->gatts_request_read_descriptor_event.offset = offset;
  event->gatts_request_read_descriptor_event.is_long = is_long ;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_request_write_characteristic_cb(int conn_id, int trans_id,
                                             const RawAddress& bda,
                                             int attr_handle, int offset,
                                             bool need_rsp, bool is_prep,
                                             std::vector<uint8_t> value) {
  ADK_LOG_NOTICE(LOGTAG "(%s) connid: %d trans_id: %d, bda: %s, attr_handle: %d, offset:%d need_rsp: %d,"
      " is_prep:%d", __FUNCTION__, conn_id, trans_id, bda.ToString().c_str(), attr_handle, offset,
      need_rsp, is_prep);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_REQUEST_WRITE_CHARACTERISTIC_EVENT;
  event->gatts_request_write_characteristic_event.conn_id = conn_id;
  event->gatts_request_write_characteristic_event.trans_id = trans_id;
  event->gatts_request_write_characteristic_event.bda = addr2Str(bda);
  event->gatts_request_write_characteristic_event.attr_handle = attr_handle;
  event->gatts_request_write_characteristic_event.offset = offset;
  event->gatts_request_write_characteristic_event.need_rsp = need_rsp;
  event->gatts_request_write_characteristic_event.is_prep = is_prep;
  event->gatts_request_write_characteristic_event.value = new std::vector<uint8_t>(value);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_request_write_descriptor_cb(int conn_id, int trans_id,
                                         const RawAddress& bda, int attr_handle,
                                         int offset, bool need_rsp,
                                         bool is_prep,
                                         std::vector<uint8_t> value) {
  ADK_LOG_NOTICE(LOGTAG "(%s) connid: %d trans_id: %d, bda: %s, attr_handle: %d, offset:%d need_rsp: %d,"
      " is_prep:%d", __FUNCTION__, conn_id, trans_id, bda.ToString().c_str(), attr_handle, offset,
      need_rsp, is_prep);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_REQUEST_WRITE_DESCRIPTOR_EVENT;
  event->gatts_request_write_descriptor_event.conn_id = conn_id;
  event->gatts_request_write_descriptor_event.trans_id = trans_id;
  event->gatts_request_write_descriptor_event.bda = addr2Str(bda);
  event->gatts_request_write_descriptor_event.attr_handle = attr_handle;
  event->gatts_request_write_descriptor_event.offset = offset;
  event->gatts_request_write_descriptor_event.need_rsp = need_rsp;
  event->gatts_request_write_descriptor_event.is_prep = is_prep;
  event->gatts_request_write_descriptor_event.value = new std::vector<uint8_t>(value);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_request_exec_write_cb(int conn_id, int trans_id,
                                   const RawAddress& bda, int exec_write) {
  ADK_LOG_NOTICE(LOGTAG "(%s) connid: %d trans_id: %d, bda: %s, exec_write: %d",
      __FUNCTION__, conn_id, trans_id, bda.ToString().c_str(), exec_write);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_REQUEST_EXEC_WRITE_EVENT;
  event->gatts_request_exec_write_event.conn_id = conn_id;
  event->gatts_request_exec_write_event.trans_id = trans_id;
  event->gatts_request_exec_write_event.bda = addr2Str(bda);;
  event->gatts_request_exec_write_event.exec_write = exec_write;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_response_confirmation_cb(int status, int handle) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status: %d handle: %d",__FUNCTION__, status, handle);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_RESPONSE_CONFIRMATION_EVENT;
  event->gatts_response_confirmation_event.status = status;
  event->gatts_response_confirmation_event.handle = handle ;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_indication_sent_cb(int conn_id, int status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id : %d status:: %d",__FUNCTION__, conn_id, status);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_INDICATION_SENT_EVENT;
  event->gatts_indication_sent_event.status = status;
  event->gatts_indication_sent_event.conn_id = conn_id ;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_congestion_cb(int conn_id, bool congested) {
  ADK_LOG_NOTICE(LOGTAG "(%s) contested: %d conn_id: %d",__FUNCTION__, congested, conn_id);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_CONGESTION_EVENT;
  event->gatts_congestion_event.congested = congested;
  event->gatts_congestion_event.conn_id = conn_id ;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_mtu_changed_cb(int conn_id, int mtu) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id: %d Mtu: %d",__FUNCTION__, conn_id, mtu);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_MTU_CHANGED_EVENT;
  event->gatts_mtu_changed_event.conn_id = conn_id;
  event->gatts_mtu_changed_event.mtu = mtu ;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_phy_updated_cb(int conn_id, uint8_t tx_phy, uint8_t rx_phy,
                            uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id: %d tx_phy: %d, rx_phy: %d, status: %d",
      __FUNCTION__,conn_id, tx_phy, rx_phy, status);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_PHY_UPDATED_EVENT;
  event->gatts_phy_updated_event.conn_id = conn_id;
  event->gatts_phy_updated_event.tx_phy = tx_phy;
  event->gatts_phy_updated_event.rx_phy = rx_phy;
  event->gatts_phy_updated_event.status= status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::btgatts_conn_updated_cb(int conn_id, uint16_t interval, uint16_t latency,
                             uint16_t timeout, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) conn_id: %d interval: %d, latency: %d, timeout: %d status: %d",
      __FUNCTION__,conn_id, interval, latency, timeout, status);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_CONN_UPDATED_EVENT;
  event->gatts_conn_updated_event.conn_id = conn_id;
  event->gatts_conn_updated_event.interval = interval;
  event->gatts_conn_updated_event.latency = latency;
  event->gatts_conn_updated_event.timeout = timeout;
  event->gatts_conn_updated_event.status = status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::readServerPhyCb(uint8_t serverIf, RawAddress bda, uint8_t tx_phy,
                            uint8_t rx_phy, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) serverIf: %d, bda: %s, tx_phy: %d, rx_phy: %d, status: %d",
      __FUNCTION__,serverIf,  bda.ToString().c_str(), tx_phy, rx_phy, status);

  BtEvent *event = new BtEvent;
  event->event_id = BTGATTS_READ_PHY_EVENT;
  event->gatts_read_phy_event.serverIf= serverIf;
  event->gatts_read_phy_event.tx_phy = tx_phy;
  event->gatts_read_phy_event.rx_phy = rx_phy;
  event->gatts_read_phy_event.status= status;
  event->gatts_read_phy_event.bda = addr2Str(bda);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

//scanner callbacks
void BleService::register_scanner_cb(const bluetooth::Uuid& app_uuid, 
                                uint8_t scannerId,
                                uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "%s scannerId: %d status: %d\n", __func__, scannerId, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_REGISTER_SCANNER_EVENT;
  // event->blescanner_register_scanner_event.app_uuid = bluetoothUuid2btUuid(app_uuid);
  event->blescanner_register_scanner_event.app_uuid = bt::Uuid::From128BitBE(app_uuid.To128BitBE());
  event->blescanner_register_scanner_event.scannerId = scannerId;
  event->blescanner_register_scanner_event.status = status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::scan_params_cmpl_cb(uint8_t client_if, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) client_if: %d status: %d",__FUNCTION__, client_if, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_SCAN_PARAMS_COMPLETE_EVENT;
  event->blescanner_scan_param_complete_event.client_if = client_if;
  event->blescanner_scan_param_complete_event.status = status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::scan_result_cb(uint16_t event_type, uint8_t addr_type,
                                RawAddress* bda, uint8_t primary_phy,
                                uint8_t secondary_phy, uint8_t advertising_sid,
                                int8_t tx_power, int8_t rssi,
                                uint16_t periodic_adv_int,
                                std::vector<uint8_t> adv_data) {

  ADK_LOG_NOTICE(LOGTAG "(%s) event_type:%d, addr_type: %d, bda: %s, primary_phy: %d, "
      "secondary_phy: %d, advertising_sid: %d, tx_power: %d, rssi: %d, periodic_adv_int:%d"
      ,__FUNCTION__, event_type, addr_type, (*bda).ToString().c_str(), primary_phy,
      secondary_phy, advertising_sid, tx_power, rssi, periodic_adv_int);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_SCAN_RESULT_EVENT;
  event->blescanner_scan_result_event.bda = addr2Str(*bda);
  event->blescanner_scan_result_event.rssi= rssi;
  event->blescanner_scan_result_event.event_type = event_type;
  event->blescanner_scan_result_event.addr_type = addr_type;
  event->blescanner_scan_result_event.primary_phy = primary_phy;
  event->blescanner_scan_result_event.secondary_phy = secondary_phy;
  event->blescanner_scan_result_event.tx_power = tx_power;
  event->blescanner_scan_result_event.periodic_adv_int = periodic_adv_int;
  event->blescanner_scan_result_event.adv_data = new std::vector<uint8_t>(adv_data);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::batchscan_reports_cb(int client_if, int status, int report_format,
                                      int num_records, std::vector<uint8_t> data) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d client_if : %d",__FUNCTION__,status, client_if);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_BATCHSCAN_REPORTS_EVENT;
  event->blescanner_batchscan_reports_event.status= status;
  event->blescanner_batchscan_reports_event.client_if= client_if;
  event->blescanner_batchscan_reports_event.report_format = report_format;
  event->blescanner_batchscan_reports_event.num_records= num_records;
  event->blescanner_batchscan_reports_event.data = new std::vector<uint8_t>(data);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::batchscan_threshold_cb(int client_if) {
  ADK_LOG_NOTICE(LOGTAG "(%s) client_if : %d", __FUNCTION__, client_if);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_BATCHSCAN_THRESHOLD_EVENT;
  event->blescanner_batchscan_threshold_event.client_if= client_if;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  };
}

void BleService::track_adv_event_cb(btgatt_track_adv_info_t* p_adv_track_info) {
  ADK_LOG_NOTICE(LOGTAG "(%s) ",__FUNCTION__);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_TRACK_ADV_EVENT_EVENT;
  event->blescanner_track_adv_event.p_adv_track_info.client_if = p_adv_track_info->client_if;
  event->blescanner_track_adv_event.p_adv_track_info.filt_index = p_adv_track_info->filt_index;
  event->blescanner_track_adv_event.p_adv_track_info.advertiser_state
      = p_adv_track_info->advertiser_state;
  event->blescanner_track_adv_event.p_adv_track_info.advertiser_info_present
      = p_adv_track_info->advertiser_info_present;
  event->blescanner_track_adv_event.p_adv_track_info.addr_type = p_adv_track_info->addr_type;
  event->blescanner_track_adv_event.p_adv_track_info.tx_power = p_adv_track_info->tx_power;
  event->blescanner_track_adv_event.p_adv_track_info.rssi_value = p_adv_track_info->rssi_value;
  event->blescanner_track_adv_event.p_adv_track_info.time_stamp = p_adv_track_info->time_stamp;
  event->blescanner_track_adv_event.p_adv_track_info.bd_addr = addr2Str(p_adv_track_info->bd_addr);

  if (p_adv_track_info->adv_pkt_len != 0) {
    event->blescanner_track_adv_event.p_adv_track_info.p_adv_pkt_data
        = new uint8_t[p_adv_track_info->adv_pkt_len];
    std::memcpy(event->blescanner_track_adv_event.p_adv_track_info.p_adv_pkt_data,
        p_adv_track_info->p_adv_pkt_data, p_adv_track_info->adv_pkt_len);
  } else {
    event->blescanner_track_adv_event.p_adv_track_info.p_adv_pkt_data = NULL;
    ADK_LOG_ERROR(LOGTAG "(%s) adv_pkt_len is 0",__FUNCTION__);
  }

  if (p_adv_track_info->scan_rsp_len != 0) {
    event->blescanner_track_adv_event.p_adv_track_info.p_scan_rsp_data
        = new uint8_t[p_adv_track_info->scan_rsp_len];
    std::memcpy(event->blescanner_track_adv_event.p_adv_track_info.p_scan_rsp_data,
        p_adv_track_info->p_scan_rsp_data , p_adv_track_info->scan_rsp_len);
  } else {
    event->blescanner_track_adv_event.p_adv_track_info.p_scan_rsp_data = NULL;
    ADK_LOG_ERROR(LOGTAG "(%s) scan_rsp_len is 0",__FUNCTION__);
  }

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::scan_filter_cfg_cb(uint8_t client_if, uint8_t filt_type,
                               uint8_t avbl_space, uint8_t action,
                               uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d client_if : %d, filt_type: %d, avbl_space: %d, action:%d",
      __FUNCTION__, status, client_if, filt_type, avbl_space, action);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_SCAN_FILTER_CFG_EVENT;
  event->blescanner_scan_filter_cfg_event.status= status;
  event->blescanner_scan_filter_cfg_event.action= action;
  event->blescanner_scan_filter_cfg_event.client_if= client_if;
  event->blescanner_scan_filter_cfg_event.filt_type=filt_type;
  event->blescanner_scan_filter_cfg_event.avbl_space=avbl_space;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::scan_filter_param_cb(uint8_t client_if, uint8_t avbl_space, uint8_t action,
                          uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d client_if : %d, avbl_space: %d, action:%d",
      __FUNCTION__, status, client_if, avbl_space, action);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_SCAN_FILTER_PARAM_EVENT;
  event->blescanner_scan_filter_param_event.status= status;
  event->blescanner_scan_filter_param_event.action= action;
  event->blescanner_scan_filter_param_event.client_if= client_if;
  event->blescanner_scan_filter_param_event.avbl_space=avbl_space;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  };
}

void BleService::scan_filter_status_cb(uint8_t client_if, uint8_t action, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d client_if : %d, action:%d",
      __FUNCTION__, status, client_if, action);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_SCAN_FILTER_STATUS_EVENT;
  event->blescanner_scan_filter_status_event.status= status;
  event->blescanner_scan_filter_status_event.action= action;
  event->blescanner_scan_filter_status_event.client_if= client_if;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::batchscan_cfg_storage_cb(uint8_t client_if, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d client_if : %d", __FUNCTION__, status, client_if);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_BATCHSCAN_CFG_STORAGE_EVENT;
  event->blescanner_batchscan_cfg_storage_event.status= status;
  event->blescanner_batchscan_cfg_storage_event.client_if= client_if;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::batchscan_start_cb(uint8_t client_if, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d client_if : %d", __FUNCTION__, status, client_if);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_BATCHSCAN_START_EVENT;
  event->blescanner_batchscan_start_event.status= status;
  event->blescanner_batchscan_start_event.client_if= client_if;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::batchscan_stop_cb(uint8_t client_if, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) status : %d client_if : %d",__FUNCTION__, status, client_if);

  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_BATCHSCAN_STOP_EVENT;
  event->blescanner_batchscan_stop_event.status= status;
  event->blescanner_batchscan_stop_event.client_if= client_if;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::onSyncStarted(int reg_id, uint8_t status, uint16_t sync_handle,
                          uint8_t sid, uint8_t address_type, RawAddress address,
                          uint8_t phy, uint16_t interval) {
  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_PERIODIC_ADVERTISING_SYNC_START_EVENT;
  event->blescanner_periodic_adv_sync_start_event.reg_id = reg_id;
  event->blescanner_periodic_adv_sync_start_event.status = status;
  event->blescanner_periodic_adv_sync_start_event.sync_handle = sync_handle;
  event->blescanner_periodic_adv_sync_start_event.sid = sid;
  event->blescanner_periodic_adv_sync_start_event.address_type = address_type;
  event->blescanner_periodic_adv_sync_start_event.bda = addr2Str(address);
  event->blescanner_periodic_adv_sync_start_event.phy = phy;
  event->blescanner_periodic_adv_sync_start_event.interval = interval;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::onSyncLost(uint16_t sync_handle) {
  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_PERIODIC_ADVERTISING_SYNC_LOST_EVENT;
  event->blescanner_periodic_adv_sync_lost_event.sync_handle= sync_handle;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}


void BleService::onSyncReport(uint16_t sync_handle, int8_t tx_power, int8_t rssi,
                         uint8_t data_status, std::vector<uint8_t> data) {
  BtEvent *event = new BtEvent;
  event->event_id = BLESCANNER_PERIODIC_ADVERTISING_SYNC_REPORT_EVENT;
  event->blescanner_periodic_adv_sync_report_event.sync_handle = sync_handle;
  event->blescanner_periodic_adv_sync_report_event.tx_power = tx_power;
  event->blescanner_periodic_adv_sync_report_event.rssi = rssi;
  event->blescanner_periodic_adv_sync_report_event.data_status = data_status;
  event->blescanner_periodic_adv_sync_report_event.data = new std::vector<uint8_t>(data);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}



void BleService::onSetAdvertisingData(uint8_t advertiser_id,
                            uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) advertiser_id: %d status: %d",__FUNCTION__, advertiser_id, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLEADVERTISER_SET_ADVERTISING_DATA_EVENT;
  event->bleadverister_set_adv_data_event.advertiser_id = advertiser_id;
  event->bleadverister_set_adv_data_event.status = status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::onSetScanResponseData(uint8_t advertiser_id,
                            uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) advertiser_id: %d status: %d",__FUNCTION__, advertiser_id, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLEADVERTISER_SET_SCAN_RESPONSE_DATA_EVENT;
  event->bleadverister_set_scan_resp_event.advertiser_id = advertiser_id;
  event->bleadverister_set_scan_resp_event.status = status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::onSetPeriodicAdvertisingParameters(uint8_t advertiser_id,
                            uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) advertiser_id: %d status: %d",__FUNCTION__, advertiser_id, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLEADVERTISER_SET_PERIODIC_ADVERTISING_PARAMETER_EVENT;
  event->bleadverister_set_periodic_adv_param_event.advertiser_id = advertiser_id;
  event->bleadverister_set_periodic_adv_param_event.status = status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::onSetPeriodicAdvertisingData(uint8_t advertiser_id,
                            uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) advertiser_id: %d status: %d",__FUNCTION__, advertiser_id, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLEADVERTISER_SET_PERIODIC_ADVERTISING_DATA_EVENT;
  event->bleadverister_set_periodic_adv_data_event.advertiser_id = advertiser_id;
  event->bleadverister_set_periodic_adv_data_event.status = status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::getOwnAddressCb(uint8_t advertiser_id, uint8_t address_type,
                            RawAddress address) {
  ADK_LOG_NOTICE(LOGTAG "(%s) advertiser_id: %d address_type: %d, address: %s",
      __FUNCTION__, advertiser_id, address_type, address.ToString().c_str());

  BtEvent *event = new BtEvent;
  event->event_id = BLEDAVERTISER_GET_OWN_ADDRESS_EVENT;
  event->bleadvertiser_get_own_address_event.advertiser_id = advertiser_id;
  event->bleadvertiser_get_own_address_event.address_type = address_type;
  event->bleadvertiser_get_own_address_event.bda = addr2Str(address);

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::ble_advertising_set_started_cb(int reg_id, uint8_t advertiser_id,
                                           int8_t tx_power, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) reg_id: %d, advertiser_id: %d, tx_power: %d, status: %d,",
      __FUNCTION__, reg_id, advertiser_id, tx_power, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLEDAVERTISER_ADVERTISING_SET_START_EVENT;
  event->bleadvertiser_adv_set_start_event.reg_id = reg_id;
  event->bleadvertiser_adv_set_start_event.advertiser_id = advertiser_id;
  event->bleadvertiser_adv_set_start_event.tx_power = tx_power;
  event->bleadvertiser_adv_set_start_event.status = status;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::ble_advertising_set_timeout_cb(uint8_t advertiser_id,
                                           uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) advertiser_id: %d status: %d",__FUNCTION__, advertiser_id, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLEDAVERTISER_ADVERTISING_SET_ENABLE_EVENT;
  event->bleadvertiser_adv_set_enable_event.advertiser_id = advertiser_id;
  event->bleadvertiser_adv_set_enable_event.status = status;
  event->bleadvertiser_adv_set_enable_event.isEnabled = false;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::ble_advertising_set_enable_Cb(uint8_t advertiser_id, bool enable, uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) advertiser_id: %d enable: %d, status: %d",__FUNCTION__, advertiser_id, enable, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLEDAVERTISER_ADVERTISING_SET_ENABLE_EVENT;
  event->bleadvertiser_adv_set_enable_event.advertiser_id = advertiser_id;
  event->bleadvertiser_adv_set_enable_event.status = status;
  event->bleadvertiser_adv_set_enable_event.isEnabled = enable;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::ble_advertising_parameters_updated_cb(uint8_t advertiser_id,
                                             uint8_t status, int8_t tx_power) {
  ADK_LOG_NOTICE(LOGTAG "(%s) advertiser_id: %d, tx_power: %d, status: %d,",
    __FUNCTION__, advertiser_id, tx_power, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLEADVERTISER_ADVERTISING_PARAMETER_UPDATED_EVENT;
  event->bleadvertiser_adv_set_param_update_event.advertiser_id = advertiser_id;
  event->bleadvertiser_adv_set_param_update_event.status = status;
  event->bleadvertiser_adv_set_param_update_event.tx_power= tx_power;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

void BleService::ble_periodic_advertising_set_enable_Cb(uint8_t advertiser_id, bool enable,
                                uint8_t status) {
  ADK_LOG_NOTICE(LOGTAG "(%s) advertiser_id: %d enable: %d, status: %d",
      __FUNCTION__, advertiser_id, enable, status);

  BtEvent *event = new BtEvent;
  event->event_id = BLEDAVERTISER_PERIODIC_ADVERTISING_SET_ENABLE_EVENT;
  event->bleadverister_periodic_adv_set_enable_event.advertiser_id = advertiser_id;
  event->bleadverister_periodic_adv_set_enable_event.status = status;
  event->bleadverister_periodic_adv_set_enable_event.isEnabled = enable;

  auto *sGattLibService = gatt::GattLibService::getGatt();
  if (sGattLibService) {
    sGattLibService->NativeEvent(event);
  }
}

/**
 * GATT callbacks
 */
static const btgatt_scanner_callbacks_t sGattScannerCallbacks = {
    BleService::scan_result_cb,
    BleService::batchscan_reports_cb,
    BleService::batchscan_threshold_cb,
    BleService::track_adv_event_cb
};

static const btgatt_client_callbacks_t sGattClientCallbacks = {
    BleService::btgattc_register_app_cb,
    BleService::btgattc_open_cb,
    BleService::btgattc_close_cb,
    BleService::btgattc_search_complete_cb,
    BleService::btgattc_register_for_notification_cb,
    BleService::btgattc_notify_cb,
    BleService::btgattc_read_characteristic_cb,
    BleService::btgattc_write_characteristic_cb,
    BleService::btgattc_read_descriptor_cb,
    BleService::btgattc_write_descriptor_cb,
    BleService::btgattc_execute_write_cb,
    BleService::btgattc_remote_rssi_cb,
    BleService::btgattc_configure_mtu_cb,
    BleService::btgattc_congestion_cb,
    BleService::btgattc_get_gatt_db_cb,
    NULL, /* services_removed_cb */
    NULL, /* services_added_cb */
    BleService::btgattc_phy_updated_cb,
    BleService::btgattc_conn_updated_cb
};


static const btgatt_server_callbacks_t sGattServerCallbacks = {
    BleService::btgatts_register_app_cb,
    BleService::btgatts_connection_cb,
    BleService::btgatts_service_added_cb,
    BleService::btgatts_service_stopped_cb,
    BleService::btgatts_service_deleted_cb,
    BleService::btgatts_request_read_characteristic_cb,
    BleService::btgatts_request_read_descriptor_cb,
    BleService::btgatts_request_write_characteristic_cb,
    BleService::btgatts_request_write_descriptor_cb,
    BleService::btgatts_request_exec_write_cb,
    BleService::btgatts_response_confirmation_cb,
    BleService::btgatts_indication_sent_cb,
    BleService::btgatts_congestion_cb,
    BleService::btgatts_mtu_changed_cb,
    BleService::btgatts_phy_updated_cb,
    BleService::btgatts_conn_updated_cb
};


static const btgatt_callbacks_t sGattCallbacks = {
    sizeof(btgatt_callbacks_t), &sGattClientCallbacks, &sGattServerCallbacks,
    &sGattScannerCallbacks,
};

BleService::BleService(Adapter *pAdapter)
    : m_adapter(pAdapter),
      m_btGattIF(nullptr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

BleService::~BleService() {
 disableService();
}

void BleService::enableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  const bt_interface_t *pBtIf = getAdapter()->getBtInterface();
  if (!pBtIf) {
    ADK_LOG_ERROR(LOGTAG "Bluetooth module is not loaded");
    return;
  }

  if (m_btGattIF != NULL) {
    ADK_LOG_WARNING(LOGTAG "Cleaning up Bluetooth GATT Interface before initializing...");
    m_btGattIF->cleanup();
    m_btGattIF = NULL;
  }

  m_btGattIF =
      (btgatt_interface_t*)pBtIf->get_profile_interface(BT_PROFILE_GATT_ID);
  if (m_btGattIF == NULL) {
    ADK_LOG_ERROR(LOGTAG "Failed to get Bluetooth GATT Interface");
    return;
  }

  bt_status_t status = m_btGattIF->init(&sGattCallbacks);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_ERROR(LOGTAG "Failed to initialize Bluetooth GATT, status: %d", status);
    m_btGattIF = NULL;
    return;
  }
}

void BleService::disableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (m_btGattIF) {
    m_btGattIF->cleanup();
    m_btGattIF = nullptr;
  }
}

/**
 * Native Client functions
 */

int BleService::gattClientGetDeviceTypeNative(string address) {
  if (!m_btGattIF) return 0;

  return m_btGattIF->client->get_device_type(str2addr(address));
}

void BleService::gattClientRegisterAppNative(bt::Uuid uuid) {
  if (!m_btGattIF) return;

  m_btGattIF->client->register_client(bluetooth::Uuid::From128BitBE(uuid.To128BitBE()));
}

void BleService::gattClientUnregisterAppNative(int clientIf) {
  if (!m_btGattIF) return;

  m_btGattIF->client->unregister_client(clientIf);
}

void BleService::registerScannerNative(bt::Uuid uuid) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->RegisterScanner(
      base::Bind(&BleService::register_scanner_cb, bluetooth::Uuid::From128BitBE(uuid.To128BitBE())));
}

void BleService::unregisterScannerNative(int scanner_id) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->Unregister(scanner_id);
}

void BleService::gattClientScanNative(bool start) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->Scan(start);
}

void BleService::gattClientConnectNative(int clientif, string address, bool isDirect,
                                    int transport, bool opportunistic,
                                    int initiating_phys) {
  if (!m_btGattIF) return;

  m_btGattIF->client->connect(clientif, str2addr(address), isDirect,
                           transport, opportunistic, initiating_phys);
}

void BleService::gattClientDisconnectNative(int clientIf, string address,
                                       int conn_id) {
  if (!m_btGattIF) return;

  m_btGattIF->client->disconnect(clientIf, str2addr(address), conn_id);
}

void BleService::gattClientSetPreferredPhyNative(int UNUSED(clientIf), string address,
                                            int tx_phy, int rx_phy,
                                            int phy_options) {
  if (!m_btGattIF) return;

  m_btGattIF->client->set_preferred_phy(str2addr(address), tx_phy, rx_phy,
                                     phy_options);
}

void BleService::gattClientReadPhyNative(int clientIf, string address) {
  if (!m_btGattIF) return;

  RawAddress bda = str2addr(address);
  m_btGattIF->client->read_phy(bda, base::Bind(&BleService::readClientPhyCb, clientIf, bda));
}

void BleService::gattClientRefreshNative(int clientIf, string address) {
  if (!m_btGattIF) return;

  m_btGattIF->client->refresh(clientIf, str2addr(address));
}

void BleService::gattClientSearchServiceNative(int conn_id, bool search_all,
                                          bt::Uuid uuid) {
  if (!m_btGattIF) return;

  bluetooth::Uuid uuid1 = bluetooth::Uuid::From128BitBE(uuid.To128BitBE());
  m_btGattIF->client->search_service(conn_id, search_all ? 0 : &uuid1);
}

void BleService::gattClientDiscoverServiceByUuidNative(int conn_id, bt::Uuid uuid) {
  if (!m_btGattIF) return;

  m_btGattIF->client->btif_gattc_discover_service_by_uuid(conn_id, 
                        bluetooth::Uuid::From128BitBE(uuid.To128BitBE()));
}

void BleService::gattClientGetGattDbNative(int conn_id) {
  if (!m_btGattIF) return;

  m_btGattIF->client->get_gatt_db(conn_id);
}

void BleService::gattClientReadCharacteristicNative(int conn_id, int handle,
                                               int authReq) {
  if (!m_btGattIF) return;

  m_btGattIF->client->read_characteristic(conn_id, handle, authReq);
}

void BleService::gattClientReadUsingCharacteristicUuidNative(
    int conn_id, bt::Uuid uuid, int s_handle, int e_handle, int authReq) {
  if (!m_btGattIF) return;

  m_btGattIF->client->read_using_characteristic_uuid(conn_id, 
                        bluetooth::Uuid::From128BitBE(uuid.To128BitBE()),
                        s_handle, e_handle, authReq);
}

void BleService::gattClientReadDescriptorNative(int conn_id, int handle,
                                           int authReq) {
  if (!m_btGattIF) return;

  m_btGattIF->client->read_descriptor(conn_id, handle, authReq);
}

void BleService::gattClientWriteCharacteristicNative(int conn_id, int handle,
                                                int write_type, int auth_req,
                                                std::vector<uint8_t> vect_val) {
  if (!m_btGattIF) return;

  if (vect_val.size() == 0) {
    ADK_LOG_NOTICE("gattClientWriteCharacteristicNative() ignoring NULL array");
    return;
  }

  m_btGattIF->client->write_characteristic(conn_id, handle, write_type, auth_req,
                                        std::move(vect_val));
}

void BleService::gattClientExecuteWriteNative(int conn_id, bool execute) {
  if (!m_btGattIF) return;

  m_btGattIF->client->execute_write(conn_id, execute ? 1 : 0);
}

void BleService::gattClientWriteDescriptorNative(int conn_id, int handle,
                                            int auth_req, std::vector<uint8_t> vect_val) {
  if (!m_btGattIF) return;

  if (vect_val.size() == 0) {
    ADK_LOG_NOTICE("gattClientWriteDescriptorNative() ignoring NULL array");
    return;
  }

  m_btGattIF->client->write_descriptor(conn_id, handle, auth_req,
                                    std::move(vect_val));
}

void BleService::gattClientRegisterForNotificationsNative(
    int clientIf, string address, int handle, bool enable) {
  if (!m_btGattIF) return;

  RawAddress bd_addr = str2addr(address);
  if (enable)
    m_btGattIF->client->register_for_notification(clientIf, bd_addr, handle);
  else
    m_btGattIF->client->deregister_for_notification(clientIf, bd_addr, handle);
}

void BleService::gattClientReadRemoteRssiNative(int clientif, string address) {
  if (!m_btGattIF) return;

  m_btGattIF->client->read_remote_rssi(clientif, str2addr(address));
}

void BleService::gattSetScanParametersNative(int client_if, int scan_phy,
                                        std::vector<uint32_t> scan_interval,
                                        std::vector<uint32_t> scan_window) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->SetScanParameters(
      scan_phy, scan_interval, scan_window,
      base::Bind(&BleService::scan_params_cmpl_cb, client_if));
}

void BleService::getOwnAddressNative(
                                int advertiser_id) {
  if (!m_btGattIF) return;

  m_btGattIF->advertiser->GetOwnAddress(
      advertiser_id, base::Bind(&BleService::getOwnAddressCb, advertiser_id));
}

void BleService::gattClientScanFilterParamAddNative(
      uint8_t client_if, uint8_t filt_index,
      std::unique_ptr<btgatt_filt_param_setup_t> filt_params) {
  if (!m_btGattIF) return;

  const int add_scan_filter_params_action = 0;

  m_btGattIF->scanner->ScanFilterParamSetup(
      client_if, add_scan_filter_params_action, filt_index,
      std::move(filt_params), base::Bind(&BleService::scan_filter_param_cb, client_if));
}

void BleService::gattClientScanFilterParamDeleteNative(uint8_t client_if, uint8_t filt_index) {
  if (!m_btGattIF) return;

  const int delete_scan_filter_params_action = 1;

  m_btGattIF->scanner->ScanFilterParamSetup(
      client_if, delete_scan_filter_params_action, filt_index, nullptr,
      base::Bind(&BleService::scan_filter_param_cb, client_if));
}

void BleService::gattClientScanFilterParamClearAllNative(uint8_t client_if) {
  if (!m_btGattIF) return;

  const int clear_scan_filter_params_action = 2;

  m_btGattIF->scanner->ScanFilterParamSetup(
      client_if, clear_scan_filter_params_action, 0 /* index, unused */,
      nullptr, base::Bind(&BleService::scan_filter_param_cb, client_if));
}

void BleService::gattClientScanFilterAddNative(int client_if,int filter_index,
    std::vector<gatt::apcf_command_t> filters) {
  if (!m_btGattIF) return;

  size_t numFilters = filters.size();
  std::vector<ApcfCommand> apcf_filters;

  for(size_t i = 0; i < numFilters; i++)
  {
    apcf_command_t temp = filters.at(i);
    ApcfCommand temp1;
    temp1.address = str2addr(temp.address);
    temp1.addr_type = temp.addr_type;
    temp1.company = temp.company;
    temp1.data = temp.data;
    temp1.data_mask = temp.data_mask;
    temp1.name = temp.name;
    temp1.type = temp.type;
    temp1.uuid = bluetooth::Uuid::From128BitBE(temp.uuid.To128BitBE());
    temp1.uuid_mask = bluetooth::Uuid::From128BitBE(temp.uuid_mask.To128BitBE());

    apcf_filters.push_back(temp1);
  }

  m_btGattIF->scanner->ScanFilterAdd(filter_index, apcf_filters,
                                  base::Bind(&BleService::scan_filter_cfg_cb, client_if));
}

void BleService::gattClientScanFilterClearNative(int client_if, int filt_index) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->ScanFilterClear(filt_index,
                                    base::Bind(&scan_filter_cfg_cb, client_if));
}

void BleService::gattClientScanFilterEnableNative(int client_if, bool enable) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->ScanFilterEnable(enable,
                                     base::Bind(&scan_filter_status_cb, client_if));
}

void BleService::gattClientConfigureMTUNative(int conn_id, int mtu) {
  if (!m_btGattIF) return;

  m_btGattIF->client->configure_mtu(conn_id, mtu);
}

void BleService::gattConnectionParameterUpdateNative(
                                                int UNUSED(client_if), string address,
                                                int min_interval,
                                                int max_interval, int latency,
                                                int timeout, int UNUSED(min_ce_len),
                                                int UNUSED(max_ce_len)) {
  if (!m_btGattIF) return;

  m_btGattIF->client->conn_parameter_update(
      str2addr(address), min_interval, max_interval, latency, timeout);
}

void BleService::gattClientConfigBatchScanStorageNative(
    int client_if, int max_full_reports_percent,
    int max_trunc_reports_percent, int notify_threshold_level_percent) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->BatchscanConfigStorage(
      client_if, max_full_reports_percent, max_trunc_reports_percent,
      notify_threshold_level_percent,
      base::Bind(&batchscan_cfg_storage_cb, client_if));
}

void BleService::gattClientStartBatchScanNative(
                                           int client_if, int scan_mode,
                                           int scan_interval_unit,
                                           int scan_window_unit,
                                           int addr_type, int discard_rule) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->BatchscanEnable(
      scan_mode, scan_interval_unit, scan_window_unit, addr_type, discard_rule,
      base::Bind(&batchscan_start_cb, client_if));
}

void BleService::gattClientStopBatchScanNative(int client_if) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->BatchscanDisable(
      base::Bind(&batchscan_stop_cb, client_if));
}

void BleService::gattClientReadScanReportsNative(int client_if, int scan_type) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->BatchscanReadReports(client_if, scan_type);
}

/**
 * Native server functions
 */
void BleService::gattServerRegisterAppNative(bt::Uuid uuid) {
  if (!m_btGattIF) return;

  m_btGattIF->server->register_server(bluetooth::Uuid::From128BitBE(uuid.To128BitBE()));
}

void BleService::gattServerUnregisterAppNative(int serverIf) {
  if (!m_btGattIF) return;

  m_btGattIF->server->unregister_server(serverIf);
}

void BleService::gattServerConnectNative(int server_if,
                                    string address, bool is_direct,
                                    int transport) {
  if (!m_btGattIF) return;

  RawAddress bd_addr = str2addr(address);
  m_btGattIF->server->connect(server_if, bd_addr, is_direct, transport);
}

void BleService::gattServerDisconnectNative(int serverIf, string address,
                                       int conn_id) {
  if (!m_btGattIF) return;
  m_btGattIF->server->disconnect(serverIf, str2addr(address), conn_id);
}

void BleService::gattServerSetPreferredPhyNative(int UNUSED(serverIf), string address,
                                            int tx_phy, int rx_phy,
                                            int phy_options) {
  if (!m_btGattIF) return;
  RawAddress bda = str2addr(address);
  m_btGattIF->server->set_preferred_phy(bda, tx_phy, rx_phy, phy_options);
}

void BleService::gattServerReadPhyNative(int serverIf, string address) {
  if (!m_btGattIF) return;

  RawAddress bda = str2addr(address);
  m_btGattIF->server->read_phy(bda, base::Bind(&BleService::readServerPhyCb, serverIf, bda));
}

void BleService::gattServerAddServiceNative(int server_if,
                                       std::vector<gatt::gatt_db_element_t> service) {
  if (!m_btGattIF) return;

  if(service.size() > 0)
  {
    std::vector<btgatt_db_element_t> gatt_service;
    for(size_t i = 0; i < service.size(); i++)
    {
      gatt_db_element_t temp = service.at(i);
      btgatt_db_element_t temp1;

      temp1.id = temp.id;
      temp1.type = (bt_gatt_db_attribute_type_t)temp.type;
      temp1.permissions = temp.permissions;
      temp1.properties = temp.properties;
      temp1.attribute_handle = temp.attribute_handle;
      temp1.start_handle = temp.start_handle;
      temp1.end_handle = temp.end_handle;
      temp1.uuid = bluetooth::Uuid::From128BitBE(temp.uuid.To128BitBE());
      gatt_service.push_back(temp1);
    }
    m_btGattIF->server->add_service(server_if, gatt_service);
  }
}

void BleService::gattServerStopServiceNative(int server_if, int svc_handle) {
  if (!m_btGattIF) return;

  m_btGattIF->server->stop_service(server_if, svc_handle);
}

void BleService::gattServerDeleteServiceNative(int server_if, int svc_handle) {
  if (!m_btGattIF) return;

  m_btGattIF->server->delete_service(server_if, svc_handle);
}

void BleService::gattServerSendIndicationNative(int server_if, int attr_handle,
                                           int conn_id, std::vector<uint8_t> vect_val) {
  if (!m_btGattIF) return;

  m_btGattIF->server->send_indication(server_if, attr_handle, conn_id,
                                   /*confirm*/ 1, std::move(vect_val));
}

void BleService::gattServerSendNotificationNative(
                                             int server_if, int attr_handle,
                                             int conn_id, std::vector<uint8_t> vect_val) {
  if (!m_btGattIF) return;

  m_btGattIF->server->send_indication(server_if, attr_handle, conn_id,
                                   /*confirm*/ 0, std::move(vect_val));
}

void BleService::gattServerSendResponseNative(
                                         int UNUSED(server_if), int conn_id,
                                         int trans_id, int status,
                                         int handle, int offset,
                                         std::vector<uint8_t> vect_val, int auth_req) {
  if (!m_btGattIF) return;

  btgatt_response_t response;

  response.attr_value.handle = handle;
  response.attr_value.auth_req = auth_req;
  response.attr_value.offset = offset;
  response.attr_value.len = 0;

  if (vect_val.size() > 0) {
    if (vect_val.size() < BTGATT_MAX_ATTR_LEN) {
      response.attr_value.len = (uint16_t)vect_val.size();
    } else {
      response.attr_value.len = BTGATT_MAX_ATTR_LEN;
    }
    uint8_t *data = vect_val.data();
    std::memcpy(response.attr_value.value, data, response.attr_value.len);
  }

  m_btGattIF->server->send_response(conn_id, trans_id, status, response);
}

void BleService::startAdvertisingSetNative(
                                      advertise_parameters_t params, std::vector<uint8_t> adv_data,
                                      std::vector<uint8_t> scan_resp,
                                      gatt::periodic_advertising_parameters_t periodic_params,
                                      std::vector<uint8_t> periodic_data, int duration,
                                      int maxExtAdvEvents, int reg_id) {
  ADK_LOG_NOTICE(LOGTAG " %s reg_id: %d",__FUNCTION__, reg_id);

  if (!m_btGattIF) return;
  ::PeriodicAdvertisingParameters periodicParams;
  AdvertiseParameters adv_params;

  periodicParams.enable = periodic_params.enable;
  periodicParams.max_interval = periodic_params.max_interval;
  periodicParams.min_interval = periodic_params.min_interval;
  periodicParams.periodic_advertising_properties = periodic_params.periodic_advertising_properties;

  adv_params.advertising_event_properties = params.advertising_event_properties;
  adv_params.channel_map = params.channel_map;
  adv_params.max_interval = params.max_interval;
  adv_params.min_interval = params.min_interval;
  adv_params.primary_advertising_phy = params.primary_advertising_phy;
  adv_params.scan_request_notification_enable = params.scan_request_notification_enable;
  adv_params.secondary_advertising_phy = params.secondary_advertising_phy;
  adv_params.tx_power = params.tx_power;

  m_btGattIF->advertiser->StartAdvertisingSet(
      base::Bind(&ble_advertising_set_started_cb, reg_id), adv_params, adv_data,
      scan_resp, periodicParams, periodic_data, duration,
      maxExtAdvEvents, base::Bind(ble_advertising_set_timeout_cb));
}

void BleService::stopAdvertisingSetNative(
                                     int advertiser_id) {
  if (!m_btGattIF) return;

  m_btGattIF->advertiser->Unregister(advertiser_id);
}

void BleService::enableAdvertisingSetNative(
                                       int advertiser_id, bool enable,
                                       int duration, int maxExtAdvEvents) {
  if (!m_btGattIF) return;

  m_btGattIF->advertiser->Enable(advertiser_id, enable,
                              base::Bind(&ble_advertising_set_enable_Cb, advertiser_id, enable),
                              duration, maxExtAdvEvents,
                              base::Bind(&ble_advertising_set_enable_Cb, advertiser_id, false));
}

void BleService::setAdvertisingDataNative(
                                     int advertiser_id, std::vector<uint8_t> data) {
  if (!m_btGattIF) return;

  m_btGattIF->advertiser->SetData(
      advertiser_id, false, data,
      base::Bind(&onSetAdvertisingData, advertiser_id));
}

void BleService::setScanResponseDataNative(
                                      int advertiser_id, std::vector<uint8_t> data) {
  if (!m_btGattIF) return;

  m_btGattIF->advertiser->SetData(
      advertiser_id, true, data,
      base::Bind(&onSetScanResponseData,
                 advertiser_id));
}

void BleService::setAdvertisingParametersNative(
                                           int advertiser_id,
                                           gatt::advertise_parameters_t params) {
  if (!m_btGattIF) return;

  AdvertiseParameters adv_params;

  adv_params.advertising_event_properties = params.advertising_event_properties;
  adv_params.channel_map = params.channel_map;
  adv_params.max_interval = params.max_interval;
  adv_params.min_interval = params.min_interval;
  adv_params.primary_advertising_phy = params.primary_advertising_phy;
  adv_params.scan_request_notification_enable = params.scan_request_notification_enable;
  adv_params.secondary_advertising_phy = params.secondary_advertising_phy;
  adv_params.tx_power = params.tx_power;

  m_btGattIF->advertiser->SetParameters(
      advertiser_id, adv_params,
      base::Bind(&ble_advertising_parameters_updated_cb, advertiser_id));
}

void BleService::setPeriodicAdvertisingParametersNative(int advertiser_id,
    gatt::periodic_advertising_parameters_t periodic_params) {
  if (!m_btGattIF) return;

  ::PeriodicAdvertisingParameters periodicParams;

  periodicParams.enable = periodic_params.enable;
  periodicParams.max_interval = periodic_params.max_interval;
  periodicParams.min_interval = periodic_params.min_interval;
  periodicParams.periodic_advertising_properties = periodic_params.periodic_advertising_properties;

  m_btGattIF->advertiser->SetPeriodicAdvertisingParameters(
      advertiser_id, periodicParams,
      base::Bind(&onSetPeriodicAdvertisingParameters, advertiser_id));
}

void BleService::setPeriodicAdvertisingDataNative(
                                             int advertiser_id,
                                             std::vector<uint8_t> data) {
  if (!m_btGattIF) return;

  m_btGattIF->advertiser->SetPeriodicAdvertisingData(
      advertiser_id, data,
      base::Bind(&onSetPeriodicAdvertisingData,
                 advertiser_id));
}

void BleService::setPeriodicAdvertisingEnableNative(
                                               int advertiser_id,
                                               bool enable) {
  if (!m_btGattIF) return;

  m_btGattIF->advertiser->SetPeriodicAdvertisingEnable(
      advertiser_id, enable,
      base::Bind(&ble_periodic_advertising_set_enable_Cb, advertiser_id, enable));
}

void BleService::startSyncNative(int sid,
                            string address, int skip, int timeout,
                            int reg_id) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->StartSync(sid, str2addr(address), skip, timeout,
                              base::Bind(&onSyncStarted, reg_id),
                              base::Bind(&onSyncReport),
                              base::Bind(&onSyncLost));
}

void BleService::stopSyncNative(int sync_handle) {
  if (!m_btGattIF) return;

  m_btGattIF->scanner->StopSync(sync_handle);
}

void BleService::gattTestNative(int command,
                           bt::Uuid uuid, string bda1,
                           int p1, int p2, int p3, int p4, int p5) {
  if (!m_btGattIF) return;

  bluetooth::Uuid uuid1 = bluetooth::Uuid::From128BitBE(uuid.To128BitBE());
  RawAddress bt_bda1 = str2addr(bda1);

  btgatt_test_params_t params;
  params.bda1 = &bt_bda1;
  params.uuid1 = &uuid1;
  params.u1 = p1;
  params.u2 = p2;
  params.u3 = p3;
  params.u4 = p4;
  params.u5 = p5;
  m_btGattIF->client->test_command(command, params);
}
