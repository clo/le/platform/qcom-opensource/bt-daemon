/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BLE_SERVICE_HPP
#define BLE_SERVICE_HPP

#include "IGattNativeV2.hpp"
#include "GattNativeDefines.hpp"

using std::string;

class Adapter;

class BleService final : public gatt::IGattNativeV2 {
  public:
    explicit BleService(Adapter *pAdapter);
    ~BleService();

    void enableService();
    void disableService();
    Adapter* getAdapter() { return m_adapter; }

    // From IGattNativeV2
    int gattClientGetDeviceTypeNative(string address);
    void gattClientRegisterAppNative(bt::Uuid uuid);
    void gattClientUnregisterAppNative(int clientIf);
    void registerScannerNative(bt::Uuid uuid);
    void unregisterScannerNative(int scanner_id);
    void gattClientScanNative(bool start);
    void gattClientConnectNative(int clientif, string address, bool isDirect,
        int transport, bool opportunistic, int initiating_phys);
    void gattClientDisconnectNative(int clientIf, string address, int conn_id);
    void gattClientSetPreferredPhyNative(int clientIf, string address,
        int tx_phy, int rx_phy, int phy_options);
    void gattClientReadPhyNative(int clientIf, string address);
    void gattClientRefreshNative(int clientIf, string address);
    void gattClientSearchServiceNative(int conn_id, bool search_all, bt::Uuid uuid);
    void gattClientDiscoverServiceByUuidNative(int conn_id, bt::Uuid uuid);
    void gattClientGetGattDbNative(int conn_id);
    void gattClientReadCharacteristicNative(int conn_id, int handle, int authReq);
    void gattClientReadUsingCharacteristicUuidNative(
        int conn_id, bt::Uuid uuid, int s_handle, int e_handle, int authReq);
    void gattClientReadDescriptorNative(int conn_id, int handle, int authReq);
    void gattClientWriteCharacteristicNative(int conn_id, int handle,
        int write_type, int auth_req, std::vector<uint8_t> vect_val);
    void gattClientExecuteWriteNative(int conn_id, bool execute);
    void gattClientWriteDescriptorNative(int conn_id, int handle, int auth_req,
        std::vector<uint8_t> vect_val);
    void gattClientRegisterForNotificationsNative(
        int clientIf, string address, int handle, bool enable);
    void gattClientReadRemoteRssiNative(int clientif, string address);
    void gattSetScanParametersNative(int client_if, int scan_phy,
        std::vector<uint32_t> scan_interval, std::vector<uint32_t> scan_window);
    void getOwnAddressNative(int advertiser_id);
    void gattClientScanFilterParamAddNative(uint8_t client_if, uint8_t filt_index,
         std::unique_ptr<btgatt_filt_param_setup_t> filt_params);
    void gattClientScanFilterParamDeleteNative(uint8_t client_if, uint8_t filt_index);
    void gattClientScanFilterParamClearAllNative(uint8_t client_if);
    void gattClientScanFilterAddNative(int client_if,int filter_index,
        std::vector<gatt::apcf_command_t> filters);
    void gattClientScanFilterClearNative(int client_if, int filt_index);
    void gattClientScanFilterEnableNative(int client_if, bool enable);
    void gattClientConfigureMTUNative(int conn_id, int mtu);
    void gattConnectionParameterUpdateNative(int client_if, string address,
        int min_interval,int max_interval, int latency, int timeout, int min_ce_len,
        int max_ce_len);
    void gattClientConfigBatchScanStorageNative(int client_if, int max_full_reports_percent,
        int max_trunc_reports_percent, int notify_threshold_level_percent);
    void gattClientStartBatchScanNative(int client_if, int scan_mode,
        int scan_interval_unit, int scan_window_unit, int addr_type, int discard_rule);
    void gattClientStopBatchScanNative(int client_if);
    void gattClientReadScanReportsNative(int client_if, int scan_type);
    void gattServerRegisterAppNative(bt::Uuid uuid);
    void gattServerUnregisterAppNative(int serverIf);
    void gattServerConnectNative(int server_if, string address, bool is_direct, int transport);
    void gattServerDisconnectNative(int serverIf, string address, int conn_id);
    void gattServerSetPreferredPhyNative(int serverIf, string address,
        int tx_phy, int rx_phy, int phy_options);
    void gattServerReadPhyNative(int serverIf, string address);
    void gattServerAddServiceNative(int server_if, std::vector<gatt::gatt_db_element_t> service);
    void gattServerStopServiceNative(int server_if, int svc_handle);
    void gattServerDeleteServiceNative(int server_if, int svc_handle);
    void gattServerSendIndicationNative(int server_if, int attr_handle,
       int conn_id, std::vector<uint8_t> vect_val);
    void gattServerSendNotificationNative(int server_if, int attr_handle,
      int conn_id, std::vector<uint8_t> vect_val);
    void gattServerSendResponseNative(int server_if, int conn_id,
        int trans_id, int status, int handle, int offset,
        std::vector<uint8_t> vect_val, int auth_req);
    void startAdvertisingSetNative(gatt::advertise_parameters_t params, std::vector<uint8_t> adv_data,
        std::vector<uint8_t> scan_resp, gatt::periodic_advertising_parameters_t periodic_params,
        std::vector<uint8_t> periodic_data, int duration, int maxExtAdvEvents, int reg_id);
    void stopAdvertisingSetNative(int advertiser_id);
    void enableAdvertisingSetNative(int advertiser_id, bool enable, int duration, int maxExtAdvEvents);
    void setAdvertisingDataNative(int advertiser_id, std::vector<uint8_t> data);
    void setScanResponseDataNative(int advertiser_id, std::vector<uint8_t> data);
    void setAdvertisingParametersNative(int advertiser_id, gatt::advertise_parameters_t parameters);
    void setPeriodicAdvertisingParametersNative(int advertiser_id, gatt::periodic_advertising_parameters_t periodic_parameters);
    void setPeriodicAdvertisingDataNative(int advertiser_id,std::vector<uint8_t> data);
    void setPeriodicAdvertisingEnableNative(int advertiser_id, bool enable);
    void startSyncNative(int sid, string address, int skip, int timeout, int reg_id);
    void stopSyncNative(int sync_handle);
    void gattTestNative(int command, bt::Uuid uuid1, string bda1, int p1, int p2, int p3, int p4, int p5);

    // Fluoride GATT callbacks
    static void scan_result_cb(uint16_t event_type, uint8_t addr_type, RawAddress* bda, uint8_t primary_phy,
        uint8_t secondary_phy, uint8_t advertising_sid, int8_t tx_power, int8_t rssi, uint16_t periodic_adv_int,
        std::vector<uint8_t> adv_data);
    static void batchscan_reports_cb(int client_if, int status, int report_format, int num_records, std::vector<uint8_t> data);
    static void batchscan_threshold_cb(int client_if);
    static void track_adv_event_cb(btgatt_track_adv_info_t* p_adv_track_info);

    static void btgattc_register_app_cb(int status, int clientIf, const bluetooth::Uuid& app_uuid);
    static void btgattc_open_cb(int conn_id, int status, int clientIf, const RawAddress& bda);
    static void btgattc_close_cb(int conn_id, int status, int clientIf, const RawAddress& bda);
    static void btgattc_search_complete_cb(int conn_id, int status);
    static void btgattc_register_for_notification_cb(int conn_id, int registered, int status, uint16_t handle);
    static void btgattc_notify_cb(int conn_id, const btgatt_notify_params_t& p_data);
    static void btgattc_read_characteristic_cb(int conn_id, int status, btgatt_read_params_t* p_data);
    static void btgattc_write_characteristic_cb(int conn_id, int status, uint16_t handle);
    static void btgattc_execute_write_cb(int conn_id, int status);
    static void btgattc_read_descriptor_cb(int conn_id, int status, const btgatt_read_params_t& p_data);
    static void btgattc_write_descriptor_cb(int conn_id, int status, uint16_t handle);
    static void btgattc_remote_rssi_cb(int client_if, const RawAddress& bda, int rssi, int status);
    static void btgattc_configure_mtu_cb(int conn_id, int status, int mtu);
    static void btgattc_congestion_cb(int conn_id, bool congested);
    static void btgattc_get_gatt_db_cb(int conn_id, const btgatt_db_element_t* db, int count);
    static void btgattc_phy_updated_cb(int conn_id, uint8_t tx_phy, uint8_t rx_phy, uint8_t status);
    static void btgattc_conn_updated_cb(int conn_id, uint16_t interval, uint16_t latency, uint16_t timeout, uint8_t status);

    static void btgatts_register_app_cb(int status, int server_if, const bluetooth::Uuid& uuid);
    static void btgatts_connection_cb(int conn_id, int server_if, int connected, const RawAddress& bda);
    static void btgatts_service_added_cb(int status, int server_if, std::vector<btgatt_db_element_t> service);
    static void btgatts_service_stopped_cb(int status, int server_if, int srvc_handle);
    static void btgatts_service_deleted_cb(int status, int server_if, int srvc_handle);
    static void btgatts_request_read_characteristic_cb(int conn_id, int trans_id, const RawAddress& bda,
        int attr_handle, int offset, bool is_long);
    static void btgatts_request_read_descriptor_cb(int conn_id, int trans_id, const RawAddress& bda, int attr_handle,
        int offset, bool is_long);
    static void btgatts_request_write_characteristic_cb(int conn_id, int trans_id, const RawAddress& bda,
        int attr_handle, int offset, bool need_rsp, bool is_prep, std::vector<uint8_t> value);
    static void btgatts_request_write_descriptor_cb(int conn_id, int trans_id, const RawAddress& bda, int attr_handle,
        int offset, bool need_rsp, bool is_prep, std::vector<uint8_t> value);
    static void btgatts_request_exec_write_cb(int conn_id, int trans_id, const RawAddress& bda, int exec_write);
    static void btgatts_response_confirmation_cb(int status, int handle);
    static void btgatts_indication_sent_cb(int conn_id, int status);
    static void btgatts_congestion_cb(int conn_id, bool congested);
    static void btgatts_mtu_changed_cb(int conn_id, int mtu);
    static void btgatts_phy_updated_cb(int conn_id, uint8_t tx_phy, uint8_t rx_phy, uint8_t status);
    static void btgatts_conn_updated_cb(int conn_id, uint16_t interval, uint16_t latency, uint16_t timeout, uint8_t status);

    static void readClientPhyCb(uint8_t clientIf, RawAddress bda, uint8_t tx_phy, uint8_t rx_phy, uint8_t status);
    static void readServerPhyCb(uint8_t serverIf, RawAddress bda, uint8_t tx_phy, uint8_t rx_phy, uint8_t status);
    static void register_scanner_cb(const bluetooth::Uuid& app_uuid, uint8_t scannerId, uint8_t status);
    static void scan_params_cmpl_cb(uint8_t client_if, uint8_t status);
    static void scan_filter_cfg_cb(uint8_t client_if, uint8_t filt_type, uint8_t avbl_space, uint8_t action, uint8_t status);
    static void scan_filter_param_cb(uint8_t client_if, uint8_t avbl_space, uint8_t action, uint8_t status);
    static void scan_filter_status_cb(uint8_t client_if, uint8_t action, uint8_t status);
    static void batchscan_cfg_storage_cb(uint8_t client_if, uint8_t status);
    static void batchscan_start_cb(uint8_t client_if, uint8_t status);
    static void batchscan_stop_cb(uint8_t client_if, uint8_t status);
    static void onSyncStarted(int reg_id, uint8_t status, uint16_t sync_handle, uint8_t sid, uint8_t address_type,
        RawAddress address, uint8_t phy, uint16_t interval);
    static void  onSyncLost(uint16_t sync_handle);
    static void onSyncReport(uint16_t sync_handle, int8_t tx_power, int8_t rssi, uint8_t data_status, std::vector<uint8_t> data);
    static void onSetAdvertisingData(uint8_t advertiser_id, uint8_t status);
    static void onSetScanResponseData(uint8_t advertiser_id, uint8_t status);
    static void onSetPeriodicAdvertisingParameters(uint8_t advertiser_id, uint8_t status);
    static void onSetPeriodicAdvertisingData(uint8_t advertiser_id, uint8_t status);
    static void getOwnAddressCb(uint8_t advertiser_id, uint8_t address_type, RawAddress address);
    static void ble_advertising_set_started_cb(int reg_id, uint8_t advertiser_id, int8_t tx_power, uint8_t status);
    static void ble_advertising_set_timeout_cb(uint8_t advertiser_id, uint8_t status);
    static void ble_advertising_set_enable_Cb(uint8_t advertiser_id, bool enable, uint8_t status);
    static void ble_advertising_parameters_updated_cb(uint8_t advertiser_id, uint8_t status, int8_t tx_power);
    static void ble_periodic_advertising_set_enable_Cb(uint8_t advertiser_id, bool enable, uint8_t status);

  private:
    Adapter *m_adapter;
    btgatt_interface_t *m_btGattIF;
};

#endif  // BLE_SERVICE_HPP
