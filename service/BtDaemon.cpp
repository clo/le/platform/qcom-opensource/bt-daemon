/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "BtDaemon.hpp"

#include <dlfcn.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/eventfd.h>
#include <unistd.h>

#include <hardware/bt_av_vendor.h>
#include <hardware/hardware.h>
#include <systemdq/sd-bus.h>

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <mutex>

#include "A2dpSinkService.hpp"
#include "A2dpSourceService.hpp"
#include "AvrcpTGService.hpp"
#include "Common.hpp"

#define BTIF_NAME "org.bluez"

#define LOGTAG "MAIN "

static BtDaemon *g_btd = nullptr;

sd_bus *g_sdbus = nullptr;
sd_event *g_eventLoop = nullptr;

static std::mutex callbackListLock;
static std::list<std::function<void(void)>> callbackList;
static int callbackFd = -1;

void queueBtCallback(std::function<void(void)> &&handler) {
  {
    std::lock_guard<std::mutex> l(callbackListLock);
    callbackList.push_back(handler);
  }

  // Wake the event loop
  static eventfd_t u = 1;
  ssize_t res = write(callbackFd, &u, sizeof(u));
  if (res != sizeof(u)) {
    ADK_LOG_ERROR(LOGTAG " Failed to write to queue fd: %d - %s\n", errno,
                  strerror(errno));
  }
}

static int callbackEventHandler(sd_event_source *UNUSED(s), int UNUSED(fd), uint32_t UNUSED(revents),
                                void *UNUSED(userdata)) {
  system_wake_lock_toggle(true, btdaemon::kWakelockMain, 0);
  eventfd_t u;
  ssize_t res = read(callbackFd, &u, sizeof(u));
  if (res != sizeof(u)) {
    ADK_LOG_ERROR(LOGTAG " Failed to read to queue fd: %d - %s\n", errno,
                  strerror(errno));
    system_wake_lock_toggle(false, btdaemon::kWakelockMain, 0);
    return -1;
  }
  if (res == 0) {
    system_wake_lock_toggle(false, btdaemon::kWakelockMain, 0);
    return 0;
  }

  // Only do one callback at a time to avoid starving the main loop if callbacks
  // are added as fast as we process them
  std::function<void(void)> handler = std::move(callbackList.front());
  {
    std::lock_guard<std::mutex> l(callbackListLock);
    callbackList.pop_front();
  }
  handler();

  system_wake_lock_toggle(false, btdaemon::kWakelockMain, 0);
  return 0;
}

// TODO
// static void disconnected_dbus(DBusConnection *conn, void *data) {
//        ADK_LOG_DEBUG (LOGTAG "Disconnected from D-Bus. Exiting.");
//        g_main_loop_quit(event_loop);
//}

static sd_bus *connect_dbus(void) {
  sd_bus *sdbus;
  int res = sd_bus_open_system(&sdbus);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Unable open D-Bus: %d - %s\n", -res, strerror(-res));
    return nullptr;
  }

  res = sd_bus_request_name(sdbus, BTIF_NAME, 0);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Failed to get name: %d - %s\n", -res,
                  strerror(-res));
    return nullptr;
  }

  // TODO
  //    g_dbus_set_disconnect_function(conn, disconnected_dbus, nullptr,
  //    nullptr); if ( g_dbus_attach_object_manager(conn) != TRUE ) {//SYSTEMD
  //            ADK_LOG_ERROR (LOGTAG" Unable to attach Object Manager\n");
  //    }
  return sdbus;
}

int main() {
  system_wake_lock_toggle(true, btdaemon::kWakelockMain, 0);
  int res;
  // initialize signal handler
  signal(SIGPIPE, SIG_IGN);

  // block signals we will want to wait on.
  // (needs to be set before we create any thread or the app will be killed
  // if we receive another a second signal while processing the first)
  sigset_t sigSet;
  sigemptyset(&sigSet);
  sigaddset(&sigSet, SIGINT);
  sigaddset(&sigSet, SIGTERM);
  sigaddset(&sigSet, SIGHUP);
  pthread_sigmask(SIG_BLOCK, &sigSet, nullptr);

  g_sdbus = connect_dbus();
  if (!g_sdbus) {
    ADK_LOG_ERROR(LOGTAG "Unable to get on D-Bus");
    goto done;
  }

  // Create deamon service object, which loads the stack and initializes Adapter
  // with default configuration
  g_btd = new BtDaemon();

  if (!g_btd->init()) {
    ADK_LOG_ERROR(LOGTAG " Daemon Init failed\n");
    goto done;
  }

  res = sd_event_default(&g_eventLoop);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Failed to get event loop: %d - %s\n", -res,
                  strerror(-res));
    goto done;
  }

  res = sd_bus_attach_event(g_sdbus, g_eventLoop, SD_EVENT_PRIORITY_NORMAL);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Failed to attach event loop: %d - %s\n", -res,
                  strerror(-res));
    goto done;
  }

  res = sd_event_add_signal(g_eventLoop, nullptr, SIGINT, nullptr, nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Failed to install SIGINT handler: %d - %s\n", -res,
                  strerror(-res));
    goto done;
  }

  res = sd_event_add_signal(g_eventLoop, nullptr, SIGTERM, nullptr, nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Failed to install SIGTERM handler: %d - %s\n", -res,
                  strerror(-res));
    goto done;
  }

  res = sd_event_add_signal(g_eventLoop, nullptr, SIGHUP, nullptr, nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Failed to install SIGHUP handler: %d - %s\n", -res,
                  strerror(-res));
    goto done;
  }

  callbackFd = eventfd(0, EFD_CLOEXEC | EFD_SEMAPHORE);
  if (callbackFd == -1) {
    ADK_LOG_ERROR(LOGTAG " Failed to create queue fd: %d - %s\n", errno,
                  strerror(errno));
    goto done;
  }

  res = sd_event_add_io(g_eventLoop, NULL, callbackFd, EPOLLIN,
                        callbackEventHandler, nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Failed to install callback handler: %d - %s\n", -res,
                  strerror(-res));
    goto done;
  }

  system_wake_lock_toggle(false, btdaemon::kWakelockMain, 0);
  res = sd_event_loop(g_eventLoop);
  system_wake_lock_toggle(true, btdaemon::kWakelockMain, 0);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Failed to run event loop: %d - %s\n", -res,
                  strerror(-res));
    goto done;
  }

done:
  if (g_btd) {
    delete g_btd;
    g_btd = nullptr;
  }
  sd_bus_flush_close_unref(g_sdbus);
  g_sdbus = nullptr;

  sd_event_unref(g_eventLoop);
  g_eventLoop = nullptr;

  if (callbackFd >= 0) {
    close(callbackFd);
    callbackFd = -1;
  }

  system_wake_lock_toggle(false, btdaemon::kWakelockMain, 0);
  return 0;
}

BtDaemon::BtDaemon()
    : m_btDev(nullptr),
      m_btIF(nullptr),
      m_adapter(nullptr),
      m_agentManager(nullptr),
      m_mediaManager(nullptr) {}

BtDaemon::~BtDaemon() { deInit(); }

bool BtDaemon::loadBtStack(void) {
  const hw_module_t *module;

  int res = hw_get_module(BT_STACK_MODULE_ID, &module);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to get BT module: %d\n", res);
    return false;
  }

  struct hw_device_t *hwDev;
  res = module->methods->open(module, BT_STACK_MODULE_ID, &hwDev);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to open BT module: %d\n", res);
    return false;
  }

  m_btDev = reinterpret_cast<bluetooth_device_t *>(hwDev);
  m_btIF = m_btDev->get_bluetooth_interface();
  if (!m_btIF) {
    ADK_LOG_ERROR(LOGTAG " hw_get_bt_interface failed\n");
    m_btDev->common.close(&m_btDev->common);
    m_btDev = nullptr;
    return false;
  }

  return true;
}

void BtDaemon::unloadBtStack(void) {
  if (m_btIF) {
    m_btIF->cleanup();
    m_btIF = nullptr;
  }

  if (m_btDev) {
    m_btDev->common.close(&m_btDev->common);
    m_btDev = nullptr;
  }
}

bool BtDaemon::init(void) {
  if (!loadBtStack()) {
    ADK_LOG_ERROR(LOGTAG " Fluoride stack load failed\n");
    return false;
  }

  // Enable org.freedesktop.DBus.ObjectManager interface on root object path
  int res = sd_bus_add_object_manager(g_sdbus, nullptr, "/");
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG
                  "failed to add object manager on root object path: %d - %s\n",
                  -res, strerror(-res));
    return false;
  }

  // create media manager
  m_mediaManager = new MediaManager();

  // Create the Adapter Object for handling BT functionlaity
  m_adapter = new Adapter(m_btIF, m_mediaManager);

  if (!m_adapter->init()) {
    ADK_LOG_ERROR(LOGTAG " Adapter handling initialization failed");
    delete m_adapter;
    m_adapter = nullptr;
    return false;
  }
  // Create an Agent Manager object of service to handle user/ app intervention
  // for device pairing
  m_agentManager = new AgentManager();

  return true;
}

void BtDaemon::deInit(void) {
  if (m_mediaManager) {
    delete m_mediaManager;
  }
  if (m_agentManager) {
    delete m_agentManager;
    m_agentManager = nullptr;
  }

  if (m_adapter) {
    delete m_adapter;
    m_adapter = nullptr;
  }
  unloadBtStack();
}
