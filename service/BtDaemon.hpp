/*
Copyright (c) 2017-2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BTDAEMON_HPP
#define BTDAEMON_HPP

#include <cutils/sockets.h>
#include <errno.h>
#include <netinet/in.h>
#include <poll.h>
#include <pthread.h>
#include <stdbool.h>
#include <string.h>
#include <sys/poll.h>
#include <sys/prctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>

#include <hardware/bluetooth.h>
#include <systemdq/sd-bus.h>

#include "Adapter.hpp"
#include "Agent.hpp"

/**
 * @file Main.hpp
 * @brief Main header file for the BT Daemon Service
 */

/**
 * @class BtDaemon
 *
 * @brief Manages the Fluoride stack loading and service config options
 *
 */

class BtDaemon {
 private:
  bluetooth_device_t *m_btDev;
  const bt_interface_t *m_btIF;

  Adapter *m_adapter;
  AgentManager *m_agentManager;
  MediaManager *m_mediaManager;

  bool loadBtStack();
  void unloadBtStack();

 public:
  /**
   * @brief Bluetooth Application Constructor
   *
   * It will initialize class members to default values
   */
  BtDaemon();

  /**
   * @ref Bluetooth Application Distructor
   *
   * It will free the config parameter
   *
   */
  ~BtDaemon();
  bool init();
  void deInit();
};

#endif  // BTDAEMON_HPP
