/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SERVICE_COMMON_HPP_
#define SERVICE_COMMON_HPP_

#include <adk/wakelock.h>
#include <adk/log.h>

#include <string>
#include <functional>
#include <vector>

#ifdef __GNUC__
#define UNUSED(x) UNUSED_##x __attribute__((__unused__))
#else
#define UNUSED(x) UNUSED_##x
#endif

// wakelocks
namespace btdaemon {
const std::string kWakelockMain = "adk.btdaemon.main";
} // namespace btdaemon

// Generic template for enums
template <class T>
T from_string(const std::string &mode) {
  return T::template_needs_to_be_specialized();
}

struct BtProperty {
  bt_property_type_t type;
  std::vector<uint8_t> value;
  BtProperty(bt_property_type_t type, const std::vector<uint8_t> &value)
      : type(type), value(value) {}
  BtProperty(bt_property_type_t type, std::vector<uint8_t> &&value)
      : type(type), value(std::move(value)) {}
  BtProperty(BtProperty &&rhs) : type(rhs.type), value(std::move(rhs.value)) {}
};

extern void queueBtCallback(std::function<void(void)> &&handler);

#endif /* SERVICE_COMMON_HPP_ */
