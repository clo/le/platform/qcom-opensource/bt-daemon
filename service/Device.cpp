/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Device.hpp"

#include <string.h>

#include <algorithm>
#include <list>
#include <map>

#include "Adapter.hpp"
#include "AvrcpCTService.hpp"
#include "AvrcpTGService.hpp"
#include "Common.hpp"
#include "Error.hpp"
#include "MediaPlayer.hpp"
#include "MediaTransport.hpp"
#include "Sdbus.hpp"

#define LOGTAG "Device "

#define DEVICE_INTERFACE "org.bluez.Device1"

struct BondingReq {
  sd_bus_message *sd_msg;
  sd_bus_track *sd_listener;
  std::weak_ptr<Agent> agent;
  Device *pDev;
  uint8_t status;
};

struct AuthReq {
  auth_type_t type;
  std::weak_ptr<Agent> agent;
  bt_bdaddr_t address;
  uint32_t passkey;
  bt_ssp_variant_t ssp_type;
};

static BondingReq *newBondingRequest(sd_bus_message *msg, Device *device,
                                     const std::shared_ptr<Agent> &agent) {
  BondingReq *pBondReq;

  pBondReq = new BondingReq();

  pBondReq->sd_msg = sd_bus_message_ref(msg);
  pBondReq->pDev = device;

  if (agent) {
    pBondReq->agent = agent;
  }

  return pBondReq;
}

static void freeBondingRequest(BondingReq *pBondReq) {
  if (!pBondReq) return;

  sd_bus_track_unref(pBondReq->sd_listener);
  pBondReq->sd_listener = nullptr;

  sd_bus_message_unref(pBondReq->sd_msg);
  pBondReq->sd_msg = nullptr;

  delete pBondReq;
}

const char *to_string(PlayState mode) {
  switch (mode) {
    case PlayState::INVALID:
      return nullptr;
    case PlayState::STOPPED:
      return "stopped";
    case PlayState::PLAYING:
      return "playing";
    case PlayState::PAUSED:
      return "paused";
    case PlayState::FWD_SEEK:
      return "forward-seek";
    case PlayState::REV_SEEK:
      return "reverse-seek";
    case PlayState::ERROR:
      return "playing";
  }
  return nullptr;
}
template <>
PlayState from_string<PlayState>(const std::string &mode) {
  if (mode == to_string(PlayState::STOPPED)) {
    return PlayState::STOPPED;
  } else if (mode == to_string(PlayState::PLAYING)) {
    return PlayState::PLAYING;
  } else if (mode == to_string(PlayState::PAUSED)) {
    return PlayState::PAUSED;
  } else if (mode == to_string(PlayState::FWD_SEEK)) {
    return PlayState::FWD_SEEK;
  } else if (mode == to_string(PlayState::REV_SEEK)) {
    return PlayState::REV_SEEK;
  } else {
    return PlayState::INVALID;
  }
}

const char *to_string(Equalizer mode) {
  switch (mode) {
    case Equalizer::INVALID:
      return nullptr;
    case Equalizer::OFF:
      return "off";
    case Equalizer::ON:
      return "on";
  }
  return nullptr;
}
template <>
Equalizer from_string<Equalizer>(const std::string &mode) {
  if (mode == to_string(Equalizer::OFF)) {
    return Equalizer::OFF;
  } else if (mode == to_string(Equalizer::ON)) {
    return Equalizer::ON;
  } else {
    return Equalizer::OFF;
  }
}

const char *to_string(Repeat mode) {
  switch (mode) {
    case Repeat::INVALID:
      return nullptr;
    case Repeat::OFF:
      return "off";
    case Repeat::SINGLE:
      return "singletrack";
    case Repeat::ALL:
      return "alltracks";
    case Repeat::GROUP:
      return "group";
  }
  return nullptr;
}
template <>
Repeat from_string<Repeat>(const std::string &mode) {
  if (mode == to_string(Repeat::OFF)) {
    return Repeat::OFF;
  } else if (mode == to_string(Repeat::SINGLE)) {
    return Repeat::SINGLE;
  } else if (mode == to_string(Repeat::ALL)) {
    return Repeat::ALL;
  } else if (mode == to_string(Repeat::GROUP)) {
    return Repeat::GROUP;
  } else {
    return Repeat::INVALID;
  }
}

const char *to_string(Shuffle mode) {
  switch (mode) {
    case Shuffle::INVALID:
      return nullptr;
    case Shuffle::OFF:
      return "off";
    case Shuffle::ALL:
      return "alltracks";
    case Shuffle::GROUP:
      return "group";
  }
  return nullptr;
}
template <>
Shuffle from_string<Shuffle>(const std::string &mode) {
  if (mode == to_string(Shuffle::OFF)) {
    return Shuffle::OFF;
  } else if (mode == to_string(Shuffle::ALL)) {
    return Shuffle::ALL;
  } else if (mode == to_string(Shuffle::GROUP)) {
    return Shuffle::GROUP;
  } else {
    return Shuffle::INVALID;
  }
}

const char *to_string(Scan mode) {
  switch (mode) {
    case Scan::INVALID:
      return nullptr;
    case Scan::OFF:
      return "off";
    case Scan::ON:
      return "alltracks";
    case Scan::GROUP:
      return "group";
  }
  return nullptr;
}
template <>
Scan from_string<Scan>(const std::string &mode) {
  if (mode == to_string(Scan::OFF)) {
    return Scan::OFF;
  } else if (mode == to_string(Scan::ON)) {
    return Scan::ON;
  } else if (mode == to_string(Scan::GROUP)) {
    return Scan::GROUP;
  } else {
    return Scan::INVALID;
  }
}

void Device::callCancelAuthAgent() {
  if (!m_authReq) {
    return;
  }
  std::shared_ptr<Agent> agent = m_authReq->agent.lock();
  if (!agent) {
    return;
  }

  switch (m_authReq->type) {
    case AUTH_TYPE_PINCODE:
      pincodeCb(bt_error_canceled(), nullptr, this);
      break;
    case AUTH_TYPE_CONFIRM:
      passkey_confirm_cb(bt_error_canceled(), this);
      break;
    case AUTH_TYPE_PASSKEY:
      // Not Supported;
      break;
    case AUTH_TYPE_NOTIFY_PASSKEY:
      // User Notify doesn't require any reply
      break;
    case AUTH_TYPE_NOTIFY_PINCODE:
      // Not Supported by fluoride
      break;
  }
  m_authReq->agent.reset();
}

void Device::cancelAuthRequest(bool aborted) {
  if (!m_authReq) return;

  ADK_LOG_INFO(LOGTAG "Canceling authentication request for %s",
               m_name.c_str());

  if (!aborted) {
    callCancelAuthAgent();
  }

  freeAuthReq();
}

int Device::sd_createBondReqExit(sd_bus_track *UNUSED(track), void *userdata) {
  Device *pDevice = static_cast<Device *>(userdata);

  if (pDevice->m_authReq) {
    pDevice->cancelAuthRequest(false);
  }

  sd_bus_track_unref(pDevice->m_bondReq->sd_listener);
  pDevice->m_bondReq->sd_listener = nullptr;

  return 0;
}

void Device::updateConnection(bt_acl_state_t state, uint8_t disconnnectReason) {
  // called upon acl status change callback for the device
  m_connected = state ? false : true;

  if (!m_connected) {
    m_disconnectReason = disconnnectReason;
  }

  ADK_LOG_NOTICE(LOGTAG "updateConnection : %d, disconnnectReason : %d\n", m_connected, disconnnectReason);

  sd_bus_emit_properties_changed(g_sdbus, m_objPath.c_str(), DEVICE_INTERFACE,
                                 "Connected", nullptr);
}

void Device::emitConnectedA2DPUUID() {
  sd_bus_emit_properties_changed(g_sdbus, m_objPath.c_str(),
                                 DEVICE_INTERFACE, "ConnectedA2DPUUID", nullptr);
}

void Device::bondStateChanged(bt_status_t status, bt_bond_state_t new_state, bool *bRemove) {
const sd_bus_error *berror = nullptr;

  ADK_LOG_DEBUG(LOGTAG "bondStateChanged : status %d state %d\n", status, new_state);

  if (m_bondState == new_state) {
    *bRemove = false;
    return;
  }

  m_bondState = new_state;

  if (m_bondState == BT_BOND_STATE_BONDING) {
    *bRemove = false;
    return;
  }

  switch (status) {
  case BT_STATUS_SUCCESS:
  case BT_STATUS_DONE: // request already completed
    // do nothing
    break;
  case BT_STATUS_FAIL: // (org.bluez.Error.Failed or org.bluez.Error.AuthenticationFailed?)
    berror = bt_error_failed();
    break;
  case BT_STATUS_PARM_INVALID: // (org.bluez.Error.InvalidArguments)
    berror = bt_error_invalid_args();
    break;
  case BT_STATUS_RMT_DEV_DOWN:  // (org.bluez.Error.ConnectionAttemptFailed)
    berror = bt_error_connection_attempt_failed();
    break;
  case BT_STATUS_AUTH_REJECTED: // (org.bluez.Error.AuthenticationRejected)
  case BT_STATUS_AUTH_FAILURE:  // This status happens when I reject the pair request from a OnePlus 7 phone
    berror = bt_error_authentication_rejected();
    break;
   default: /* (org.bluez.Error.Failed or org.bluez.Error.AuthenticationFailed?) */
    /*
      BT_STATUS_NOT_READY,
      BT_STATUS_NOMEM,
      BT_STATUS_BUSY,
      BT_STATUS_UNSUPPORTED,
      BT_STATUS_UNHANDLED,
      BT_STATUS_JNI_ENVIRONMENT_ERROR,
      BT_STATUS_JNI_THREAD_ATTACH_ERROR,
      BT_STATUS_WAKELOCK_ERROR */
    berror = bt_error_failed();
  break;
  }

  // Update the State change to the listeners
  sd_bus_emit_properties_changed(g_sdbus, m_objPath.c_str(), DEVICE_INTERFACE,
                                 "Paired", nullptr);

  freeAuthReq();

  if (m_bondReq) {
    ADK_LOG_NOTICE(LOGTAG "m_bondReq exists...");
    if (m_bondReq->sd_msg) {
      ADK_LOG_NOTICE(LOGTAG "and send sd_bus reply.");
       if (berror != nullptr) {
         sd_bus_reply_method_error(m_bondReq->sd_msg, berror);
       } else {
         sd_bus_reply_method_return(m_bondReq->sd_msg, nullptr);
       }
    }
    freeBondingRequest(m_bondReq);
    m_bondReq = nullptr;
  }
}

int Device::sd_pairDevice(sd_bus_message *m, void *userdata,
                          sd_bus_error *ret_error) {
  const char *sender;
  Device *pDevice = static_cast<Device *>(userdata);

  if (pDevice->m_bondReq) {
    return bt_error_in_progress(ret_error);
  }

  if (pDevice->m_bondState == BT_BOND_STATE_BONDED) {
    return bt_error_already_exists(ret_error);
  }

  sender = sd_bus_message_get_sender(m);
  std::shared_ptr<Agent> agent = AgentManager::getAgent(sender);

  pDevice->m_bondReq = newBondingRequest(m, pDevice, agent);

  int res = sd_bus_track_new(g_sdbus, &(pDevice->m_bondReq->sd_listener),
                             Device::sd_createBondReqExit, pDevice);
  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Failed to create tracker: %d - %s\n", -res,
                    strerror(-res));
  }
  res = sd_bus_track_add_name(pDevice->m_bondReq->sd_listener, sender);
  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Failed to track name: %d - %s\n", -res,
                    strerror(-res));
  }

  int32_t ret = pDevice->m_adapter->createBonding(&pDevice->m_addr, pDevice->m_deviceType);
  if (ret != 0) {
    // The return value would generally be bt_status_t, in future have error
    // description elaborate string conversion of specific error.
    freeBondingRequest(pDevice->m_bondReq);
    pDevice->m_bondReq = nullptr;
    return bt_error_failed(ret_error);
  }
  // sd_bus seems to expect 1 to indicate success,
  // (note that 0 causes org.freedesktop.DBus.Error.UnknownMethod)
  return 1;
}

int Device::sd_cancelDevicePairing(sd_bus_message *m, void *userdata,
                                   sd_bus_error *ret_error) {
  Device *pDevice = static_cast<Device *>(userdata);

  if (!pDevice->m_bondReq) {
    return bt_error_does_not_exist(ret_error);
  }

  // Should the bonding request be returned here or upon cancellation ???
  sd_bus_reply_method_error(pDevice->m_bondReq->sd_msg,
                            bt_error_auth_cancelled());

  pDevice->m_adapter->cancelBonding(&pDevice->m_addr);

  freeBondingRequest(pDevice->m_bondReq);
  pDevice->m_bondReq = nullptr;

  return sd_bus_reply_method_return(m, nullptr);
}

void Device::devRemoveConnection(const std::string &uuid) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  m_connectedUUIDs.erase(uuid);
  emitConnectedA2DPUUID();
  if (m_sd_disconnReq && (m_connectedUUIDs.empty() || (!m_disconnAll))) {
    sd_event_source_unref(m_disconnTimer);
    m_disconnTimer = nullptr;

    sd_bus_reply_method_return(m_sd_disconnReq, nullptr);
    sd_bus_message_unref(m_sd_disconnReq);
    m_sd_disconnReq = nullptr;

    m_disconnAll = false;
  }
}

uint32_t Device::removeBonding(sd_bus_message *UNUSED(m)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  return m_adapter->removeBond(&m_addr);
}

void Device::devAddConnection(const std::string &uuid) {
  if (m_connectedUUIDs.find(uuid) != m_connectedUUIDs.end()) {
    // Already in the connected list
    return;
  }
  if (m_sd_connectReq) {
    sd_event_source_unref(m_connectTimer);
    m_connectTimer = nullptr;

    sd_bus_reply_method_return(m_sd_connectReq, nullptr);
    sd_bus_message_unref(m_sd_connectReq);
    m_sd_connectReq = nullptr;
  }
  m_connectedUUIDs.insert(uuid);

  emitConnectedA2DPUUID();
}

void Device::setDisconnectReqTimer(sd_bus_message *msg) {
  if (!m_sd_disconnReq) {
    m_sd_disconnReq = sd_bus_message_ref(msg);

    int res = sd_event_add_time(g_eventLoop, &m_disconnTimer, CLOCK_MONOTONIC,
                                get_timer_delay(ENABLE_TIMEOUT_DELAY), 0,
                                Device::disconnTimerExpired, this);
    if (res < 0) {
      ADK_LOG_WARNING(LOGTAG "Failed to create connect timer: %d - %s\n", -res,
                      strerror(-res));
    }
  }
}

void Device::setConnectReqTimer(sd_bus_message *msg) {
  if (!m_sd_connectReq) {
    m_sd_connectReq = sd_bus_message_ref(msg);

    int res = sd_event_add_time(g_eventLoop, &m_connectTimer, CLOCK_MONOTONIC,
                                get_timer_delay(ENABLE_TIMEOUT_DELAY), 0,
                                Device::connectTimerExpired, this);
    if (res < 0) {
      ADK_LOG_WARNING(LOGTAG "Failed to create connect timer: %d - %s\n", -res,
                      strerror(-res));
    }
  }
}

void Device::setRemoveDeviceReqTimer(sd_bus_message *msg) {
  ADK_LOG_NOTICE(LOGTAG "setRemoveDeviceReqTimer\n");
  if (!m_sd_removeDeviceReq) {
    m_sd_removeDeviceReq = sd_bus_message_ref(msg);

    int res =
        sd_event_add_time(g_eventLoop, &m_removeDeviceTimer, CLOCK_MONOTONIC,
                          get_timer_delay(REMOVEDEVICE_TIMEOUT_DELAY), 0,
                          Device::removeDeviceTimerExpired, this);
    if (res < 0) {
      ADK_LOG_WARNING(LOGTAG "Failed to create remove device timer: %d - %s\n",
                      -res, strerror(-res));
    }
  }
}

void Device::removeDeviceReqComplete() {
  ADK_LOG_NOTICE(LOGTAG "removeDeviceReqComplete\n");

  if (m_sd_removeDeviceReq) {
    sd_event_source_unref(m_removeDeviceTimer);
    m_removeDeviceTimer = nullptr;

    sd_bus_reply_method_return(m_sd_removeDeviceReq, nullptr);
    sd_bus_message_unref(m_sd_removeDeviceReq);
    m_sd_removeDeviceReq = nullptr;
  }
}

int Device::disconnTimerExpired(sd_event_source *UNUSED(s), uint64_t UNUSED(usec),
                                void *userdata) {
  Device *pDev = static_cast<Device *>(userdata);
  if (pDev->m_sd_disconnReq) {
    sd_event_source_unref(pDev->m_disconnTimer);
    pDev->m_disconnTimer = nullptr;

    sd_bus_reply_method_return(pDev->m_sd_disconnReq, nullptr);
    sd_bus_message_unref(pDev->m_sd_disconnReq);
    pDev->m_sd_disconnReq = nullptr;
  }

  return 0;
}

int Device::connectTimerExpired(sd_event_source *UNUSED(s), uint64_t UNUSED(usec),
                                void *userdata) {
  Device *pDev = static_cast<Device *>(userdata);
  if (pDev->m_sd_connectReq) {
    sd_event_source_unref(pDev->m_connectTimer);
    pDev->m_connectTimer = nullptr;

    sd_bus_reply_method_error(pDev->m_sd_connectReq, bt_error_timedout());
    sd_bus_message_unref(pDev->m_sd_connectReq);
    pDev->m_sd_connectReq = nullptr;
  }
  pDev->m_disconnAll = false;

  return 0;
}

int Device::removeDeviceTimerExpired(sd_event_source *UNUSED(s), uint64_t UNUSED(usec),
                                     void *userdata) {
  ADK_LOG_NOTICE(LOGTAG "removeDeviceTimerExpired\n");
  Device *pDev = static_cast<Device *>(userdata);
  if (pDev->m_sd_removeDeviceReq) {
    sd_event_source_unref(pDev->m_removeDeviceTimer);
    pDev->m_removeDeviceTimer = nullptr;

    sd_bus_reply_method_return(pDev->m_sd_removeDeviceReq, nullptr);
    sd_bus_message_unref(pDev->m_sd_removeDeviceReq);
    pDev->m_sd_removeDeviceReq = nullptr;
  }
  return 0;
}

int Device::sd_connect(sd_bus_message *m, void *userdata,
                       sd_bus_error *ret_error) {
  Device *pDev = static_cast<Device *>(userdata);
  ADK_LOG_NOTICE(LOGTAG " Connect request\n");

  if (pDev->m_sd_connectReq) {
    // Connection is in progress
    return bt_error_in_progress(ret_error);
  }
  return pDev->m_adapter->connectProfiles(pDev, m, std::string(), ret_error);
}

int Device::sd_disconnect(sd_bus_message *m, void *userdata,
                          sd_bus_error *ret_error) {
  Device *pDev = static_cast<Device *>(userdata);
  ADK_LOG_NOTICE(LOGTAG " Disconnect request\n");
  if (pDev->m_sd_connectReq) {
    sd_event_source_unref(pDev->m_connectTimer);
    pDev->m_connectTimer = nullptr;

    sd_bus_reply_method_error(pDev->m_sd_connectReq, bt_error_failed());
    sd_bus_message_unref(pDev->m_sd_connectReq);
    pDev->m_sd_connectReq = nullptr;
  }
  if (pDev->m_sd_disconnReq) {
    // disconnection is in progress
    return bt_error_in_progress(ret_error);
  }
  pDev->m_disconnAll = true;
  return pDev->m_adapter->disconnectProfiles(pDev, m, std::string(), ret_error);
}

int Device::sd_connectProfile(sd_bus_message *m, void *userdata,
                              sd_bus_error *ret_error) {
  Device *pDev = static_cast<Device *>(userdata);

  const char *uuid = nullptr;
  if (sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &uuid) < 0) {
    return bt_error_invalid_args(ret_error);
  }

  ADK_LOG_NOTICE(LOGTAG " Got Connect request for profile %s\n", uuid);
  return pDev->m_adapter->connectProfiles(pDev, m, uuid, ret_error);
}

int Device::sd_disconnectProfile(sd_bus_message *m, void *userdata,
                                 sd_bus_error *ret_error) {
  Device *pDev = static_cast<Device *>(userdata);
  if (pDev->m_sd_disconnReq) {
    // disonnection is in progress
    return bt_error_in_progress(ret_error);
  }

  const char *uuid = nullptr;
  if (sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &uuid) < 0) {
    return bt_error_invalid_args(ret_error);
  }

  ADK_LOG_NOTICE(LOGTAG " Got disconnect request for profile %s\n", uuid);
  pDev->m_disconnAll = false;
  return pDev->m_adapter->disconnectProfiles(pDev, m, uuid, ret_error);
}

int Device::sd_propertyGetAddress(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                  const char *UNUSED(interface), const char *UNUSED(property),
                                  sd_bus_message *reply, void *userdata,
                                  sd_bus_error *UNUSED(ret_error)) {
  Device *pDev = static_cast<Device *>(userdata);
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING,
                                     pDev->m_addr.ToString().c_str());
}

int Device::sd_propertyGetName(sd_bus *UNUSED(bus), const char *UNUSED(path),
                               const char *UNUSED(interface), const char *UNUSED(property),
                               sd_bus_message *reply, void *userdata,
                               sd_bus_error *UNUSED(ret_error)) {
  Device *pDev = static_cast<Device *>(userdata);
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING,
                                     pDev->m_name.c_str());
}

int Device::sd_propertyGetAlias(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                const char *UNUSED(interface), const char *UNUSED(property),
                                sd_bus_message *reply, void *userdata,
                                sd_bus_error *UNUSED(ret_error)) {
  Device *pDev = static_cast<Device *>(userdata);
  std::string &alias =
      pDev->m_aliasName.empty() ? pDev->m_name : pDev->m_aliasName;
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING, alias.c_str());
}

int Device::sd_propertySetAlias(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                const char *UNUSED(interface), const char *UNUSED(property),
                                sd_bus_message *UNUSED(value), void *UNUSED(userdata),
                                sd_bus_error *ret_error) {
  // Set the Alias for the device using the bt interface available with Apater
  // object associated with the device
  // TODO
  return bt_error_not_implemented(ret_error);
}

int Device::sd_propertyGetClass(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                const char *UNUSED(interface), const char *UNUSED(property),
                                sd_bus_message *reply, void *userdata,
                                sd_bus_error *UNUSED(ret_error)) {
  Device *pDev = static_cast<Device *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_UINT32>::value,
                               pDev->m_cod);
}

int Device::sd_propertyGetPaired(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  Device *pDev = static_cast<Device *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value,
                               pDev->getIsBonded());
}

int Device::sd_propertyGetUuids(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                const char *UNUSED(interface), const char *UNUSED(property),
                                sd_bus_message *reply, void *userdata,
                                sd_bus_error *UNUSED(ret_error)) {
  Device *pDev = static_cast<Device *>(userdata);
  ADK_LOG_NOTICE(LOGTAG " propertyGetUuids\n");

  int res = sd_bus_message_open_container(reply, SD_BUS_TYPE_ARRAY,
                                          TYPE_TO_STR<SD_BUS_TYPE_STRING>::value);
  if (res < 0) {
    return res;
  }

  for (const auto &i : pDev->m_uuids) {
    res = sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING, i.c_str());
    if (res < 0) {
      return res;
    }
  }

  return sd_bus_message_close_container(reply);
}

int Device::sd_propertyGetConnectedA2DPUUID(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                const char *UNUSED(interface), const char *UNUSED(property),
                                sd_bus_message *reply, void *userdata,
                                sd_bus_error *UNUSED(ret_error)) {
  Device *pDev = static_cast<Device *>(userdata);
  ADK_LOG_NOTICE(LOGTAG " propertyGetConnectedA2dpUUID, return %s\n", (*(pDev->m_connectedUUIDs.begin())).c_str());

  // Just return the first entry in the Connected Device list as
  // we only support 1 connection at the moment.
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING,
                               (*(pDev->m_connectedUUIDs.begin())).c_str());
}

int Device::sd_propertyGetAdapter(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                  const char *UNUSED(interface), const char *UNUSED(property),
                                  sd_bus_message *reply, void *userdata,
                                  sd_bus_error *UNUSED(ret_error)) {
  Device *pDev = static_cast<Device *>(userdata);
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_OBJECT_PATH,
                                     pDev->m_adapter->getAdapterObjectPath());
}

int Device::sd_propertyGetConnected(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                    const char *UNUSED(interface), const char *UNUSED(property),
                                    sd_bus_message *reply, void *userdata,
                                    sd_bus_error *UNUSED(ret_error)) {
  Device *pDev = static_cast<Device *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value,
                               pDev->m_connected);
}

int Device::sd_getDisconnectReason(sd_bus *UNUSED(bus), const char *UNUSED(path),
                             const char *UNUSED(interface), const char *UNUSED(property),
                             sd_bus_message *reply, void *userdata,
                             sd_bus_error *UNUSED(ret_error)) {
  Device *pDev = static_cast<Device *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_UINT32>::value,
                                     pDev->m_disconnectReason);
}

int Device::sd_getRSSI(sd_bus *bus, const char *path,
                       const char *interface, const char *property,
                       sd_bus_message *reply, void *userdata,
                       sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG " %s", __func__);
  Device *pDev = static_cast<Device *>(userdata);
  // Units in dBm (as specified in the core spec)
  return sd_bus_message_append(reply, "n", pDev->m_rssi);
}

bool Device::getIsBonded() { return (m_bondState > BT_BOND_STATE_BONDING); }

Device::Device(Adapter *adapter, const bt_bdaddr_t &address,
               const std::string &name, const std::string &friendlyName,
               int8_t rssi, uint32_t cod, bt_device_type_t device_type, bt_bond_state_t bond_state)
    : m_adapter(adapter),
      m_addr(address),
      m_name(name),
      m_aliasName(friendlyName),
      m_rssi(rssi),
      m_cod(cod),
      m_deviceType(device_type),
      m_bondState(bond_state),
      m_connected(false),
      m_disconnectReason(0),
      m_bondReq(nullptr),
      m_authReq(nullptr),
      m_connectTimer(nullptr),
      m_disconnTimer(nullptr),
      m_removeDeviceTimer(nullptr),
      m_disconnAll(false),
      m_sdbusDeviceIFSlot(nullptr),
      m_sdbusControlIFSlot(nullptr),
      m_playState(PlayState::STOPPED),
      m_position(0),
      m_volumeNotifRequested(false),
      m_volume(kDefaultVolume),
      m_muted(false),
      m_volumeLabel(0),
      m_absVolumeSupport(false),
      m_setAbsVolInprogress(false),
      m_equalizerMode(Equalizer::OFF),
      m_repeatMode(Repeat::OFF),
      m_shuffleMode(Shuffle::OFF),
      m_scanMode(Scan::OFF),
      m_sd_connectReq(nullptr),
      m_sd_disconnReq(nullptr),
      m_sd_removeDeviceReq(nullptr) {
  createDbusDeviceObject();
}

Device::~Device() {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);

  freeAuthReq();

  m_player.reset();

  if (m_bondReq) {
    if (m_bondReq->sd_msg) {
      sd_bus_reply_method_return(m_bondReq->sd_msg, nullptr);
    }
    freeBondingRequest(m_bondReq);
    m_bondReq = nullptr;
  }

  if (m_sd_connectReq) {
    sd_bus_reply_method_return(m_sd_connectReq, nullptr);
    sd_bus_message_unref(m_sd_connectReq);
    m_sd_connectReq = nullptr;
  }
  sd_event_source_unref(m_connectTimer);
  m_connectTimer = nullptr;

  if (m_sd_disconnReq) {
    sd_bus_reply_method_return(m_sd_disconnReq, nullptr);
    sd_bus_message_unref(m_sd_disconnReq);
    m_sd_disconnReq = nullptr;
  }
  sd_event_source_unref(m_disconnTimer);
  m_disconnTimer = nullptr;

  if (m_sd_removeDeviceReq) {
    sd_bus_reply_method_return(m_sd_removeDeviceReq, nullptr);
    sd_bus_message_unref(m_sd_removeDeviceReq);
    m_sd_removeDeviceReq = nullptr;
  }
  sd_event_source_unref(m_removeDeviceTimer);
  m_removeDeviceTimer = nullptr;

  // Unregistration of the control interface (i.e. avrcp service/profile)
  // for this device will be done through the adapter object
  m_adapter->onDeviceRemoval(this);

  sd_bus_emit_object_removed(g_sdbus, m_objPath.c_str());

  sd_bus_slot_unref(m_sdbusDeviceIFSlot);
  m_sdbusDeviceIFSlot = nullptr;
  ADK_LOG_NOTICE(LOGTAG " Object Destroyed for %s\n", m_name.c_str());
}

void Device::createDbusDeviceObject() {
  std::string addrSr = m_addr.ToString();
  std::replace(addrSr.begin(), addrSr.end(), ':', '_');

  const char *adapter_path = m_adapter->getAdapterObjectPath();
  m_objPath = std::string(adapter_path) + "/dev_" + addrSr;

  ADK_LOG_NOTICE(LOGTAG " Creating device object @ %s for %s \n",
                 m_objPath.c_str(), m_name.c_str());

  static const sd_bus_vtable sSdDeviceVTable[] = {
      SD_BUS_VTABLE_START(0),

      SD_BUS_METHOD("Pair", nullptr, nullptr, Device::sd_pairDevice,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("CancelPairing", nullptr, nullptr,
                    Device::sd_cancelDevicePairing, SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Connect", nullptr, nullptr, Device::sd_connect,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Disconnect", nullptr, nullptr, Device::sd_disconnect,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("ConnectProfile", "s", nullptr, Device::sd_connectProfile,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("DisconnectProfile", "s", nullptr,
                    Device::sd_disconnectProfile, SD_BUS_VTABLE_UNPRIVILEGED),

      SD_BUS_PROPERTY("Address", "s", Device::sd_propertyGetAddress, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Name", "s", Device::sd_propertyGetName, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("Alias", "s", Device::sd_propertyGetAlias,
                               Device::sd_propertySetAlias, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Class", "u", Device::sd_propertyGetClass, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("UUIDs", "as", Device::sd_propertyGetUuids, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Paired", "b", Device::sd_propertyGetPaired, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Adapter", "o", Device::sd_propertyGetAdapter, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Connected", "b", Device::sd_propertyGetConnected, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("DisconnectReason", "u", Device::sd_getDisconnectReason, 0,
                      SD_BUS_VTABLE_PROPERTY_EXPLICIT),
      SD_BUS_PROPERTY("ConnectedA2DPUUID", "s", Device::sd_propertyGetConnectedA2DPUUID, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("RSSI", "n", Device::sd_getRSSI, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_VTABLE_END};

  int res =
      sd_bus_add_object_vtable(g_sdbus, &m_sdbusDeviceIFSlot, m_objPath.c_str(),
                               DEVICE_INTERFACE, sSdDeviceVTable, this);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "interface init failed on path %s: %d - %s\n",
                   m_objPath.c_str(), -res, strerror(-res));
    return;
  }

  res = sd_bus_emit_object_added(g_sdbus, m_objPath.c_str());

  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG
                   "object manager failed to signal new path %s: %d - %s\n",
                   m_objPath.c_str(), -res, strerror(-res));
    return;
  }
}

void Device::createDbusMediaPlayerObject() {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);
  if (!m_player) {
    m_player = std::make_shared<MediaPlayer>(this);
    if (!m_player->registerDeviceMediaPlayerObject()) {
      m_player.reset();
    }
  }
}

void Device::updateDeviceProperties(const std::vector<BtProperty> &properties) {
  ADK_LOG_NOTICE(LOGTAG "%s", __func__);

  for (const BtProperty &property : properties) {
    switch (property.type) {
      case BT_PROPERTY_BDNAME:
        ADK_LOG_INFO(LOGTAG " -- BT_PROPERTY_BDNAME\n");
        m_name =
            std::string(reinterpret_cast<const char *>(property.value.data()),
                        property.value.size());
        sd_bus_emit_properties_changed(g_sdbus, m_objPath.c_str(),
                                       DEVICE_INTERFACE, "Name", nullptr);
        break;
      case BT_PROPERTY_CLASS_OF_DEVICE:
        ADK_LOG_INFO(LOGTAG " -- BT_PROPERTY_CLASS_OF_DEVICE\n");
        memcpy(&m_cod, property.value.data(), property.value.size());
        sd_bus_emit_properties_changed(g_sdbus, m_objPath.c_str(),
                                       DEVICE_INTERFACE, "Class", nullptr);
        break;
      case BT_PROPERTY_REMOTE_RSSI:
        ADK_LOG_INFO(LOGTAG " -- BT_PROPERTY_REMOTE_RSSI\n");
        memcpy(&m_rssi, property.value.data(), property.value.size());
        sd_bus_emit_properties_changed(g_sdbus, m_objPath.c_str(),
                                       DEVICE_INTERFACE, "RSSI", nullptr);
        break;
      case BT_PROPERTY_REMOTE_FRIENDLY_NAME:
        ADK_LOG_INFO(LOGTAG " -- BT_PROPERTY_REMOTE_FRIENDLY_NAME\n");
        m_aliasName =
            std::string(reinterpret_cast<const char *>(property.value.data()),
                        property.value.size());
        sd_bus_emit_properties_changed(g_sdbus, m_objPath.c_str(),
                                       DEVICE_INTERFACE, "Alias", nullptr);
        break;
      case BT_PROPERTY_TYPE_OF_DEVICE:
        ADK_LOG_INFO(LOGTAG " -- BT_PROPERTY_TYPE_OF_DEVICE\n");
        memcpy(&m_deviceType, property.value.data(), property.value.size());
        break;
      case BT_PROPERTY_UUIDS: {
        ADK_LOG_INFO(LOGTAG " -- BT_PROPERTY_UUIDS\n");
        m_uuids.clear();
        for (size_t i = 0; i < property.value.size() / 16; i++) {
          bluetooth::Uuid::UUID128Bit uuid;
          memcpy((uint8_t *)&uuid[0], property.value.data() + (i * 16), 16);
          m_uuids.insert(bluetooth::Uuid::From128BitBE(uuid).ToString());
        }
        sd_bus_emit_properties_changed(g_sdbus, m_objPath.c_str(),
                                       DEVICE_INTERFACE, "UUIDs", nullptr);
        m_adapter->probeDeviceServices(this);
      } break;
      default:
        break;
    }
  }
}

AuthReq *Device::createAuthReq(auth_type_t type,
                               const std::shared_ptr<Agent> &agent) {
  AuthReq *pAuth;

  ADK_LOG_NOTICE(LOGTAG " Requesting agent authentication for %s",
                 m_name.c_str());

  if (m_authReq) {
    ADK_LOG_NOTICE(LOGTAG " Authentication already requested for %s",
                   m_name.c_str());
    return nullptr;
  }

  pAuth = new AuthReq();
  pAuth->agent = agent;
  pAuth->address = m_addr;
  pAuth->type = type;
  return pAuth;
}

void Device::freeAuthReq() {
  if (!m_authReq) {
    return;
  }

  delete m_authReq;

  m_authReq = nullptr;
}

void Device::pincodeCb(const sd_bus_error *err, const char *pin, void *data) {
  Device *pDev = static_cast<Device *>(data);
  bt_pin_code_t pinCode;
  int pin_len = 0;
  /* No need to reply anything if the authentication already failed or performed
   */
  if (pDev->m_authReq->agent.expired()) return;

  uint8_t accept = sd_bus_error_is_set(err) ? 0 : 1;

  if (pin) {
    pin_len =
        (strlen(pin) < sizeof(pinCode.pin)) ? strlen(pin) : sizeof(pinCode.pin);
    memcpy(&pinCode.pin[0], pin, pin_len);
  }

  pDev->m_adapter->setPinReply(&pDev->m_addr, accept, pin_len, &pinCode);
}

void Device::passkey_confirm_cb(const sd_bus_error *err, void *data) {
  Device *pDev = static_cast<Device *>(data);

  /* No need to reply anything if the authentication already failed */
  if (pDev->m_authReq->agent.expired()) {
    return;
  }

  uint8_t accept = sd_bus_error_is_set(err) ? 0 : 1;
  pDev->m_adapter->sspReply(&pDev->m_addr, pDev->m_authReq->ssp_type, accept,
                            pDev->m_authReq->passkey);
}

void Device::deviceSspRequest(bt_ssp_variant_t pairingVariant,
                              uint32_t passKey) {
  std::shared_ptr<Agent> pAgent;

  if (m_bondReq) {
    pAgent = m_bondReq->agent.lock();
  }
  if (!pAgent) {
    pAgent = AgentManager::getAgent(std::string());
  }

  if (pAgent && (pAgent->capability != AgentCapability::NOINPUTNOOUTPUT)) {
    AuthReq *pAuth = nullptr;
    int err = 0;

    pAuth = createAuthReq(AUTH_TYPE_PASSKEY, pAgent);
    if (!pAuth) {
      return;
    }
    m_authReq = pAuth;
    m_authReq->passkey = passKey;
    m_authReq->ssp_type = pairingVariant;

    switch (pairingVariant) {
      case BT_SSP_VARIANT_PASSKEY_CONFIRMATION:
        m_authReq->type = AUTH_TYPE_CONFIRM;
        err = agentRequestConfirmation(pAgent, passKey,
                                       Device::passkey_confirm_cb, this);
        break;
      case BT_SSP_VARIANT_PASSKEY_ENTRY:
        m_authReq->type = AUTH_TYPE_PASSKEY;
        // This type is not supported by fluoride yet.
        // If supported in future then need to check for agent cap before
        // posting the request as of now return error
        err = -ENOSYS;
        break;
      case BT_SSP_VARIANT_CONSENT:
        m_authReq->type = AUTH_TYPE_CONFIRM;
        err =
            agentRequestAuthorization(pAgent, Device::passkey_confirm_cb, this);
        break;
      case BT_SSP_VARIANT_PASSKEY_NOTIFICATION:
        m_authReq->type = AUTH_TYPE_NOTIFY_PASSKEY;
        err = agentDisplayPasskey(pAgent, passKey, passKey);
        break;
      default:
        // unsupported ssp type
        break;
    }
    if (err < 0) {
      ADK_LOG_ERROR(LOGTAG "Failed requesting authentication for ssp");
      freeAuthReq();
    }
  } else {
    m_adapter->sspReply(&m_addr, pairingVariant, 1, passKey);
  }
}

void Device::deviceRequestPin(bool bSecure) {
  bt_pin_code_t pincode;
  std::shared_ptr<Agent> pAgent;

  memset(&pincode, 0, sizeof(pincode));
  /*most common pincode "0000" */
  pincode.pin[0] = '0';
  pincode.pin[1] = '0';
  pincode.pin[2] = '0';
  pincode.pin[3] = '0';

  // Respond with failure as bluez interface havent added support for secure
  // pairing
  if (bSecure) {
    m_adapter->setPinReply(&m_addr, 0, sizeof(bt_pin_code_t), &pincode);
    return;
  }
  // Check if there is bonding request associate for the device, if so check for
  // the capablility and respond accordingly
  if (m_bondReq) {
    pAgent = m_bondReq->agent.lock();
  }
  if (!pAgent) {
    pAgent = AgentManager::getAgent(std::string());
  }

  if ((pAgent) && (pAgent->capability != AgentCapability::NOINPUTNOOUTPUT)) {
    AuthReq *pAuth = nullptr;
    int err = 0;
    pAuth = createAuthReq(AUTH_TYPE_PINCODE, pAgent);
    if (!pAuth) {
      return;
    }
    m_authReq = pAuth;
    err = agentRequestPincode(pAgent, Device::pincodeCb, this);
    if (err < 0) {
      ADK_LOG_ERROR(LOGTAG "Failed requesting authentication");
      freeAuthReq();
    }
  } else {
    m_adapter->setPinReply(&m_addr, 1, 4, &pincode);
  }
  return;
}

void Device::setPlayState(PlayState state) {
  m_playState = state;
  if (m_player) {
    m_player->playStateChanged();
  }
}

void Device::setPlayPosition(uint32_t position) {
  m_position = position;
  if (m_player) {
    m_player->playPositionChanged();
  }
}

uint16_t Device::registerVolumeNotif(uint8_t label) {
  m_volumeLabel = label;
  m_volumeNotifRequested = true;
  return m_volume;
}

void Device::setVolumeFromBt(uint16_t volume) {
  if(volume > kMaxVolume) {
    m_volume = kMaxVolume;
  }
  else {
    m_volume = volume;
  }
  if (m_transport) {
    m_transport->volumeChanged();
  }
}

void Device::setVolumeFromPlayer(uint16_t volume) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  if ((!m_absVolumeSupport) || m_volume == volume) {
    return;
  }
  m_volume = volume;
  if (m_volumeNotifRequested) {
    AvrcpCTService *avrcp = m_adapter->findConnectableService<AvrcpCTService>();
    if (avrcp) {
      avrcp->setVolume(this, volume, m_volumeLabel);
    }
    m_volumeNotifRequested = false;
  } else {
    AvrcpTGService *avrcp = m_adapter->findConnectableService<AvrcpTGService>();
    if (avrcp && (!getIsAbsVolInprogress())) {
      ADK_LOG_INFO(LOGTAG " -- AvrcpTGService->setVolume...\n");
      setAbsVolInprogress(true);
      avrcp->setVolume(volume);
    }
  }
}

void Device::setMutedFromBt() {
  m_muted = !m_muted;
  if (m_transport) {
    m_transport->mutedChanged();
  }
}

void Device::setMutedFromPlayer(bool muted) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (m_muted == muted) {
    return;
  }
  m_muted = muted;
  AvrcpCTService *avrcp = m_adapter->findConnectableService<AvrcpCTService>();
  if (avrcp) {
    avrcp->handlePassThroughCmdReq(AvrcpProfileService::CMD_ID_MUTED,this);
  }
}

void Device::setAbsVolumeSupport(bool absVolumeSupport) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if(m_absVolumeSupport == absVolumeSupport) {
    return;
  }
  m_absVolumeSupport = absVolumeSupport;
  if (m_transport) {
    m_transport->absVolumeSupportChanged();
  }
}

void Device::setMetadata(const Metadata &metadata) {
  if (metadata == m_metadata) {
    return;
  }
  m_metadata = metadata;
  if (m_player) {
    m_player->metadataChanged();
  }
  ADK_LOG_NOTICE(LOGTAG
                 "Item:\n"
                 "\tTitle: %s\n"
                 "\tArtist: %s\n"
                 "\tAlbum: %s\n"
                 "\tGenre: %s\n"
                 "\tTrack: %u/%u\n"
                 "\tDuration: %u\n",
                 m_metadata.title.c_str(), m_metadata.artist.c_str(),
                 m_metadata.album.c_str(), m_metadata.genre.c_str(),
                 m_metadata.trackNum, m_metadata.trackCount,
                 m_metadata.duration);
}

void Device::setEqualizerFromBt(Equalizer mode) {
  if (m_equalizerMode == mode) {
    return;
  }
  m_equalizerMode = mode;
  if (m_player) {
    m_player->equalizerChanged();
  }
}

void Device::setEqualizerFromPlayer(Equalizer mode) {
  if (m_equalizerMode == mode) {
    return;
  }
  m_equalizerMode = mode;

  AvrcpCTService *avrcp = m_adapter->findConnectableService<AvrcpCTService>();
  if (avrcp) {
    avrcp->setEqualizer(this, mode);
  }
}

void Device::setRepeatFromBt(Repeat mode) {
  if (m_repeatMode == mode) {
    return;
  }
  m_repeatMode = mode;
  if (m_player) {
    m_player->repeatChanged();
  }
}

void Device::setRepeatFromPlayer(Repeat mode) {
  if (m_repeatMode == mode) {
    return;
  }
  m_repeatMode = mode;

  AvrcpCTService *avrcp = m_adapter->findConnectableService<AvrcpCTService>();
  if (avrcp) {
    avrcp->setRepeat(this, mode);
  }
}

void Device::setShuffleFromBt(Shuffle mode) {
  if (m_shuffleMode == mode) {
    return;
  }
  m_shuffleMode = mode;
  if (m_player) {
    m_player->shuffleChanged();
  }
}

void Device::setShuffleFromPlayer(Shuffle mode) {
  if (m_shuffleMode == mode) {
    return;
  }
  m_shuffleMode = mode;

  AvrcpCTService *avrcp = m_adapter->findConnectableService<AvrcpCTService>();
  if (avrcp) {
    avrcp->setShuffle(this, mode);
  }
}

void Device::setScanFromBt(Scan mode) {
  if (m_scanMode == mode) {
    return;
  }
  m_scanMode = mode;
  if (m_player) {
    m_player->scanChanged();
  }
}

void Device::setScanFromPlayer(Scan mode) {
  if (m_scanMode == mode) {
    return;
  }
  m_scanMode = mode;

  AvrcpCTService *avrcp = m_adapter->findConnectableService<AvrcpCTService>();
  if (avrcp) {
    avrcp->setScan(this, mode);
  }
}
