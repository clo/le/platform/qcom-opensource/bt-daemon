/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef REM_DEVICE_HPP
#define REM_DEVICE_HPP

#include <sys/types.h>

#include <hardware/bluetooth.h>
#include <hardware/bt_av_vendor.h>
#include <systemdq/sd-bus.h>
#include <systemdq/sd-event.h>

#include <map>
#include <memory>
#include <set>
#include <string>

#include "Agent.hpp"
#include "Common.hpp"

#define MAX_NAME_LENGTH 248

class Adapter;
class Device;
class MediaPlayer;
class MediaTransport;

struct BondingReq;
struct AuthReq;

typedef enum {
  AUTH_TYPE_PINCODE,
  AUTH_TYPE_PASSKEY,
  AUTH_TYPE_CONFIRM,
  AUTH_TYPE_NOTIFY_PASSKEY,
  AUTH_TYPE_NOTIFY_PINCODE,
} auth_type_t;

enum struct PlayState {
  INVALID = -1,
  // Values must match Bluetooth's (see btrc_play_status_t)
  STOPPED = 0x00,
  PLAYING = 0x01,
  PAUSED = 0x02,
  FWD_SEEK = 0x03,
  REV_SEEK = 0x04,
  ERROR = 0xFF,
};
const char *to_string(PlayState mode);
template <>
PlayState from_string<PlayState>(const std::string &mode);

enum struct Equalizer {
  INVALID = -1,
  // Values must match Bluetooth's (see btrc_player_equalizer_val_t)
  OFF = 0x01,
  ON = 0x02,
};
const char *to_string(Equalizer mode);
template <>
Equalizer from_string<Equalizer>(const std::string &mode);

enum struct Repeat {
  INVALID = -1,
  // Values must match Bluetooth's (see btrc_player_repeat_val_t)
  OFF = 0x01,
  SINGLE = 0x02,
  ALL = 0x03,
  GROUP = 0x04,
};
const char *to_string(Repeat mode);
template <>
Repeat from_string<Repeat>(const std::string &mode);

enum struct Shuffle {
  INVALID = -1,
  // Values must match Bluetooth's (see btrc_player_shuffle_val_t)
  OFF = 0x01,
  ALL = 0x02,
  GROUP = 0x03,
};
const char *to_string(Shuffle mode);
template <>
Shuffle from_string<Shuffle>(const std::string &mode);

enum struct Scan {
  INVALID = -1,
  // Values must match Bluetooth's (see btrc_player_scan_val_t)
  OFF = 0x01,
  ON = 0x02,
  GROUP = 0x03,
};
const char *to_string(Scan mode);
template <>
Scan from_string<Scan>(const std::string &mode);

struct Metadata {
  std::string title;
  std::string album;
  std::string artist;
  std::string genre;
  uint32_t duration = UINT32_MAX;
  uint32_t trackNum = UINT32_MAX;
  uint32_t trackCount = UINT32_MAX;

  std::string currentTrackId;
  std::vector<std::string> tracklist_map;

  bool operator==(const Metadata &rhs) const {
    return ((title == rhs.title) && (album == rhs.album) &&
            (artist == rhs.artist) && (genre == rhs.genre) &&
            (duration == rhs.duration) && (trackNum == rhs.trackNum) &&
            (trackCount == rhs.trackCount) && (currentTrackId == rhs.currentTrackId) &&
            (tracklist_map == rhs.tracklist_map));
  }
};

class Device {
 private:
  static constexpr uint16_t kDefaultVolume = 20;
  static constexpr uint16_t kMaxVolume = 127;

  Adapter *m_adapter;

  bt_bdaddr_t m_addr;  // Address of Bluetooth Adapter device
  std::string m_name;  // Name currently in use for the Adapter (must be
                       // <MAX_NAME_LENGTH)
  std::string m_aliasName;

  std::set<std::string> m_uuids;  // UUID's supported by Device
  int8_t m_rssi;
  uint32_t m_cod;                 // Adapter's Class of Device
  bt_device_type_t m_deviceType;

  bt_bond_state_t m_bondState;
  bool m_connected;
  uint32_t m_disconnectReason;
  std::string m_objPath;  // Object path of D-Bus interface

  BondingReq *m_bondReq;
  AuthReq *m_authReq;

  std::set<std::string> m_connectedUUIDs;
  sd_event_source *m_connectTimer;
  sd_event_source *m_disconnTimer;
  sd_event_source *m_removeDeviceTimer;

  bool m_disconnAll;

  sd_bus_slot *m_sdbusDeviceIFSlot;
  sd_bus_slot *m_sdbusControlIFSlot;

  std::shared_ptr<MediaPlayer> m_player;
  std::shared_ptr<MediaTransport> m_transport;

  PlayState m_playState;
  uint32_t m_position;
  bool m_volumeNotifRequested;
  uint16_t m_volume;
  bool m_muted;
  uint8_t m_volumeLabel;
  bool m_absVolumeSupport;
  bool m_setAbsVolInprogress;
  Metadata m_metadata;
  Equalizer m_equalizerMode;
  Repeat m_repeatMode;
  Shuffle m_shuffleMode;
  Scan m_scanMode;

 public:
  sd_bus_message *m_sd_connectReq;
  sd_bus_message *m_sd_disconnReq;
  sd_bus_message *m_sd_removeDeviceReq;

  Device(Adapter *adapter, const bt_bdaddr_t &address, const std::string &name,
         const std::string &friendlyName, int8_t rssi, uint32_t cod,
         bt_device_type_t device_type, bt_bond_state_t bond_state);
  ~Device();

  void setConnectReqTimer(sd_bus_message *msg);
  void setDisconnectReqTimer(sd_bus_message *msg);
  void setRemoveDeviceReqTimer(sd_bus_message *msg);
  void removeDeviceReqComplete();

  static int connectTimerExpired(sd_event_source *s, uint64_t usec,
                                 void *userdata);
  static int disconnTimerExpired(sd_event_source *s, uint64_t usec,
                                 void *userdata);
  static int removeDeviceTimerExpired(sd_event_source *s, uint64_t usec,
                                      void *userdata);
  static int sd_pairDevice(sd_bus_message *m, void *userdata,
                           sd_bus_error *ret_error);
  static int sd_cancelDevicePairing(sd_bus_message *m, void *userdata,
                                    sd_bus_error *ret_error);
  static int sd_connect(sd_bus_message *m, void *userdata,
                        sd_bus_error *ret_error);
  static int sd_disconnect(sd_bus_message *m, void *userdata,
                           sd_bus_error *ret_error);
  static int sd_connectProfile(sd_bus_message *m, void *userdata,
                               sd_bus_error *ret_error);
  static int sd_disconnectProfile(sd_bus_message *m, void *userdata,
                                  sd_bus_error *ret_error);

  static int sd_propertyGetAddress(sd_bus *bus, const char *path,
                                   const char *interface, const char *property,
                                   sd_bus_message *reply, void *userdata,
                                   sd_bus_error *ret_error);
  static int sd_propertyGetName(sd_bus *bus, const char *path,
                                const char *interface, const char *property,
                                sd_bus_message *reply, void *userdata,
                                sd_bus_error *ret_error);
  static int sd_propertyGetAlias(sd_bus *bus, const char *path,
                                 const char *interface, const char *property,
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *ret_error);
  static int sd_propertySetAlias(sd_bus *bus, const char *path,
                                 const char *interface, const char *property,
                                 sd_bus_message *value, void *userdata,
                                 sd_bus_error *ret_error);
  static int sd_propertyGetClass(sd_bus *bus, const char *path,
                                 const char *interface, const char *property,
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *ret_error);
  static int sd_propertyGetPaired(sd_bus *bus, const char *path,
                                  const char *interface, const char *property,
                                  sd_bus_message *reply, void *userdata,
                                  sd_bus_error *ret_error);
  static int sd_propertyGetUuids(sd_bus *bus, const char *path,
                                 const char *interface, const char *property,
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *ret_error);
  static int sd_propertyGetConnectedA2DPUUID(sd_bus *bus, const char *path,
                                 const char *interface, const char *property,
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *ret_error);
  static int sd_propertyGetAdapter(sd_bus *bus, const char *path,
                                   const char *interface, const char *property,
                                   sd_bus_message *reply, void *userdata,
                                   sd_bus_error *ret_error);
  static int sd_propertyGetConnected(sd_bus *bus, const char *path,
                                     const char *interface,
                                     const char *property,
                                     sd_bus_message *reply, void *userdata,
                                     sd_bus_error *ret_error);
  static int sd_getDisconnectReason(sd_bus *bus, const char *path,
                                    const char *interface, const char *property,
                                    sd_bus_message *reply, void *userdata,
                                    sd_bus_error *ret_error);
  static int sd_getRSSI(sd_bus *bus, const char *path,
                        const char *interface, const char *property,
                        sd_bus_message *reply, void *userdata,
                        sd_bus_error *ret_error);

  static void pincodeCb(const sd_bus_error *err, const char *pin, void *data);
  static void passkey_confirm_cb(const sd_bus_error *err, void *data);
  static int sd_createBondReqExit(sd_bus_track *track, void *userdata);

  void createDbusDeviceObject();
  void createDbusMediaPlayerObject();
  void updateDeviceProperties(const std::vector<BtProperty> &properties);
  bool getIsBonded();

  bool isACLConnected() {return m_connected;}

  void deviceRequestPin(bool bSecure);
  void deviceSspRequest(bt_ssp_variant_t pairingVariant, uint32_t passKey);

  AuthReq *createAuthReq(auth_type_t type, const std::shared_ptr<Agent> &agent);
  void freeAuthReq();
  void cancelAuthRequest(bool bAbort);
  void callCancelAuthAgent();

  void bondStateChanged(bt_status_t status, bt_bond_state_t new_state, bool *bRemove);
  void updateConnection(bt_acl_state_t state, uint8_t disconnnectReason);

  void devAddConnection(const std::string &uuid);
  void devRemoveConnection(const std::string &uuid);
  uint32_t removeBonding(sd_bus_message *m);

  Adapter *getAdapter() { return m_adapter; }
  std::string getDeviceObjectPath() { return m_objPath; }
  bt_bdaddr_t getDeviceAddr() { return m_addr; }
  std::set<std::string> getSupportedUUIDs() { return m_uuids; }
  std::string getName() { return m_name; }
  sd_bus_slot **ControlIFSlot() { return &m_sdbusControlIFSlot; }

  void setTransport(const std::shared_ptr<MediaTransport> &transport) {
    m_transport = transport;
  }
  std::shared_ptr<MediaTransport> getTransport() { return m_transport; }

  void setPlayState(PlayState state);
  PlayState getPlayState() { return m_playState; }
  void setPlayPosition(uint32_t position);
  uint32_t getPlayPosition() { return m_position; }

  uint16_t registerVolumeNotif(uint8_t label);
  void setVolumeFromBt(uint16_t volume);
  void setVolumeFromPlayer(uint16_t volume);
  uint16_t getVolume() { return m_volume; }

  void setMutedFromBt();
  void setMutedFromPlayer(bool muted);
  bool getMuted() { return m_muted; }

  void setAbsVolumeSupport(bool absVolumeSupport);
  bool getIsAbsVolumeSupported() { return m_absVolumeSupport; }

  void setAbsVolInprogress(bool status) {
    m_setAbsVolInprogress = status;
  }
  bool getIsAbsVolInprogress() { return m_setAbsVolInprogress; }

  void setMetadata(const Metadata &metadata);
  Metadata getMetadata() { return m_metadata; }

  void setEqualizerFromBt(Equalizer mode);
  void setEqualizerFromPlayer(Equalizer mode);
  Equalizer getEqualizer() { return m_equalizerMode; }

  void setRepeatFromBt(Repeat mode);
  void setRepeatFromPlayer(Repeat mode);
  Repeat getRepeat() { return m_repeatMode; }

  void setShuffleFromBt(Shuffle mode);
  void setShuffleFromPlayer(Shuffle mode);
  Shuffle getShuffle() { return m_shuffleMode; }

  void setScanFromBt(Scan mode);
  void setScanFromPlayer(Scan mode);
  Scan getScan() { return m_scanMode; }

private:
  void emitConnectedA2DPUUID();
};

#endif  // REM_DEVICE_HPP
