/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SERVICE_ERROR_H
#define SERVICE_ERROR_H

#include <systemdq/sd-bus.h>

#define ERROR_INTERFACE "org.bluez.Error"

class bt_error {
  char const *const name;
  char const *const msg;
  const sd_bus_error err;

 public:
  constexpr bt_error(const char *name, const char *msg)
      : name{name}, msg{msg}, err(SD_BUS_ERROR_MAKE_CONST(name, msg)) {}
  int operator()(sd_bus_error *error) const {
    return sd_bus_error_set_const(error, name, msg);
  }
  constexpr const sd_bus_error *operator()() const { return &err; }
  int operator()(sd_bus_error *error, const char *_msg) const {
    return sd_bus_error_setf(error, name, "%s", _msg);
  }
};

static const bt_error bt_error_invalid_args(ERROR_INTERFACE ".InvalidArguments",
                                            "Invalid arguments in method call");
static const bt_error bt_error_busy(ERROR_INTERFACE ".InProgress",
                                    "In Progress");
static const bt_error bt_error_already_exists(ERROR_INTERFACE ".AlreadyExists",
                                              "Already Exists");
static const bt_error bt_error_not_supported(ERROR_INTERFACE ".NotSupported",
                                             "Operation is not supported");
static const bt_error bt_error_not_connected(ERROR_INTERFACE ".NotConnected",
                                             "Not Connected");
static const bt_error bt_error_already_connected(ERROR_INTERFACE
                                                 ".AlreadyConnected",
                                                 "Already Connected");
static const bt_error bt_error_connection_attempt_failed(ERROR_INTERFACE
                                                 ".ConnectionAttemptFailed",
                                                 "Connection Attempt Failed");
static const bt_error bt_error_in_progress(ERROR_INTERFACE ".InProgress",
                                           "In Progress");
static const bt_error bt_error_not_available(
    ERROR_INTERFACE ".NotAvailable", "Operation currently not available");
static const bt_error bt_error_does_not_exist(ERROR_INTERFACE ".DoesNotExist",
                                              "Does Not Exist");
static const bt_error bt_error_not_authorized(ERROR_INTERFACE ".NotAuthorized",
                                              "Operation Not Authorized");
static const bt_error bt_error_no_such_adapter(ERROR_INTERFACE ".NoSuchAdapter",
                                               "No such adapter");
static const bt_error bt_error_agent_not_available(ERROR_INTERFACE
                                                   ".AgentNotAvailable",
                                                   "Agent Not Available");
static const bt_error bt_error_not_ready(ERROR_INTERFACE ".NotReady",
                                         "Resource Not Ready");
static const bt_error bt_error_auth_cancelled(ERROR_INTERFACE
                                              ".AuthenticationCanceled",
                                              "Authentication Canceled");
static const bt_error bt_error_authentication_rejected(ERROR_INTERFACE
                                              ".AuthenticationRejected",
                                              "Authentication Rejected");
static const bt_error bt_error_not_implemented(ERROR_INTERFACE
                                               ".NotImplemented",
                                               "Not implemented");
static const bt_error bt_error_timedout(ERROR_INTERFACE ".TimedOut",
                                        "Timed out");
static const bt_error bt_error_canceled(ERROR_INTERFACE ".Canceled",
                                        "Canceled");
static const bt_error bt_error_failed(ERROR_INTERFACE ".Failed",
                                      "Operation failed");

#endif  // SERVICE_ERROR_H
