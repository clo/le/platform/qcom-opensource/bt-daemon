/*
Copyright (c) 2016-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "HidService.hpp"
#include <algorithm>
#include <iomanip>
#include <vector>
#include <sstream>
#include <string>
#include "Adapter.hpp"
#include "Common.hpp"
#include "MediaManager.hpp"

#define LOGTAG "HID_SERVICE "

HidService *HidService::gHidService = nullptr;

void HidService::connection_state_cb(bt_bdaddr_t *bd_addr, bthh_connection_state_t state) {
  ADK_LOG_NOTICE(LOGTAG "Received Connection State Callback %d\n", state);
  bool device_connected = false;
  switch (state) {
    case BTHH_CONN_STATE_CONNECTED: {
      device_connected = true;
    } break;
    case BTHH_CONN_STATE_DISCONNECTED: {
      device_connected = false;
    } break;
    case BTHH_CONN_STATE_CONNECTING:
      return;
    case BTHH_CONN_STATE_DISCONNECTING:
      return;
    case BTHH_CONN_STATE_FAILED_MOUSE_FROM_HOST:
    case BTHH_CONN_STATE_FAILED_KBD_FROM_HOST:
    case BTHH_CONN_STATE_FAILED_TOO_MANY_DEVICES:
    case BTHH_CONN_STATE_FAILED_NO_BTHID_DRIVER:
    case BTHH_CONN_STATE_FAILED_GENERIC:
    case BTHH_CONN_STATE_UNKNOWN:
      ADK_LOG_NOTICE(LOGTAG "Unknown State of Hid Connection");
      return;
    default:
      return;
      break;
  }

  if (gHidService == nullptr) {
    return;
  }
  gHidService->UpdateDeviceList(*bd_addr, device_connected);
  if (gHidService->m_mediaManager == nullptr) {
    ADK_LOG_WARNING(LOGTAG "No Media Manager Registered. Can't update property");
    return;
  }
  // Find the device object in the adapter device list for the bluetooth address.
  auto adapter_ptr = gHidService->getAdapter();
  if (adapter_ptr == nullptr) {
    return;
  }

  Device *device_object = adapter_ptr->getDevice(*bd_addr);
  if (device_object == nullptr) {
    return;
  }
  if (!gHidService->m_mediaManager->setDeviceConnected(device_object, device_connected)) {
    ADK_LOG_ERROR(LOGTAG "setDeviceConnected failed");
    return;
  }
}

void HidService::handshake_cb(bt_bdaddr_t *UNUSED(bd_addr), bthh_status_t UNUSED(hh_status)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

static bthh_callbacks_t sBluetoothHidCallbacks = {
    sizeof(sBluetoothHidCallbacks),
    HidService::connection_state_cb,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    HidService::handshake_cb,
};

void HidService::raw_hid_data_cb(const RawAddress &device_address, uint8_t *rpt, uint16_t len, bool report_id_flag) {
  if (gHidService->m_mediaManager == nullptr) {
    ADK_LOG_WARNING(LOGTAG "No Media Manager Registered. Can't send raw hid report");
    return;
  }
  std::string device_object_path;
  if (getDeviceObjectPath(device_address, &device_object_path)) {
    // It is assumed that fluoride accepts that the data is blocked while
    // the call to send the HID Report is made.
    gHidService->m_mediaManager->SendHidReport(device_object_path, rpt, len, report_id_flag);
  }
}

void HidService::conn_params_cb(const RawAddress &device_address, uint16_t interval,
    uint16_t latency, uint16_t timeout, uint8_t hh_status) {
  if (hh_status != BTHH_OK) {
    return;
  }
  ADK_LOG_NOTICE(LOGTAG "%s -- conn_params_cb: bda %s interval %d latency %d timeout %d",
      __func__, device_address.ToString().c_str(), interval, latency, timeout);

  if (gHidService->m_mediaManager == nullptr) {
    ADK_LOG_WARNING(LOGTAG "No Media Manager Registered. Can't Update Connection Parameters");
    return;
  }
  std::string device_object_path;
  if (getDeviceObjectPath(device_address, &device_object_path)) {
    gHidService->m_mediaManager->UpdateConnectionParameters(device_object_path, interval, latency, timeout);
  }
}

void HidService::cfg_mtu_cb(const RawAddress &device_address, uint16_t mtu, uint8_t hh_status) {
  ADK_LOG_NOTICE(LOGTAG "%s -- bda %s mtu %d", __func__, device_address.ToString().c_str(), mtu);
  if (hh_status != BTHH_OK) {
    ADK_LOG_ERROR(LOGTAG "%s -- HH_Error: %d", __func__, hh_status);
    return;
  }
  if (gHidService->m_mediaManager == nullptr) {
    ADK_LOG_WARNING(LOGTAG "No Media Manager Registered. Can't Update MTU");
    return;
  }
  std::string device_object_path;
  if (getDeviceObjectPath(device_address, &device_object_path)) {
    gHidService->m_mediaManager->UpdateMtu(device_object_path, mtu);
  }
}

static bthh_vendor_callbacks_t sBtHhVendorCallbacks = {
    sizeof(sBtHhVendorCallbacks),
    HidService::raw_hid_data_cb,
    HidService::cfg_mtu_cb,
    HidService::conn_params_cb};

void HidService::UpdateDeviceList(const bt_bdaddr_t &device_address, bool device_connected) {
  bool device_exists = false;
  for (auto address : m_connectedDeviceAddressList) {
    if (address == device_address) {
      device_exists = true;
      break;
    }
  }
  if (!device_exists) {
    m_connectedDeviceAddressList.push_back(device_address);
  } else if (!device_connected) {
    m_connectedDeviceAddressList.remove(device_address);
  }
}

std::string HidService::EncodeReportData(const uint8_t *data, size_t data_size) {
  std::stringstream data_hex_string_stream;
  for (size_t i = 0; i < data_size; i++) {
    data_hex_string_stream << std::hex << std::setw(2) << std::setfill('0') << int(data[i]);
  }
  return data_hex_string_stream.str();
}

bool HidService::getDeviceObjectPath(const RawAddress &device_address, std::string *device_object_path) {
  if (gHidService == nullptr){
    return false;
  }
  auto adapter = gHidService->getAdapter();
  if (adapter == nullptr) {
    ADK_LOG_WARNING(LOGTAG "Could not get Adapter object");
    return false;
  }
  auto device_object = adapter->getDevice(device_address);
  if (device_object == nullptr) {
    ADK_LOG_WARNING(LOGTAG "Could not get Device object");
    return false;
  }
  *device_object_path = device_object->getDeviceObjectPath();
  return true;
}

bool HidService::getDeviceAddress(const std::string device_object_path, RawAddress *device_address) {
  if (gHidService == nullptr){
    return false;
  }
  if (device_object_path == "") {
    return false;
  }
  auto adapter = gHidService->getAdapter();
  if (adapter == nullptr) {
    ADK_LOG_WARNING(LOGTAG "Could not get Adapter object");
    return false;
  }

  auto device_ptr = adapter->getDeviceFromPath(device_object_path);
  if (device_ptr == nullptr) {
    ADK_LOG_WARNING(LOGTAG "Could not get Device object");
    return false;
  }
  *device_address = device_ptr->getDeviceAddr();
  return true;
}

HidService::HidService(Adapter *pAdapter)
    : ProfileService(pAdapter, LOCAL_UUID, REMOTE_UUID) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

HidService::~HidService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

void HidService::enableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  const bt_interface_t *pBtIf = getAdapter()->getBtInterface();
  if (!pBtIf) {
    ADK_LOG_NOTICE(LOGTAG "enableService failed: no Bt interface\n");
    return;
  }
  m_btHidIF = (bthh_interface_t *)pBtIf->get_profile_interface(BT_PROFILE_HIDHOST_ID);
  m_btHidVendorIF = (bthh_vendor_interface_t *)pBtIf->get_profile_interface(BT_PROFILE_HID_VENDOR_ID);

  if (!m_btHidIF || !m_btHidVendorIF) {
    ADK_LOG_NOTICE(LOGTAG "enableService failed: HID\n");
    return;
  }

  bt_status_t status = m_btHidIF->init(&sBluetoothHidCallbacks);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "failed to init HID: %d\n", status);
  }

  status = m_btHidVendorIF->init_vendor(&sBtHhVendorCallbacks);
  if (status != BT_STATUS_SUCCESS) {
    ADK_LOG_NOTICE(LOGTAG "failed to init HID vendor: %d\n", status);
  }

  serviceState() = State::kRunning;

  // Reference for callback handling
  gHidService = this;
  ADK_LOG_NOTICE(LOGTAG "enableService: running\n");
}

void HidService::disableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Clear any object associated with the base service like
  // endpoint connections and transport (if any).
  for (auto address : m_connectedDeviceAddressList) {
    Device *device = getAdapter()->getDevice(address);
    if (device) {
      device->devRemoveConnection(srvUUID());
    }
    if (!m_mediaManager->setDeviceConnected(device, false)) {
      ADK_LOG_ERROR(LOGTAG "setDeviceConnected failed");
      return;
    }
  }

  serviceState() = State::kStopped;
  m_connectedDeviceAddressList.clear();
  gHidService = nullptr;
}

bool HidService::connect(bt_bdaddr_t *devAddr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Send the connection request for the device and update the device connection
  // state to pending
  if ((m_btHidIF) && (m_btHidIF->connect(devAddr) == BT_STATUS_SUCCESS)) {
    ADK_LOG_NOTICE(LOGTAG
        " HID connection initiated with device with %s\n",
        devAddr->ToString().c_str());
    return true;
  }
  return false;
}

bool HidService::disconnect(bt_bdaddr_t *devAddr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (std::find(m_connectedDeviceAddressList.begin(), m_connectedDeviceAddressList.end(), *devAddr) != m_connectedDeviceAddressList.end()) {
    ADK_LOG_NOTICE(LOGTAG " HID Disconnecting from %s\n",
        devAddr->ToString().c_str());
    if (m_btHidIF) {
      m_btHidIF->disconnect(devAddr);
      return true;
    }
  }
  return false;
}

void HidService::setMtu(std::string device_object_path, int mtu) {
  ADK_LOG_NOTICE(LOGTAG "%s object_path:%s, mtu:%d", __func__, device_object_path.c_str(), mtu);
  if (gHidService->m_mediaManager == nullptr) {
    return;
  }
  RawAddress device_address;
  if (getDeviceAddress(device_object_path, &device_address)) {
    if (m_btHidVendorIF != nullptr) {
      m_btHidVendorIF->configure_mtu(device_address, mtu);
    }
  }
}

void HidService::setConnectionParameters(std::string device_object_path, uint16_t interval, uint16_t latency, uint16_t timeout) {
  ADK_LOG_NOTICE(LOGTAG "%s object_path:%s, interval:%d latency:%d timeout:%d", __func__, device_object_path.c_str(), interval, latency, timeout);
  if (gHidService->m_mediaManager == nullptr) {
    return;
  }
  RawAddress device_address;
  if (getDeviceAddress(device_object_path, &device_address)) {
    if (m_btHidVendorIF != nullptr) {
      m_btHidVendorIF->conn_parameter_update(device_address, interval, interval, latency, timeout, 0, interval);
    }
  }
}

void HidService::sendOutputReport(std::string device_object_path, const uint8_t *report_data, size_t report_size) {
  if (gHidService->m_mediaManager == nullptr) {
    return;
  }

  RawAddress device_address;
  if (getDeviceAddress(device_object_path, &device_address)) {
    if (m_btHidVendorIF != nullptr) {
      // Fluoride is expecting an ascii encoded hex decimal null terminated char array.
      // This is because set_report, get_report_callback and set_data are a
      // part of AOSP, which is maintained same between Android and LE platform.
      std::string ascii_encoded_data = EncodeReportData(report_data, report_size);
      // Create char array from std::string
      std::vector<char> cstr(ascii_encoded_data.c_str(), ascii_encoded_data.c_str() + ascii_encoded_data.size() + 1);
      m_btHidIF->set_report(&device_address, BTHH_OUTPUT_REPORT, cstr.data());
    }
  }
}

void HidService::ConfirmDevices() {
  for (auto device : m_connectedDeviceAddressList) {
    if (gHidService == nullptr) {
      return;
    }
    if (gHidService->m_mediaManager == nullptr) {
      return;
    }
    // Find the device object in the adapter device list for the bluetooth address.
    auto adapter_ptr = gHidService->getAdapter();
    if (adapter_ptr == nullptr) {
      return;
    }

    Device *device_object = adapter_ptr->getDevice(device);
    if (device_object == nullptr) {
      return;
    }
    if (!gHidService->m_mediaManager->setDeviceConnected(device_object, true)) {
      ADK_LOG_ERROR(LOGTAG "setDeviceConnected failed");
      return;
    }
  }
}
