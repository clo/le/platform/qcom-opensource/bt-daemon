/*
Copyright (c) 2016-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef HIDSERVICE_HPP
#define HIDSERVICE_HPP

#include <hardware/bluetooth.h>
#include <hardware/bt_hh.h>
#include <hardware/bt_hh_vendor.h>
#include <types/raw_address.h>

#include <list>
#include <vector>

#include "ProfileService.hpp"
class MediaManager;

class HidService : public ProfileService {
  typedef std::list<bt_bdaddr_t> Device_list;
  public:
  enum DeviceState { kDisconnected,
    kPending,
    kConnected };

  explicit HidService(Adapter *pAdapter);

  static constexpr const char *REMOTE_UUID =
      "00001812-0000-1000-8000-00805f9b34fb";
  static constexpr const char *LOCAL_UUID =
      "00001812-0000-1000-8000-00805f9b34fb";

  virtual ~HidService();

  void enableService();
  void disableService();
  bool connect(bt_bdaddr_t *devAddr) final;
  bool disconnect(bt_bdaddr_t *devAddr) final;

  void registerMediaManager(MediaManager *media_manager) { m_mediaManager = media_manager; }
  void unregisterMediaManager() { m_mediaManager = nullptr; }

  // These methods should be modified in the future to take a bluetooth address
  // rather than the device object path string. However this requires the caller
  // to have knowledge of the adapter which is not present in the current design.
  void setMtu(std::string device_object_path, int mtu);
  void setConnectionParameters(std::string device_object_path, uint16_t interval,
                        uint16_t latency, uint16_t timeout);

  void sendOutputReport(std::string device_object_path, const uint8_t *report_data, size_t report_size);
  // Interfaces to Fluoride stack
  bool isConnectable() final { return true; }
  DeviceState getDeviceState() const { return m_deviceState; }
  Device_list getConnectedDevicesList() const { return m_connectedDeviceAddressList; }
  void ConfirmDevices();
  static void connection_state_cb(bt_bdaddr_t *bd_addr, bthh_connection_state_t state);
  static void handshake_cb(bt_bdaddr_t *bd_addr, bthh_status_t hh_status);

  static void raw_hid_data_cb(const RawAddress &device_address, uint8_t *rpt, uint16_t len, bool rpt_id_flag);
  static void conn_params_cb(const RawAddress &device_address, uint16_t interval,
      uint16_t latency, uint16_t timeout, uint8_t hh_status);
  static void cfg_mtu_cb(const RawAddress &device_address, uint16_t mtu, uint8_t hh_status);
  private:

  void UpdateDeviceList(const bt_bdaddr_t &device_address, bool device_connected);

  DeviceState m_deviceState;
  Device_list m_connectedDeviceAddressList;
  bthh_interface_t *m_btHidIF;
  bthh_vendor_interface_t *m_btHidVendorIF;
  MediaManager *m_mediaManager = nullptr;

  // Service reference for Fluoride callbacks
  static HidService *gHidService;

  static std::string EncodeReportData(const uint8_t * data, size_t data_size);
  static bool getDeviceObjectPath (const RawAddress &device_address, std::string *device_object_path);
  static bool getDeviceAddress (const std::string device_object_path, RawAddress *device_address);
};

#endif  // HIDSERVICE_HPP
