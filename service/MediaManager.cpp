/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "MediaManager.hpp"

#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <algorithm>    // std::find


#include "A2dpSinkService.hpp"
#include "A2dpSourceService.hpp"
#include "AvrcpTGService.hpp"
#include "Common.hpp"
#include "MediaTransport.hpp"
#include "Sdbus.hpp"

#define LOGTAG "MEDIA_MANAGER "
#define MEDIA_INTERFACE "org.bluez.Media1"
#define MEDIA_ENDPOINT_INTERFACE "org.bluez.MediaEndpoint1"
#define MEDIA_MPRIS_PLAYER_INTERFACE "org.mpris.MediaPlayer2.Player"
#define HID_APP_INTERFACE "com.qualcomm.qti.HOGP1"
#define ADAPTER_PATH "/org/bluez/hci0"

#define SD_BUS_PROPERTY_INTERFACE "org.freedesktop.DBus.Properties"

#define REQUEST_TIMEOUT (3 * 1000) /* 3 seconds */

// The difference between two floating point numbers below which they
// are considered equal.
#define ACCEPTABLE_DIFFERENCE 0.001

struct MediaEndPointRequest;

struct MediaEndPoint {
  std::string sender; /* Endpoint DBus bus id */
  std::string path;   /* Endpoint object path */
  uint8_t codec;      /* Endpoint codec */
  std::string uuid;   /* Endpoint uuid profile */
  btav_codec_configuration_t codec_config;
  sd_bus_track *sd_watch;
  std::set<MediaEndPointRequest *> requests;
  std::shared_ptr<MediaTransport> transport;
};

struct MediaEndPointRequest {
  MediaEndPoint *endpoint;
  sd_bus_slot *slot;
  MediaEndPointCallBack cb;
  void *user_data;
};

MediaManager *g_MediaManager;

static const char *getSbcSamplingRate(uint8_t rate) {
  switch (rate) {
    case SBC_SAMP_FREQ_48:
      return "48 kHz ";
    case SBC_SAMP_FREQ_44:
      return "44.1 kHz ";
    case SBC_SAMP_FREQ_32:
      return "32 kHz ";
    case SBC_SAMP_FREQ_16:
      return "16 kHz ";
    default:
      return "";
  }
}

static const char *getSbcChannelCount(uint8_t count) {
  switch (count) {
    case SBC_CH_JOINT:
      return "Joint stereo ";
    case SBC_CH_STEREO:
      return "Stereo ";
    case SBC_CH_DUAL:
      return "Dual ";
    case SBC_CH_MONO:
      return "Mono ";
    default:
      return "";
  }
}

static const char *getMp3Layer(uint8_t layer) {
  switch (layer) {
    case MP3_LAYER_1:
      return "MP1";
    case MP3_LAYER_2:
      return "MP2";
    case MP3_LAYER_3:
      return "MP3";
    default:
      return "MPEG";
  }
}

static const char *getMp3SamplingRate(uint8_t rate) {
  switch (rate) {
    case MP3_SAMP_FREQ_48000:
      return "48 kHz ";
    case MP3_SAMP_FREQ_44100:
      return "44.1 kHz ";
    case MP3_SAMP_FREQ_32000:
      return "32 kHz ";
    case MP3_SAMP_FREQ_24000:
      return "24 kHz ";
    case MP3_SAMP_FREQ_22050:
      return "22.05 kHz ";
    case MP3_SAMP_FREQ_16000:
      return "16 kHz ";
    default:
      return "";
  }
}

static const char *getMp3ChannelCount(uint8_t count) {
  switch (count) {
    case MP3_CHANNEL_JOINT_STEREO:
      return "Joint stereo ";
    case MP3_CHANNEL_STEREO:
      return "Stereo ";
    case MP3_CHANNEL_DUAL:
      return "Dual ";
    case MP3_CHANNEL_MONO:
      return "Mono ";
    default:
      return "";
  }
}

static const char *getAacObjectType(uint8_t obj_type) {
  switch (obj_type) {
    case AAC_OBJ_TYPE_MPEG_2_AAC_LC:
      return "MPEG-2 AAC LC";
    case AAC_OBJ_TYPE_MPEG_4_AAC_LC:
      return "MPEG-4 AAC LC";
    case AAC_OBJ_TYPE_MPEG_4_AAC_LTP:
      return "MPEG-4 AAC LTP";
    case AAC_OBJ_TYPE_MPEG_4_AAC_SCA:
      return "MPEG-4 AAC SCA";
    default:
      return "MPEG-2,4";
  }
}

static const char *getAacSamplingRate(uint16_t rate) {
  switch (rate) {
    case AAC_SAMP_FREQ_96000:
      return "96 kHz ";
    case AAC_SAMP_FREQ_88200:
      return "88.2 kHz ";
    case AAC_SAMP_FREQ_64000:
      return "64 kHz ";
    case AAC_SAMP_FREQ_48000:
      return "48 kHz ";
    case AAC_SAMP_FREQ_44100:
      return "44.1 kHz ";
    case AAC_SAMP_FREQ_32000:
      return "32 kHz ";
    case AAC_SAMP_FREQ_24000:
      return "24 kHz ";
    case AAC_SAMP_FREQ_22050:
      return "22.05 kHz ";
    case AAC_SAMP_FREQ_16000:
      return "16 kHz ";
    case AAC_SAMP_FREQ_12000:
      return "12 kHz ";
    case AAC_SAMP_FREQ_11025:
      return "11.025 kHz ";
    case AAC_SAMP_FREQ_8000:
      return "8 kHz ";
    default:
      return "";
  }
}

static const char *getAacChannelCount(uint8_t count) {
  switch (count) {
    case AAC_CHANNELS_1:
      return "Mono ";
    case AAC_CHANNELS_2:
      return "Stereo ";
    default:
      return "";
  }
}

static const char *getAptxSamplingRate(uint8_t rate) {
  switch (rate) {
    case APTX_SAMPLERATE_48000:
      return "48 kHz ";
    case APTX_SAMPLERATE_44100:
      return "44.1 kHz ";
    default:
      return "";
  }
}

static const char *getAptxChannelCount(uint8_t count) {
  switch (count) {
    case APTX_CHANNELS_MONO:
      return "Mono ";
    case APTX_CHANNELS_STEREO:
      return "Stereo ";
    default:
      return "";
  }
}

static const char *getAptxAdSamplingRate(uint8_t rate) {
  switch (rate) {
    case APTX_AD_SAMPLERATE_48000:
      return "48 kHz ";
    case APTX_AD_SAMPLERATE_44100:
      return "44.1 kHz ";
    default:
      return "";
  }
}

static const char *getAptxAdChannelCount(uint8_t count) {
  return getAptxChannelCount(count);
}

const char *MediaManager::getCodecName(uint16_t codec) {
  switch (codec) {
    case A2DP_SINK_AUDIO_CODEC_SBC:
      return "SBC";
    case A2DP_SINK_AUDIO_CODEC_MP3:
      return "MP3";
    case A2DP_SINK_AUDIO_CODEC_AAC:
      return "AAC";
    case A2DP_SINK_AUDIO_CODEC_ATRAC:
      return "ATRAC";
    case A2DP_SINK_AUDIO_CODEC_APTX:
      return "APTX";
    case A2DP_SINK_AUDIO_CODEC_APTX_AD:
      return "APTX_AD";
    case A2DP_SINK_AUDIO_CODEC_PCM:
      return "PCM";
    case NON_A2DP_CODEC:
      return "VENDOR with embeded codec";
    default:
      return "<unknown>";
  }
}

std::string MediaManager::getCodecInfoString(
    uint16_t codec, const btav_codec_config_t &config) {
  std::stringstream info;
  switch (codec) {
    case A2DP_SINK_AUDIO_CODEC_PCM: {
      info << getSbcSamplingRate(config.sbc_config.samp_freq)
           << getSbcChannelCount(config.sbc_config.ch_mode)
           << getCodecName(codec) << " (16-bit)";
      break;
    }
    case A2DP_SINK_AUDIO_CODEC_SBC: {
      info << getSbcSamplingRate(config.sbc_config.samp_freq)
           << getSbcChannelCount(config.sbc_config.ch_mode)
           << getCodecName(codec);
      break;
    }
    case A2DP_SINK_AUDIO_CODEC_MP3: {
      uint16_t bit_rate = config.mp3_config.bit_rate;
      info << getMp3SamplingRate(config.mp3_config.sampling_freq)
           << getMp3ChannelCount(config.mp3_config.channel_count)
           << (bit_rate > 0 ? std::to_string(bit_rate) : "")
           << (bit_rate > 0 ? " Kbps " : "")
           << (config.mp3_config.vbr ? "(VBR) " : "")
           << getMp3Layer(config.mp3_config.layer)
           << (config.mp3_config.crc ? " [CRC]" : "")
           << (config.mp3_config.mpf ? " [MPF-2]" : "");
      break;
    }
    case A2DP_SINK_AUDIO_CODEC_AAC: {
      uint32_t bit_rate = config.aac_config.bit_rate;
      info << getAacSamplingRate(config.aac_config.sampling_freq)
           << getAacChannelCount(config.aac_config.channel_count)
           << (bit_rate > 0 ? std::to_string(bit_rate / 1024) : "")
           << (bit_rate > 0 ? " Kbps " : "")
           << (config.aac_config.vbr ? "(VBR) " : "")
           << getAacObjectType(config.aac_config.obj_type);
      break;
    }
    case A2DP_SINK_AUDIO_CODEC_APTX: {
      info << getAptxSamplingRate(config.aptx_config.sampling_freq)
           << getAptxChannelCount(config.aptx_config.channel_count)
           << getCodecName(codec);
      break;
    }
    case A2DP_SINK_AUDIO_CODEC_APTX_AD: {
      info << getAptxAdSamplingRate(config.aptx_config.sampling_freq)
           << getAptxAdChannelCount(config.aptx_config.channel_count)
           << getCodecName(codec);
      break;
    }
    default: {
      info << "<unsupported codec (0x" << std::hex << codec << ")>";
      break;
    }
  }
  return info.str();
}

bool MediaManager::getCodecConfigFromCapabilities(
    uint8_t codec, const uint8_t *capabilities, size_t size,
    btav_codec_configuration_t &config) {
  switch (codec) {
    case A2DP_SINK_AUDIO_CODEC_PCM:
      // This is likely SBC with the A2DP_SINK_ENABLE_SBC_DECODING flag set in
      // Fluoride to use its own internal decoder (which then outputs PCM)
    case A2DP_SINK_AUDIO_CODEC_SBC:
      if (size != 4 * sizeof(uint8_t)) {
        ADK_LOG_ERROR(LOGTAG "%s: Invalid SBC capabilities length", __func__);
        return false;
      }
      ADK_LOG_DEBUG(LOGTAG "%s: [%02x %02x %02x %02x]\n", __func__,
                    capabilities[0], capabilities[1], capabilities[2],
                    capabilities[3]);
      config.codec_config.sbc_config.samp_freq = capabilities[0] & 0xf0;
      config.codec_config.sbc_config.ch_mode = capabilities[0] & 0x0f;
      config.codec_config.sbc_config.block_len = capabilities[1] & 0xf0;
      config.codec_config.sbc_config.num_subbands = capabilities[1] & 0x0c;
      config.codec_config.sbc_config.alloc_mthd = capabilities[1] & 0x03;
      config.codec_config.sbc_config.min_bitpool = capabilities[2] & 0xff;
      config.codec_config.sbc_config.max_bitpool = capabilities[3] & 0xff;
      config.codec_type = codec;
	  break;

    case A2DP_SINK_AUDIO_CODEC_MP3:
      if (size != 4 * sizeof(uint8_t)) {
        ADK_LOG_ERROR(LOGTAG "%s: Invalid MP3 capabilities length", __func__);
        return false;
      }
      ADK_LOG_DEBUG(LOGTAG "%s: [%02x %02x %02x %02x]\n", __func__,
                    capabilities[0], capabilities[1], capabilities[2],
                    capabilities[3]);
      config.codec_config.mp3_config.layer = capabilities[0] & 0xe0;
      config.codec_config.mp3_config.crc = capabilities[0] & 0x10;
      config.codec_config.mp3_config.channel_count = capabilities[0] & 0x0f;
      config.codec_config.mp3_config.mpf = capabilities[1] & 0x40;
      config.codec_config.mp3_config.sampling_freq = capabilities[1] & 0x3f;
      config.codec_config.mp3_config.vbr = capabilities[2] & 0x80;
      config.codec_config.mp3_config.bit_rate =
          ((capabilities[2] & 0x7f) << 8) | (capabilities[3] & 0xff);
      config.codec_type = codec;
      break;

    case A2DP_SINK_AUDIO_CODEC_AAC:
      if (size != 6 * sizeof(uint8_t)) {
        ADK_LOG_ERROR(LOGTAG "%s: Invalid AAC capabilities length", __func__);
        return false;
      }
      ADK_LOG_DEBUG(LOGTAG "%s: [%02x %02x %02x %02x %02x %02x]\n", __func__,
                    capabilities[0], capabilities[1], capabilities[2],
                    capabilities[3], capabilities[4], capabilities[5]);
      config.codec_config.aac_config.obj_type = capabilities[0] & 0xff;
      config.codec_config.aac_config.sampling_freq =
          ((capabilities[1] & 0xff) << 8) | (capabilities[2] & 0xf0);
      config.codec_config.aac_config.channel_count = capabilities[2] & 0x0c;
      config.codec_config.aac_config.vbr = capabilities[3] & 0x80;
      config.codec_config.aac_config.bit_rate =
          ((capabilities[3] & 0x7f) << 16) | ((capabilities[4] & 0xff) << 8) |
          (capabilities[5] & 0xff);
      config.codec_type = codec;
      break;

    case NON_A2DP_CODEC: {
      uint32_t vendor_id = capabilities[0] | (capabilities[1] << 8) |
                           (capabilities[2] << 16) | (capabilities[3] << 24);
      uint16_t codec_id = capabilities[4] | (capabilities[5] << 8);
      if ((vendor_id == APT_VENDOR_ID) &&
          (codec_id == APTX_CODEC_ID_CLASSIC)) {
        if (size != 17 * sizeof(uint8_t)) {
          ADK_LOG_ERROR(LOGTAG "%s: Invalid AptX capabilities length", __func__);
          return false;
        }
        ADK_LOG_DEBUG(LOGTAG "%s: [%02x %02x %02x %02x %02x %02x %02x %02x %02x]\n",
                    __func__, capabilities[0], capabilities[1], capabilities[2],
                    capabilities[3], capabilities[4], capabilities[5],
                    capabilities[6], capabilities[7], capabilities[8]);
        config.codec_config.aptx_config.vendor_id = vendor_id;
        config.codec_config.aptx_config.codec_id = codec_id; // vendor codec id
        config.codec_config.aptx_config.sampling_freq = capabilities[6] & 0xf0;
        config.codec_config.aptx_config.channel_count = capabilities[6] & 0x0f;
        config.codec_type = A2DP_SINK_AUDIO_CODEC_APTX; // Fluoride codec id
      }
      else if ((vendor_id == QTI_VENDOR_ID) &&
        (codec_id == APTX_CODEC_ID_ADAPTIVE)) {
        if (size != 40 * sizeof(uint8_t)) {
          ADK_LOG_ERROR(LOGTAG "%s: Invalid AptX_AD capabilities length", __func__);
          return false;
        }
        ADK_LOG_DEBUG(LOGTAG "%s: [%02x %02x %02x %02x %02x %02x %02x %02x %02x]\n",
                    __func__, capabilities[0], capabilities[1], capabilities[2],
                    capabilities[3], capabilities[4], capabilities[5],
                    capabilities[6], capabilities[7], capabilities[8]);
        config.codec_config.aptx_ad_config.vendor_id = vendor_id; // vendor codec id
        config.codec_config.aptx_ad_config.codec_id = codec_id;
        config.codec_config.aptx_ad_config.sampling_freq = capabilities[6];
        config.codec_config.aptx_ad_config.channel_count = capabilities[7];
        config.codec_type = A2DP_SINK_AUDIO_CODEC_APTX_AD; // Fluoride codec id
      }
      else {
        ADK_LOG_ERROR(LOGTAG "%s: Unsupported vendor codec id (%u)", __func__, codec_id);
        return false;
      }
      break;
    }

    default:
      ADK_LOG_ERROR(LOGTAG "%s: Unsupported codec (%u)", __func__, codec);
      return false;
  }
  return true;
}

void MediaManager::clearConfiguration(MediaEndPoint *endpoint) {
  ADK_LOG_INFO(LOGTAG "bus call " MEDIA_ENDPOINT_INTERFACE
                      ".ClearConfiguration(%s)\n",
               endpoint->transport->getTransportPath().c_str());

  int res = sd_bus_call_method_async(
      g_sdbus, nullptr, endpoint->sender.c_str(), endpoint->path.c_str(),
      MEDIA_ENDPOINT_INTERFACE, "ClearConfiguration",
      MediaManager::endpointCleared, nullptr, "o",
      endpoint->transport->getTransportPath().c_str());

  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Could not call ClearConfiguration: %d - %s\n", -res,
                    strerror(-res));
    return;
  }
}

void MediaManager::clearEndpoint(MediaEndPoint *endpoint) {
  ADK_LOG_NOTICE(LOGTAG " -->%s\n", __func__);
  mediaEndPointCancelAll(endpoint);

  if (endpoint->transport) {
    clearConfiguration(endpoint);
  }
}

int MediaManager::endpointCleared(sd_bus_message *reply, void *UNUSED(userdata),
                                  sd_bus_error *ret_error) {
  if (sd_bus_error_is_set(ret_error)) {
    ADK_LOG_NOTICE(LOGTAG "DBus error calling ClearConfiguration: %d - %s\n",
                   sd_bus_error_get_errno(ret_error), ret_error->name);
    return sd_bus_error_get_errno(ret_error);
  }

  const sd_bus_error *reply_error = sd_bus_message_get_error(reply);
  if (sd_bus_error_is_set(reply_error)) {
    ADK_LOG_WARNING(LOGTAG "ClearConfiguration() reply error: %d - %s (%s)\n",
                    sd_bus_error_get_errno(reply_error), reply_error->name,
                    reply_error->message);
    return sd_bus_error_get_errno(reply_error);
  }

  return 0;
}

void MediaManager::releaseMediaEndPoint(MediaEndPoint *endpoint) {
  int res;

  ADK_LOG_NOTICE(LOGTAG " -->%s\n", __func__);
  if (endpoint->sd_watch) {
    clearEndpoint(endpoint);

    ADK_LOG_INFO(LOGTAG "bus call " MEDIA_ENDPOINT_INTERFACE ".Release()\n");
    res = sd_bus_call_method_async(
        g_sdbus, nullptr, endpoint->sender.c_str(), endpoint->path.c_str(),
        MEDIA_ENDPOINT_INTERFACE, "Release", MediaManager::endpointReleased,
        nullptr, nullptr);

    if (res < 0) {
      ADK_LOG_NOTICE(LOGTAG "Could not call Release: %d - %s\n", -res,
                     strerror(-res));
      // Endpoint probably exited without waiting for Release(), continue
    }
  }

  // Remove this endpoint from the MediaManager database
  MediaManager *pMediaManager = g_MediaManager;
  pMediaManager->m_EndPointsList.erase(endpoint);

  // Notify other clients that endpoint is no longer available
  sd_bus_emit_properties_changed(g_sdbus, ADAPTER_PATH, MEDIA_INTERFACE,
                                 "RegisteredEndpoints", nullptr);

  mediaEndPointDestroy(endpoint);
}

int MediaManager::endpointReleased(sd_bus_message *reply, void *UNUSED(userdata),
                                   sd_bus_error *ret_error) {
  if (sd_bus_error_is_set(ret_error)) {
    ADK_LOG_NOTICE(LOGTAG "DBus error calling Release: %d - %s\n",
                   sd_bus_error_get_errno(ret_error), ret_error->name);
    return sd_bus_error_get_errno(ret_error);
  }

  const sd_bus_error *reply_error = sd_bus_message_get_error(reply);
  if (sd_bus_error_is_set(reply_error)) {
    ADK_LOG_WARNING(LOGTAG "Release() reply error: %d - %s (%s)\n",
                    sd_bus_error_get_errno(reply_error), reply_error->name,
                    reply_error->message);
    return sd_bus_error_get_errno(reply_error);
  }

  return 0;
}

void MediaManager::endpointRequestFree(MediaEndPointRequest *request) {
  if (request->slot) {
    sd_bus_slot_unref(request->slot);
  }

  delete request;
}

// Cancels pending dbus requests like setConfiguration, etc
void MediaManager::mediaEndPointCancel(MediaEndPointRequest *request) {
  MediaEndPoint *endpoint = request->endpoint;

  ADK_LOG_NOTICE(LOGTAG " --%s\n", __func__);

  if (request->slot) {
    sd_bus_slot_unref(request->slot);
  }

  endpoint->requests.erase(request);

  if (request->cb) request->cb(endpoint, -1, request->user_data);

  endpointRequestFree(request);
}

// Cancels all pending requests from this endpoint
void MediaManager::mediaEndPointCancelAll(MediaEndPoint *endpoint) {
  ADK_LOG_NOTICE(LOGTAG " --%s\n", __func__);
  while (endpoint->requests.size() != 0) {
    mediaEndPointCancel(*endpoint->requests.begin());
  }
}

void MediaManager::mediaEndPointDestroy(MediaEndPoint *endpoint) {
  ADK_LOG_NOTICE(LOGTAG " --%s\n", __func__);
  mediaEndPointCancelAll(endpoint);

  sd_bus_track_unref(endpoint->sd_watch);
  endpoint->sd_watch = nullptr;

  // Do not assume that transport is always present
  if (endpoint->transport) {
    endpoint->transport->triggerTransportRelease();
  }
  delete endpoint;
}

// Returns TRUE if end point is created otherwise FALSE
bool MediaManager::mediaEndPointCreate(const char *sender, const char *path,
                                       const char *uuid, bool UNUSED(delay_reporting),
                                       uint8_t codec,
                                       const uint8_t *capabilities,
                                       size_t size) {
  MediaEndPoint *endpoint;
  MediaManager *pMediaManagerObj = g_MediaManager;

  endpoint = new MediaEndPoint();
  if (!endpoint) return false;

  endpoint->sender = sender;
  endpoint->path = path;
  endpoint->codec = codec;

  // Store endpoint uuid in lower_case
  std::string lower_case_uuid = uuid;
  for (auto &c : lower_case_uuid) {
    c = tolower(c);
  }
  endpoint->uuid = lower_case_uuid;

  // Initialise transport as nullptr
  endpoint->transport = nullptr;

  int res = sd_bus_track_new(g_sdbus, &(endpoint->sd_watch),
                             MediaManager::sd_mediaEndPointExit, endpoint);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to create tracker: %d - %s\n", -res,
                  strerror(-res));
    return false;
  }
  res = sd_bus_track_add_name(endpoint->sd_watch, sender);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to track name: %d - %s\n", -res,
                  strerror(-res));
    return false;
  }

  // Add end point to the media manager object database
  pMediaManagerObj->m_EndPointsList.insert(endpoint);

  // Extract codec config from capabilities and add to relevant service
  // NOTE: For Vendor codecs (i.e. 0xFF) the actual embedded codec type
  // within codec_config will contain the mapping to the local Fluoride
  // codec id, i.e: APTX_CODEC_ID_ADAPTIVE => A2DP_SINK_AUDIO_CODEC_APTX_AD
  if (!getCodecConfigFromCapabilities(codec, capabilities, size,
                                      endpoint->codec_config)) {
    ADK_LOG_ERROR(LOGTAG "%s: Could not parse codec capabilities\n", __func__);
    return false;
  }

  // Notify there is a new codec available which may need to go into the preferred
  // list of codecs we send to Fluoride.
  if (pMediaManagerObj->m_ad2pSinkService &&
      endpoint->uuid.compare(A2dpSinkService::LOCAL_UUID) == 0) {
    pMediaManagerObj->m_ad2pSinkService->addSupportedCodec(endpoint->codec_config);
  } else if (pMediaManagerObj->m_ad2pSourceService &&
             endpoint->uuid.compare(A2dpSourceService::LOCAL_UUID) == 0) {
    pMediaManagerObj->m_ad2pSourceService->addSupportedCodec(endpoint->codec_config);
  }

  ADK_LOG_NOTICE(LOGTAG " Endpoint registered: sender=%s path=%s\n", sender,
                 path);

  // Notify other clients that endpoint is now ready to use
  sd_bus_emit_properties_changed(g_sdbus, ADAPTER_PATH, MEDIA_INTERFACE,
                                 "RegisteredEndpoints", nullptr);

  return true;
}

MediaEndPoint *MediaManager::mediaFindEndPoint(const char *sender,
                                               const char *path) {
  MediaManager *pMediaManagerObj = g_MediaManager;

  for (const auto &endpoint : pMediaManagerObj->m_EndPointsList) {
    if (sender && (endpoint->sender != sender)) {
      continue;
    }

    if (path && (endpoint->path != path)) {
      continue;
    }

    return endpoint;
  }

  return nullptr;
}

int MediaManager::sd_parseEndPointProperties(
    sd_bus_message *m, sd_bus_error *ret_error, const char **uuid,
    bool *delay_reporting, uint8_t *codec, const uint8_t **capabilities,
    size_t *size) {
  bool has_uuid = false, has_codec = false;

  char type;
  const char *contents;
  int res = sd_bus_message_peek_type(m, &type, &contents);
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  if (type != SD_BUS_TYPE_ARRAY) {
    return bt_error_invalid_args(ret_error);
  }

  res = sd_bus_message_enter_container(m, type, contents);
  if (res < 0) {
    return bt_error_failed(ret_error);
  }

  for (;;) {
    res = sd_bus_message_peek_type(m, &type, &contents);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    if (res == 0) {
      break;
    }

    if (type != SD_BUS_TYPE_DICT_ENTRY) {
      return bt_error_invalid_args(ret_error);
    }

    res = sd_bus_message_enter_container(m, type, contents);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }

    const char *key;
    res = sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &key);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }

    if (strcasecmp(key, "uuid") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_STRING>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, uuid);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }
      has_uuid = true;

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "codec") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_BYTE>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
        return -1;
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_BYTE, codec);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }
      has_codec = true;

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "delayreporting") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_BOOLEAN, delay_reporting);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "capabilities") == 0) {
      res = sd_bus_message_enter_container(
          m, SD_BUS_TYPE_VARIANT,
          TYPE_TO_STR<SD_BUS_TYPE_ARRAY, SD_BUS_TYPE_BYTE>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_array(
          m, SD_BUS_TYPE_BYTE, reinterpret_cast<const void **>(capabilities),
          size);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else {
      res = sd_bus_message_skip(m, TYPE_TO_STR<SD_BUS_TYPE_VARIANT>::value);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    }

    res = sd_bus_message_exit_container(m);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
  };

  if ((!has_uuid) || (!has_codec)) {
    return bt_error_invalid_args(ret_error);
  }
  return 0;
}

// Sends set configuration via DBUS
bool MediaManager::sendSetConfiguration(MediaEndPoint *endpoint,
                                        MediaEndPointCallBack cb,
                                        void *user_data) {
  sd_bus_message *m = nullptr;
  MediaEndPointRequest *request = nullptr;
  int res = 0;
  const char *selfName;

  if (!endpoint) {
    ADK_LOG_NOTICE(LOGTAG " %s endpoint is not present\n", __func__);
    goto done;
  }

  ADK_LOG_NOTICE(LOGTAG " %s set_configuration sender %s path %s \n", __func__,
                 endpoint->sender.c_str(), endpoint->path.c_str());

  request = new MediaEndPointRequest();
  request->endpoint = endpoint;
  request->slot = nullptr;
  request->cb = cb;
  request->user_data = user_data;
  endpoint->requests.insert(request);

  res = sd_bus_get_unique_name(g_sdbus, &selfName);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " %s Failed to get own name: %d - %s\n", __func__,
                  -res, strerror(-res));
    goto done;
  }

  res = sd_bus_message_new_method_call(
      g_sdbus, &m, selfName, endpoint->transport->getTransportPath().c_str(),
      "org.freedesktop.DBus.Properties", "GetAll");
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " %s Failed to allocate message: %d - %s\n", __func__,
                  -res, strerror(-res));
    goto done;
  }
  res = sd_bus_message_append_basic(m, SD_BUS_TYPE_STRING,
                                    "org.bluez.MediaTransport1");
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " %s Failed to set parameter: %d - %s\n", __func__,
                  -res, strerror(-res));
    goto done;
  }

  res = sd_bus_call_async(g_sdbus, &(request->slot), m,
                          MediaManager::sendSetConfiguration_1, request,
                          REQUEST_TIMEOUT * 1000);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " %s Failed to call D-Bus function: %d - %s\n",
                  __func__ - res, strerror(-res));
    goto done;
  }

  request = nullptr;  // so it won't be freed

done:
  if (request) {
    endpoint->requests.erase(request);
    endpointRequestFree(request);
  }
  sd_bus_message_unref(m);

  return (res >= 0);
}

int MediaManager::sendSetConfiguration_1(sd_bus_message *reply, void *userdata,
                                         sd_bus_error *ret_error) {
  sd_bus_message *m = nullptr;
  MediaEndPointRequest *request = static_cast<MediaEndPointRequest *>(userdata);
  MediaEndPoint *endpoint = request->endpoint;
  int res = 0;
  const sd_bus_error *reply_error = nullptr;

  if (sd_bus_error_is_set(ret_error)) {
    ADK_LOG_NOTICE(LOGTAG " %s Failed to get conf properties: %s\n", __func__,
                   ret_error->name);
    res = sd_bus_error_get_errno(ret_error);

    /* Clear endpoint configuration in case of NO_REPLY error */
    if (strcmp(ret_error->name, SD_BUS_ERROR_NO_REPLY) == 0) {
      if (request->cb) {
        request->cb(endpoint, res, request->user_data);
      }
      clearEndpoint(endpoint);
      return 0;
    }

    goto done;
  }
  reply_error = sd_bus_message_get_error(reply);
  if (sd_bus_error_is_set(reply_error)) {
    ADK_LOG_ERROR(LOGTAG " %s Endpoint replied with an error: %s (%s)\n",
                  __func__, reply_error->name, reply_error->message);
    res = sd_bus_error_get_errno(reply_error);
    goto done;
  }

  res = sd_bus_message_new_method_call(
      g_sdbus, &m, endpoint->sender.c_str(), endpoint->path.c_str(),
      MEDIA_ENDPOINT_INTERFACE, "SetConfiguration");
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " %s Couldn't allocate message: %d - %s\n", __func__,
                  -res, strerror(-res));
    goto done;
  }

  res = sd_bus_message_append_basic(
      m, SD_BUS_TYPE_OBJECT_PATH,
      endpoint->transport->getTransportPath().c_str());
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " %s Failed to set parameter: %d - %s\n", __func__,
                  -res, strerror(-res));
    goto done;
  }

  res = sd_bus_message_copy(m, reply, true);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " %s Failed to copy configuration: %d - %s\n",
                  __func__, -res, strerror(-res));
    goto done;
  }

  res = sd_bus_call_async(g_sdbus, &(request->slot), m,
                          MediaManager::sendSetConfiguration_2, request,
                          REQUEST_TIMEOUT * 1000);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " %s Failed to set configuration: %d - %s\n", __func__,
                  -res, strerror(-res));
    goto done;
  }

  request = nullptr;  // so it won't be freed

done:
  if (request) {
    if (request->cb) {
      request->cb(endpoint, sd_bus_error_get_errno(ret_error),
                  request->user_data);
    }
    endpoint->requests.erase(request);
    endpointRequestFree(request);
  }
  sd_bus_message_unref(m);

  return res;
}

int MediaManager::sendSetConfiguration_2(sd_bus_message *UNUSED(m), void *userdata,
                                         sd_bus_error *ret_error) {
  MediaEndPointRequest *request = static_cast<MediaEndPointRequest *>(userdata);
  MediaEndPoint *endpoint = request->endpoint;

  if (sd_bus_error_is_set(ret_error)) {
    ADK_LOG_NOTICE(LOGTAG " %s Endpoint replied with an error: %s", __func__,
                   ret_error->name);

    /* Clear endpoint configuration in case of NO_REPLY error */
    if (strcmp(ret_error->name, SD_BUS_ERROR_NO_REPLY) == 0) {
      if (request->cb) {
        request->cb(endpoint, sd_bus_error_get_errno(ret_error),
                    request->user_data);
      }
      clearEndpoint(endpoint);
      return 0;
    }

    goto done;
  }

  ADK_LOG_NOTICE(LOGTAG " %s set_configuration done\n", __func__,
                 endpoint->sender.c_str(), endpoint->path.c_str());

done:
  if (request->cb) {
    request->cb(endpoint, sd_bus_error_get_errno(ret_error),
                request->user_data);
  }
  endpoint->requests.erase(request);
  endpointRequestFree(request);

  return 0;
}

// Returns true on success, false on failure
bool MediaManager::registerMediaInterface(void) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  if (!g_sdbus) {
    ADK_LOG_ERROR(LOGTAG " Dbus connection to system bus is not available\n");
    return false;
  }

  // Create a DBus interface for the media functionalities
  static const sd_bus_vtable sSdMediaMethods[] = {
      SD_BUS_VTABLE_START(0),
      SD_BUS_METHOD("RegisterEndpoint", "oa{sv}", nullptr,
                    MediaManager::sd_registerEndpoint,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("UnregisterEndpoint", "o", nullptr,
                    MediaManager::sd_unregisterEndpoint,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("RegisterPlayer", "oa{sv}", nullptr,
                    MediaManager::sd_registerPlayer,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("UnregisterPlayer", "o", nullptr,
                    MediaManager::sd_unregisterPlayer,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("RegisterMediaApp", "oa{sv}", nullptr,
                    MediaManager::sd_registerMediaApp,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("UnregisterMediaApp", "o", nullptr,
                    MediaManager::sd_unregisterMediaApp,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_PROPERTY("RegisteredEndpoints", "as",
                      MediaManager::sd_propertyGetEndpoints, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("SinkPrefCodecList", "s", 0,
                      MediaManager::sd_setSinkPreferences, 0,
                      SD_BUS_VTABLE_PROPERTY_EXPLICIT),

      SD_BUS_VTABLE_END};

  int res = sd_bus_add_object_vtable(g_sdbus, &m_sdbusSlot, ADAPTER_PATH,
                                     MEDIA_INTERFACE, sSdMediaMethods, this);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "interface init failed on path %s: %d - %s\n",
                   ADAPTER_PATH, -res, strerror(-res));
    return false;
  }
  return true;
}

// Returns true on sucess and false on failure
void MediaManager::unRegisterMediaInterface() {
  ADK_LOG_NOTICE(LOGTAG " --%s\n", __func__);
  sd_bus_slot_unref(m_sdbusSlot);
  m_sdbusSlot = nullptr;
}

MediaAppType MediaManager::getMediaAppType(PropertiesList properties_list)
{
  std::string uuid;
  auto uuid_it = properties_list.find("UUID");
  if (uuid_it == properties_list.end()){
    ADK_LOG_ERROR(LOGTAG "Could not find UUID... Failed to create media app");
    return MediaAppType::UNKNOWN_APP;
  }
  uuid = uuid_it->second.value.type_string;
  return getMediaAppTypeFromUuid(uuid);
}

MediaAppType MediaManager::getMediaAppTypeFromUuid(const std::string &uuid)
{
  std::string lower_case_uuid = uuid;
  for (auto &c : lower_case_uuid) {
    c = tolower(c);
  }
  MediaAppType app_type = MediaAppType::UNKNOWN_APP;
  if (lower_case_uuid == HidService::LOCAL_UUID){
    app_type = MediaAppType::HID_APP;
  }
  return app_type;
}


/*************************************************************************************************/
// This CodecParser is internal functionality to the MediaManager.
// The MediaManager::CodecParser has been added as a tool to facilitate testing of Codecs
// during development and although it can be evolved into a production feature it is currently
// limited in functionality and lacking robustness checks. Therefore, it is advisable to use
// this tool following the guidance and examples given below.
//
// MediaManager has been instrumented with a "SinkPrefCodecList" property to allow user
// input that can be fed into the parser in order to allow setting of codec preference in Fluoride.
// Currently we only support configuration of codecs for a2dpsink role. Future versions may add support
// for codec configuration for a2dpSource role.
//
// valid codecs: "aac", "sbc", "aptx"
// valid sbc freq: "16", "32", "44.1", "48"
// valid aac freq: "8", "11.025", "12", "16", "22.05", "24", "32", "44.1", "48", "64", "88.2", "96"
// valid aptx freq: "44.1", "48"
// valid aac obj types: "MPEG-2-LC", "MPEG-4-LC", "MPEG-4-LTP", "MPEG-4-SC"
//
// Examples of usage for non-split mode:
// busctl --no-pager set-property org.bluez /org/bluez/hci0 org.bluez.Media1 SinkPrefCodecList s "sbc,48"
// busctl --no-pager set-property org.bluez /org/bluez/hci0 org.bluez.Media1 SinkPrefCodecList s "sbc,44.1"
//
// Examples of usage for split mode:
// busctl --no-pager set-property org.bluez /org/bluez/hci0 org.bluez.Media1 SinkPrefCodecList s "aptx,48"
// busctl --no-pager set-property org.bluez /org/bluez/hci0 org.bluez.Media1 SinkPrefCodecList s "aptx,48,aptx,44.1"
// busctl --no-pager set-property org.bluez /org/bluez/hci0 org.bluez.Media1 SinkPrefCodecList s "aac,48,MPEG-2-LC"
// busctl --no-pager set-property org.bluez /org/bluez/hci0 org.bluez.Media1 SinkPrefCodecList s "aac,48,MPEG-2-LC,aac,48,MPEG-4-LC"
// busctl --no-pager set-property org.bluez /org/bluez/hci0 org.bluez.Media1 SinkPrefCodecList s "aptx,48,aptx,44.1,aac,48,MPEG-2-LC,sbc,48"
//
// Current Limitations:
// - case sensitivy and no spaces allowed on the string property.
// - no built in checks on actual supported codecs for split or non-split mode.
/*************************************************************************************************/
struct MediaManager::CodecParser {
  static const uint8_t SBC_PARAM_LEN { 1 };
  static const uint8_t APTX_PARAM_LEN { 1 };
  static const uint8_t AAC_PARAM_LEN { 2 };

  bool A2dpCodecList(char *codec_param_list, btav_codec_configuration_t *codec, int *num_codec_configs);
  const std::vector<std::string> ParseUserInput(const std::string &input);
  template<std::size_t S>
  int find_str_in_list(const std::string &str, const std::array<std::string, S> &list);

  std::array<std::string, 3> valid_codecs = { "aac", "sbc", "aptx"};

  std::array<uint8_t, 3> valid_codec_values = {
    A2DP_SINK_AUDIO_CODEC_AAC,
    A2DP_SINK_AUDIO_CODEC_SBC,
    A2DP_SINK_AUDIO_CODEC_APTX
  };

  std::array<std::string, 4> valid_sbc_freq = { "16", "32", "44.1", "48" };

  std::array<uint8_t, 4> valid_sbc_freq_values = {
    SBC_SAMP_FREQ_16,
    SBC_SAMP_FREQ_32,
    SBC_SAMP_FREQ_44,
    SBC_SAMP_FREQ_48
  };

  std::array<std::string, 12> valid_aac_freq = { "8", "11.025", "12", "16", "22.05", "24",
                                     "32", "44.1", "48", "64", "88.2", "96"};

  std::array<uint16_t, 12> valid_aac_freq_values = {
    AAC_SAMP_FREQ_8000,
    AAC_SAMP_FREQ_11025,
    AAC_SAMP_FREQ_12000,
    AAC_SAMP_FREQ_16000,
    AAC_SAMP_FREQ_22050,
    AAC_SAMP_FREQ_24000,
    AAC_SAMP_FREQ_32000,
    AAC_SAMP_FREQ_44100,
    AAC_SAMP_FREQ_48000,
    AAC_SAMP_FREQ_64000,
    AAC_SAMP_FREQ_88200,
    AAC_SAMP_FREQ_96000,
  };

  std::array<std::string, 4> valid_aac_obj_type = { "MPEG-2-LC", "MPEG-4-LC", "MPEG-4-LTP", "MPEG-4-SC"};

  std::array<uint8_t, 4> valid_aac_obj_type_values = {
    AAC_OBJ_TYPE_MPEG_2_AAC_LC,
    AAC_OBJ_TYPE_MPEG_4_AAC_LC,
    AAC_OBJ_TYPE_MPEG_4_AAC_LTP,
    AAC_OBJ_TYPE_MPEG_4_AAC_SCA,
  };

  std::array<std::string, 2> valid_aptx_freq = { "44.1", "48"};

  std::array<uint8_t, 2> valid_aptx_freq_values = {
    APTX_SAMPLERATE_44100,
    APTX_SAMPLERATE_48000,
  };
};

template<std::size_t S>
int MediaManager::CodecParser::find_str_in_list(const std::string &str, const std::array<std::string, S> &list)
{
  int index = list.size();
  if (!str.empty() && list.size() != 0) {
    auto pos = std::find(std::begin(list), std::end(list), str);
    if ( pos != std::end(list)) {
      index = std::distance(std::begin(list), pos);
    }
  }
  return index;
}

const std::vector<std::string> MediaManager::CodecParser::ParseUserInput(const std::string &input) {
  std::vector<std::string> pref_list;
  if (!input.empty()) {
    std::istringstream ss(input);
    std::string param;
    while(std::getline(ss, param, ',')) {
      pref_list.emplace_back(param);
    }
  }
  return pref_list;
}

// Parses string which represents codec list.
bool MediaManager::CodecParser::A2dpCodecList(char *codec_param_list,
                                              btav_codec_configuration_t *a2dpSnkCodecList,
                                              int *num_codec_configs)
{
  ADK_LOG_NOTICE(LOGTAG " %s\n", __func__);
  if ((codec_param_list != nullptr) && (num_codec_configs != nullptr) &&
      (*codec_param_list == '\0')) {
    return false;
  }
  const std::vector<std::string> output_list = ParseUserInput(codec_param_list);
  int codec_params_list_size = output_list.size();
  if (codec_params_list_size == 0) {
    return false;
  }

  int i = 0, j = 0, k = 0;
  while (j < codec_params_list_size) {
    i = find_str_in_list(output_list[j], valid_codecs);
    if (i == valid_codecs.size()) {
      return false;
    }
    j++;
    a2dpSnkCodecList[k].codec_type = valid_codec_values[i];
    switch (a2dpSnkCodecList[k].codec_type) {
      case A2DP_SINK_AUDIO_CODEC_AAC:
        ADK_LOG_DEBUG(LOGTAG " --> A2DP_SINK_AUDIO_CODEC_AAC \n");
        if (j + AAC_PARAM_LEN > codec_params_list_size + 1) {
          ADK_LOG_ERROR(LOGTAG " --> Invalid AAC Parameters passed \n");
          return false;
        }
        i = find_str_in_list(output_list[j], valid_aac_freq);
        if (i == valid_aac_freq.size()) {
          ADK_LOG_ERROR(LOGTAG " --> Invalid AAC Sampling Freq\n");
          return false;
        }
        a2dpSnkCodecList[k].codec_config.aac_config.sampling_freq =
        valid_aac_freq_values[i];
        j ++;
        i = find_str_in_list(output_list[j], valid_aac_obj_type);
        if (i == valid_aac_obj_type.size()) {
          ADK_LOG_ERROR(LOGTAG " --> Invalid AAC object type\n");
          return false;
        }
        a2dpSnkCodecList[k].codec_config.aac_config.obj_type =
                    valid_aac_obj_type_values[i];
        j ++;
        break;
      case A2DP_SINK_AUDIO_CODEC_SBC:
        ADK_LOG_DEBUG(LOGTAG "  --> A2DP_SINK_AUDIO_CODEC_SBC \n");
        if (j + SBC_PARAM_LEN > codec_params_list_size + 1) {
          ADK_LOG_ERROR(LOGTAG "  --> Invalid SBC Parameters passed \n");
          return false;
        }
        i = find_str_in_list(output_list[j], valid_sbc_freq);
        if (i == valid_sbc_freq.size()) {
          ADK_LOG_ERROR(LOGTAG "  -->Invalid SBC Sampling Freq\n");
          return false;
        }
        a2dpSnkCodecList[k].codec_config.sbc_config.samp_freq =
                    valid_sbc_freq_values[i-1];
        j ++;
        break;
      case A2DP_SINK_AUDIO_CODEC_APTX:
        ADK_LOG_DEBUG(LOGTAG "  --> A2DP_SINK_AUDIO_CODEC_APTX \n");
        if (j + APTX_PARAM_LEN > codec_params_list_size + 1) {
          ADK_LOG_ERROR(LOGTAG "  --> Invalid APTX Parameters passed \n");
          return false;
        }
        i = find_str_in_list(output_list[j], valid_aptx_freq);
        if (i == valid_aptx_freq.size()) {
          ADK_LOG_ERROR(LOGTAG "  --> Invalid APTX Sampling Freq\n");
          return false;
        }
        a2dpSnkCodecList[k].codec_config.aptx_config.sampling_freq =
                    valid_aptx_freq_values[i];
        j ++;
        break;
      }
      k++;
      if (k >= MAX_NUM_CODEC_CONFIGS) {
        ADK_LOG_ERROR(LOGTAG "  --> Total list of codec configs (%d) exceeds max number(%d)\n",
                k, MAX_NUM_CODEC_CONFIGS);
        return false;
      }
    }
    *num_codec_configs = k;
    ADK_LOG_ERROR(LOGTAG "  --> num_codec_configs  = %d \n", *num_codec_configs);
    return true;
}

/***************************************************************************************/

MediaManager::MediaManager()
    : m_ad2pSinkService(nullptr),
      m_ad2pSourceService(nullptr),
      m_avrcpTgService(nullptr),
      m_sdbusSlot(nullptr),
      m_parser(new CodecParser()) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (!registerMediaInterface()) {
    ADK_LOG_ERROR(LOGTAG " Could not register Media Interface\n");
  }
  // Initialise global reference to media manager for dbus callbacks
  g_MediaManager = this;
}

MediaManager::~MediaManager() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  unRegisterMediaInterface();
  if (m_hidService!=nullptr){
    m_hidService->unregisterMediaManager();
  }
  m_EndPointsList.clear();  // TODO: leak?
  g_MediaManager = nullptr;
}

// Member Functions
int MediaManager::sd_mediaEndPointExit(sd_bus_track *UNUSED(track), void *userdata) {
  MediaEndPoint *endpoint = static_cast<MediaEndPoint *>(userdata);
  MediaManager *pMediaManager = g_MediaManager;

  ADK_LOG_NOTICE(LOGTAG " --%s\n", __func__);
  sd_bus_track_unref(endpoint->sd_watch);
  endpoint->sd_watch = nullptr;

  ADK_LOG_NOTICE(LOGTAG " Endpoint unregistered: sender=%s path=%s",
                 endpoint->sender.c_str(), endpoint->path.c_str());

  // Notify this codec is not available if it was included as part of the preferred
  // list of codecs we send to Fluoride.
  if (g_MediaManager->m_ad2pSinkService &&
      endpoint->uuid.compare(A2dpSinkService::LOCAL_UUID) == 0) {
    g_MediaManager->m_ad2pSinkService->removeSupportedCodec(endpoint->codec_config);
  } else if (g_MediaManager->m_ad2pSourceService &&
             endpoint->uuid.compare(A2dpSourceService::LOCAL_UUID) == 0) {
    g_MediaManager->m_ad2pSourceService->removeSupportedCodec(endpoint->codec_config);
  }

  // Remove this endpoint from the object database
  pMediaManager->m_EndPointsList.erase(endpoint);

  // Notify clients that endpoint is no longer available
  sd_bus_emit_properties_changed(g_sdbus, ADAPTER_PATH, MEDIA_INTERFACE,
                                 "RegisteredEndpoints", nullptr);

  // Destroy the end point info from the DBUS database
  mediaEndPointDestroy(endpoint);

  return 0;
}

void MediaManager::mediaInterfacePathFree() {
  MediaManager *pMediaManager = g_MediaManager;
  // release all the endpoints created
  while (!pMediaManager->m_EndPointsList.empty()) {
    releaseMediaEndPoint(*(pMediaManager->m_EndPointsList).begin());
  }
  // TBD the interface is unregistered is this possible?
}

// Callback set configuration is executed by dbus
void MediaManager::setConfigurationCallBack(MediaEndPoint *endpoint, int ret,
                                            void *UNUSED(user_data)) {
  ADK_LOG_NOTICE(LOGTAG " --%s (Status: %d)\n", __func__, ret);
  // Do not assume that transport is always present
  if (endpoint->transport) {
    endpoint->transport->setConfigurationCb();
  }
  else {
    ADK_LOG_ERROR(LOGTAG " --> Transport object is not present!!\n");
  }
}

// TRUE if processing of set configuration is successful, otherwise false
bool MediaManager::setConfiguration(std::shared_ptr<MediaTransport> transport) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  MediaEndPoint *endpoint = nullptr;

  // Get the endpoint corresponding to this type
  for (const auto &iter : m_EndPointsList) {
    ADK_LOG_DEBUG(LOGTAG
                  " -- endpoint uuid %s, service uuid %s \n",
                  iter->uuid.c_str(), (transport->getUUID()).c_str());
    if (iter->uuid.compare(transport->getUUID()) == 0) {
      if (iter->codec == transport->getCodec()) {
        ADK_LOG_DEBUG(LOGTAG " --> Found endpoint for codecType %d (%s)\n",
                    transport->getCodec(), getCodecName(transport->getCodec()));
        endpoint = iter;
        break;
      }
      else if (iter->codec == NON_A2DP_CODEC) {
        // Currently the only supported NON A2DP CODECs are:
        // A2DP_SINK_AUDIO_CODEC_APTX and A2DP_SINK_AUDIO_CODEC_APTX_AD
        // Verify the vendor codec id matches the endpoint
        if (iter->codec_config.codec_type == A2DP_SINK_AUDIO_CODEC_APTX) {
          if (transport->getVendorCodec() ==
              iter->codec_config.codec_config.aptx_config.codec_id) {
            ADK_LOG_DEBUG(LOGTAG " --> Found endpoint for A2DP_SINK_AUDIO_CODEC_APTX)\n");
            endpoint = iter;
            break;
          }
        }
        else if (iter->codec_config.codec_type == A2DP_SINK_AUDIO_CODEC_APTX_AD) {
          if (transport->getVendorCodec() ==
              iter->codec_config.codec_config.aptx_ad_config.codec_id) {
            ADK_LOG_DEBUG(LOGTAG " --> Found endpoint for A2DP_SINK_AUDIO_CODEC_APTX_AD)\n");
            endpoint = iter;
            break;
          }
        }
      }
    }
  }

  if (!endpoint) {
    ADK_LOG_NOTICE(LOGTAG " -->%s No matching endpoint is found\n", __func__);
    return false;
  }

  // Associate transport object to the media endpoint
  if (!endpoint->transport) {
    ADK_LOG_NOTICE(LOGTAG " - %s link the transport to this endpoint \n", __func__);
    endpoint->transport = transport;
    // Call setconfig on Registered Endpoints
    if (!sendSetConfiguration(endpoint, MediaManager::setConfigurationCallBack,
                              nullptr)) {
      ADK_LOG_NOTICE(LOGTAG " <--%s set Configuration fails \n", __func__);
      endpoint->transport = nullptr;
      return false;
    }
  }
  else {
    ADK_LOG_ERROR(LOGTAG
      " - %s Trying to override transport on this endpoint!\n", __func__);
  }
  return true;
}

// TRUE if processing of set configuration is successful, otherwise false
bool MediaManager::findAndClearConfiguration(uint8_t codecType,
                                             const std::string &uuid) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  MediaEndPoint *endpoint = nullptr;

  // Get the endpoint corresponding to this type
  for (const auto &iter : m_EndPointsList) {
    if ((iter->uuid.compare(uuid) == 0) &&
       ((iter->codec == codecType) ||
       ((iter->codec == NON_A2DP_CODEC) && (iter->codec_config.codec_type == codecType)))) {

      ADK_LOG_NOTICE(LOGTAG " %s Found endpoint for codecType %d (%s)\n",
                     __func__, codecType, getCodecName(codecType));
      endpoint = iter;
      break;
    }
  }

  if (endpoint && endpoint->transport) {
    clearConfiguration(endpoint);
    endpoint->transport = nullptr;
  }
  return true;
}

void MediaManager::updateAdapterServices(A2dpSinkService *ad2pSinkService,
                                         A2dpSourceService *ad2pSourceService,
                                         AvrcpTGService *avrcpTgService,
                                         HidService *hidService) {
  m_ad2pSinkService = ad2pSinkService;
  m_ad2pSourceService = ad2pSourceService;
  m_avrcpTgService = avrcpTgService;
  m_hidService = hidService;
  if (m_hidService!=nullptr){
    m_hidService->registerMediaManager(this);
  }
}

void MediaManager::updateMediaInterfaceLists() {
  ADK_LOG_NOTICE(LOGTAG " --%s \n", __func__);

  A2dpProfileService* service = nullptr;

  for (const auto &endpoint : m_EndPointsList) {
    if (endpoint->uuid.compare(A2dpSinkService::LOCAL_UUID) == 0) {
      ADK_LOG_DEBUG(LOGTAG " ---- A2dpsink endpoint codec %u: %s\n",
                    endpoint->codec, getCodecName(endpoint->codec));
      service = m_ad2pSinkService;
     } else if (endpoint->uuid.compare(A2dpSourceService::LOCAL_UUID) == 0) {
      ADK_LOG_DEBUG(LOGTAG " ---- A2dpSource endpoint codec %u: %s\n",
                    endpoint->codec, getCodecName(endpoint->codec));
      service = m_ad2pSourceService;
     }

    if (service != nullptr) { 
      service->addSupportedCodec(endpoint->codec_config);
    }
  }
}

// DBUS interface method calls
int MediaManager::sd_registerEndpoint(sd_bus_message *m, void *UNUSED(userdata),
                                      sd_bus_error *ret_error) {
  const char *sender, *path, *uuid;
  bool delay_reporting = false;
  uint8_t codec;
  const uint8_t *capabilities;
  size_t size = 0;
  ADK_LOG_NOTICE(LOGTAG " -->%s\n", __func__);

  sender = sd_bus_message_get_sender(m);

  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_OBJECT_PATH, &path);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Invalid args\n");
    return bt_error_invalid_args(ret_error);
  }

  if (mediaFindEndPoint(sender, path)) {
    ADK_LOG_NOTICE(LOGTAG "Endpoint for %s (%s) already exists\n", path,
                   sender);
    return bt_error_already_exists(ret_error);
  }

  if (sd_parseEndPointProperties(m, ret_error, &uuid, &delay_reporting, &codec,
                                 &capabilities, &size) < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to parse\n");
    return -1;
  }

  ADK_LOG_NOTICE(
      LOGTAG
      " registerEndpoint : Sender %s, Path: %s, UUID: %s , codec: %x (%s)\n",
      sender, path, uuid, codec, getCodecName(codec));

  if (!mediaEndPointCreate(sender, path, uuid, delay_reporting, codec,
                           capabilities, size)) {
    return bt_error_not_available(ret_error);
  }

  ADK_LOG_NOTICE(LOGTAG " <--%s\n", __func__);
  // To reply nothing :-)
  return sd_bus_reply_method_return(m, nullptr);
}

int MediaManager::sd_unregisterEndpoint(sd_bus_message *m, void *UNUSED(userdata),
                                        sd_bus_error *ret_error) {
  MediaEndPoint *endpoint = nullptr;
  const char *sender, *path;
  ADK_LOG_NOTICE(LOGTAG " -->%s\n", __func__);

  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_OBJECT_PATH, &path);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }

  sender = sd_bus_message_get_sender(m);
  endpoint = mediaFindEndPoint(sender, path);
  if (!endpoint) {
    return bt_error_does_not_exist(ret_error);
  }

  releaseMediaEndPoint(endpoint);
  ADK_LOG_NOTICE(LOGTAG " <--%s\n", __func__);
  return sd_bus_reply_method_return(m, nullptr);
}

int MediaManager::sd_registerPlayer(sd_bus_message *m, void *UNUSED(userdata),
                                    sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  const char *sender, *path;

  sender = sd_bus_message_get_sender(m);

  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_OBJECT_PATH, &path);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Invalid args\n");
    return bt_error_invalid_args(ret_error);
  }

  if (mediaFindPlayer(sender, path)) {
    ADK_LOG_ERROR(LOGTAG "Found! Player for %s (%s) already exists!\n", path,
                  sender);
    return bt_error_already_exists(ret_error);
  }

  MediaMPRISPlayer *mp = mediaPlayerCreate(sender, path);
  if (mp == nullptr) {
    return bt_error_not_available(ret_error);
  }

  if (parsePlayerProperties(mp, m, ret_error) < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to parse\n");
    return -1;
  }

  return sd_bus_reply_method_return(m, nullptr);
}

int MediaManager::sd_unregisterPlayer(sd_bus_message *m, void *UNUSED(userdata),
                                      sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  MediaMPRISPlayer *player = nullptr;
  const char *sender, *path;

  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_OBJECT_PATH, &path);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }

  sender = sd_bus_message_get_sender(m);
  player = mediaFindPlayer(sender, path);
  if (!player) {
    return bt_error_does_not_exist(ret_error);
  }

  // Remove this player from both MediaManager and DBUS databases
  removeMediaPlayer(player);

  return sd_bus_reply_method_return(m, nullptr);
}

int MediaManager::sd_registerMediaApp(sd_bus_message *m, void *UNUSED(userdata),
                                      sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  const char *sender, *path;

  sender = sd_bus_message_get_sender(m);

  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_OBJECT_PATH, &path);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Invalid args\n");
    return bt_error_invalid_args(ret_error);
  }

  if (mediaFindApp(sender, path)) {
    ADK_LOG_ERROR(LOGTAG "Found! Player for %s (%s) already exists!\n", path,
                  sender);
    return bt_error_already_exists(ret_error);
  }

  MediaApp app;
  PropertiesList properties_list;
  if (parseMediaAppProperties(properties_list, m, ret_error) < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to parse\n");
    return -1; // error already set by parseMediaAppProperties
  }

  if (!mediaAppCreate(sender, path, properties_list)) {
    ADK_LOG_ERROR(LOGTAG "mediaAppCreate Failed");
    return bt_error_not_available(ret_error);
  }

  // After app has been created if there is a registered hid service.
  // Ask it to update with any existing devices
  if (g_MediaManager != nullptr){
    if (g_MediaManager->m_hidService != nullptr) {
      g_MediaManager->m_hidService->ConfirmDevices();
    }
  }

  return sd_bus_reply_method_return(m, nullptr);
}

int MediaManager::sd_unregisterMediaApp(sd_bus_message *m, void *UNUSED(userdata),
                                        sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  MediaApp *app = nullptr;
  const char *sender, *path;

  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_OBJECT_PATH, &path);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }

  sender = sd_bus_message_get_sender(m);
  app = mediaFindApp(sender, path);
  if (!app) {
    return bt_error_does_not_exist(ret_error);
  }

  removeMediaApp(app);

  ADK_LOG_NOTICE(LOGTAG " <--%s\n", __func__);

  return sd_bus_reply_method_return(m, nullptr);
}

int MediaManager::sd_propertyGetEndpoints(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                          const char *UNUSED(interface),
                                          const char *UNUSED(property),
                                          sd_bus_message *reply, void *UNUSED(userdata),
                                          sd_bus_error *UNUSED(ret_error)) {
    ADK_LOG_DEBUG(LOGTAG "%s\n", __func__);

    int res = sd_bus_message_open_container(reply, SD_BUS_TYPE_ARRAY,
                                            TYPE_TO_STR<SD_BUS_TYPE_STRING>::value);
    if (res < 0) {
        return res;
    }

    for (const auto &endpoint : g_MediaManager->m_EndPointsList) {
        res = sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING, endpoint->path.c_str());
        if (res < 0) {
            return res;
        }
    }

    return sd_bus_message_close_container(reply);
}

int MediaManager::sd_setSinkPreferences(sd_bus *bus,
                                        const char *path,
                                        const char *interface,
                                        const char *property,
                                        sd_bus_message *value,
                                        void *userdata,
                                        sd_bus_error *ret_error) {
  MediaManager *pMediaManagerObj = g_MediaManager;
  char *prefCodecList;

  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_STRING, &prefCodecList);
  if (res < 0) {
    return res;
  }
  int num_codec_configs = 0;
  btav_codec_configuration_t a2dpSnkCodecList[MAX_NUM_CODEC_CONFIGS];

  if (!pMediaManagerObj->m_ad2pSinkService) {
    return bt_error_failed(ret_error, "A2dp service not ready");
  } else if (!(g_MediaManager->m_parser->A2dpCodecList(prefCodecList, a2dpSnkCodecList, &num_codec_configs)) ||
      num_codec_configs == 0) {
    return bt_error_failed(ret_error, "Malformed SinkPrefCodecList value, ex: \"aptx,48,aptx,44.1,aac,44.1,MPEG-2-LC,aac,48,MPEG-2-LC,sbc,48\"");
  } else if (!pMediaManagerObj->m_ad2pSinkService->forcePreferredCodecList(a2dpSnkCodecList, num_codec_configs)) {
    return bt_error_failed(ret_error, "Setting codec preferences has failed");
  }
  return 0;
}

MediaMPRISPlayer *MediaManager::mediaFindPlayer(const char *sender,
                                                const char *path) {
  MediaManager *pMediaManagerObj = g_MediaManager;

  for (const auto &player : pMediaManagerObj->m_PlayerList) {
    if (sender && (player->sender != sender)) {
      continue;
    }

    if (path && (player->path != path)) {
      continue;
    }

    return player;
  }

  return nullptr;
}

MediaApp *MediaManager::mediaFindApp(const char *sender, const char *path) {
  MediaManager *pMediaManagerObj = g_MediaManager;

  for (const auto &app : pMediaManagerObj->m_MediaAppList) {
    if (sender && (app->sender != sender)) {
      continue;
    }

    if (path && (app->path != path)) {
      continue;
    }

    return app;
  }

  return nullptr;
}

MediaMPRISPlayer *MediaManager::mediaPlayerCreate(const char *sender,
                                                  const char *path) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  MediaMPRISPlayer *player;
  MediaManager *pMediaManagerObj = g_MediaManager;

  player = new MediaMPRISPlayer();

  if (!player) return nullptr;

  player->sender = sender;
  player->path = path;

  int res = sd_bus_track_new(g_sdbus, &(player->sd_watch),
                             MediaManager::sd_mediaPlayerExit, player);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to create tracker: %d - %s\n", -res,
                  strerror(-res));
    return nullptr;
  }
  res = sd_bus_track_add_name(player->sd_watch, sender);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to track name: %d - %s\n", -res,
                  strerror(-res));
    return nullptr;
  }
  std::string properties_match = "type='signal',"
           "sender='" + player->sender + "',"
           "interface='org.freedesktop.DBus.Properties'";

  res = sd_bus_add_match(g_sdbus, nullptr,
           properties_match.c_str(),
           sd_playerPropertiesChanged,
           nullptr);

  std::string seeked_match = "type='signal',"
           "sender='" + player->sender + "',"
           "interface='org.mpris.MediaPlayer2.Player'";

  res = sd_bus_add_match(g_sdbus, nullptr,
           seeked_match.c_str(),
           sd_playerSeekedSignal,
           nullptr);

  // Register MPRIS Player with Target Controller
  if (pMediaManagerObj->m_avrcpTgService) {
    pMediaManagerObj->m_avrcpTgService->registerMPRISPlayer(player);
  }

  // Add player to the media manager object database
  pMediaManagerObj->m_PlayerList.insert(player);

  ADK_LOG_NOTICE(LOGTAG " Player registered: sender=%s path=%s\n", sender,
                 path);
  return player;
}

int MediaManager::parsePlayerProperties(MediaMPRISPlayer *mpris,
                                        sd_bus_message *m,
                                        sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  char type;
  const char *contents;
  bool has_properties = false;
  bool boolproperty;
  double doubleproperty;

  int res = sd_bus_message_peek_type(m, &type, &contents);
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  if (type != SD_BUS_TYPE_ARRAY) {
    return bt_error_invalid_args(ret_error);
  }

  res = sd_bus_message_enter_container(m, type, contents);
  if (res < 0) {
    return bt_error_failed(ret_error);
  }

  for (;;) {
    res = sd_bus_message_peek_type(m, &type, &contents);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    if (res == 0) {
      if (!has_properties) {
        ADK_LOG_INFO(LOGTAG "MPRIS player registered without properties!\n");
      }
      break;
    }

    if (!has_properties) {
      has_properties = true;
    }

    if (type != SD_BUS_TYPE_DICT_ENTRY) {
      return bt_error_invalid_args(ret_error);
    }

    res = sd_bus_message_enter_container(m, type, contents);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }

    const char *key;
    res = sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &key);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }

    // TODO: extract common block
    if (strcasecmp(key, "PlaybackStatus") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_STRING>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }
      char *playback_status;
      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &playback_status);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      mpris->playbackStatus = playback_status;

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }

      if (g_MediaManager != nullptr){
        if (g_MediaManager->m_avrcpTgService != nullptr){
          if (!g_MediaManager->m_avrcpTgService->setPlaybackStatus(mpris->playbackStatus)){
            ADK_LOG_ERROR(LOGTAG "Failed to set PlaybackState");
          }
        } else {
          ADK_LOG_ERROR(LOGTAG "No AvrcpTG object registered");
        }
      } else {
        ADK_LOG_ERROR(LOGTAG "No MediaManager object");
      }

    } else if (strcasecmp(key, "CanPlay") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_BOOLEAN, &boolproperty);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      mpris->canPlay = boolproperty;

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "CanPause") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_BOOLEAN, &boolproperty);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      mpris->canPause = boolproperty;

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "CanGoNext") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_BOOLEAN, &boolproperty);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      mpris->canGoNext = boolproperty;

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "CanGoPrevious") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_BOOLEAN, &boolproperty);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      mpris->canGoPrevious = boolproperty;

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "CanControl") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_BOOLEAN, &boolproperty);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      mpris->canControl = boolproperty;

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "Volume") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_DOUBLE>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_DOUBLE, &doubleproperty);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      mpris->volume = doubleproperty;
      ADK_LOG_NOTICE(LOGTAG "%d\n", mpris->volume);

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "Rate") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_DOUBLE>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_DOUBLE, &doubleproperty);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      mpris->rate = doubleproperty;
      ADK_LOG_NOTICE(LOGTAG "%f\n", mpris->rate);

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "MinimumRate") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_DOUBLE>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_DOUBLE, &doubleproperty);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      mpris->minimumRate = doubleproperty;
      ADK_LOG_NOTICE(LOGTAG "%f\n", mpris->minimumRate);

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "MaximumRate") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           TYPE_TO_STR<SD_BUS_TYPE_DOUBLE>::value);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_read_basic(m, SD_BUS_TYPE_DOUBLE, &doubleproperty);
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      mpris->maximumRate = doubleproperty;
      ADK_LOG_NOTICE(LOGTAG "%f\n", mpris->maximumRate);

      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    } else if (strcasecmp(key, "Metadata") == 0) {
      res = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT,
                                           "a{sv}");
      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      char metadata_array_type;
      const char *metadata_array_contents;
      int res = sd_bus_message_peek_type(m, &metadata_array_type, &metadata_array_contents);

      if (res < 0) {
        return bt_error_invalid_args(ret_error);
      }

      if (metadata_array_type != SD_BUS_TYPE_ARRAY) {
        return bt_error_invalid_args(ret_error);
      }

      res = sd_bus_message_enter_container(m, metadata_array_type, metadata_array_contents);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }

      Metadata metadata;
      bool more_metadata_fields_present = true;
      while (more_metadata_fields_present) {
        char dictonary_type;
        const char *dictonary_contents;
        res = sd_bus_message_peek_type(m, &dictonary_type, &dictonary_contents);
        if (res < 0) {
          return bt_error_failed(ret_error);
        }
        if (res == 0) {
          more_metadata_fields_present=false;
        } else {
          if (dictonary_type != SD_BUS_TYPE_DICT_ENTRY) {
            return bt_error_invalid_args(ret_error);
          }
          res = sd_bus_message_enter_container(m, dictonary_type, "sv");
          if (res < 0) {
            return bt_error_failed(ret_error);
          }
          const char *key;
          res = sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &key);
          if (res < 0) {
            return bt_error_failed(ret_error);
          }
          if (strcasecmp(key, "mpris:trackid") == 0) {
            std::string trackid;
            if(!getObjectStringFromMessage(m, &trackid)){
              ADK_LOG_ERROR(LOGTAG "Could not parse trackid");
              return bt_error_invalid_args(ret_error);
            }
            // If trackid is not in map then add it
            auto it = std::find(metadata.tracklist_map.begin(),metadata.tracklist_map.end(), trackid);
            if ( it == metadata.tracklist_map.end()) {
              metadata.tracklist_map.push_back(trackid);
            }
            metadata.currentTrackId = trackid;
          }
          if (strcasecmp(key, "mpris:length") == 0) {
            int64_t duration;
            if(!getInt64FromMessage(m, &duration)){
              return bt_error_invalid_args(ret_error);
            }
            metadata.duration = (uint32_t)duration;
          }
          if (strcasecmp(key, "xesam:trackNumber") == 0) {
            int32_t track_number;
            if(!getInt32FromMessage(m, &track_number)){
              return bt_error_invalid_args(ret_error);
            }
            metadata.trackNum = (uint32_t)track_number;
          }
          else if (strcasecmp(key, "xesam:album") == 0) {
            if(!getStringFromMessage(m, &metadata.album)){
              return bt_error_invalid_args(ret_error);
            }
          }
          else if (strcasecmp(key, "xesam:artist") == 0) {
            std::vector<std::string> artist_vector;
            if(!getStringArrayFromMessage(m, &artist_vector)){
              return bt_error_invalid_args(ret_error);
            }
            // Choose the first artist in list
            // to convert from xesam format to BlueZ
            if (artist_vector.size()!=0){
              metadata.artist = artist_vector[0];
            }
          }
          else if (strcasecmp(key, "xesam:title") == 0) {
            if(!getStringFromMessage(m, &metadata.title)){
              return bt_error_invalid_args(ret_error);
            }
          }
          else if (strcasecmp(key, "xesam:genre") == 0) {
            std::vector<std::string> genre_vector;
            if(!getStringArrayFromMessage(m, &genre_vector)) {
              return bt_error_invalid_args(ret_error);
            }
            // Choose the first genre in list
            // to convert from xesam format to BlueZ
            if (genre_vector.size()!=0){
              metadata.genre = genre_vector[0];
            }
          }
          else {
            res = sd_bus_message_skip(m, TYPE_TO_STR<SD_BUS_TYPE_VARIANT>::value);
            if (res < 0) {
              return bt_error_failed(ret_error);
            }
          }
          res = sd_bus_message_exit_container(m);
          if (res < 0) {
            return bt_error_failed(ret_error);
          }
        }
      }
      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        ADK_LOG_ERROR(LOGTAG "%s ERROR\n", __func__);
        return bt_error_failed(ret_error);
      }
      res = sd_bus_message_exit_container(m);
      if (res < 0) {
        ADK_LOG_ERROR(LOGTAG "%s ERROR\n", __func__);
        return bt_error_failed(ret_error);
      }
      if (g_MediaManager != nullptr){
        if (g_MediaManager->m_avrcpTgService != nullptr){
          if (!g_MediaManager->m_avrcpTgService->setDeviceMetadata(metadata)){
            ADK_LOG_ERROR(LOGTAG "Failed to set Device Metadata");
          }
        } else {
          ADK_LOG_ERROR(LOGTAG "No AvrcpTG object registered");
        }
      } else {
        ADK_LOG_ERROR(LOGTAG "No MediaManager object");
      }
    }
    else {
      res = sd_bus_message_skip(m, TYPE_TO_STR<SD_BUS_TYPE_VARIANT>::value);
      if (res < 0) {
        return bt_error_failed(ret_error);
      }
    }
    res = sd_bus_message_exit_container(m);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
  };

  ADK_LOG_NOTICE(LOGTAG "%s finished parsing properties\n", __func__);
  ADK_LOG_NOTICE(LOGTAG "%s current player contents:", __func__);
  ADK_LOG_NOTICE(LOGTAG
                 "\n\tcanPlay: %d\n"
                 "\tcanPause: %d\n"
                 "\tcanGoNext: %d\n"
                 "\tcanGoPrevious: %d\n"
                 "\tcanControl: %d\n"
                 "\trate: %f\n"
                 "\tminimumRate: %f\n"
                 "\tmaximumRate: %f\n"
                 "\tvolume: %d\n"
                 "\tsender: %s\n"
                 "\tpath: %s\n"
                 "\tsd_watch!=nullptr: %d\n",
                 mpris->canPlay,mpris->canPause,
                 mpris->canGoNext, mpris->canGoPrevious,
                 mpris->canControl, mpris->rate,
                 mpris->minimumRate, mpris->maximumRate,
                 mpris->volume, mpris->sender.c_str(),
                 mpris->path.c_str(), mpris->sd_watch!=nullptr);
  return 0;
}

int MediaManager::sd_playerRequestCompleted(sd_bus_message *reply, void *UNUSED(userdata),
                                            sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  if (sd_bus_error_is_set(ret_error)) {
    ADK_LOG_NOTICE(LOGTAG
                   "DBus error completing MPRIS Player request: %d - %s\n",
                   sd_bus_error_get_errno(ret_error), ret_error->name);
    return sd_bus_error_get_errno(ret_error);
  }

  const sd_bus_error *reply_error = sd_bus_message_get_error(reply);
  if (sd_bus_error_is_set(reply_error)) {
    ADK_LOG_WARNING(LOGTAG "Player Request() %d reply error: %d - %s (%s)\n",
                    sd_bus_error_get_errno(reply_error), reply_error->name,
                    reply_error->message);
    return sd_bus_error_get_errno(reply_error);
  }

  return 0;
}

int MediaManager::sd_playerPropertiesChanged(sd_bus_message *m, void *UNUSED(userdata),
                                   sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  if (!sd_bus_message_is_signal(m,
              "org.freedesktop.DBus.Properties",
              "PropertiesChanged")) {
    ADK_LOG_ERROR(LOGTAG "-- this is not a PropertiesChanged signal\n");

    return 0;
  }

  const char *sender = sd_bus_message_get_sender(m);
  const char *path = sd_bus_message_get_path(m);
  MediaMPRISPlayer *player = mediaFindPlayer(sender, path);

  if (!player) {
    ADK_LOG_NOTICE(LOGTAG "Player for %s (%s) does not exists\n", path, sender);
    return bt_error_does_not_exist(ret_error);
  }

  // First read string "org.mpris.MediaPlayer2.Player"
  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &path);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Invalid args\n");
    return bt_error_invalid_args(ret_error);
  }

  if (parsePlayerProperties(player, m, ret_error) < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to parse\n");
    return -1;
  }
  
  return 1;
}

int MediaManager::sd_playerSeekedSignal(sd_bus_message *m, void *UNUSED(userdata),
                                   sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  if (!sd_bus_message_is_signal(m,
              "org.mpris.MediaPlayer2.Player",
              "Seeked")) {
    ADK_LOG_ERROR(LOGTAG "-- this is not a Seeked signal\n");
    return 0;
  }

  const char *sender = sd_bus_message_get_sender(m);
  const char *path = sd_bus_message_get_path(m);
  MediaMPRISPlayer *player = mediaFindPlayer(sender, path);

  if (!player) {
    ADK_LOG_NOTICE(LOGTAG "Player for %s (%s) does not exists\n", path, sender);
    return bt_error_does_not_exist(ret_error);
  } else {
    int64_t position;
    int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_INT64, &position);
    if (res < 0) {
      ADK_LOG_NOTICE(LOGTAG "Invalid args\n");
      return bt_error_invalid_args(ret_error);
    }
    player->Seeked(position);
  }
  return 1;
}

void MediaManager::removeMediaPlayer(MediaMPRISPlayer *player) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  MediaManager *pMediaManagerObj = g_MediaManager;

  // Remove this player from MediaManager database
  g_MediaManager->m_PlayerList.erase(player);

  if (pMediaManagerObj->m_avrcpTgService) {
    pMediaManagerObj->m_avrcpTgService->unregisterMPRISPlayer(player);
  }

  sd_bus_track_unref(player->sd_watch);
  player->sd_watch = nullptr;

  delete player;
}

int MediaManager::sd_mediaPlayerExit(sd_bus_track *UNUSED(track), void *userdata) {
  ADK_LOG_NOTICE(LOGTAG " --%s\n", __func__);
  MediaMPRISPlayer *player = static_cast<MediaMPRISPlayer *>(userdata);
  ADK_LOG_DEBUG(LOGTAG " -- sender=%s path=%s", player->sender.c_str(),
                player->path.c_str());

  // Remove this player from both MediaManager and DBUS databases
  removeMediaPlayer(player);

  return 0;
}

MediaApp *MediaManager::mediaAppCreate(const char *sender, const char *path, const PropertiesList &properties_list) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  MediaManager *pMediaManagerObj = g_MediaManager;

  MediaApp *app;
  switch (getMediaAppType(properties_list)) {
    case MediaAppType::HID_APP:
    {
      ADK_LOG_NOTICE(LOGTAG "Creating HidApp with uuid:%s sender:%s path:%s", HidService::LOCAL_UUID, sender, path);
      HidApp *hid_app = new HidApp();
      hid_app->uuid = HidService::LOCAL_UUID;
      hid_app->sender = sender;
      hid_app->path = path;
      setHidAppContents(hid_app, properties_list);
      app = dynamic_cast<MediaApp*>(hid_app);
    }
    break;
    case MediaAppType::UNKNOWN_APP:
    {
      ADK_LOG_ERROR(LOGTAG "UUID not recognised... Failed to create media app");
      return nullptr;
    }
    break;
  }

  if (!app) {
    return nullptr;
  }

  int res = sd_bus_track_new(g_sdbus, &(app->sd_watch),
                             MediaManager::sd_mediaAppExit, app);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to create tracker: %d - %s\n", -res,
                  strerror(-res));
    return nullptr;
  }
  res = sd_bus_track_add_name(app->sd_watch, sender);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to track name: %d - %s\n", -res,
                  strerror(-res));
    return nullptr;
  }
  std::string properties_match = "type='signal',"
           "sender='" + app->sender + "',"
           "interface='org.freedesktop.DBus.Properties'";

  res = sd_bus_add_match(g_sdbus, nullptr,
           properties_match.c_str(),
           sd_mediaAppPropertiesChanged,
           nullptr);

  std::string report_properties_match = "type='signal',"
           "sender='" + app->sender + "',"
           "interface='"+HID_APP_INTERFACE+"'";

  res = sd_bus_add_match(g_sdbus, nullptr,
           report_properties_match.c_str(),
           sd_outputReport,
           nullptr);

  // Add player to the media manager object database
  pMediaManagerObj->m_MediaAppList.insert(app);

  ADK_LOG_NOTICE(LOGTAG " Media App registered: sender=%s path=%s\n", sender,
                 path);
  return app;
}

bool MediaManager::setHidAppContents(HidApp *hid_app, const PropertiesList &properties_list)
{
  bool connection_parameters_updated = false;
  bool mtu_updated = false;
  ADK_LOG_DEBUG(LOGTAG "%s", __func__);
  for (auto property:properties_list)
  {
    if (hid_app == nullptr){
      return false;
    }
    std::string property_name = property.first;
    if (property_name == "DeviceConnected")
    {
      hid_app->device_connected = property.second.value.type_boolean;
    }
    else if (property_name == "Interval")
    {
      if (hid_app->interval != property.second.value.type_uint16){
        hid_app->interval = property.second.value.type_uint16;
        connection_parameters_updated = true;
      }
    }
    else if (property_name == "Latency")
    {
      if (hid_app->latency != property.second.value.type_uint16){
        hid_app->latency = property.second.value.type_uint16;
        connection_parameters_updated = true;
      }
    }
    else if (property_name == "Timeout")
    {
      if (hid_app->timeout != property.second.value.type_uint16){
        hid_app->timeout = property.second.value.type_uint16;
        connection_parameters_updated = true;
      }
    }
    else if (property_name == "MTU")
    {
      if (hid_app->mtu != property.second.value.type_uint16){
        hid_app->mtu = property.second.value.type_uint16;
        mtu_updated = true;
      }
    }
    else if (property_name == "Device")
    {
      hid_app->device = property.second.value.type_object_path;
    }
  }
  // If the connection parameters were updated call the update method on the
  // connected HID service
  if (g_MediaManager==nullptr){
    return false;
  }
  if (g_MediaManager->m_hidService==nullptr){
    return false;
  }
  if (connection_parameters_updated){
    g_MediaManager->m_hidService->setConnectionParameters(hid_app->device,
                                                             hid_app->interval,
                                                             hid_app->latency,
                                                             hid_app->timeout);
  }
  if (mtu_updated){
    g_MediaManager->m_hidService->setMtu(hid_app->device,hid_app->mtu);
  }
  return true;
}

int MediaManager::parseMediaAppProperties(PropertiesList &properties_list,
                                          sd_bus_message *m,
                                          sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  char type;
  const char *contents;
  bool has_properties = false;

  int res = sd_bus_message_peek_type(m, &type, &contents);
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  if (type != SD_BUS_TYPE_ARRAY) {
    ADK_LOG_DEBUG(LOGTAG "Properties not an array\n");
    return bt_error_invalid_args(ret_error);
  }

  res = sd_bus_message_enter_container(m, type, contents);
  if (res < 0) {
    return bt_error_failed(ret_error);
  }

  for (;;) {
    res = sd_bus_message_peek_type(m, &type, &contents);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    if (res == 0) {
      if (!has_properties) {
        ADK_LOG_INFO(LOGTAG "Media App registered without properties!\n");
      }
      break;
    }

    if (!has_properties) {
      has_properties = true;
    }

    if (type != SD_BUS_TYPE_DICT_ENTRY) {
      ADK_LOG_DEBUG(LOGTAG "Array item is not dict entry\n");
      return bt_error_invalid_args(ret_error);
    }

    res = sd_bus_message_enter_container(m, type, contents);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }

    const char *key;
    SdBusVariant variant;

    res = sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &key);
    if (res < 0) {
      ADK_LOG_DEBUG(LOGTAG "Could not read property name string\n");
      return bt_error_failed(ret_error);
    }

    res = sd_bus_message_peek_type(m, &type, &contents);
    if (res < 0) {
      ADK_LOG_DEBUG(LOGTAG "Could not peek variant type\n");
      return bt_error_failed(ret_error);
    }

    res = sd_bus_message_read(m, TYPE_TO_STR<SD_BUS_TYPE_VARIANT>::value, contents, &variant.value);
    if (res < 0) {
      ADK_LOG_DEBUG(LOGTAG "Could not read variant contents '%s'\n", contents);
      return bt_error_invalid_args(ret_error);
    }

    variant.type = contents[0];

    ADK_LOG_NOTICE(LOGTAG "Adding property: %s (type='%c')", key, variant.type);
    properties_list[std::string(key)] = variant;

    res = sd_bus_message_exit_container(m);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
  };

  return 0;
}

int MediaManager::sd_mediaAppRequestCompleted(sd_bus_message *reply,
                                              void *UNUSED(userdata),
                                              sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  if (sd_bus_error_is_set(ret_error)) {
    ADK_LOG_NOTICE(LOGTAG
                   "DBus error completing MediaApp request: %d - %s\n",
                   sd_bus_error_get_errno(ret_error), ret_error->name);
    return sd_bus_error_get_errno(ret_error);
  }

  const sd_bus_error *reply_error = sd_bus_message_get_error(reply);
  if (sd_bus_error_is_set(reply_error)) {
    ADK_LOG_WARNING(LOGTAG "MediaApp Request() reply error: %d - %s (%s)\n",
                    sd_bus_error_get_errno(reply_error), reply_error->name,
                    reply_error->message);
    return sd_bus_error_get_errno(reply_error);
  }

  return 0;
}

int MediaManager::sd_mediaAppPropertiesChanged(sd_bus_message *m,
                                               void *UNUSED(userdata),
                                               sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  if (!sd_bus_message_is_signal(m,
              "org.freedesktop.DBus.Properties",
              "PropertiesChanged")) {
    ADK_LOG_ERROR(LOGTAG "-- this is not a PropertiesChanged signal\n");

    return 0;
  }

  const char *sender = sd_bus_message_get_sender(m);
  const char *path = sd_bus_message_get_path(m);
  MediaApp *app = mediaFindApp(sender, path);

  if (!app) {
    ADK_LOG_NOTICE(LOGTAG "MediaApp for %s (%s) does not exists\n", path, sender);
    return bt_error_does_not_exist(ret_error);
  }

  // First read interface string
  int res = sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &path);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Invalid args\n");
    return bt_error_invalid_args(ret_error);
  }

  PropertiesList property_list;
  parseMediaAppProperties(property_list, m, ret_error);

  if (app->uuid == HidService::LOCAL_UUID) {
    setHidAppContents(static_cast<HidApp*>(app), property_list);
  }
  return 1;
}

int MediaManager::sd_outputReport(sd_bus_message *m, void *UNUSED(userdata), sd_bus_error *ret_error)
{
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  if (!sd_bus_message_is_signal(m,
                                HID_APP_INTERFACE,
                                "OutputReport")) {
    ADK_LOG_ERROR(LOGTAG "-- this is not a Output Report signal\n");
    return 0;
  }

  const char *sender = sd_bus_message_get_sender(m);
  const char *path = sd_bus_message_get_path(m);
  MediaApp *media_app = mediaFindApp(sender, path);

  if (!media_app) {
    ADK_LOG_NOTICE(LOGTAG "Media App for %s (%s) does not exists\n", path, sender);
    return bt_error_does_not_exist(ret_error);
  } else {
    const uint8_t * report_data;
    size_t report_size;

    int res = sd_bus_message_read_array(m, SD_BUS_TYPE_BYTE, (const void **)&report_data, &report_size);
    if (res < 0) {
      return bt_error_invalid_args(ret_error);
    }
    if (getMediaAppTypeFromUuid(media_app->uuid) == MediaAppType::HID_APP &&
        media_app->device.length()!=0){
        g_MediaManager->m_hidService->sendOutputReport(media_app->device,report_data,report_size);
    }
  }
  return 1;
}


void MediaManager::removeMediaApp(MediaApp *app) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Remove this player from MediaManager database
  g_MediaManager->m_MediaAppList.erase(app);

  sd_bus_track_unref(app->sd_watch);
  app->sd_watch = nullptr;

  delete app;
}

void MediaManager::SendHidReport(const std::string &device_object_path, uint8_t *value, uint16_t report_size, bool report_id_flag)
{
  // Find the corresponding hid_app object associated with the device object path
  for (auto media_app :g_MediaManager->m_MediaAppList) {
    if (getMediaAppTypeFromUuid(media_app->uuid) == MediaAppType::HID_APP &&
        media_app->device.length()!=0){
      if (media_app->device == device_object_path){
        // Send the report on that hid_app object
        static_cast<HidApp *>(media_app)->SendReport(value,report_size,report_id_flag);
      }
    }
  }
}

bool MediaManager::setDeviceConnected(Device *device, bool status)
{
  ADK_LOG_DEBUG(LOGTAG " --%s\n", __func__);

  bool device_exists = false;
  // If device pointer is null then reset
  if (device == nullptr) {
    return false;
  }

  // Find the HidApp in the MediaAppList that has a device with device_address.
  for (auto media_app: g_MediaManager->m_MediaAppList){
    if (getMediaAppTypeFromUuid(media_app->uuid)!=MediaAppType::HID_APP){
      continue;
    }
    auto hid_app = static_cast<HidApp*>(media_app);
    if (hid_app->device != device->getDeviceObjectPath()){
      continue;
    }
    // If the device status has changed then update the property
    if (hid_app->device_connected != status){
      hid_app->setDeviceConnected(status);
    }
    // If device is disconnected then reset the hid app
    if (!status) {
      media_app->setDevicePath("");
    }
    // Device is in media_app.
    device_exists = true;
    break;
  }

  // If no HidApp Exists with device_address. Find the first HidApp in the MediaAppList
  // that has no device object.
  if (!device_exists && status){
    for (auto media_app: g_MediaManager->m_MediaAppList){
      if (media_app->device.length()==0 &&
          getMediaAppTypeFromUuid(media_app->uuid)==MediaAppType::HID_APP){
        auto hid_app = static_cast<HidApp*>(media_app);
        // Make media_app device equal to the device object found.
        hid_app->setDevicePath(device->getDeviceObjectPath());
        // Update the device status
        hid_app->setDeviceConnected(status);
        g_MediaManager->m_hidService->setMtu(hid_app->device,hid_app->mtu);
        g_MediaManager->m_hidService->setConnectionParameters(hid_app->device,hid_app->interval,hid_app->latency,hid_app->timeout);
        break;
      }
    }
  }
  return true;
}

void MediaManager::UpdateMtu(const std::string &device_object_path, uint16_t mtu)
{
  ADK_LOG_NOTICE(LOGTAG " --%s\n", __func__);

  for (auto media_app: g_MediaManager->m_MediaAppList){
    if (getMediaAppTypeFromUuid(media_app->uuid)!=MediaAppType::HID_APP){
      continue;
    }
    auto hid_app = static_cast<HidApp*>(media_app);
    if (hid_app->device != device_object_path){
      continue;
    }
    hid_app->setMtu(mtu);
    break;
  }
}

void MediaManager::UpdateConnectionParameters(const std::string &device_object_path, uint16_t interval, uint16_t latency, uint16_t timeout)
{
  ADK_LOG_NOTICE(LOGTAG " --%s\n", __func__);
  for (auto media_app: g_MediaManager->m_MediaAppList){
    if (getMediaAppTypeFromUuid(media_app->uuid)!=MediaAppType::HID_APP){
      continue;
    }
    auto hid_app = static_cast<HidApp*>(media_app);
    if (hid_app->device != device_object_path){
      continue;
    }
    hid_app->setInterval(interval);
    hid_app->setLatency(latency);
    hid_app->setTimeout(timeout);
    break;
  }
}

int MediaManager::sd_mediaAppExit(sd_bus_track *UNUSED(track), void *userdata) {
  ADK_LOG_NOTICE(LOGTAG " --%s\n", __func__);
  MediaApp *app = static_cast<MediaApp *>(userdata);
  ADK_LOG_DEBUG(LOGTAG " -- sender=%s path=%s", app->sender.c_str(),
                app->path.c_str());

  // Remove this player from both MediaManager and DBUS databases
  removeMediaApp(app);

  return 0;
}

/***** MPRIS client *****/

void MediaMPRISPlayer::Play() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (canPlay && canControl && sd_watch) {
    ADK_LOG_INFO(LOGTAG "bus call " MEDIA_MPRIS_PLAYER_INTERFACE ".Play()\n");
    int res = sd_bus_call_method_async(
        g_sdbus, nullptr, sender.c_str(), path.c_str(),
        MEDIA_MPRIS_PLAYER_INTERFACE, "Play",
		MediaManager::sd_playerRequestCompleted, nullptr, nullptr);
    if (res < 0) {
      ADK_LOG_NOTICE(LOGTAG "Could not call Play: %d - %s\n", -res,
                     strerror(-res));
    }
  }
}

void MediaMPRISPlayer::Stop() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (canControl && sd_watch) {
    ADK_LOG_INFO(LOGTAG "bus call " MEDIA_MPRIS_PLAYER_INTERFACE ".Stop()\n");
    int res = sd_bus_call_method_async(
        g_sdbus, nullptr, sender.c_str(), path.c_str(),
        MEDIA_MPRIS_PLAYER_INTERFACE, "Stop",
        MediaManager::sd_playerRequestCompleted, nullptr, nullptr);
    if (res < 0) {
      ADK_LOG_NOTICE(LOGTAG "Could not call Stop: %d - %s\n", -res,
                     strerror(-res));
    }
  }
}

void MediaMPRISPlayer::Pause() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (canPause && canControl && sd_watch) {
    ADK_LOG_INFO(LOGTAG "bus call " MEDIA_MPRIS_PLAYER_INTERFACE ".Pause()\n");
    int res = sd_bus_call_method_async(
        g_sdbus, nullptr, sender.c_str(), path.c_str(),
        MEDIA_MPRIS_PLAYER_INTERFACE, "Pause",
        MediaManager::sd_playerRequestCompleted, nullptr, nullptr);
    if (res < 0) {
      ADK_LOG_NOTICE(LOGTAG "Could not call Pause: %d - %s\n", -res,
                     strerror(-res));
    }
  }
}

void MediaMPRISPlayer::Next() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (canGoNext && canControl && sd_watch) {
    ADK_LOG_INFO(LOGTAG "bus call " MEDIA_MPRIS_PLAYER_INTERFACE ".Next()\n");
    int res = sd_bus_call_method_async(
        g_sdbus, nullptr, sender.c_str(), path.c_str(),
        MEDIA_MPRIS_PLAYER_INTERFACE, "Next",
        MediaManager::sd_playerRequestCompleted, nullptr, nullptr);
    if (res < 0) {
      ADK_LOG_NOTICE(LOGTAG "Could not call Next: %d - %s\n", -res,
                     strerror(-res));
    }
  }
}

void MediaMPRISPlayer::Previous() {
  ADK_LOG_NOTICE(LOGTAG "%s in sender %s in path %s \n", __func__,
                 sender.c_str(), path.c_str());
  if (canGoPrevious && canControl && sd_watch) {
    ADK_LOG_INFO(LOGTAG "bus call " MEDIA_MPRIS_PLAYER_INTERFACE
                        ".Previous()\n");
    int res = sd_bus_call_method_async(
        g_sdbus, nullptr, sender.c_str(), path.c_str(),
        MEDIA_MPRIS_PLAYER_INTERFACE, "Previous",
        MediaManager::sd_playerRequestCompleted, nullptr, nullptr);
    if (res < 0) {
      ADK_LOG_NOTICE(LOGTAG "Could not call Previous: %d - %s\n", -res,
                     strerror(-res));
    }
  }
}

void MediaMPRISPlayer::Rewind() {
  ADK_LOG_NOTICE(LOGTAG "%s in sender %s in path %s \n", __func__,
                 sender.c_str(), path.c_str());
  if (canControl && sd_watch) {
    // Check if the rate is equal to the minimum rate by checking if the
    // difference is less than a small number (ACCEPTABLE_DIFFERENCE).
    // If so then don't send an update
    if (fabs(rate - minimumRate)<ACCEPTABLE_DIFFERENCE) {
      return;
    }
    // Create a new variable so that stored rate is not modified. This will only
    // be modified when a property changed notification is received in response
    // to this property set.
    double send_rate = rate;
    // increment the rewind rate.
    if (send_rate >= 1.0){
      send_rate = -1.0;
    } else {
      send_rate -= 1.0;
    }

    // if the rate is now less than the minimum then set to minimum
    if (send_rate < minimumRate) {
      send_rate = minimumRate;
    }

    int r = sd_bus_call_method_async(
        g_sdbus,
        nullptr,
        sender.c_str(),
        path.c_str(),
        SD_BUS_PROPERTY_INTERFACE,
        "Set",
        MediaManager::sd_playerRequestCompleted,
        nullptr,
        "ssv",
        MEDIA_MPRIS_PLAYER_INTERFACE,
        "Rate",
        "d",
        send_rate
        );
    if (r < 0) {
      ADK_LOG_ERROR(LOGTAG "Failed to set property:r=%d", r);
    }
  }
}

void MediaMPRISPlayer::Fastforward() {
  ADK_LOG_NOTICE(LOGTAG "%s in sender %s in path %s \n", __func__,
                 sender.c_str(), path.c_str());
  if (canControl && sd_watch                   ) {
    // Check if the rate is equal to the maximum rate by checking if the
    // difference is less than a small number (ACCEPTABLE_DIFFERENCE).
    // If so then don't send an update
    if (fabs(rate - maximumRate)<ACCEPTABLE_DIFFERENCE) {
      return;
    }
    // Create a new variable so that stored rate is not modified. This will only
    // be modified when a property changed notification is received in response
    // to this property set.
    double send_rate = rate;
    // increment the fast forward rate.
    if (send_rate <= 1.0){
      send_rate =  2.0;
    } else {
      send_rate += 1.0;
    }

    // if the rate is now more that the maximum then set to maximum
    if (send_rate > maximumRate) {
      send_rate = maximumRate;
    }

    int r = sd_bus_call_method_async(
          g_sdbus,
          nullptr,
          sender.c_str(),
          path.c_str(),
          SD_BUS_PROPERTY_INTERFACE,
          "Set",
          MediaManager::sd_playerRequestCompleted,
          nullptr,
          "ssv",
          MEDIA_MPRIS_PLAYER_INTERFACE,
          "Rate",
          "d",
          send_rate
          );

      if (r < 0) {
        ADK_LOG_ERROR(LOGTAG "Failed to set property:r=%d", r);
      }
  }
}

void MediaMPRISPlayer::Resume()
{
  ADK_LOG_NOTICE(LOGTAG "%s in sender %s in path %s \n", __func__,
                 sender.c_str(), path.c_str());

  // Check if the rate is already equal to 1.0 by checking if the
  // difference is less than a small number (ACCEPTABLE_DIFFERENCE).
  // If so then don't send an update
  if (fabs(rate - 1.0)<ACCEPTABLE_DIFFERENCE) {
    return;
  }
  // Reset play rate to 1.0
  if (canControl && sd_watch) {
    int r = sd_bus_call_method_async(
          g_sdbus,
          nullptr,
          sender.c_str(),
          path.c_str(),
          SD_BUS_PROPERTY_INTERFACE,
          "Set",
          MediaManager::sd_playerRequestCompleted,
          nullptr,
          "ssv",
          MEDIA_MPRIS_PLAYER_INTERFACE,
          "Rate",
          "d",
          1.0
          );

      if (r < 0) {
        ADK_LOG_ERROR(LOGTAG "Failed to set property:r=%d", r);
      }
  }
}

void MediaMPRISPlayer::Seeked(int x) {
  ADK_LOG_NOTICE(LOGTAG "%s %d\n", __func__, x);
  // TODO: Needs to call setPlayPosition() in device ???
}

/***** HOGP Client *****/

void HidApp::SendReport(uint8_t *value, uint16_t report_size,
                             bool report_id_flag) {
  sd_bus_message *m;

  int res = sd_bus_message_new_method_call(
    g_sdbus,
    &m,
    sender.c_str(),
    path.c_str(),
    HID_APP_INTERFACE,
    "RawReport");
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Could not create bus message: %d - %s\n", -res,
                  strerror(-res));
    return;
  }

  res = sd_bus_message_append_array(m, SD_BUS_TYPE_BYTE, value,
                                    sizeof(uint8_t) * report_size);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Could not append array to message: %d - %s\n", -res,
                  strerror(-res));
    return;
  }

  res = sd_bus_message_append_basic(m, SD_BUS_TYPE_UINT16, &report_size);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Could not append size to message: %d - %s\n", -res,
                  strerror(-res));
    return;
  }

  res = sd_bus_message_append_basic(m, SD_BUS_TYPE_BOOLEAN, &report_id_flag);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Could not append report_id flag to message: %d - %s\n", -res,
                  strerror(-res));
    return;
  }
  res = sd_bus_message_set_expect_reply(m, 0);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG "Could not set expect reply flag: %d - %s\n", -res,
                  strerror(-res));
    return;
  }

  // Use sd_bus_send to send the message to sd_bus without expecting a reply.
  // This should be a non-blocking call that wont hold up the process.
  res = sd_bus_send(g_sdbus, m, nullptr);

  if (res < 0) {
    ADK_LOG_WARNING(LOGTAG "Could not call RawReport: %d - %s\n", -res,
                    strerror(-res));
    return;
  }
}

bool HidApp::setDeviceConnected(bool status)
{
  ADK_LOG_DEBUG(LOGTAG " --%s (%d)\n", __func__, status);
  device_connected = status;
  int r;
  bool return_status = true;
  // Update the DeviceConnected property
  r = sd_bus_call_method_async(
      g_sdbus,
      nullptr,
      sender.c_str(),
      path.c_str(),
      SD_BUS_PROPERTY_INTERFACE,
      "Set",
      MediaManager::sd_mediaAppRequestCompleted,
      nullptr,
      "ssv",
      HID_APP_INTERFACE,
      "DeviceConnected",
      "b",
      status
      );

  if (r < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to set property:r=%d", r);
    return_status = false;
  }
  return return_status;
}

bool HidApp::setMtu(uint16_t new_mtu)
{
  ADK_LOG_DEBUG(LOGTAG " --%s (%d)\n", __func__, new_mtu);
  mtu = new_mtu;
  int r;
  bool return_status = true;
  // Update the DeviceConnected property
  r = sd_bus_call_method_async(
      g_sdbus,
      nullptr,
      sender.c_str(),
      path.c_str(),
      SD_BUS_PROPERTY_INTERFACE,
      "Set",
      MediaManager::sd_mediaAppRequestCompleted,
      nullptr,
      "ssv",
      HID_APP_INTERFACE,
      "MTU",
      "q",
      new_mtu
      );

  if (r < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to set property:r=%d", r);
    return_status = false;
  }
  return return_status;
}

bool HidApp::setLatency(uint16_t new_latency)
{
  ADK_LOG_DEBUG(LOGTAG " --%s (%d)\n", __func__, new_latency);
  latency = new_latency;
  int r;
  bool return_status = true;
  // Update the DeviceConnected property
  r = sd_bus_call_method_async(
      g_sdbus,
      nullptr,
      sender.c_str(),
      path.c_str(),
      SD_BUS_PROPERTY_INTERFACE,
      "Set",
      MediaManager::sd_mediaAppRequestCompleted,
      nullptr,
      "ssv",
      HID_APP_INTERFACE,
      "Latency",
      "q",
      new_latency
      );

  if (r < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to set property:r=%d", r);
    return_status = false;
  }
  return return_status;
}

bool HidApp::setInterval(uint16_t new_interval)
{
  ADK_LOG_DEBUG(LOGTAG " --%s (%d)\n", __func__, new_interval);
  interval = new_interval;
  int r;
  bool return_status = true;
  // Update the DeviceConnected property
  r = sd_bus_call_method_async(
      g_sdbus,
      nullptr,
      sender.c_str(),
      path.c_str(),
      SD_BUS_PROPERTY_INTERFACE,
      "Set",
      MediaManager::sd_mediaAppRequestCompleted,
      nullptr,
      "ssv",
      HID_APP_INTERFACE,
      "Interval",
      "q",
      new_interval
      );

  if (r < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to set property:r=%d", r);
    return_status = false;
  }
  return return_status;
}

bool HidApp::setTimeout(uint16_t new_timeout)
{
  ADK_LOG_DEBUG(LOGTAG " --%s (%d)\n", __func__, new_timeout);
  timeout = new_timeout;
  int r;
  bool return_status = true;
  // Update the DeviceConnected property
  r = sd_bus_call_method_async(
      g_sdbus,
      nullptr,
      sender.c_str(),
      path.c_str(),
      SD_BUS_PROPERTY_INTERFACE,
      "Set",
      MediaManager::sd_mediaAppRequestCompleted,
      nullptr,
      "ssv",
      HID_APP_INTERFACE,
      "Timeout",
      "q",
      new_timeout
      );

  if (r < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to set property:r=%d", r);
    return_status = false;
  }
  return return_status;
}

bool MediaApp::setDevicePath(std::string device_path)
{
  ADK_LOG_DEBUG(LOGTAG " --%s (%s)", __func__, device_path.c_str());
  device = device_path;
  int r;
  bool return_status = true;
  // Update the Device property
  r = sd_bus_call_method_async(
      g_sdbus,
      nullptr,
      sender.c_str(),
      path.c_str(),
      SD_BUS_PROPERTY_INTERFACE,
      "Set",
      MediaManager::sd_mediaAppRequestCompleted,
      nullptr,
      "ssv",
      HID_APP_INTERFACE,
      "Device",
      "s",
      device_path.c_str()
      );

  if (r < 0) {
    ADK_LOG_ERROR(LOGTAG "Failed to set property:r=%d", r);
    return_status = false;
  }
  return return_status;
}

bool MediaManager::getStringFromMessage(sd_bus_message *sd_bus_message_ptr,std::string *return_string){
  const char * string_property;
  int res;
  res = sd_bus_message_enter_container(sd_bus_message_ptr, SD_BUS_TYPE_VARIANT,
                                       TYPE_TO_STR<SD_BUS_TYPE_STRING>::value);
  if (res < 0) {
    return false;
  }

  res = sd_bus_message_read_basic(sd_bus_message_ptr, SD_BUS_TYPE_STRING, &string_property);
  if (res < 0) {
    return false;
  }
  if (return_string != nullptr){
    return_string->assign(string_property);
  } else {
    return false;
  }
  res = sd_bus_message_exit_container(sd_bus_message_ptr);
  if (res < 0) {
    return false;
  }
  return true;
}

bool MediaManager::getObjectStringFromMessage(sd_bus_message *sd_bus_message_ptr,std::string *return_string){
  const char * string_property;
  int res;
  res = sd_bus_message_enter_container(sd_bus_message_ptr, SD_BUS_TYPE_VARIANT,
                                       TYPE_TO_STR<SD_BUS_TYPE_OBJECT_PATH>::value);
  if (res < 0) {
    return false;
  }

  res = sd_bus_message_read_basic(sd_bus_message_ptr, SD_BUS_TYPE_OBJECT_PATH, &string_property);
  if (res < 0) {
    return false;
  }
  if (return_string != nullptr){
    return_string->assign(string_property);
  } else {
    return false;
  }
  res = sd_bus_message_exit_container(sd_bus_message_ptr);
  if (res < 0) {
    return false;
  }
  return true;
}

bool MediaManager::getInt64FromMessage(sd_bus_message *sd_bus_message_ptr,int64_t *return_int){
  int res;
  res = sd_bus_message_enter_container(sd_bus_message_ptr, SD_BUS_TYPE_VARIANT,
                                       TYPE_TO_STR<SD_BUS_TYPE_INT64>::value);
  if (res < 0) {
    return false;
  }
  res = sd_bus_message_read_basic(sd_bus_message_ptr, SD_BUS_TYPE_INT64, return_int);
  if (res < 0) {
    return false;
  }
  res = sd_bus_message_exit_container(sd_bus_message_ptr);
  if (res < 0) {
    return false;
  }
  return true;
}

bool MediaManager::getInt32FromMessage(sd_bus_message *sd_bus_message_ptr,int32_t *return_int){
  int res;
  res = sd_bus_message_enter_container(sd_bus_message_ptr, SD_BUS_TYPE_VARIANT,
                                       TYPE_TO_STR<SD_BUS_TYPE_INT32>::value);
  if (res < 0) {
    return false;
  }
  res = sd_bus_message_read_basic(sd_bus_message_ptr, SD_BUS_TYPE_INT32, return_int);
  if (res < 0) {
    return false;
  }
  res = sd_bus_message_exit_container(sd_bus_message_ptr);
  if (res < 0) {
    return false;
  }
  return true;
}

bool MediaManager::getStringArrayFromMessage(sd_bus_message *sd_bus_message_ptr, std::vector<std::string> *return_string_vector){
  int res;
  char array_type;
  const char *array_contents;

  res = sd_bus_message_peek_type(sd_bus_message_ptr, &array_type, &array_contents);
  if (res < 0) {
    return false;
  }

  res = sd_bus_message_enter_container(sd_bus_message_ptr, array_type, array_contents);
  if (res < 0) {
    return false;
  }

  res = sd_bus_message_peek_type(sd_bus_message_ptr, &array_type, &array_contents);
  if (res < 0) {
    return false;
  }

  if (array_type != SD_BUS_TYPE_ARRAY) {
    return false;
  }

  res = sd_bus_message_enter_container(sd_bus_message_ptr, array_type, array_contents);
  if (res < 0) {
    return false;
  }

  char type_new;
  const char * contents_new;
  bool more_strings_in_array = true;
  while (more_strings_in_array) {
    res = sd_bus_message_peek_type(sd_bus_message_ptr, &type_new, &contents_new);
    if (res < 0){
      return false;
    }
    if (res == 0) {
      more_strings_in_array = false;
    } else {
      char * string_array_entry;
      res = sd_bus_message_read_basic(sd_bus_message_ptr, SD_BUS_TYPE_STRING, &string_array_entry);
      if (res < 0) {
        return false;
      }
      return_string_vector->push_back(std::string(string_array_entry));
    }
  }
  res = sd_bus_message_exit_container(sd_bus_message_ptr);
  if (res < 0) {
    return false;
  }

  res = sd_bus_message_exit_container(sd_bus_message_ptr);
  if (res < 0) {
    return false;
  }
  return true;
}

