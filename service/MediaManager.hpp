/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef MEDIA_MANAGER_HPP
#define MEDIA_MANAGER_HPP

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <hardware/bluetooth.h>
#include <hardware/bt_av_vendor.h>
#include <hardware/bt_sock.h>
#include <hardware/vendor.h>
#include <systemdq/sd-bus.h>

#include <list>
#include <memory>
#include <map>
#include <set>
#include <string>
#include <vector>
#include "Error.hpp"
#include "HidService.hpp"
#include "ProfileService.hpp"
#include "Device.hpp"

/**
 * @file MediaManager.hpp
 * @brief MediaManager header file
 */
struct MediaEndPoint;
struct MediaEndPointRequest;
struct MediaMPRISPlayer;
class MediaTransport;
class A2dpSinkService;
class A2dpSourceService;
class AvrcpTGService;

typedef void (*MediaEndPointCallBack)(MediaEndPoint *endpoint, int ret,
                                      void *user_data);

union SdBusValue{
  uint8_t type_byte;
  int type_boolean;
  int16_t type_int16;
  uint16_t type_uint16;
  int32_t type_int32;
  uint32_t type_uint32;
  int64_t type_int64;
  uint64_t type_uint64;
  double type_double;
  char * type_string;
  char * type_object_path;
  char * type_signature;
  int  type_unix_fd;
};

struct SdBusVariant{
  char type;
  SdBusValue value;
};

typedef  std::map<std::string, SdBusVariant> PropertiesList;
// MediaMPRISPlayer implements a client of the org.mpris.MediaPlayer2.Player
// MPRIS interface
struct MediaMPRISPlayer {
  // Methods
  void Next();
  void Previous();
  void Pause();
  void Stop();
  void Play();
  void Rewind();
  void Fastforward();

  void Resume();

  // Signals
  void Seeked(int x);

  // Properties
  bool canPlay;
  bool canPause;
  bool canGoNext;
  bool canGoPrevious;
  bool canControl;

  std::string playbackStatus;
  double rate;
  double minimumRate;
  double maximumRate;

  uint32_t volume; // TODO: is this the correct type, double?

  // Other data
  std::string sender; /* Player DBus bus id */
  std::string path;   /* Player object path */
  sd_bus_track *sd_watch;
};

// Implements a MediaApp object that can be registered with a modified version
// of the org.bluez.Media1 interface supporting arbitrary media applications.
struct MediaApp {
  // Properties
  std::string uuid;
  std::string device;

  // Other data
  std::string sender; /* Player DBus bus id */
  std::string path;   /* Player object path */
  sd_bus_track *sd_watch;

  // Property Set Methods
  bool setDevicePath(std::string device_path);
};

// Implements a client of the com.qualcomm.qti.HOGP1 interface
struct HidApp : MediaApp {
  // Methods
  void SendReport(uint8_t* value, uint16_t report_size, bool report_id_flag);
  // Properties
  bool device_connected;
  uint16_t mtu;
  uint16_t latency;
  uint16_t interval;
  uint16_t timeout;

  // Property Set Methods
  bool setDeviceConnected(bool status);
  bool setMtu(uint16_t new_mtu);
  bool setLatency(uint16_t new_latency);
  bool setInterval(uint16_t new_interval);
  bool setTimeout(uint16_t new_timeout);

};

enum MediaAppType{
  HID_APP,
  UNKNOWN_APP
};

#define NON_A2DP_CODEC 0xFF            // A2DP Vendor specific codec
#define QTI_VENDOR_ID 0x000000D7       // Bluetooth SIG VendorID for "Qualcomm Technologies, Inc. Vendor"
#define APTX_CODEC_ID_ADAPTIVE 0x00AD  // Aptx Adaptive Vendor Codec ID
#define APT_VENDOR_ID 0x0000004F       // Bluetooth SIG Vendor ID for "APT Ltd Vendor"
#define APTX_CODEC_ID_CLASSIC 0x0001   // Aptx Classic Vendor Codec ID

class MediaManager {
 private:
  std::set<MediaEndPoint *> m_EndPointsList;
  std::set<MediaMPRISPlayer *> m_PlayerList;
  std::set<MediaApp *> m_MediaAppList;
  A2dpSinkService *m_ad2pSinkService;
  A2dpSourceService *m_ad2pSourceService;
  AvrcpTGService *m_avrcpTgService;
  HidService *m_hidService;

  sd_bus_slot *m_sdbusSlot;

  struct CodecParser;
  std::unique_ptr<CodecParser> m_parser;

 public:
  MediaManager();
  ~MediaManager();

  static void clearConfiguration(MediaEndPoint *);
  static void clearEndpoint(MediaEndPoint *);
  static int endpointCleared(sd_bus_message *, void *, sd_bus_error *);
  static void releaseMediaEndPoint(MediaEndPoint *);
  static int endpointReleased(sd_bus_message *, void *, sd_bus_error *);
  static void endpointRequestFree(MediaEndPointRequest *);
  static void mediaEndPointCancel(MediaEndPointRequest *);
  static void mediaEndPointCancelAll(MediaEndPoint *);
  static void mediaEndPointDestroy(MediaEndPoint *);
  static bool mediaEndPointCreate(const char *, const char *, const char *,
                                  bool, uint8_t, const uint8_t *, size_t);
  static MediaEndPoint *mediaFindEndPoint(const char *, const char *);
  static int sd_parseEndPointProperties(sd_bus_message *,
                                        sd_bus_error *ret_error, const char **,
                                        bool *, uint8_t *, const uint8_t **,
                                        size_t *);

  static MediaMPRISPlayer *mediaFindPlayer(const char *, const char *);
  static MediaMPRISPlayer *mediaPlayerCreate(const char *, const char *);
  static void mediaPlayerDestroy(MediaMPRISPlayer *player);
  static int parsePlayerProperties(MediaMPRISPlayer *player,
                                   sd_bus_message *msg, sd_bus_error *er);
  static void removeMediaPlayer(MediaMPRISPlayer *player);

  static int sd_playerRequestCompleted(sd_bus_message *, void *, sd_bus_error *);
  static int sd_playerPropertiesChanged(sd_bus_message *, void *, sd_bus_error *);
  static int sd_playerSeekedSignal(sd_bus_message *, void *, sd_bus_error *);

  static MediaAppType getMediaAppType(PropertiesList properties_list);
  static MediaAppType getMediaAppTypeFromUuid(const std::string &uuid);
  static void ResetMediaApp(MediaApp * media_app){ media_app->device = ""; }
  static MediaApp *mediaFindApp(const char *, const char *);
  static MediaApp *mediaAppCreate(const char *sender, const char *path, const PropertiesList &properties_list);
  static bool setHidAppContents(HidApp *hid_app, const PropertiesList &properties_list);
  static void mediaAppDestroy(MediaApp *media_app);
  static int parseMediaAppProperties(PropertiesList &properties_list, sd_bus_message *msg,
                                     sd_bus_error *er);
  static void removeMediaApp(MediaApp *media_app);
  static void SendHidReport(const std::string &device_object_path, uint8_t* value, uint16_t report_size, bool report_id_flag);
  static bool setDeviceConnected(Device* device, bool status);

  static void UpdateMtu(const std::string & device_object_path, uint16_t mtu);
  static void UpdateConnectionParameters(const std::string &device_object_path, uint16_t interval, uint16_t latency, uint16_t timeout);

  static int sd_mediaAppRequestCompleted(sd_bus_message *, void *, sd_bus_error *);
  static int sd_mediaAppPropertiesChanged(sd_bus_message *, void *, sd_bus_error *);

  static int sd_outputReport(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);


  static bool sendSetConfiguration(MediaEndPoint *, MediaEndPointCallBack,
                                   void *);
  static int sendSetConfiguration_1(sd_bus_message *m, void *userdata,
                                    sd_bus_error *ret_error);
  static int sendSetConfiguration_2(sd_bus_message *m, void *userdata,
                                    sd_bus_error *ret_error);
  bool registerMediaInterface(void);
  void unRegisterMediaInterface();

  // Cleanup callbacks
  static void mediaInterfacePathFree();
  static void setConfigurationCallBack(MediaEndPoint *, int, void *);
  static int sd_mediaEndPointExit(sd_bus_track *track, void *userdata);
  static int sd_mediaPlayerExit(sd_bus_track *, void *);
  static int sd_mediaAppExit(sd_bus_track *, void *);

  // Media Interface functions exposed on dbus
  static int sd_registerPlayer(sd_bus_message *m, void *userdata,
                               sd_bus_error *ret_error);
  static int sd_unregisterPlayer(sd_bus_message *m, void *userdata,
                                 sd_bus_error *ret_error);
  static int sd_registerEndpoint(sd_bus_message *m, void *userdata,
                                 sd_bus_error *ret_error);
  static int sd_unregisterEndpoint(sd_bus_message *m, void *userdata,
                                   sd_bus_error *ret_error);
  static int sd_registerMediaApp(sd_bus_message *m, void *userdata,
                                 sd_bus_error *ret_error);
  static int sd_unregisterMediaApp(sd_bus_message *m, void *userdata,
                                   sd_bus_error *ret_error);
  static int sd_propertyGetEndpoints(sd_bus *bus, const char *path,
                                     const char *interface, const char *property,
                                     sd_bus_message *reply, void *userdata,
                                     sd_bus_error *ret_error);
  static int sd_setSinkPreferences(sd_bus *bus, const char *path,
                                   const char *interface, const char *property,
                                   sd_bus_message *reply, void *userdata,
                                   sd_bus_error *ret_error);
  // Returns true if processing of set configuration is successful, otherwise
  // false
  bool setConfiguration(std::shared_ptr<MediaTransport> transport);
  bool findAndClearConfiguration(uint8_t bluezCodecType,
                                 const std::string &uuid);
  void updateAdapterServices(A2dpSinkService *ad2pSinkService,
                             A2dpSourceService *ad2pSourceService,
                             AvrcpTGService *avrcpTgService,
                             HidService *hidService);
  void updateMediaInterfaceLists();

  // Returns a string representing the codec and its config parameters
  static const char *getCodecName(uint16_t codec);
  static std::string getCodecInfoString(uint16_t codec,
                                        const btav_codec_config_t &config);

  // Utility functions
  static bool getCodecConfigFromCapabilities(
      uint8_t codec, const uint8_t *capabilities, size_t size,
      btav_codec_configuration_t &config);

  private:
  static bool getStringFromMessage(sd_bus_message *sd_bus_message_ptr, std::string *return_string);
  static bool getObjectStringFromMessage(sd_bus_message *sd_bus_message_ptr, std::string *return_string);
  static bool getInt64FromMessage(sd_bus_message *sd_bus_message_ptr, int64_t *return_int);
  static bool getInt32FromMessage(sd_bus_message *sd_bus_message_ptr, int32_t *return_int);

  static bool getStringArrayFromMessage(sd_bus_message *sd_bus_message_ptr, std::vector<std::string> *return_string_vector);

};

#endif  // MEDIA_MANAGER_HPP
