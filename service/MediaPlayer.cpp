/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "MediaPlayer.hpp"

#include "Adapter.hpp"
#include "AvrcpCTService.hpp"
#include "Common.hpp"
#include "Device.hpp"
#include "Error.hpp"
#include "Sdbus.hpp"

#define LOGTAG "MEDIA_PLAYER "
#define MEDIAPLAYER_INTERFACE "org.bluez.MediaPlayer1"

MediaPlayer::MediaPlayer(Device *device)
    : m_device(device), m_sdbusSlot(nullptr) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

MediaPlayer::~MediaPlayer() {
  int res = sd_bus_emit_interfaces_removed(g_sdbus, m_path.c_str(),
                                           MEDIAPLAYER_INTERFACE, nullptr);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG
                   "object manager failed to signal deleted interface %s in %s: %d\n",
                   MEDIAPLAYER_INTERFACE, m_path.c_str(), -res, strerror(-res));
  }
  sd_bus_slot_unref(m_sdbusSlot);
  m_sdbusSlot = nullptr;
}

bool MediaPlayer::registerDeviceMediaPlayerObject() {
  ADK_LOG_NOTICE(LOGTAG " -->%s\n", __func__);

  if (!g_sdbus) {
    ADK_LOG_ERROR(LOGTAG " Dbus connection to system bus is not available\n");
    return false;
  }

  // Create a DBus interface for the media functionalities
  static const sd_bus_vtable sSdMediaplayerMethods[] = {
      SD_BUS_VTABLE_START(0),
      SD_BUS_METHOD("Play", nullptr, nullptr, MediaPlayer::sd_playerAction,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Pause", nullptr, nullptr, MediaPlayer::sd_playerAction,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Stop", nullptr, nullptr, MediaPlayer::sd_playerAction,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Next", nullptr, nullptr, MediaPlayer::sd_playerAction,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Previous", nullptr, nullptr, MediaPlayer::sd_playerAction,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("FastForward", nullptr, nullptr,
                    MediaPlayer::sd_playerAction, SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Rewind", nullptr, nullptr, MediaPlayer::sd_playerAction,
                    SD_BUS_VTABLE_UNPRIVILEGED),

      SD_BUS_WRITABLE_PROPERTY("Equalizer", "s", MediaPlayer::sd_getEqualizer,
                               MediaPlayer::sd_setEqualizer, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("Repeat", "s", MediaPlayer::sd_getRepeat,
                               MediaPlayer::sd_setRepeat, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("Shuffle", "s", MediaPlayer::sd_getShuffle,
                               MediaPlayer::sd_setShuffle, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("Scan", "s", MediaPlayer::sd_getScan,
                               MediaPlayer::sd_setScan, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Status", "s", MediaPlayer::sd_getStatus, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Position", "u", MediaPlayer::sd_getPosition, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Track", "a{sv}", MediaPlayer::sd_getTrack, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Device", "o", MediaPlayer::sd_getDevice, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Name", "s", MediaPlayer::sd_getName, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      // SD_BUS_PROPERTY("Type", "s", MediaPlayer::sd_getType, 0,
      // SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),  SD_BUS_PROPERTY("Subtype", "s",
      // MediaPlayer::sd_getSubtype, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      // SD_BUS_PROPERTY("Browsable", "b", MediaPlayer::sd_getBrowsable, 0,
      // SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),  SD_BUS_PROPERTY("Searchable",
      // "b", MediaPlayer::sd_getSearchable, 0,
      // SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE), SD_BUS_PROPERTY("Playlist", "o",
      // MediaPlayer::sd_getPlaylist, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_VTABLE_END};

  m_path = m_device->getDeviceObjectPath() + "/player0";
  int res = sd_bus_add_object_vtable(g_sdbus, &m_sdbusSlot, m_path.c_str(),
                                     MEDIAPLAYER_INTERFACE,
                                     sSdMediaplayerMethods, this);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "interface init failed on path %s: %d - %s\n",
                   m_path.c_str(), -res, strerror(-res));
    return false;
  }

  res = sd_bus_emit_interfaces_added(g_sdbus, m_path.c_str(),
                                     MEDIAPLAYER_INTERFACE, nullptr);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG
                   "object manager failed to signal new interface %s in %s: %d - %s\n",
                   MEDIAPLAYER_INTERFACE, m_path.c_str(), -res, strerror(-res));
    return false;
  }

  return true;
}

void MediaPlayer::playStateChanged() {
  ADK_LOG_NOTICE(LOGTAG " -- %s", __func__);
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIAPLAYER_INTERFACE, "Status", nullptr);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to emit status changed: %d - %s\n", -res,
                   strerror(-res));
    return;
  }
}

void MediaPlayer::playPositionChanged() {
  ADK_LOG_NOTICE(LOGTAG " -- %s", __func__);
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIAPLAYER_INTERFACE, "Position", nullptr);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to emit position changed: %d - %s\n", -res,
                   strerror(-res));
    return;
  }
}

void MediaPlayer::metadataChanged() {
  ADK_LOG_NOTICE(LOGTAG " -- %s", __func__);
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIAPLAYER_INTERFACE, "Track", nullptr);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to emit track changed: %d - %s\n", -res,
                   strerror(-res));
    return;
  }
}

void MediaPlayer::equalizerChanged() {
  ADK_LOG_NOTICE(LOGTAG " -- %s", __func__);
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIAPLAYER_INTERFACE, "Equalizer", nullptr);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to emit equalizer changed: %d - %s\n", -res,
                   strerror(-res));
    return;
  }
}

void MediaPlayer::repeatChanged() {
  ADK_LOG_NOTICE(LOGTAG " -- %s", __func__);
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIAPLAYER_INTERFACE, "Repeat", nullptr);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to emit repeat changed: %d - %s\n", -res,
                   strerror(-res));
    return;
  }
}

void MediaPlayer::shuffleChanged() {
  ADK_LOG_NOTICE(LOGTAG " -- %s", __func__);
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIAPLAYER_INTERFACE, "Shuffle", nullptr);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to emit shuffle changed: %d - %s\n", -res,
                   strerror(-res));
    return;
  }
}

void MediaPlayer::scanChanged() {
  ADK_LOG_NOTICE(LOGTAG " -- %s", __func__);
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIAPLAYER_INTERFACE, "Scan", nullptr);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "Failed to emit scan changed: %d - %s\n", -res,
                   strerror(-res));
    return;
  }
}

int MediaPlayer::sd_playerAction(sd_bus_message *m, void *userdata,
                                 sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);

  std::string member = sd_bus_message_get_member(m);
  AvrcpCTService::PassthroughKeys key;
  if (member == "Play") {
    key = AvrcpCTService::CMD_ID_PLAY;
  } else if (member == "Pause") {
    key = AvrcpCTService::CMD_ID_PAUSE;
  } else if (member == "Stop") {
    key = AvrcpCTService::CMD_ID_STOP;
  } else if (member == "Next") {
    key = AvrcpCTService::CMD_ID_FORWARD;
  } else if (member == "Previous") {
    key = AvrcpCTService::CMD_ID_BACKWARD;
  } else if (member == "FastForward") {
    key = AvrcpCTService::CMD_ID_FF;
  } else if (member == "Rewind") {
    key = AvrcpCTService::CMD_ID_REWIND;
  } else {
    return bt_error_not_supported(ret_error);
  }

  AvrcpCTService *avrcp =
      player->m_device->getAdapter()->findConnectableService<AvrcpCTService>();
  avrcp->handlePassThroughCmdReq(key, player->m_device);

  sd_bus_reply_method_return(m, nullptr);
  return 0;
}

int MediaPlayer::sd_getEqualizer(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  int res = sd_bus_message_append_basic(
      reply, SD_BUS_TYPE_STRING, to_string(player->m_device->getEqualizer()));
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  return 0;
}

int MediaPlayer::sd_setEqualizer(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *value, void *userdata,
                                 sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  const char *valueStr;
  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_STRING, &valueStr);
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  Equalizer mode = from_string<Equalizer>(valueStr);
  if (mode == Equalizer::INVALID) {
    return bt_error_invalid_args(ret_error);
  }
  player->m_device->setEqualizerFromPlayer(mode);
  return 0;
}

int MediaPlayer::sd_getRepeat(sd_bus *UNUSED(bus), const char *UNUSED(path),
                              const char *UNUSED(interface), const char *UNUSED(property),
                              sd_bus_message *reply, void *userdata,
                              sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  int res = sd_bus_message_append_basic(
      reply, SD_BUS_TYPE_STRING, to_string(player->m_device->getRepeat()));
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  return 0;
}

int MediaPlayer::sd_setRepeat(sd_bus *UNUSED(bus), const char *UNUSED(path),
                              const char *UNUSED(interface), const char *UNUSED(property),
                              sd_bus_message *value, void *userdata,
                              sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  const char *valueStr;
  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_STRING, &valueStr);
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  Repeat mode = from_string<Repeat>(valueStr);
  if (mode == Repeat::INVALID) {
    return bt_error_invalid_args(ret_error);
  }
  player->m_device->setRepeatFromPlayer(mode);
  return 0;
}

int MediaPlayer::sd_getShuffle(sd_bus *UNUSED(bus), const char *UNUSED(path),
                               const char *UNUSED(interface), const char *UNUSED(property),
                               sd_bus_message *reply, void *userdata,
                               sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  int res = sd_bus_message_append_basic(
      reply, SD_BUS_TYPE_STRING, to_string(player->m_device->getShuffle()));
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  return 0;
}

int MediaPlayer::sd_setShuffle(sd_bus *UNUSED(bus), const char *UNUSED(path),
                               const char *UNUSED(interface), const char *UNUSED(property),
                               sd_bus_message *value, void *userdata,
                               sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  const char *valueStr;
  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_STRING, &valueStr);
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  Shuffle mode = from_string<Shuffle>(valueStr);
  if (mode == Shuffle::INVALID) {
    return bt_error_invalid_args(ret_error);
  }
  player->m_device->setShuffleFromPlayer(mode);
  return 0;
}

int MediaPlayer::sd_getScan(sd_bus *UNUSED(bus), const char *UNUSED(path),
                            const char *UNUSED(interface), const char *UNUSED(property),
                            sd_bus_message *reply, void *userdata,
                            sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  int res = sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING,
                                        to_string(player->m_device->getScan()));
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  return 0;
}

int MediaPlayer::sd_setScan(sd_bus *UNUSED(bus), const char *UNUSED(path),
                            const char *UNUSED(interface), const char *UNUSED(property),
                            sd_bus_message *value, void *userdata,
                            sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  const char *valueStr;
  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_STRING, &valueStr);
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  Scan mode = from_string<Scan>(valueStr);
  if (mode == Scan::INVALID) {
    return bt_error_invalid_args(ret_error);
  }
  player->m_device->setScanFromPlayer(mode);
  return 0;
}

int MediaPlayer::sd_getStatus(sd_bus *UNUSED(bus), const char *UNUSED(path),
                              const char *UNUSED(interface), const char *UNUSED(property),
                              sd_bus_message *reply, void *userdata,
                              sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  int res = sd_bus_message_append_basic(
      reply, SD_BUS_TYPE_STRING, to_string(player->m_device->getPlayState()));
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  return 0;
}

int MediaPlayer::sd_getPosition(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                const char *UNUSED(interface), const char *UNUSED(property),
                                sd_bus_message *reply, void *userdata,
                                sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  int res = sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_UINT32>::value,
                                  player->m_device->getPlayPosition());
  if (res < 0) {
    return bt_error_failed(ret_error);
  }
  return 0;
}

int MediaPlayer::sd_getTrack(sd_bus *UNUSED(bus), const char *UNUSED(path),
                             const char *UNUSED(interface), const char *UNUSED(property),
                             sd_bus_message *reply, void *userdata,
                             sd_bus_error *ret_error) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  Metadata metadata = player->m_device->getMetadata();

  int res = sd_bus_message_open_container(reply, SD_BUS_TYPE_ARRAY, "{sv}");
  if (res < 0) {
    return bt_error_failed(ret_error);
  }

  auto putString = [&](const char *key, const std::string &value) -> int {
    if (res < 0) {
      return res;
    }
    if (value.empty()) {
      return 0;
    }
    res = sd_bus_message_open_container(reply, SD_BUS_TYPE_DICT_ENTRY, "sv");
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    res = sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING, key);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    res = sd_bus_message_open_container(reply, SD_BUS_TYPE_VARIANT, "s");
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    res = sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING, value.c_str());
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    res = sd_bus_message_close_container(reply);  // Variant
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    res = sd_bus_message_close_container(reply);  // Dictionary
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    return 0;
  };
  auto putUint32 = [&](const char *key, uint32_t value) -> int {
    if (res < 0) {
      return res;
    }
    if (value == UINT32_MAX) {
      return 0;
    }
    res = sd_bus_message_open_container(reply, SD_BUS_TYPE_DICT_ENTRY, "sv");
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    res = sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING, key);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    res = sd_bus_message_open_container(reply, SD_BUS_TYPE_VARIANT,
                                        TYPE_TO_STR<SD_BUS_TYPE_UINT32>::value);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    res = sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_UINT32>::value, value);
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    res = sd_bus_message_close_container(reply);  // Variant
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    res = sd_bus_message_close_container(reply);  // Dictionary
    if (res < 0) {
      return bt_error_failed(ret_error);
    }
    return 0;
  };

  res = putString("Title", metadata.title);
  res = putString("Artist", metadata.artist);
  res = putString("Album", metadata.album);
  res = putString("Genre", metadata.genre);
  res = putUint32("NumberOfTracks", metadata.trackCount);
  res = putUint32("TrackNumber", metadata.trackNum);
  res = putUint32("Duration", metadata.duration);
  if (res < 0) {
    // ret_error already set
    return res;
  }

  res = sd_bus_message_close_container(reply);  // Array
  if (res < 0) {
    return bt_error_failed(ret_error);
  }

  return 0;
}

int MediaPlayer::sd_getDevice(sd_bus *UNUSED(bus), const char *UNUSED(path),
                              const char *UNUSED(interface), const char *UNUSED(property),
                              sd_bus_message *reply, void *userdata,
                              sd_bus_error *UNUSED(ret_error)) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  return sd_bus_message_append_basic(
      reply, SD_BUS_TYPE_OBJECT_PATH,
      player->m_device->getDeviceObjectPath().c_str());
}

int MediaPlayer::sd_getName(sd_bus *UNUSED(bus), const char *UNUSED(path),
                            const char *UNUSED(interface), const char *UNUSED(property),
                            sd_bus_message *reply, void *userdata,
                            sd_bus_error *UNUSED(ret_error)) {
  MediaPlayer *player = static_cast<MediaPlayer *>(userdata);
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING,
                                     player->m_device->getName().c_str());
}
