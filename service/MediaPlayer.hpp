/*
Copyright (c) 2017-2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SERVICE_MEDIAPLAYER_HPP_
#define SERVICE_MEDIAPLAYER_HPP_

#include <systemdq/sd-bus.h>

#include <string>

class Device;
class Adapter;

class MediaPlayer {
 private:
  Device *m_device;
  std::string m_path;

  sd_bus_slot *m_sdbusSlot;

 public:
  MediaPlayer(Device *device);
  ~MediaPlayer();

  bool registerDeviceMediaPlayerObject();

  void playStateChanged();
  void playPositionChanged();
  void metadataChanged();

  void equalizerChanged();
  void repeatChanged();
  void shuffleChanged();
  void scanChanged();

 private:
  static int sd_playerAction(sd_bus_message *m, void *userdata,
                             sd_bus_error *ret_error);

  static int sd_getEqualizer(sd_bus *bus, const char *path,
                             const char *interface, const char *property,
                             sd_bus_message *reply, void *userdata,
                             sd_bus_error *ret_error);
  static int sd_setEqualizer(sd_bus *bus, const char *path,
                             const char *interface, const char *property,
                             sd_bus_message *value, void *userdata,
                             sd_bus_error *ret_error);
  static int sd_getRepeat(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *reply,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_setRepeat(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *value,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_getShuffle(sd_bus *bus, const char *path, const char *interface,
                           const char *property, sd_bus_message *reply,
                           void *userdata, sd_bus_error *ret_error);
  static int sd_setShuffle(sd_bus *bus, const char *path, const char *interface,
                           const char *property, sd_bus_message *value,
                           void *userdata, sd_bus_error *ret_error);
  static int sd_getScan(sd_bus *bus, const char *path, const char *interface,
                        const char *property, sd_bus_message *reply,
                        void *userdata, sd_bus_error *ret_error);
  static int sd_setScan(sd_bus *bus, const char *path, const char *interface,
                        const char *property, sd_bus_message *value,
                        void *userdata, sd_bus_error *ret_error);
  static int sd_getStatus(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *reply,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_getPosition(sd_bus *bus, const char *path,
                            const char *interface, const char *property,
                            sd_bus_message *reply, void *userdata,
                            sd_bus_error *ret_error);
  static int sd_getTrack(sd_bus *bus, const char *path, const char *interface,
                         const char *property, sd_bus_message *reply,
                         void *userdata, sd_bus_error *ret_error);
  static int sd_getDevice(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *reply,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_getName(sd_bus *bus, const char *path, const char *interface,
                        const char *property, sd_bus_message *reply,
                        void *userdata, sd_bus_error *ret_error);
};

#endif /* SERVICE_MEDIAPLAYER_HPP_ */
