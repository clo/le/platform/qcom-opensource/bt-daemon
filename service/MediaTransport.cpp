/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "MediaTransport.hpp"

#include <errno.h>

#include <iomanip>
#include <sstream>

#include "ProfileService.hpp"
#include "A2dpSinkService.hpp"
#include "A2dpSourceService.hpp"

#include "MediaManager.hpp"
#include "Device.hpp"
#include "Error.hpp"
#include "Sdbus.hpp"

#define LOGTAG "MEDIA_TRANSPORT "
#define MEDIA_TRANSPORT_INTERFACE "org.bluez.MediaTransport1"

// Iterative index for each transport fd
int MediaTransport::fdIndex = 0;


MediaTransport::MediaTransport(Device *pDevice,
                               A2dpProfileService *pService,
                               uint16_t codec)
    : m_delay(0),
      m_device(pDevice),
      m_service(pService),
      m_codec(codec),
      m_state(State::kIdle),
      m_sdbusSlot(nullptr),
      m_imtu(0),
      m_omtu(0),
      m_scmst(false),
      m_cpScms(BTAV_CP_SCMS_COPY_NEVER) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Remap codecs where needed.
  // NOTE: When DUT is a2dpsink the btav_a2dp_codec_index_t type
  // is not used and it is a known issue that BTAV_A2DP_CODEC_INDEX_SOURCE_APTX
  // clashes with A2DP_SINK_AUDIO_CODEC_AAC. As an interim solution while Fluoride
  // types are tidied up the check below avoids the clash.
  if (m_service->srvUUID() == A2dpSourceService::LOCAL_UUID) {
    switch (codec) {
      case BTAV_A2DP_CODEC_INDEX_SOURCE_SBC:
        m_codec = A2DP_SINK_AUDIO_CODEC_SBC;
      break;
      case BTAV_A2DP_CODEC_INDEX_SOURCE_APTX:
        m_codec = A2DP_SINK_AUDIO_CODEC_APTX;
      break;
      case BTAV_A2DP_CODEC_INDEX_SOURCE_AAC:
        m_codec = A2DP_SINK_AUDIO_CODEC_AAC;
      break;
    }
  }
}

MediaTransport::~MediaTransport() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  mediaTransportDeInit();
}

void MediaTransport::startMediaTransport() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  doStartMediaTransport();
}

void MediaTransport::stopMediaTransport() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  doStopMediaTransport();
  updateTransportStateOnInterface(State::kIdle);
}

bool MediaTransport::setMTUs(uint16_t imtu, uint16_t omtu) {
  m_imtu = imtu;
  m_omtu = omtu;
  return (doSetMTUs());
}

void MediaTransport::triggerTransportRelease() {
  bt_bdaddr_t bdAddr = m_device->getDeviceAddr();
  ADK_LOG_NOTICE(
      LOGTAG "%s Endpoint is unregistered, initiate service disconnection\n",
      __func__);
  // Disconnect A2DP as the endpoint is unable to consume
  m_service->disconnect(&bdAddr);
}

void MediaTransport::volumeChanged() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIA_TRANSPORT_INTERFACE, "Volume", nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Emit failed (%s): %d - %s\n", m_path.c_str(), -res,
                  strerror(-res));
    return;
  }
}

void MediaTransport::mutedChanged() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIA_TRANSPORT_INTERFACE, "Muted", nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Emit failed (%s): %d - %s\n", m_path.c_str(), -res,
                  strerror(-res));
    return;
  }
}


void MediaTransport::absVolumeSupportChanged() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIA_TRANSPORT_INTERFACE, "AbsVolumeSupport", nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Emit failed (%s): %d - %s\n", m_path.c_str(), -res,
                  strerror(-res));
    return;
  }
}

std::string MediaTransport::getUUID() const {
  return m_service->srvUUID();
}

uint16_t MediaTransport::getVendorCodec() const {
  return ((m_codec == A2DP_SINK_AUDIO_CODEC_APTX_AD || m_codec == A2DP_SINK_AUDIO_CODEC_APTX)
                     ? (m_configuration[4] | (m_configuration[5] << 8)) : 0);
}

void MediaTransport::setScmstCapabilities(bool scmst) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  m_scmst = scmst;
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIA_TRANSPORT_INTERFACE, "Scmst", nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Emit failed (%s): %d - %s\n", m_path.c_str(), -res,
                  strerror(-res));
    return;
  }
}

void MediaTransport::setCpScms(uint8_t cpScms) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  m_cpScms = cpScms;
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIA_TRANSPORT_INTERFACE, "CpScms", nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Emit failed (%s): %d - %s\n", m_path.c_str(), -res,
                  strerror(-res));
    return;
  }
}

int MediaTransport::sd_acquire(sd_bus_message *m, void *userdata,
                               sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  ADK_LOG_DEBUG(LOGTAG " --> state: %s\n", MediaTransport::to_string(pTransport->m_state));
  return pTransport->doAcquire(m, ret_error);
}

int MediaTransport::sd_try_acquire(sd_bus_message *m, void *userdata,
                                   sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);

  // Check if the transport has been started for streaming, raise an error accordingly
  if (pTransport->m_state != State::kPending) {
    return bt_error_not_available(ret_error);
  }
  return sd_acquire(m, userdata, ret_error);
}

int MediaTransport::sd_release(sd_bus_message *m, void *userdata,
                               sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  return pTransport->doRelease(m, ret_error);
}

int MediaTransport::sd_set_delay(sd_bus *bus, const char *path, const char *interface,
                                 const char *property, sd_bus_message *value,
                                 void *userdata, sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  return pTransport->doSetDelay(bus, path, interface, property, value, ret_error);
}

int MediaTransport::sd_get_device(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_OBJECT_PATH,
      pTransport->m_device->getDeviceObjectPath().c_str());
}

int MediaTransport::sd_get_uuid(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING,
                                     pTransport->getUUID().c_str());
}

int MediaTransport::sd_get_codec(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  uint8_t codec = pTransport->m_codec;
  if (codec == A2DP_SINK_AUDIO_CODEC_APTX || codec == A2DP_SINK_AUDIO_CODEC_APTX_AD) {
    // For NON_A2DP_CODEC, i.e. not standard A2dp codecs) the actual vendor codec id
    // will be extracted from the capabilities blob
    codec = NON_A2DP_CODEC;
  }
  ADK_LOG_NOTICE(LOGTAG "%s codec: %d \n", __func__, codec);
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_BYTE, &codec);
}

int MediaTransport::sd_get_configuration(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);

  std::ostringstream log;
  for (size_t i = 0;
       i < std::min(((size_t)16), pTransport->m_configuration.size()); i++) {
    log << " " << std::hex << std::setw(2) << std::setfill('0')
        << (uint32_t)(pTransport->m_configuration[i]);
  }
  ADK_LOG_DEBUG(LOGTAG "GetConfiguration: [%u] - %s\n",
                pTransport->m_configuration.size(), log.str().c_str());
  return sd_bus_message_append_array(
      reply, SD_BUS_TYPE_BYTE, pTransport->m_configuration.data(),
      pTransport->m_configuration.size() *
          sizeof(decltype(pTransport->m_configuration)::value_type));
}

int MediaTransport::sd_get_state(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  const char *state = MediaTransport::to_string(pTransport->m_state);
  return sd_bus_message_append_basic(reply, SD_BUS_TYPE_STRING, state);
}

int MediaTransport::sd_get_AbsVolumeSupport(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value,
                               pTransport->m_device->getIsAbsVolumeSupported());
}

int MediaTransport::sd_get_delay(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_UINT16>::value,
                               pTransport->m_delay);
}

int MediaTransport::sd_get_volume(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                  const char *UNUSED(interface), const char *UNUSED(property),
                                  sd_bus_message *reply, void *userdata,
                                  sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_UINT16>::value,
                               pTransport->m_device->getVolume());
}

int MediaTransport::sd_set_volume(sd_bus *bus, const char *path,
                                  const char *interface, const char *property,
                                  sd_bus_message *value, void *userdata,
                                  sd_bus_error *ret_error) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  uint16_t volume;

  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_UINT16, &volume);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }

  if (volume > 127) {
    return bt_error_invalid_args(ret_error);
  }
  pTransport->m_device->setVolumeFromPlayer(volume);

  // Do Nothing with the volume TBD
  res = sd_bus_emit_properties_changed(bus, path, interface, property, nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Emit failed (%s - %s - %s): %d - %s\n", path,
                  interface, property, -res, strerror(-res));
  }
  return 0;
}


int MediaTransport::sd_get_muted(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                  const char *UNUSED(interface), const char *UNUSED(property),
                                  sd_bus_message *reply, void *userdata,
                                  sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value,
                               pTransport->m_device->getMuted());
}

int MediaTransport::sd_set_muted(sd_bus *bus, const char *path,
                                  const char *interface, const char *property,
                                  sd_bus_message *value, void *userdata,
                                  sd_bus_error *ret_error) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  int muted;
  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_BOOLEAN, &muted);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }
  pTransport->m_device->setMutedFromPlayer((bool)muted);

  res = sd_bus_emit_properties_changed(bus, path, interface, property, nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Emit failed (%s - %s - %s): %d - %s\n", path,
                  interface, property, -res, strerror(-res));
  }
  return 0;
}

int MediaTransport::sd_get_scmst(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BOOLEAN>::value,
                               pTransport->m_scmst);
}

int MediaTransport::sd_get_cpscms(sd_bus *UNUSED(bus), const char *UNUSED(path),
                                 const char *UNUSED(interface), const char *UNUSED(property),
                                 sd_bus_message *reply, void *userdata,
                                 sd_bus_error *UNUSED(ret_error)) {
  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  return sd_bus_message_append(reply, TYPE_TO_STR<SD_BUS_TYPE_BYTE>::value,
                               pTransport->m_cpScms);
}

int MediaTransport::sd_set_cpscms(sd_bus *bus, const char *path,
                                  const char *interface, const char *property,
                                  sd_bus_message *value, void *userdata,
                                  sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  MediaTransport *pTransport = static_cast<MediaTransport *>(userdata);
  uint8_t cpScms;
  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_BYTE, &cpScms);
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }
  auto devAddr = pTransport->m_device->getDeviceAddr();
  if (pTransport->m_service->setCpScms(&devAddr, cpScms) == BT_STATUS_SUCCESS) {
    pTransport->m_cpScms = cpScms;
    res = sd_bus_emit_properties_changed(bus, path, interface, property, nullptr);
    if (res < 0) {
      ADK_LOG_ERROR(LOGTAG " Emit failed (%s - %s - %s): %d - %s\n", path,
                    interface, property, -res, strerror(-res));
    }
    return 0;
  }
  else {
    return bt_error_failed(ret_error);
  }
}

bool MediaTransport::mediaTransportInit(const btav_codec_config_t &codec_config) {
  ADK_LOG_NOTICE(LOGTAG "%s Codec: %s\n", __func__,
  MediaManager::getCodecInfoString(getCodec(), codec_config).c_str());
  updateConfigurationData(codec_config);
  m_device->setTransport(shared_from_this());

  std::string devPath = m_device->getDeviceObjectPath();
  m_path = devPath + "/fd" + std::to_string(fdIndex++);

  m_state = State::kIdle;

  static const sd_bus_vtable vtable[] = {
      SD_BUS_VTABLE_START(0),

      SD_BUS_METHOD("Acquire", nullptr, "hqq", MediaTransport::sd_acquire,
                    SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("TryAcquire", nullptr, "hqq",
                    MediaTransport::sd_try_acquire, SD_BUS_VTABLE_UNPRIVILEGED),
      SD_BUS_METHOD("Release", nullptr, nullptr, MediaTransport::sd_release,
                    SD_BUS_VTABLE_UNPRIVILEGED),

      SD_BUS_PROPERTY("Device", "o", MediaTransport::sd_get_device, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("UUID", "s", MediaTransport::sd_get_uuid, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Codec", "y", MediaTransport::sd_get_codec, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Configuration", "ay",
                      MediaTransport::sd_get_configuration, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("State", "s", MediaTransport::sd_get_state, 0,
                      SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("AbsVolumeSupport", "b",MediaTransport::sd_get_AbsVolumeSupport,
                      0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("Delay", "q", MediaTransport::sd_get_delay,
                               MediaTransport::sd_set_delay, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("Volume", "q", MediaTransport::sd_get_volume,
                               MediaTransport::sd_set_volume, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("Muted", "b", MediaTransport::sd_get_muted,
                               MediaTransport::sd_set_muted, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_PROPERTY("Scmst", "b", MediaTransport::sd_get_scmst, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_WRITABLE_PROPERTY("CpScms", "y", MediaTransport::sd_get_cpscms,
                               MediaTransport::sd_set_cpscms, 0,
                               SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
      SD_BUS_VTABLE_END};

  int res = sd_bus_add_object_vtable(g_sdbus, &m_sdbusSlot, m_path.c_str(),
                                 MEDIA_TRANSPORT_INTERFACE, vtable,
                                 this);
  if (res < 0) {
    ADK_LOG_NOTICE(LOGTAG "interface init failed on path %s: %d - %s\n",
                   m_path.c_str(), -res, strerror(-res));
    goto fail;
  }

  res = sd_bus_emit_object_added(g_sdbus, m_path.c_str());

  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG
                  "object manager failed to signal new path %s: %d - %s\n",
                  m_path.c_str(), -res, strerror(-res));
    goto fail;
  }
  ADK_LOG_DEBUG(LOGTAG "New transport for codec %d in path %s\n", m_codec,
                m_path.c_str());
  return true;
fail:
  mediaTransportDeInit();
  return false;
}

void MediaTransport::mediaTransportDeInit() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  sd_bus_emit_object_removed(g_sdbus, m_path.c_str());
  ADK_LOG_DEBUG(LOGTAG "transport object removed");

  sd_bus_slot_unref(m_sdbusSlot);
  m_sdbusSlot = nullptr;

  m_path.clear();
}

void MediaTransport::updateTransportStateOnInterface(State state) {
  ADK_LOG_NOTICE(LOGTAG "%s: state %d\n", __func__, state);
  m_state = state;
  int res = sd_bus_emit_properties_changed(
      g_sdbus, m_path.c_str(), MEDIA_TRANSPORT_INTERFACE, "State", nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Emit failed (%s): %d - %s\n", m_path.c_str(), -res,
                  strerror(-res));
    return;
  }
}

void MediaTransport::updateConfigurationData(const btav_codec_config_t &codec_config) {
  switch (m_codec) {
    case A2DP_SINK_AUDIO_CODEC_PCM:
      // This is likely SBC with the A2DP_SINK_ENABLE_SBC_DECODING flag set in
      // Fluoride to use its own internal decoder (which then outputs PCM)
    case A2DP_SINK_AUDIO_CODEC_SBC:
      // Append 4 bytes of configuration details
      m_configuration.resize(4);
      // The following assume that the value can nicely be packed,
      // e.g samp_freq in bits [4-7], ch_mod in bits [0-3]
      m_configuration[0] =
          codec_config.sbc_config.samp_freq | codec_config.sbc_config.ch_mode;
      m_configuration[1] = codec_config.sbc_config.block_len |
                           codec_config.sbc_config.num_subbands |
                           codec_config.sbc_config.alloc_mthd;
      m_configuration[2] = codec_config.sbc_config.min_bitpool;
      m_configuration[3] = codec_config.sbc_config.max_bitpool;
      ADK_LOG_NOTICE(LOGTAG " %s: [%02x %02x %02x %02x]\n", __func__,
                     m_configuration[0], m_configuration[1], m_configuration[2],
                     m_configuration[3]);
      break;

    case A2DP_SINK_AUDIO_CODEC_MP3:
      // Append 4 bytes of configuration details
      m_configuration.resize(4);
      m_configuration[0] = codec_config.mp3_config.layer |
                           codec_config.mp3_config.crc |
                           codec_config.mp3_config.channel_count;
      m_configuration[1] =
          codec_config.mp3_config.mpf | codec_config.mp3_config.sampling_freq;
      m_configuration[2] = codec_config.mp3_config.vbr |
                           ((codec_config.mp3_config.bit_rate >> 8) & 0x7f);
      m_configuration[3] = ((codec_config.mp3_config.bit_rate) & 0xff);
      ADK_LOG_NOTICE(LOGTAG " %s: [%02x %02x %02x %02x]\n", __func__,
                     m_configuration[0], m_configuration[1], m_configuration[2],
                     m_configuration[3]);
      break;

    case A2DP_SINK_AUDIO_CODEC_AAC:
      // Append 6 bytes of configuration details
      m_configuration.resize(6);
      m_configuration[0] = codec_config.aac_config.obj_type;
      m_configuration[1] =
          ((codec_config.aac_config.sampling_freq >> 8) & 0xff);
      m_configuration[2] = ((codec_config.aac_config.sampling_freq) & 0xf0) |
                           codec_config.aac_config.channel_count;
      m_configuration[3] = codec_config.aac_config.vbr |
                           ((codec_config.aac_config.bit_rate >> 16) & 0x7f);
      m_configuration[4] = ((codec_config.aac_config.bit_rate >> 8) & 0xff);
      m_configuration[5] = ((codec_config.aac_config.bit_rate) & 0xff);
      ADK_LOG_NOTICE(LOGTAG " %s: [%02x %02x %02x %02x %02x %02x]\n", __func__,
                     m_configuration[0], m_configuration[1], m_configuration[2],
                     m_configuration[3], m_configuration[4],
                     m_configuration[5]);
      break;

    case A2DP_SINK_AUDIO_CODEC_APTX:
      // Append 7 bytes of configuration details
      m_configuration.resize(17);
      m_configuration[0] = (codec_config.aptx_config.vendor_id) & 0xff;
      m_configuration[1] = (codec_config.aptx_config.vendor_id >> 8) & 0xff;
      m_configuration[2] = (codec_config.aptx_config.vendor_id >> 16) & 0xff;
      m_configuration[3] = (codec_config.aptx_config.vendor_id >> 24) & 0xff;
      m_configuration[4] = (codec_config.aptx_config.codec_id) & 0xff;
      m_configuration[5] = (codec_config.aptx_config.codec_id >> 8) & 0xff;
      m_configuration[6] = codec_config.aptx_config.sampling_freq |
                           codec_config.aptx_config.channel_count;
      ADK_LOG_NOTICE(LOGTAG " %s: [%02x %02x %02x %02x %02x %02x %02x]\n",
                     __func__, m_configuration[0], m_configuration[1],
                     m_configuration[2], m_configuration[3], m_configuration[4],
                     m_configuration[5], m_configuration[6]);
      // Currently, Fluoride is only sending codec_id, vendor_id, sample_rate and
      // channel_mode.
      // Initialize the rest of the capability blob (upto a total of 17 octets)
      for (int i = 7; i < 17 ; ++i)
        m_configuration[i] = 0;
      break;

    case A2DP_SINK_AUDIO_CODEC_APTX_AD: // Fluoride Aptx_AD definition
      // Append 7 bytes of configuration details
      m_configuration.resize(40);
      m_configuration[0] = (codec_config.aptx_ad_config.vendor_id) & 0xff;
      m_configuration[1] = (codec_config.aptx_ad_config.vendor_id >> 8) & 0xff;
      m_configuration[2] = (codec_config.aptx_ad_config.vendor_id >> 16) & 0xff;
      m_configuration[3] = (codec_config.aptx_ad_config.vendor_id >> 24) & 0xff;
      m_configuration[4] = (codec_config.aptx_ad_config.codec_id) & 0xff; // Vendor codec id
      m_configuration[5] = (codec_config.aptx_ad_config.codec_id >> 8) & 0xff;
      m_configuration[6] = codec_config.aptx_ad_config.sampling_freq;
      m_configuration[7] = codec_config.aptx_ad_config.channel_count;
      ADK_LOG_NOTICE(LOGTAG " %s: [%02x %02x %02x %02x %02x %02x %02x %02x]\n",
                     __func__, m_configuration[0], m_configuration[1],
                     m_configuration[2], m_configuration[3], m_configuration[4],
                     m_configuration[5], m_configuration[6], m_configuration[7]);
      // Currently, Fluoride is only sending codec_id, vendor_id, sample_rate and
      // channel_mode.
      // Initialize the rest of the capability blob (upto a total of 40 octets)
      for (int i = 8; i < 40 ; ++i)
        m_configuration[i] = 0;
      break;

    default:
      ADK_LOG_NOTICE(
          LOGTAG
          " %s: Unsupported codec, dont allow any configuration to be set\n",
          __func__);
      break;
  }
}

int MediaTransport::doSetDelay(sd_bus *UNUSED(bus), const char *UNUSED(path), const char *UNUSED(interface),
                               const char *UNUSED(property), sd_bus_message *UNUSED(value),
                               sd_bus_error *ret_error) {
  return bt_error_not_supported(ret_error);
}

const char *MediaTransport::to_string(State state) {
  switch (state) {
    case State::kIdle:
      return "idle";
    case State::kPending:
      return "pending";
    case State::kActive:
      return "active";
  }
  return nullptr;
}

bool MediaTransport::convertCodecConfigTypes(
                              const btav_a2dp_codec_config_t &a2dp_codec_config,
                              btav_codec_config_t *codec_config) {
  switch (a2dp_codec_config.codec_type) {
    case BTAV_A2DP_CODEC_INDEX_SOURCE_SBC:
      switch (a2dp_codec_config.sample_rate) {
        case BTAV_A2DP_CODEC_SAMPLE_RATE_44100:
          codec_config->sbc_config.samp_freq = SBC_SAMP_FREQ_44;
          break;
        case BTAV_A2DP_CODEC_SAMPLE_RATE_48000:
          codec_config->sbc_config.samp_freq = SBC_SAMP_FREQ_48;
          break;
        case BTAV_A2DP_CODEC_SAMPLE_RATE_16000:
          codec_config->sbc_config.samp_freq = SBC_SAMP_FREQ_16;
          break;
        default:
          codec_config->sbc_config.samp_freq = SBC_SAMP_FREQ_NONE;
          break;
      }
      switch (a2dp_codec_config.channel_mode) {
        case BTAV_A2DP_CODEC_CHANNEL_MODE_MONO:
          codec_config->sbc_config.ch_mode = SBC_CH_MONO;
          break;
        case BTAV_A2DP_CODEC_CHANNEL_MODE_STEREO:
          codec_config->sbc_config.ch_mode = SBC_CH_JOINT;
          break;
        default:
          codec_config->sbc_config.ch_mode = SBC_CH_NONE;
          break;
      }
      codec_config->sbc_config.block_len =
          a2dp_codec_config.codec_specific_1 & 0xff;
      codec_config->sbc_config.num_subbands =
          a2dp_codec_config.codec_specific_2 & 0xff;
      codec_config->sbc_config.alloc_mthd =
          a2dp_codec_config.codec_specific_3 & 0xff;
      codec_config->sbc_config.max_bitpool =
          a2dp_codec_config.codec_specific_4 & 0xff;
      codec_config->sbc_config.min_bitpool =
          a2dp_codec_config.codec_specific_5 & 0xff;
      break;

    case BTAV_A2DP_CODEC_INDEX_SOURCE_APTX:
      codec_config->aptx_config.vendor_id = 0x0000004f;
      codec_config->aptx_config.codec_id = 0x0001;
      switch (a2dp_codec_config.channel_mode) {
        case BTAV_A2DP_CODEC_CHANNEL_MODE_MONO:
          codec_config->aptx_config.channel_count = APTX_CHANNELS_MONO;
          break;
        case BTAV_A2DP_CODEC_CHANNEL_MODE_STEREO:
          codec_config->aptx_config.channel_count = APTX_CHANNELS_STEREO;
          break;
        default:
          codec_config->aptx_config.channel_count = 0;
          break;
      }
      switch (a2dp_codec_config.sample_rate) {
        case BTAV_A2DP_CODEC_SAMPLE_RATE_44100:
          codec_config->aptx_config.sampling_freq = APTX_SAMPLERATE_44100;
          break;
        case BTAV_A2DP_CODEC_SAMPLE_RATE_48000:
          codec_config->aptx_config.sampling_freq = APTX_SAMPLERATE_48000;
          break;
        default:
          codec_config->aptx_config.sampling_freq = 0;
          break;
      }
      break;

    default:
      ADK_LOG_NOTICE(LOGTAG " %s: Unsupported codec\n", __func__);
      return false;
  }
  return true;
}
