/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef MEDIATRANSPORT_HPP
#define MEDIATRANSPORT_HPP


#include <hardware/bt_av.h>
#include <hardware/bt_av_vendor.h>
#include <systemdq/sd-bus.h>
#include <systemdq/sd-event.h>
#include <string>
#include <vector>
#include <memory>

class Device;
class A2dpProfileService;

/**
 * @file MediaTransport.hpp
 * @brief MediaTransport header file
 */
class MediaTransport : public std::enable_shared_from_this<MediaTransport>{
 public:
  enum class State { kIdle, kPending, kActive };

  virtual ~MediaTransport();

  // Generic Public APIs for A2DP services to handle transport
  virtual bool mediaTransportInit(const btav_codec_config_t &codec_config);
  void startMediaTransport();
  void stopMediaTransport();
  bool setMTUs(uint16_t imtu, uint16_t omtu);
  void triggerTransportRelease();

  // Emit remote device Volume changes in DBUS transport interface
  void volumeChanged();
  // Emit remote device Volume changes in DBUS transport interface
  void mutedChanged();
  // Emit Absvolumesupport property changes in DBUS transport interface
  void absVolumeSupportChanged();

  // Accessors:
  // DBus object transport path getter
  std::string getTransportPath() const { return m_path; }
  // Transport state getter to query whether transport is acquired
  State getTransportState() const { return m_state; }
  // UUID getter retuning profile for which this transport has been setup
  std::string getUUID() const;
  // Codec getter for this transport
  uint16_t getCodec() const { return m_codec; }
  // Vendor codec getter for this transport
  uint16_t getVendorCodec() const;
  // Getter for Service associated to this transport
  A2dpProfileService * getService() const { return m_service; }
  // Setter/Getter for 1/10ms delay associated to this transport
  uint16_t delay() const { return m_delay; }
  uint16_t& delay() { return m_delay; }
  void setScmstCapabilities(bool scmst);
  void setCpScms(uint8_t cpScms);
  virtual void setConfigurationCb() {};

  // SD-bus static entries:
  // RPC methods
  static int sd_acquire(sd_bus_message *m, void *userdata,
                        sd_bus_error *ret_error);
  static int sd_try_acquire(sd_bus_message *m, void *userdata,
                            sd_bus_error *ret_error);
  static int sd_release(sd_bus_message *m, void *userdata,
                        sd_bus_error *ret_error);
  // RPC Property getters
  static int sd_get_device(sd_bus *bus, const char *path, const char *interface,
                           const char *property, sd_bus_message *reply,
                           void *userdata, sd_bus_error *ret_error);
  static int sd_get_uuid(sd_bus *bus, const char *path, const char *interface,
                         const char *property, sd_bus_message *reply,
                         void *userdata, sd_bus_error *ret_error);
  static int sd_get_codec(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *reply,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_get_configuration(sd_bus *bus, const char *path,
                                  const char *interface, const char *property,
                                  sd_bus_message *reply, void *userdata,
                                  sd_bus_error *ret_error);
  static int sd_get_state(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *reply,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_get_AbsVolumeSupport(sd_bus *bus, const char *path,
                                     const char *interface, const char *property,
                                     sd_bus_message *reply, void *userdata,
                                     sd_bus_error *ret_error);
  static int sd_get_delay(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *reply,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_set_delay(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *value,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_get_volume(sd_bus *bus, const char *path, const char *interface,
                           const char *property, sd_bus_message *reply,
                           void *userdata, sd_bus_error *ret_error);
  static int sd_get_muted(sd_bus *bus, const char *path, const char *interface,
                           const char *property, sd_bus_message *reply,
                           void *userdata, sd_bus_error *ret_error);
  static int sd_get_scmst(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *reply,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_get_cpscms(sd_bus *bus, const char *path, const char *interface,
                           const char *property, sd_bus_message *reply,
                           void *userdata, sd_bus_error *ret_error);

  // RPC Property setters
  static int sd_set_volume(sd_bus *bus, const char *path, const char *interface,
                           const char *property, sd_bus_message *value,
                           void *userdata, sd_bus_error *ret_error);
  static int sd_set_muted(sd_bus *bus, const char *path, const char *interface,
                          const char *property, sd_bus_message *value,
                          void *userdata, sd_bus_error *ret_error);
  static int sd_set_cpscms(sd_bus *bus, const char *path, const char *interface,
                           const char *property, sd_bus_message *reply,
                           void *userdata, sd_bus_error *ret_error);

  // Utilities
  static bool convertCodecConfigTypes(const btav_a2dp_codec_config_t &a2dp_codecConfig,
                                      btav_codec_config_t *codecConfig);

 protected:
  MediaTransport(Device *pDevice, A2dpProfileService *pService, uint16_t codec);
  virtual void mediaTransportDeInit();
  void updateTransportStateOnInterface(State state);

  // Accessors
  Device * getDevice() const { return m_device; }
  uint16_t iMtu() const { return m_imtu; }
  uint16_t oMtu() const { return m_omtu; }

private:
  virtual void doStartMediaTransport() {}
  virtual void doStopMediaTransport() {}
  virtual bool doSetMTUs() { return true; }

  virtual int doAcquire(sd_bus_message *m, sd_bus_error *ret_error) = 0;
  virtual int doRelease(sd_bus_message *m, sd_bus_error *ret_error) = 0;
  virtual int doSetDelay(sd_bus *bus, const char *path, const char *interface,
                         const char *property, sd_bus_message *value,
                         sd_bus_error *ret_error);

  void updateConfigurationData(const btav_codec_config_t &codec_config);

  static const char *to_string(State state);

  // Iterative index for each transport fd
  static int fdIndex;

  // DBUS private data
  // Transport DBUS object path name
  std::string m_path;

  // Transport delay in 1/10ms units to match the bluez media-api
  uint16_t m_delay;

  // Remote Device using the transport
  Device *m_device;

  // Service using the transport
  A2dpProfileService *m_service;

  // Fluoride codec_type id for the transport
  uint16_t m_codec;

  // Transport internal state
  State m_state;

  // Transport Service SD BUS slot
  sd_bus_slot *m_sdbusSlot;

  // Transport codec configuration
  std::vector<uint8_t> m_configuration;

  // Transport input and output mtus
  uint16_t m_imtu;
  uint16_t m_omtu;

  // Scms-t flags for content protection management
  bool m_scmst;
  uint8_t m_cpScms;
};
#endif  // MEDIATRANSPORT_HPP
