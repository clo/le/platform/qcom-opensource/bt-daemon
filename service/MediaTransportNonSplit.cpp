/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "MediaTransportNonSplit.hpp"

#include <sys/socket.h>

#include "Device.hpp"
#include "MediaManager.hpp"
#include "Error.hpp"
#include "Sdbus.hpp"


#define LOGTAG "MEDIA_TRANSPORT_NONSPLIT "

#define DATA_TIMER_PRECISION (2 * 1000ull)  // 2ms

MediaTransportNonSplit::MediaTransportNonSplit(Device *pDevice,
                                               A2dpProfileService *pService,
                                               uint16_t codec,
                                               IA2dpDataPathProcessing *pPath)
    : MediaTransport(pDevice, pService, codec),
      dataPathService_m(pPath),
      m_pollFds{-1, -1},
      m_transportFd{-1, -1},
      m_dataTimer(nullptr),
      m_timerDuration(0) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  pthread_mutex_init(&processDataMutex(), nullptr);
}

MediaTransportNonSplit::~MediaTransportNonSplit() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  stopMediaTransport();
  pthread_mutex_destroy(&processDataMutex());

  if (m_dataTimer) {
    sd_event_source_unref(m_dataTimer);
    m_dataTimer = nullptr;
  }
}

int MediaTransportNonSplit::sd_dataTimerExpiry(sd_event_source *s,
                                               uint64_t usec,
                                               void *userdata) {
  MediaTransportNonSplit *pTransport = static_cast<MediaTransportNonSplit *>(userdata);
  pTransport->processData();
  sd_event_source_set_time(s, usec + pTransport->timerDuration());
  return 0;
}

bool MediaTransportNonSplit::mediaTransportInit(const btav_codec_config_t &codec_config) {
  ADK_LOG_NOTICE(LOGTAG " %s Codec: %s\n", __func__,
      MediaManager::getCodecInfoString(getCodec(), codec_config).c_str());

  // Create socket pair
  if (socketpair(AF_UNIX, SOCK_SEQPACKET | SOCK_CLOEXEC, 0, m_transportFd) == -1) {
    ADK_LOG_ERROR(LOGTAG " < --%s socket pair creation failed: %d - %s\n",
                  __func__, errno, strerror(errno));
    goto fail;
  }

  return MediaTransport::mediaTransportInit(codec_config);

  fail:
  mediaTransportDeInit();
  return false;
}

void MediaTransportNonSplit::mediaTransportDeInit() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  MediaTransport::mediaTransportDeInit();

  if (m_transportFd[kServiceFd] != -1) {
    close(m_transportFd[kServiceFd]);
    m_transportFd[kServiceFd] = -1;
  }
  if (m_transportFd[kApplicationFd] != -1) {
    close(m_transportFd[kApplicationFd]);
    m_transportFd[kApplicationFd] = -1;
  }
}

void MediaTransportNonSplit::doStartMediaTransport(short event) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Prepare the pollfd events to be monitored
  m_pollFds[kSocketServiceFd].fd = m_transportFd[kServiceFd];
  m_pollFds[kSocketServiceFd].events = event;
}

int MediaTransportNonSplit::doAcquire(sd_bus_message *m,
                                      sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);

  // Check if the transport is already acquired, raise an error accordingly
  if (getTransportState() == State::kActive) {
    return bt_error_not_available(ret_error);
  }

  ADK_LOG_NOTICE(LOGTAG " --%s - m_imtu: %d, m_omtu: %d\n", __func__,
                 iMtu(), oMtu());
  int res = sd_bus_reply_method_return(
      m,
      TYPE_TO_STR<SD_BUS_TYPE_UNIX_FD, SD_BUS_TYPE_UINT16, SD_BUS_TYPE_UINT16>::value,
      m_transportFd[kApplicationFd], iMtu(), oMtu());
  if (!res) {
    ADK_LOG_ERROR(LOGTAG " Reply failed: %d - %s\n", -res, strerror(-res));
    return -1;
  }

  updateTransportStateOnInterface(State::kActive);

  return res;
}

int MediaTransportNonSplit::doRelease(sd_bus_message *m,
                                      sd_bus_error *UNUSED(ret_error)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  updateTransportStateOnInterface(State::kIdle);
  return sd_bus_reply_method_return(m, nullptr);
}

// Starts the data fetch timer
void MediaTransportNonSplit::startDataTimer() {
  ADK_LOG_NOTICE(LOGTAG "%s %d\n", __func__, timerDuration());
  if (!m_dataTimer) {
    int res = sd_event_add_time(g_eventLoop, &m_dataTimer, CLOCK_MONOTONIC,
                                get_timer_delay(timerDuration()),
                                DATA_TIMER_PRECISION,
                                MediaTransportNonSplit::sd_dataTimerExpiry, this);
    if (res < 0) {
      ADK_LOG_WARNING(LOGTAG "Failed to start timer: %d - %s\n", -res,
                      strerror(-res));
      return;
    }
    res = sd_event_source_set_enabled(m_dataTimer, SD_EVENT_ON);
    if (res < 0) {
      ADK_LOG_WARNING(LOGTAG "Failed to make timer periodic: %d - %s\n", -res,
                      strerror(-res));
      return;
    }
  }
}

void MediaTransportNonSplit::stopDataTimer() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  if (m_dataTimer) {
    sd_event_source_unref(m_dataTimer);
    m_dataTimer = nullptr;
  } else {
    ADK_LOG_ERROR(LOGTAG " %s Data timer is absent\n", __func__);
  }
}
