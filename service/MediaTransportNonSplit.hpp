/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef MEDIATRANSPORTNONSPLIT_HPP
#define MEDIATRANSPORTNONSPLIT_HPP

#include <sys/poll.h>

#include "MediaTransport.hpp"

class IA2dpDataPathProcessing;

/**
 * @file  MediaTransportNonSplit.hpp
 * @brief Definition for the transport specialized for non split mode.
 */
class MediaTransportNonSplit : public MediaTransport{
 public:
  enum SocketDescriptors { kServiceFd, kApplicationFd, kSocketPairOfFds };
  enum TransportDescriptors { kSocketServiceFd, kMaxFds };

  typedef struct {
    uint8_t *buffer;
    uint32_t size;
    uint16_t codec;
  } TransportBuffer;

  ~MediaTransportNonSplit() override;

  static int sd_dataTimerExpiry(sd_event_source *s, uint64_t usec, void *userdata);

 protected:
  MediaTransportNonSplit(Device *pDevice,
                         A2dpProfileService *pService,
                         uint16_t codec,
                         IA2dpDataPathProcessing *pPath);

  bool mediaTransportInit(const btav_codec_config_t &codec_config) override;
  void mediaTransportDeInit() final;

  // Accessor
  uint64_t timerDuration() const {return m_timerDuration;}
  uint64_t& timerDuration() {return m_timerDuration;}
  pthread_mutex_t& processDataMutex() {return m_processDataMutex;}
  std::vector<uint8_t>& dataBuffer() {return m_buffer;}

  void doStartMediaTransport(short event);

  // From MediaTransport
  int doAcquire(sd_bus_message *m, sd_bus_error *ret_error) override;
  int doRelease(sd_bus_message *m, sd_bus_error *ret_error) override;

  void startDataTimer();
  void stopDataTimer();

  // Interface to A2dp datapath
  IA2dpDataPathProcessing *dataPathService_m;

  // Fds polled by the specialized implementations of this transport
  struct pollfd m_pollFds[kMaxFds];

private:
  virtual void processData() = 0;

    // Fd to poll if this transport is stopped.
  int m_stopFd;

  // Pair of socket descriptors for this transport
  int m_transportFd[kSocketPairOfFds];

  pthread_mutex_t m_processDataMutex;
  std::vector<uint8_t> m_buffer;
  sd_event_source *m_dataTimer;
  uint64_t m_timerDuration;
};
#endif  // MEDIATRANSPORTNONSPLIT_HPP
