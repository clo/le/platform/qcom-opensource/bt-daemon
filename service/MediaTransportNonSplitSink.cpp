/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "MediaTransportNonSplitSink.hpp"

#include "Device.hpp"
#include "A2dpSinkServiceNonSplit.hpp"
#include "Error.hpp"

#define LOGTAG "MEDIA_TRANSPORT_NONSPLIT_SINK "

// Timer to fetch data from Fluoride stack in millisecs
#define DATA_SINK_TIMER_DURATION (30 * 1000ull)  // 30ms

// Input and Output MTUs
#define A2DP_STREAM_IMTU (1024)
#define A2DP_STREAM_OMTU (100)


MediaTransportNonSplitSink::MediaTransportNonSplitSink(Device *pDevice,
                                                       A2dpProfileService *pService,
                                                       uint16_t codec,
                                                       IA2dpDataPathProcessing *pPath)
    : MediaTransportNonSplit(pDevice, pService, codec, pPath){
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

bool MediaTransportNonSplitSink::mediaTransportInit(
                                   const btav_codec_config_t &codec_config) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Prepare transport buffers and timers to be used
  if (setMTUs(A2DP_STREAM_IMTU, A2DP_STREAM_OMTU) == false){
    return false;
  }

  timerDuration() = DATA_SINK_TIMER_DURATION;
  return MediaTransportNonSplit::mediaTransportInit(codec_config);
}

void MediaTransportNonSplitSink::audioDataReady() {
  // Using Fluoride Callback mechanism to fetch data from Fluoride
  // This is called at least in two scenarios.
  // When Transport is acquired and when callback to fetch data is received.
  if (getTransportState() == State::kActive) {
    processData();
  }
}

void MediaTransportNonSplitSink::doStartMediaTransport() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  updateTransportStateOnInterface(State::kPending);
  MediaTransportNonSplit::doStartMediaTransport(POLLOUT);
}

int MediaTransportNonSplitSink::doAcquire(sd_bus_message *m, sd_bus_error *ret_error) {
  int res = MediaTransportNonSplit::doAcquire(m, ret_error);
  if (res >= 0){
    // A2dpSink non split relies on dataReady callback mechanism from Fluoride
    // which may already have been issued. Check if there is any data ready...
    audioDataReady();
  }
  return res;
}

bool MediaTransportNonSplitSink::doSetMTUs() {
  dataBuffer().resize(calculateBufferSize());
  if (dataBuffer().size() == 0) {
    return false;
  }
  return true;
}

int MediaTransportNonSplitSink::doSetDelay(sd_bus *bus, const char *path,
                                           const char *interface, const char *property,
                                           sd_bus_message *value, sd_bus_error *ret_error) {
  if (getTransportState() != State::kActive) {
    return bt_error_not_available(ret_error);
  }

  // TODO: check that the sender is the current transport's acquirer
  int res = sd_bus_message_read_basic(value, SD_BUS_TYPE_UINT16, &delay());
  if (res < 0) {
    return bt_error_invalid_args(ret_error);
  }

  A2dpSinkServiceNonSplit *sink =
        dynamic_cast<A2dpSinkServiceNonSplit *>(getService());
  if (sink) {
    sink->updateDelay(delay());
  }
  res =
      sd_bus_emit_properties_changed(bus, path, interface, property, nullptr);
  if (res < 0) {
    ADK_LOG_ERROR(LOGTAG " Emit failed (%s): %d - %s\n", path, -res,
                  strerror(-res));
  }
  return 0;
}

void MediaTransportNonSplitSink::processData() {
  if (pthread_mutex_trylock(&processDataMutex())) {
    ADK_LOG_ERROR(LOGTAG "  -->%s Waiting for processData to complete\n", __func__);
    return;
  }
  // Controls the piggybacked read/write
  uint32_t nBytesToWrite = 0;
  TransportBuffer transportBuffer;

  for (;;) {
    int rv = poll(m_pollFds, kMaxFds, 10);
    if (rv < 0) {
      ADK_LOG_NOTICE(LOGTAG "  %s: Error Occured in Poll: %d - %s\n", __func__,
                     -rv, strerror(-rv));
      break;
    }

    // Process poll events, check if App endpoint is ready for writing
    if (!(m_pollFds[kSocketServiceFd].revents & POLLOUT)) {
      ADK_LOG_INFO(LOGTAG "  %s: App endpoint not ready for writing\n", __func__);
      // exit loop, application socket is not ready for writing\n",
      break;
    }

    // Get Data from Service
    if (nBytesToWrite == 0) {
      transportBuffer.buffer = dataBuffer().data();
      transportBuffer.size = dataBuffer().size();
      transportBuffer.codec = getCodec();
      if (dataPathService_m->serviceProcessData(&transportBuffer) >= 0) {
        // Post serviceProcessData, get num of bytes to write
        nBytesToWrite = transportBuffer.size;
      }

      if (nBytesToWrite == 0) {
        // Buffer now empty, exit the loop
        //ADK_LOG_DEBUG(LOGTAG "  %s: No more data from service\n", __func__);
        break;
      }
    }

    // Proceed Write
    uint32_t nBytesWritten =
        write(m_pollFds[kSocketServiceFd].fd, (void *)transportBuffer.buffer,
              nBytesToWrite);
    if (nBytesWritten < 0) {
      ADK_LOG_NOTICE(LOGTAG "  %s: Error Writing to FD %d Bytes\n", __func__,
                     nBytesWritten);
      // Disconnect A2DP as the endpoint is unable to consume
      triggerTransportRelease();
      break;
    }
    //ADK_LOG_DEBUG(LOGTAG "  %s:  -- Writing to FD %d Bytes\n", __func__, nBytesWritten);

    // Continuing write
    transportBuffer.buffer += nBytesWritten;
    nBytesToWrite -= nBytesWritten;
  }

  pthread_mutex_unlock(&processDataMutex());
}

uint32_t MediaTransportNonSplitSink::calculateBufferSize() {
  uint32_t bufferSize = 0;
  switch (getCodec()) {
    case A2DP_SINK_AUDIO_CODEC_SBC:
      // Max bit rate = 345 kbps @ 48kHz stereo, high quality
      bufferSize = (DATA_SINK_TIMER_DURATION * 345000 + 7999999) / (8 * 1000000);
      break;
    case A2DP_SINK_AUDIO_CODEC_MP3:
      // Max bit rate = 320 kbps stereo
      bufferSize = (DATA_SINK_TIMER_DURATION * 320000 + 7999999) / (8 * 1000000);
      break;
    case A2DP_SINK_AUDIO_CODEC_AAC:
      // TODO: Max bit rate = ??? kbps (assuming 320 as per MP3 for now)
      bufferSize = (DATA_SINK_TIMER_DURATION * 320000 + 7999999) / (8 * 1000000);
      break;
    case A2DP_SINK_AUDIO_CODEC_APTX:
      // Max bit rate = 384 kbps @ 48kHz stereo
      bufferSize = (DATA_SINK_TIMER_DURATION * 384000 + 7999999) / (8 * 1000000);
      break;
    default:
      // TBD: For now other codec types are not supported
      ADK_LOG_ERROR(LOGTAG "%s Codec not supported\n", __func__);
      break;
  }
  return bufferSize;
}

