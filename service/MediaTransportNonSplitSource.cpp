/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "MediaTransportNonSplitSource.hpp"

#include "Device.hpp"
#include "A2dpSourceService.hpp"

#define LOGTAG "MEDIA_TRANSPORT_NONSPLIT_SOURCE "

// Timer to push data from Fluoride stack in millisecs
#define DATA_SOURCE_TIMER_DURATION (20 * 1000ull)  // 20ms


MediaTransportNonSplitSource::MediaTransportNonSplitSource(Device *pDevice,
                                                           A2dpProfileService *pService,
                                                           uint16_t codec,
                                                           IA2dpDataPathProcessing *pPath)
    : MediaTransportNonSplit(pDevice, pService, codec, pPath) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

bool MediaTransportNonSplitSource::mediaTransportInit(
                                   const btav_codec_config_t &codec_config) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  timerDuration() = DATA_SOURCE_TIMER_DURATION;
  return MediaTransportNonSplit::mediaTransportInit(codec_config);
}

void MediaTransportNonSplitSource::doStartMediaTransport() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  MediaTransportNonSplit::doStartMediaTransport(POLLIN);

  // A2dpSrc uses timer mechanism to feed data to Fluoride
  if (getTransportState() == State::kActive) {
    startDataTimer();
  }
}

void MediaTransportNonSplitSource::doStopMediaTransport() {
  stopDataTimer();
}

int MediaTransportNonSplitSource::doAcquire(sd_bus_message *m, sd_bus_error *ret_error) {
  int res = MediaTransportNonSplit::doAcquire(m, ret_error);
  if (res >= 0) {
    // A2dpSrc uses timer mechanism to feed data to Fluoride.
    // As transport is being acquired start the timer.
    if (getTransportState() == State::kActive) {
      startDataTimer();
    }
    A2dpSourceService *srcService = dynamic_cast<A2dpSourceService *>(getService());
    if (srcService) {
      auto devAddr = getDevice()->getDeviceAddr();
      if (srcService->startStream(&devAddr) != BT_STATUS_SUCCESS) {
        ADK_LOG_ERROR(LOGTAG "  --> Error returned on startStream!\n");
      }
    }
  }
  return res;
}

int MediaTransportNonSplitSource::doRelease(sd_bus_message *m, sd_bus_error *ret_error) {
  // A2dpSrc in non split modeuses timer mechanism to feed data to Fluoride.
  // Therefore, stop the timer and then release transport.
  stopDataTimer();
  A2dpSourceService *srcService = dynamic_cast<A2dpSourceService *>(getService());
  if (srcService) {
    auto devAddr = getDevice()->getDeviceAddr();
    if (srcService->suspendStream(&devAddr) != BT_STATUS_SUCCESS) {
      ADK_LOG_ERROR(LOGTAG "  --> Error returned on suspendStream!\n");
    }
  }
  return MediaTransportNonSplit::doRelease(m, ret_error);
}


bool MediaTransportNonSplitSource::doSetMTUs() {
  dataBuffer().resize(iMtu());
  return true;
}

void MediaTransportNonSplitSource::processData() {
  if (pthread_mutex_trylock(&processDataMutex())) {
    ADK_LOG_ERROR(LOGTAG "  -->%s Waiting for processData to complete\n", __func__);
    return;
  }

  auto mtuSize = iMtu();

  for (;;) {
    int rv = poll(m_pollFds, kMaxFds, 10);
    if (rv < 0) {
      ADK_LOG_NOTICE(LOGTAG "  %s: Error Occured in Poll: %d - %s\n", __func__,
                     -rv, strerror(-rv));
      break;
    }

    // Process poll events, check if App endpoint is ready for reading
    if (!(m_pollFds[kSocketServiceFd].revents & POLLIN)) {
      // ADK_LOG_INFO(LOGTAG "  %s: App endpoint not ready for reading\n", __func__);
      break;
    }

    TransportBuffer transportBuffer;
    transportBuffer.buffer = dataBuffer().data();
    transportBuffer.codec = getCodec();

    // Proceed to Read from FD
    uint32_t nBytesRead =
        read(m_pollFds[kSocketServiceFd].fd, (void *)transportBuffer.buffer, mtuSize);

    // TODO: review line below
    // if (errno == EINTR) continue;
    if (nBytesRead <= 0) {
      ADK_LOG_NOTICE(LOGTAG "  %s: Error Reading from FD %d Bytes\n",
                    __func__, nBytesRead);
      // Disconnect A2DP as the endpoint is unable to provide
      triggerTransportRelease();
      break;
    }

    // Feed Data to Service
    if (nBytesRead > 0) {
      transportBuffer.size = nBytesRead;
      dataPathService_m->serviceProcessData(&transportBuffer);
      // ADK_LOG_DEBUG(
      //    LOGTAG " -- ret from  btav_send_encoded_data_vendor: %d\n", ret);
    }
  }

  pthread_mutex_unlock(&processDataMutex());
}
