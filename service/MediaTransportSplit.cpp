/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "MediaTransportSplitSink.hpp"

#include "Device.hpp"
#include "Error.hpp"
#include "Sdbus.hpp"

#define LOGTAG "MEDIA_TRANSPORT_SPLIT "

MediaTransportSplit::MediaTransportSplit(Device *pDevice,
                                         A2dpProfileService *pService,
                                         uint16_t codec)
    : MediaTransport(pDevice, pService, codec){
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

int MediaTransportSplit::doAcquire(sd_bus_message *m,
                                   sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  // Check if the transport is already acquired, raise an error accordingly
  if (getTransportState() == State::kActive) {
    return bt_error_not_available(ret_error);
  }
  int res = sd_bus_reply_method_return(m,
      TYPE_TO_STR<SD_BUS_TYPE_UNIX_FD, SD_BUS_TYPE_UINT16, SD_BUS_TYPE_UINT16>::value,
      0, 0, 0);
  if (!res) {
    ADK_LOG_ERROR(LOGTAG "%s Reply failed: %d - %s\n",__func__, -res, strerror(-res));
  }
  else {
    updateTransportStateOnInterface(State::kActive);
  }
  return res;
}

int MediaTransportSplit::doRelease(sd_bus_message *m,
                                   sd_bus_error *UNUSED(ret_error)) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  updateTransportStateOnInterface(State::kIdle);
  return sd_bus_reply_method_return(m, nullptr);
}
