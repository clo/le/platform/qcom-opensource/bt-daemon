/*
Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef MEDIATRANSPORTSPLITSINK_HPP
#define MEDIATRANSPORTSPLITSINK_HPP

#include "MediaTransportSplit.hpp"


/**
 * @file MediaTransportSplitSink.hpp
 * @brief Definition for the transport specialized for a2dp when playing sink role
 *        in split mode.
 */
class MediaTransportSplitSink final : public MediaTransportSplit{
public:
  MediaTransportSplitSink(Device *pDevice, A2dpProfileService *pService, uint16_t codec);
  ~MediaTransportSplitSink() final = default;

private:
  // From MediaTransport
  void doStartMediaTransport() final;
  int doAcquire(sd_bus_message *m, sd_bus_error *ret_error) final;
  int doRelease(sd_bus_message *m, sd_bus_error *ret_error) final;
};
#endif  // MEDIATRANSPORTSPLITSINK_HPP
