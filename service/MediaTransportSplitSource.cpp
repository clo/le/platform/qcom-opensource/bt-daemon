/*
Copyright (c) 2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "MediaTransportSplitSource.hpp"

#include "Device.hpp"
#include "A2dpSourceServiceSplit.hpp"
#include "Error.hpp"
#include "Sdbus.hpp"

#define LOGTAG "MEDIA_TRANSPORT_A2DPSOURCE_SPLIT "

MediaTransportSplitSource::MediaTransportSplitSource(Device *pDevice,
                                                 A2dpProfileService *pService,
                                                 uint16_t codec)
    : MediaTransportSplit(pDevice, pService, codec){
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
}

void MediaTransportSplitSource::setConfigurationCb() {
  // This is needed with current implementation of AudioManager which
  // always waits for KPending state notification.
  // Note that this behaviour is slightly different in non-split mode
  // where pulseaudio acquires transport in KIdle state when DUT is a2dpSrc.
  updateTransportStateOnInterface(State::kPending);
}

int MediaTransportSplitSource::doAcquire(sd_bus_message *m,
                                       sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  int res = MediaTransportSplit::doAcquire(m, ret_error);
  A2dpSourceService *srcService = dynamic_cast<A2dpSourceService *>(getService());
  if (srcService) {
    auto devAddr = getDevice()->getDeviceAddr();
	  // Ideally startStream should be called here to indicate PlayBack has now started
	  // however it is being sent on receipt of ConnecitonStateCb(connected)
	  // as otherwise we seem to get audioSateCb too soon and then this call fails!
    if (srcService->startStream(&devAddr) != BT_STATUS_SUCCESS) {
     ADK_LOG_ERROR(LOGTAG "  --> Error returned on startStream!\n");
    }
  }
  return res;
}

int MediaTransportSplitSource::doRelease(sd_bus_message *m,
                                       sd_bus_error *ret_error) {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  int res = MediaTransportSplit::doRelease(m, ret_error);
  A2dpSourceServiceSplit *srcService = dynamic_cast<A2dpSourceServiceSplit *>(getService());
  if (srcService) {
    auto devAddr = getDevice()->getDeviceAddr();
    if (srcService->suspendStream(&devAddr) != BT_STATUS_SUCCESS) {
      ADK_LOG_ERROR(LOGTAG "  --> Error returned on suspendStream!\n");
	}
  }
  return res;
}
