/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "ProfileService.hpp"
#include "Adapter.hpp"

#define LOGTAG "A2DP_SERVICE "

void A2dpProfileService::setTransportConfig(
                             const std::shared_ptr<MediaTransport> &transport)
{
  Device *device = getAdapter()->getDevice(m_connectedDevAddr);
  if (device) {
    // If device is not yet bonded, or is still missing its supported UUIDs update,
    // wait until a Bonded callback  and BT_PROPERTY_UUIDS update is received
    // before triggering setConfiguration on end points.
    if (device->getIsBonded() && !device->getSupportedUUIDs().empty()) {
      // Update the details to the end point
      if (!getAdapter()->m_mediaManager->setConfiguration(
            transport)) {
        ADK_LOG_ERROR(LOGTAG "- setConfiguration failed\n");
        // Disconnect the BT ACL link as the MediaTransport interface isn't ready
        transport->triggerTransportRelease();
      }
    }
  }
}

void A2dpProfileService::clearTransportConfig()
{
  ADK_LOG_NOTICE(LOGTAG "%s %d  %s\n", __func__, m_deviceState,
                         m_connectedDevAddr.ToString().c_str());
  Device *device = getAdapter()->getDevice(m_connectedDevAddr);
  if (device) {
    std::shared_ptr<MediaTransport> transport = device->getTransport();
    if (transport) {
      getAdapter()->m_mediaManager->findAndClearConfiguration(
           transport->getCodec(), srvUUID());
      device->setTransport({});
    }
    device->devRemoveConnection(srvUUID());
  }
}

void A2dpProfileService::disableService() {
  ADK_LOG_NOTICE(LOGTAG "%s\n", __func__);
  clearTransportConfig();
  serviceState() = State::kStopped;
  m_deviceState = DeviceState::kDisconnected;
  m_connectedDevAddr = {};
}

bt_status_t A2dpProfileService::setCpScms(bt_bdaddr_t *UNUSED(bd_addr), uint8_t UNUSED(cpScms)) {
  ADK_LOG_ERROR(LOGTAG "%s Invalid use of CpScms flag setting.\n", __func__);
  return BT_STATUS_UNSUPPORTED;
}

void A2dpProfileService::setScmstCapabilities(const bt_bdaddr_t &bd_addr, bool scmstEnabled) {
  ADK_LOG_NOTICE(LOGTAG "%s SCMST %s: \n", __func__,
                 scmstEnabled? "true":"false");

    Device *device = getAdapter()->getDevice(bd_addr);
    if (!device) {
      ADK_LOG_ERROR(LOGTAG "- Unexpectedly remote device is not available! \n");
      return;
    }
    else {
      std::shared_ptr<MediaTransport> transport = device->getTransport();
      if (!transport) {
       ADK_LOG_ERROR(LOGTAG "- Unexpectedly remote device is not available! \n");
       return;
      }
      transport->setScmstCapabilities(scmstEnabled);
    }
}
