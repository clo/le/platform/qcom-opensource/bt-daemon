/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef PROFILESERVICE_HPP
#define PROFILESERVICE_HPP

#include <hardware/bluetooth.h>
#include <hardware/bt_av_vendor.h>

#include <memory>
#include <string>

#include "MediaTransport.hpp"
#include "Common.hpp"

class Adapter;

class ProfileService {
public:
  enum class State { kStopped, kInitialized, kRunning };

  ProfileService(Adapter *pAdapter,
                 const std::string &srvUUID,
                 const std::string &remUUID)
    : m_adapter(pAdapter),
      m_state(State::kStopped),
      m_srvUUID(srvUUID),
      m_remoteUUID(remUUID) {}
  virtual ~ProfileService() noexcept = default;

  // Accessors
  State serviceState() const { return m_state; }
  State& serviceState() { return m_state; }
  std::string srvUUID() const { return m_srvUUID; }
  std::string remoteUUID() const { return m_remoteUUID; }
  Adapter* getAdapter() { return m_adapter; }

  // Methods requiring specialization
  virtual void enableService() = 0;
  virtual void disableService() = 0;
  virtual bool connect(bt_bdaddr_t *devAddr) = 0;
  virtual bool disconnect(bt_bdaddr_t *devAddr) = 0;
  virtual bool isConnectable() = 0;

private:
  Adapter *m_adapter;
  State m_state;
  std::string m_srvUUID;
  std::string m_remoteUUID;
};

// Note: Avrcp Profiles are not connectable in isolation. Instead:
// - AvrcpTG is connected when A2dpSrc profile is connected
// - AvrcpCT is connected when A2dpSnk profile is connected
class AvrcpProfileService : public ProfileService {
public:
  enum PassthroughKeys {
    CMD_ID_INVALID = 0,
    CMD_ID_POWER = 0x40,
    CMD_ID_VOL_UP = 0x41,
    CMD_ID_VOL_DOWN = 0x42,
    CMD_ID_MUTED = 0x43,
    CMD_ID_PLAY = 0x44,
    CMD_ID_STOP = 0x45,
    CMD_ID_PAUSE = 0x46,
    CMD_ID_REWIND = 0x48,
    CMD_ID_FF = 0x49,
    CMD_ID_FORWARD = 0x4B,
    CMD_ID_BACKWARD = 0x4C
  };

  enum KeyStatus { KEY_PRESSED = 0, KEY_RELEASED = 1 };

  AvrcpProfileService(Adapter *pAdapter, const std::string &srvUUID,
                const std::string &remUUID) :
                ProfileService(pAdapter, srvUUID, remUUID) {}
  ~AvrcpProfileService() override = default;

  // Disabling copy and assignment operators
  AvrcpProfileService(const AvrcpProfileService&) = delete;
  AvrcpProfileService& operator=(const AvrcpProfileService&) = delete;

  // From ProfileService
  void enableService() override = 0;
  void disableService() override = 0;
  bool connect(bt_bdaddr_t *UNUSED(devAddr)) final { return false; }
  bool disconnect(bt_bdaddr_t *UNUSED(devAddr)) final { return false; }
  bool isConnectable() final { return true; }
};

class A2dpProfileService : public ProfileService {
public:
  enum DeviceState { kDisconnected, kPending, kConnected };

  A2dpProfileService(Adapter *pAdapter, const std::string &srvUUID,
                     const std::string &remUUID) :
                     ProfileService(pAdapter, srvUUID, remUUID),
                     m_deviceState(DeviceState::kDisconnected),
                     m_connectedDevAddr(RawAddress::kEmpty) {}
  ~A2dpProfileService() override = default;

  // Disabling copy and assignment operators
  A2dpProfileService(const A2dpProfileService&) = delete;
  A2dpProfileService& operator=(const A2dpProfileService&) = delete;

  // Accessors
  DeviceState getDeviceState() const { return m_deviceState; }
  bt_bdaddr_t getConnectedDevice() const { return m_connectedDevAddr; }
  void setDeviceForService(DeviceState deviceState, bt_bdaddr_t devAddr) {
    m_deviceState = deviceState;
    m_connectedDevAddr = devAddr;
    }

  // From ProfileService
  void enableService() override = 0;
  void disableService() override = 0;
  bool connect(bt_bdaddr_t *devAddr) override = 0;
  bool disconnect(bt_bdaddr_t *devAddr) override = 0;
  bool isConnectable() final {
    return (m_deviceState != kDisconnected) ? false : true;
  }

  virtual bool addSupportedCodec(const btav_codec_configuration_t &codec) = 0;
  virtual bool removeSupportedCodec(const btav_codec_configuration_t &codec) = 0;
  virtual bt_status_t setCpScms(bt_bdaddr_t *d_addr, uint8_t cpScms);

  void clearTransportConfig();
  void setTransportConfig(const std::shared_ptr<MediaTransport> &transport);

protected:
  void setScmstCapabilities(const bt_bdaddr_t &bd_addr, bool scmstEnabled);

private:
  DeviceState m_deviceState;
  bt_bdaddr_t m_connectedDevAddr;
};

// Pure Interface to provide access to A2dp datapath in non-split mode
class IA2dpDataPathProcessing {
public:
  virtual int serviceProcessData(void *data) = 0;
};

#endif  // PROFILESERVICE_HPP
