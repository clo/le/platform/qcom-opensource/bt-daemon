/*
Copyright (c) 2017-2020, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SERVICE_SDBUS_HPP_
#define SERVICE_SDBUS_HPP_

#include <systemdq/sd-bus.h>

#include "Common.hpp"

extern sd_bus *g_sdbus;
extern sd_event *g_eventLoop;

template <char... Args>
 struct TYPE_TO_STR {
     static constexpr const char value[] = {Args..., '\0'};
 };
 template <char... Args>
 constexpr const char TYPE_TO_STR<Args...>::value[];

static inline uint64_t get_timer_delay(uint64_t delay_us) {
  uint64_t usec;
  int res = sd_event_now(g_eventLoop, CLOCK_MONOTONIC, &usec);
  if (res < 0) {
    ADK_LOG_ERROR("Failed to get current time: %d - %s\n", -res,
                  strerror(-res));
    return UINT64_MAX;
  }
  usec += delay_us;
  return usec;
}


#endif /* SERVICE_SDBUS_HPP_ */